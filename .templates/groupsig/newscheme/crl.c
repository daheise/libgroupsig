/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: %%newscheme%%_crl.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 17:52:06 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 14:55:10 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

#include "sys/mem.h"
#include "misc/misc.h"
#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/crl.h"
#include "groupsig/%%newscheme%%/trapdoor.h"

/* Private functions */
static int _is_supported_format(crl_format_t format) {
  
  uint32_t i;
  
  for(i=0; i<SUPPORTED_CRL_FORMATS_N; i++) {
    if(format == SUPPORTED_CRL_FORMATS[i]) return 1;
  }
  
  return 0;
  
}

static %%newscheme%%_crl_entry_t* _crl_entry_import_file(FILE *fd, crl_format_t format,
						 uint8_t *eof) {
  if(!fd || !eof) {
    LOG_EINVAL(&logger, __FILE__, "_crl_entry_import_file", __LINE__, LOGERROR);
    return IOK;
  }

  return NULL;

}

static crl_t* _crl_import_file(char *filename) {

  if(!filename) {
    LOG_EINVAL(&logger, __FILE__, "_crl_import_file", __LINE__, LOGERROR);
    return IOK;
  }

  return NULL;

}

static int _crl_export_file(crl_t *crl, char *filename) {

  if(!crl || !filename) {
    LOG_EINVAL(&logger, __FILE__, "_crl_export_file", __LINE__, LOGERROR);
    return IOK;
  }

  return IERROR;

}

/* Public functions */

/* entry functions  */

%%newscheme%%_crl_entry_t* %%newscheme%%_crl_entry_init() {
  
  return NULL;

}

int %%newscheme%%_crl_entry_free(%%newscheme%%_crl_entry_t *entry) {

  if(!entry) {
    LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_entry_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  return IERROR;

}

int %%newscheme%%_crl_entry_cmp_id(void *entry1, void *entry2) {

  if(!entry1 || !entry2) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_entry_cmp_id", __LINE__, LOGERROR);
    return 0;
  }

  return 0;

}

int %%newscheme%%_crl_entry_cmp_trapdoors(void *entry1, void *entry2) {

  %%newscheme%%_crl_entry_t *e1, *e2;

  if(!entry1 || !entry2) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_entry_cmp_trapdoors", __LINE__, LOGERROR);
    return 0;
  }

  return 0;

}

char* %%newscheme%%_crl_entry_to_string(%%newscheme%%_crl_entry_t *entry) {

  if(!entry) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_entry_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;
 
}

/* list functions */

crl_t* %%newscheme%%_crl_init() {

  /* crl_t *crl; */

  /* if(!(crl = (crl_t *) malloc(sizeof(crl_t)))) { */
  /*   LOG_ERRORCODE(&logger, __FILE__, "%%newscheme%%_crl_init", __LINE__, errno, LOGERROR); */
  /*   return NULL; */
  /* } */

  /* crl->scheme = GROUPSIG_%%NEWSCHEME%%_CODE; */
  /* crl->entries = NULL; */
  /* crl->n = 0; */

  /* return crl; */

  return NULL;

}

int %%newscheme%%_crl_free(crl_t *crl) {

  /* uint64_t i; */

  /* if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_free", __LINE__,  */
  /* 		   "Nothing to free.", LOGWARN); */
  /*   return IOK; */
  /* } */

  /* for(i=0; i<crl->n; i++) { */
  /*   %%newscheme%%_crl_entry_free(crl->entries[i]); */
  /* } */

  /* mem_free(crl->entries); crl->entries = NULL; */
  /* mem_free(crl); */

  /* return IOK; */

  return IERROR;

}

int %%newscheme%%_crl_insert(crl_t *crl, void *entry) {

  if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_insert", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

int %%newscheme%%_crl_remove(crl_t *crl, uint64_t index) {

  if(!crl  || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_remove", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;
  
}

void* %%newscheme%%_crl_get(crl_t *crl, uint64_t index) {

  if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_get", __LINE__, LOGERROR);
    return NULL;
  }

  /* if(index >= crl->n) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_get", __LINE__, "Invalid index.", */
  /* 		   LOGERROR); */
  /*   return NULL; */
  /* } */

  /* return crl->entries[index]; */

  return NULL;
  
}

crl_t* %%newscheme%%_crl_import(crl_format_t format, void *src) {

  if(!src) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_import", __LINE__, */
  /* 		   "Unsupported CRL type.", LOGERROR); */
  /*   return NULL; */
  /* } */

  /* /\* If the received source is empty, means that we have to */
  /*    return an empty (new) CRL *\/ */

  /* switch(format) { */
  /* case CRL_FILE: */
  /*   return _crl_import_file((char *) src); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_import", __LINE__, */
  /* 		   "Unsupported CRL format.", LOGERROR); */
  /*   return NULL; */
  /* } */

  return NULL;
 
}

int %%newscheme%%_crl_export(crl_t *crl, void *dst, crl_format_t format) {

  if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !dst) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_export", __LINE__, */
  /* 		   "Unsupported CRL format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* switch(format) { */
  /* case CRL_FILE: */
  /*   return _crl_export_file(crl, (char *) dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_crl_export", __LINE__, */
  /* 		   "Unsupported CRL format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;

}

int %%newscheme%%_crl_entry_exists(crl_t *crl, void *entry) {

  if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_entry_exists", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

int %%newscheme%%_crl_trapdoor_exists(crl_t *crl, trapdoor_t *trap) {

  if(!crl || crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !trap || trap->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_crl_entry_exists", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;


}

/* %%newscheme%%_crl.c ends here */
