/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: crl.h
 * @brief: %%NEWSCHEME%% specific CRLs.
 * @author: jesus
 * Maintainer: jesus
 * @date: lun jun 11 17:52:09 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun ago  5 11:50:31 2013 (+0200)
 *           By: jesus
 *     Update #: 2
 * URL: bitbucket.org/jdiazvico/libgroupsig
 * @todo This should be made independent to schemes in order to allow, e.g.
 *  several schemes sharing the same CRL implementation.
 */

#ifndef _%%NEWSCHEME%%_CRL_H
#define _%%NEWSCHEME%%_CRL_H

#include <stdint.h>
#include "types.h"
#include "sysenv.h"
#include "include/crl.h"
#include "bigz.h"
#include "groupsig/%%newscheme%%/identity.h"

/**
 * @def SUPPORTED_CRL_FORMATS_N
 * @brief Number of supported CRL formats in %%NEWSCHEME%%.
 */
#define SUPPORTED_CRL_FORMATS_N 1

/**
 * @var SUPPORTED_CRL_FORMATS
 * @brief List of formast supported by %%NEWSCHEME%%.
 */
static const int SUPPORTED_CRL_FORMATS[SUPPORTED_CRL_FORMATS_N] = {
  CRL_FILE,
};

/**
 * @struct %%newscheme%%_crl_entry_t 
 * @brief CRL entries for KYT04.
 */
typedef struct {
  identity_t *id; /**< The user identity. */
  bigz_t trapdoor; /**<  The tracing trapdoor for this user. */
} %%newscheme%%_crl_entry_t;

/* Entry public functions */

/**
 * @fn %%newscheme%%_crl_entry_t* %%newscheme%%_crl_entry_init()
 * @brief Creates a new CRL entry and initializes its fields.
 *
 * @return The created crl entry or NULL if error.
 */
%%newscheme%%_crl_entry_t* %%newscheme%%_crl_entry_init();

/**
 * @fn int %%newscheme%%_crl_entry_free(%%newscheme%%_crl_entry_t *entry)
 * @brief Frees the fields of the given CRL entry.
 *
 * @param[in,out] entry The CRL entry to free.
 *
 * @return IOK or IERROR
 */
int %%newscheme%%_crl_entry_free(%%newscheme%%_crl_entry_t *entry);

/** 
 * @fn char* %%newscheme%%_crl_entry_to_string(%%newscheme%%_crl_entry_t *entry)
 * Converts the received %%NEWSCHEME%% CRL entry to a printable string.
 *
 * @param[in] entry The CRL entry.
 * 
 * @return The converted string or NULL if error.
 */
char* %%newscheme%%_crl_entry_to_string(%%newscheme%%_crl_entry_t *entry);

/* List public functions */

/** 
 * @fn crl_t* %%newscheme%%_crl_init()
 * Initializes a CRL structure.
 *
 * @return A pointer to the initialized structure.
 */
crl_t* %%newscheme%%_crl_init();

/** 
 * @fn int %%newscheme%%_crl_free(crl_t *crl)
 * Frees the received CRL structure. Note that it does not free the entries.
 * If memory has been allocated for them, the caller must free it.
 *
 * @param[in,out] crl The CRL to free.
 * 
 * @return IOK.
 */
int %%newscheme%%_crl_free(crl_t *crl);

/** 
 * @fn int %%newscheme%%_crl_insert(crl_t *crl, void *entry)
 * Inserts the given entry into the crl. The memory pointed by the new entry is
 * not duplicated.
 *
 * @param[in,out] crl The CRL.
 * @param[in] entry The entry to insert.
 * 
 * @return IOK or IERROR with errno updated.
 */
int %%newscheme%%_crl_insert(crl_t *crl, void *entry);

/** 
 * @fn int %%newscheme%%_crl_remove(crl_t *crl, uint64_t index)
 * Removes the entry at position <i>index</i> from the CRL. The caller is 
 * responsible for removing the contents of the entry itself.
 *
 * @param[in,out] crl The CRL.
 * @param[in] index The index of the entry to remove.
 * 
 * @return IOK or IERROR with errno updated.
 */
int %%newscheme%%_crl_remove(crl_t *crl, uint64_t index);

/** 
 * @fn void* %%newscheme%%_crl_get(crl_t *crl, uint64_t index)
 * Returns a pointer to the CRL entry at the specified position.
 *
 * @param[in] crl The CRL.
 * @param[in] index The index of the entry to retrieve.
 * 
 * @return A pointer to the specified entry or NULL if error.
 */
void* %%newscheme%%_crl_get(crl_t *crl, uint64_t index);

/**
 * @fn crl_t* %%newscheme%%_crl_import(crl_type_t type, void *src)
 * @brief Loads the Group Members List stored in the given source, of the
 *  specified type, and returns a initialized CRL structure.
 *
 * @param[in] type The type of source.
 * @param[in] src The element containing the crl.
 *
 * @return The imported CRL or NULL if error.
 */
crl_t* %%newscheme%%_crl_import(crl_format_t type, void *src);

/**
 * @fn int %%newscheme%%_crl_export(crl_t *crl, void *dst, crl_format_t format)
 * @brief Exports the given Group Members List structure into the given destination.
 *
 * @param[in] crl The CRL structure to save.
 * @param[in] dst The destination.
 * @param[in] format The type of destination.
 *
 * @return IOK or IERROR
 */
int %%newscheme%%_crl_export(crl_t *crl, void *dst, crl_format_t format);

/** 
 * @fn int %%newscheme%%_crl_entry_cmp_id(void *entry1, void *entry2)
 * Compares the ID fields of two %%newscheme%%_crl_entry_t structures. 
 *
 * @param[in] entry1 The first operand.
 * @param[in] entry2 The second operand.
 * 
 * @return 0 if both entries have the same ID, != 0 if not. If an error
 *  occurs, errno is updated.
 */
int %%newscheme%%_crl_entry_cmp_id(void *entry1, void *entry2);

/** 
 * @fn int %%newscheme%%_crl_entry_cmp_trapdoors(void *entry1, void *entry2)
 * Compares the trapdoor fields of two %%newscheme%%_crl_entry_t structures. 
 *
 * @param[in] entry1 The first operand.
 * @param[in] entry2 The second operand.
 * 
 * @return 0 if both entries have the same trapdoor, != 0 if not. If an error
 *  occurs, errno is updated.
 */
int %%newscheme%%_crl_entry_cmp_trapdoors(void *entry1, void *entry2);


/** 
 * @fn int %%newscheme%%_crl_entry_exists(crl_t *crl, void *entry)
 * Returns 0 if there is no entry with the same trapdoor, 1 if there is.
 *
 * @param[in] crl The CRL.
 * @param[in] entry The entry to check.
 * 
 * @return 1 if the entry exists, 0 if not. On error, errno is updated.
 */
int %%newscheme%%_crl_entry_exists(crl_t *crl, void *entry);

/** 
 * @fn int %%newscheme%%_crl_trapdoor_exists(crl_t *crl, trapdoor_t *trap)
 * Returns 0 if there is no entry with the same trapdoor, 1 if there is.
 *
 * @param[in] crl The CRL
 * @param[in] trap The trapdoor.
 * 
 * @return 1 if the trapdoor exists, 0 if not. On error, errno is updated.
 */
int %%newscheme%%_crl_trapdoor_exists(crl_t *crl, trapdoor_t *trap);

/**
 * @var %%newscheme%%_crl_handle
 * @brief The set of functions for managing %%NEWSCHEME%% CRLs.
 */
static const crl_handle_t %%newscheme%%_crl_handle = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< Handle code. */
  &%%newscheme%%_crl_init, /**< Initialization function. */
  &%%newscheme%%_crl_free, /**< Free function. */
  &%%newscheme%%_crl_insert, /**< Insert a new entry. */
  &%%newscheme%%_crl_remove, /**< Remove a specific entry. */
  &%%newscheme%%_crl_get, /**< Get an specific entry (without removing). */
  &%%newscheme%%_crl_import, /**< Import from an external source. */
  &%%newscheme%%_crl_export, /**< Export to an external source. */
  &%%newscheme%%_crl_entry_exists, /**< Test if a specific entry already exists
			      in the CRL. */
  &%%newscheme%%_crl_trapdoor_exists, /**< Test if a specific trapdoor already
				 exists in the CRL. */
};

#endif /* _%%NEWSCHEME%%_CRL_H */

/* crl.h ends here */
