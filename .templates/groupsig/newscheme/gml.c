/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: %%newscheme%%_gml.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun may  7 09:59:16 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 15:05:37 2013 (+0200)
 *           By: jesus
 *     Update #: 253
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "misc/misc.h"
#include "sys/mem.h"
#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/gml.h"
#include "groupsig/%%newscheme%%/identity.h"
#include "groupsig/%%newscheme%%/trapdoor.h"

/* Private functions */
static int _is_supported_format(gml_format_t format) {
  
  uint32_t i;
  
  for(i=0; i<SUPPORTED_GML_FORMATS_N; i++) {
    if(format == SUPPORTED_GML_FORMATS[i]) return 1;
  }
  
  return 0;
  
}

static %%newscheme%%_gml_entry_t* _gml_entry_import_file(FILE *fd, gml_format_t format,
						 uint8_t *eof) {
    
  if(!fd || !eof) {
    LOG_EINVAL(&logger, __FILE__, "_gml_entry_import_file",
 	       __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

static gml_t* _gml_import_file(char *filename) {

  if(!filename) {
    LOG_EINVAL(&logger, __FILE__, "_gml_import_file", __LINE__, LOGERROR);
    return NULL;
  }
  
  return NULL;

}

int _gml_export_new_entry_file(%%newscheme%%_gml_entry_t *entry, char *filename) {

  if(!entry || !filename) {
    LOG_EINVAL(&logger, __FILE__, "_gml_export_new_entry_file", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

static int _gml_export_file(gml_t *gml, char *filename) {

  if(!gml || !filename) {
    LOG_EINVAL(&logger, __FILE__, "_gml_export_file", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

/* Public functions */

/* entry functions  */

%%newscheme%%_gml_entry_t* %%newscheme%%_gml_entry_init() {

  return NULL;

}


int %%newscheme%%_gml_entry_free(%%newscheme%%_gml_entry_t *entry) {

  if(!entry) {
    LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_entry_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }
  
  return IERROR;

}

int %%newscheme%%_gml_entry_cmp_trapdoors(void *entry1, void *entry2) {

  if(!entry1 || !entry2) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_entry_cmp_trapdoors", __LINE__, LOGERROR);
    return 0;
  }

  return 0;

}

char* %%newscheme%%_gml_entry_to_string(%%newscheme%%_gml_entry_t *entry) {

  if(!entry) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_entry_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;
 
}

/* list functions */

gml_t* %%newscheme%%_gml_init() {

  /* gml_t *gml; */

  /* if(!(gml = (gml_t *) malloc(sizeof(gml_t)))) { */
  /*   LOG_ERRORCODE(&logger, __FILE__, "%%newscheme%%_gml_init", __LINE__, errno, LOGERROR); */
  /*   return NULL; */
  /* } */

  /* gml->scheme = GROUPSIG_%%NEWSCHEME%%_CODE; */
  /* gml->entries = NULL; */
  /* gml->n = 0; */

  /* return gml; */

  return NULL;

}

int %%newscheme%%_gml_free(gml_t *gml) {

  /* uint64_t i; */

  /* if(!gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_free", __LINE__,  */
  /* 		   "Nothing to free.", LOGWARN); */
  /*   return IOK; */
  /* } */

  /* for(i=0; i<gml->n; i++) { */
  /*   %%newscheme%%_gml_entry_free(gml->entries[i]); */
  /* } */

  /* mem_free(gml->entries); gml->entries = NULL; */
  /* mem_free(gml); */

  /* return IOK; */

  return IERROR;

}

int %%newscheme%%_gml_insert(gml_t *gml, void *entry) {

  if(!gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_insert", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

int %%newscheme%%_gml_remove(gml_t *gml, uint64_t index) {

  if(!gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_remove", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

void* %%newscheme%%_gml_get(gml_t *gml, uint64_t index) {

  if(!gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_get", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;
  
}

gml_t* %%newscheme%%_gml_import(gml_format_t format, void *src) {

  if(!src) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_import", __LINE__, */
  /* 		   "Unsupported GML type.", LOGERROR); */
  /*   return NULL; */
  /* } */

  /* /\* If the received source is empty, means that we have to */
  /*    return an empty (new) GML *\/ */

  /* switch(format) { */
  /* case GML_FILE: */
  /*   return _gml_import_file((char *) src); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_import", __LINE__, */
  /* 		   "Unsupported GML format.", LOGERROR); */
  /*   return NULL; */
  /* } */

  return NULL;
 
}

int %%newscheme%%_gml_export(gml_t *gml, void *dst, gml_format_t format) {

  if(!gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE || !dst) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_export", __LINE__, */
  /* 		   "Unsupported GML format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* switch(format) { */
  /* case GML_FILE: */
  /*   return _gml_export_file(gml, (char *) dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_export", __LINE__, */
  /* 		   "Unsupported GML format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;

}

int %%newscheme%%_gml_export_new_entry(void *entry, void *dst, gml_format_t format) {

  if(!entry || !dst) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_gml_export_new_entry", __LINE__, LOGERROR);
    return IERROR;
  }

  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_export_new_entry", __LINE__, */
  /* 		   "Unsupported GML format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* switch(format) { */
  /* case GML_FILE: */
  /*   return _gml_export_new_entry_file(entry, (char *) dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_gml_export_new_entry", __LINE__, */
  /* 		   "Unsupported GML format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;  

}

/* %%newscheme%%_gml.c ends here */
