/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: gml.h
 * @brief: GML implementation for %%NEWSCHEME%%.
 * @author: jesus
 * Maintainer: JESUS
 * @date: lun may  7 09:59:41 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun ago  5 15:18:49 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: bitbucket.org/jdiazvico/libgroupsig
 * @todo This should be made independent of the group signature scheme, for instance
 *  in order to allow that several group signature schemes share the same GML
 *  implementation.
 */

#ifndef _%%NEWSCHEME%%_GML_H
#define _%%NEWSCHEME%%_GML_H

#include "types.h"
#include "sysenv.h"
#include "include/gml.h"
#include "include/trapdoor.h"
#include "groupsig/%%newscheme%%/identity.h"
#include "%%newscheme%%.h"

/**
 * @def SUPPORTED_GML_FORMATS_N
 * @brief Number GML formats supported by %%NEWSCHEME%%.
 */
#define SUPPORTED_GML_FORMATS_N 1

/**
 * @var SUPPORTED_GML_FORMATS
 * @brief List of GML formats supported by %%NEWSCHEME%%. 
 */
static const int SUPPORTED_GML_FORMATS[SUPPORTED_GML_FORMATS_N] = {
  GML_FILE,
};

/** 
 * @struct %%newscheme%%_gml_entry_t
 * @brief Structure for %%NEWSCHEME%% GML entries.
 */
typedef struct {
  identity_t *id; /**< Member's ID. */
  trapdoor_t *trapdoor; /**< Member's trapdoor. */
  /* bigz_t A; /\**< A field computed by the Manager during manager_join. *\/ */
} %%newscheme%%_gml_entry_t;

/* Entry public functions */

/**
 * @fn %%newscheme%%_gml_entry_t* %%newscheme%%_gml_entry_init()
 * @brief Creates a new GML entry and initializes its fields.
 *
 * @return The created gml entry or NULL if error.
 */
%%newscheme%%_gml_entry_t* %%newscheme%%_gml_entry_init();

/**
 * @fn int %%newscheme%%_gml_entry_free(%%newscheme%%_gml_entry_t *entry)
 * @brief Frees the fields of the given GML entry.
 *
 * @param[in,out] entry The GML entry to free.
 *
 * @return IOK or IERROR
 */
int %%newscheme%%_gml_entry_free(%%newscheme%%_gml_entry_t *entry);

/** 
 * @fn char* %%newscheme%%_gml_entry_to_string(%%newscheme%%_gml_entry_t *entry)
 * @brief Converts the received %%NEWSCHEME%% GML entry to a printable string.
 *
 * @param[in] entry The GML entry.
 * 
 * @return The converted string or NULL if error.
 */
char* %%newscheme%%_gml_entry_to_string(%%newscheme%%_gml_entry_t *entry);

/* List public functions */

/** 
 * @fn gml_t* %%newscheme%%_gml_init()
 * @brief Initializes a GML structure.
 * 
 * @return A pointer to the initialized structure.
 */
gml_t* %%newscheme%%_gml_init();

/** 
 * @fn int %%newscheme%%_gml_free(gml_t *gml)
 * @brief Frees the received GML structure. 
 *
 * Note that it does not free the entries. If memory has been allocated for 
 * them, the caller must free it.
 *
 * @param[in,out] gml The GML to free.
 * 
 * @return IOK.
 */
int %%newscheme%%_gml_free(gml_t *gml);

/** 
 * @fn int %%newscheme%%_gml_insert(gml_t *gml, void *entry)
 * @brief Inserts the given entry into the gml. The memory pointed by the new entry is
 * not duplicated.
 *
 * @param[in,out] gml The GML.
 * @param[in] entry The entry to insert.
 * 
 * @return IOK or IERROR with errno updated.
 */
int %%newscheme%%_gml_insert(gml_t *gml, void *entry);

/** 
 * @fn int %%newscheme%%_gml_remove(gml_t *gml, uint64_t index)
 * @brief Removes the entry at position <i>index</i> from the GML. The caller is 
 * responsible for removing the contents of the entry itself.
 *
 * @param[in,out] gml The GML.
 * @param[in] index The index of the entry to remove.
 * 
 * @return IOK or IERROR with errno updated.
 */
int %%newscheme%%_gml_remove(gml_t *gml, uint64_t index);

/** 
 * @fn void* %%newscheme%%_gml_get(gml_t *gml, uint64_t index)
 * @brief Returns a pointer to the GML entry at the specified position.
 *
 * @param[in] gml The GML.
 * @param[in] index The index of the entry to retrieve.
 * 
 * @return A pointer to the specified entry or NULL if error.
 */
void* %%newscheme%%_gml_get(gml_t *gml, uint64_t index);

/**
 * @fn gml_t* %%newscheme%%_gml_import(gml_type_t type, void *src)
 * @brief Loads the Group Members List stored in the given source, of the
 *  specified type, and returns a initialized GML structure.
 *
 * @param[in] type The type of source.
 * @param[in] src The element containing the gml.
 *
 * @return The imported GML or NULL if error.
 */
gml_t* %%newscheme%%_gml_import(gml_format_t type, void *src);

/**
 * @fn int %%newscheme%%_gml_export(gml_t *gml, void *dst, gml_format_t format)
 * @brief Exports the given Group Members List structure into the given destination.
 *
 * @param[in] gml The GML structure to save.
 * @param[in] dst The destination.
 * @param[in] format The type of destination.
 *
 * @return IOK or IERROR
 */
int %%newscheme%%_gml_export(gml_t *gml, void *dst, gml_format_t format);

/** 
 * @fn int %%newscheme%%_gml_export_new_entry(void *entry, void *dst, gml_format_t format)
 * @brief Adds the given new entry to the GML exported in the specified destination. 
 *
 * @param[in] entry The entry to add.
 * @param[in] dst The destination
 * @param[in] format The GML format.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_gml_export_new_entry(void *entry, void *dst, gml_format_t format);

/** 
 * @fn int %%newscheme%%_gml_compare_entries(void *entry1, void *entry2)
 * @brief Compares two %%newscheme%%_gml_entry_t structures. Just tells if they have the same
 * contents or not.
 *
 * @param[in] entry1 The first operand.
 * @param[in] entry2 The second operand.
 * 
 * @return 0 if both entries have the same contents != 0 if not. If an error
 *  occurs, errno is updated.
 */
int %%newscheme%%_gml_compare_entries(void *entry1, void *entry2);

/**
 * @var %%newscheme%%_gml_handle
 * @brief Set of functions for managing %%NEWSCHEME%% GMLs.
 */
static const gml_handle_t %%newscheme%%_gml_handle = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< Scheme code. */
  &%%newscheme%%_gml_init, /**< GML initialization. */
  &%%newscheme%%_gml_free, /**< GML free. */
  &%%newscheme%%_gml_insert, /**< Insert a new entry. */
  &%%newscheme%%_gml_remove, /**< Remove an existing entry. */
  &%%newscheme%%_gml_get, /**< Gets (without removing) a specific entry. */
  &%%newscheme%%_gml_import, /**< Import a GML at an external source. */
  &%%newscheme%%_gml_export, /**< Export the GML to an external destination. */
  &%%newscheme%%_gml_export_new_entry, /**< Add a new entry to an exported GML. */
};

#endif /* %%NEWSCHEME%%_GML_H */

/* %%newscheme%%_gml.h ends here */
