/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: grp_key.h
 * @brief: %%NEWSCHEME%% group keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié may  9 17:11:58 2012 (+0200)
 * @version: 0.1 
 * Last-Updated: vie ago 23 11:00:05 2013 (+0200)
 *           By: jesus
 *     Update #: 6
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _%%NEWSCHEME%%_GRP_KEY_H
#define _%%NEWSCHEME%%_GRP_KEY_H

#include <stdint.h>
#include "types.h"
#include "sysenv.h"
#include "%%newscheme%%.h"
#include "include/grp_key.h"

/**
 * @struct %%newscheme%%_grp_key_t
 * @brief Structure for %%NEWSCHEME%% group keys.
 *
 * Defineme.
 */
typedef struct {
  void *defineme;
} %%newscheme%%_grp_key_t;

/**
 * @def %%NEWSCHEME%%_GRP_KEY_BEGIN_MSG
 * @brief Begin string to prepend to headers of files containing %%NEWSCHEME%% group keys
 */
#define %%NEWSCHEME%%_GRP_KEY_BEGIN_MSG "BEGIN %%NEWSCHEME%% GROUPKEY"

/**
 * @def %%NEWSCHEME%%_GRP_KEY_END_MSG
 * @brief End string to prepend to headers of files containing %%NEWSCHEME%% group keys
 */
#define %%NEWSCHEME%%_GRP_KEY_END_MSG "END %%NEWSCHEME%% GROUPKEY"

/** 
 * @fn groupsig_key_t* %%newscheme%%_grp_key_init()
 * @brief Creates a new group key.
 *
 * @return A pointer to the initialized group key or NULL in case of error.
 */
groupsig_key_t* %%newscheme%%_grp_key_init();

/** 
 * @fn int %%newscheme%%_grp_key_free(groupsig_key_t *key)
 * @brief Frees the variables of the given group key.
 *
 * @param[in,out] key The group key to initialize.
 * 
 * @return IOK or IERROR
 */
int %%newscheme%%_grp_key_free(groupsig_key_t *key);

/** 
 * @fn int %%newscheme%%_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies a group key.
 *
 * Copies the source key into the destination key (which must be initialized by 
 * the caller).
 *
 * @param[in,out] dst The destination key.
 * @param[in] src The source key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int %%newscheme%%_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst)
 * @brief Exports the given group key.
 *
 * Exports the given %%NEWSCHEME%% group key, to the specified destination, using the given format.
 *
 * @param[in] key The group key to export.
 * @param[in] format The format to use for exporting the key. The available key 
 *  formats in %%NEWSCHEME%% are defined in @ref %%newscheme%%.h.
 * @param[in] dst The destination's description.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);

/** 
 * @fn groupsig_key_t* %%newscheme%%_grp_key_import(groupsig_key_format_t format, void *source)
 * @brief Imports a group key.
 *
 * Imports a %%NEWSCHEME%% group key from the specified source, of the specified format.
 * 
 * @param[in] format The source format. The available key formats in %%NEWSCHEME%% are
 *  defined in @ref %%newscheme%%.h.
 * @param[in] source The source's description.
 * 
 * @return A pointer to the imported key, or NULL if error.
 */
groupsig_key_t* %%newscheme%%_grp_key_import(groupsig_key_format_t format, void *source);

/** 
 * @fn char* %%newscheme%%_grp_key_to_string(groupsig_key_t *key)
 * @brief Converts the key to a printable string.
 *
 * Returns a printable string associated to the given key.
 *
 * @param[in] key The key to convert.
 * 
 * @return The printable string associated to the key, or NULL if error.
 */
char* %%newscheme%%_grp_key_to_string(groupsig_key_t *key);

/**
 * @var %%newscheme%%_grp_key_handle
 * @brief The set of functions to manage %%NEWSCHEME%% group keys.
 */
static const grp_key_handle_t %%newscheme%%_grp_key_handle = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< Scheme. */
  &%%newscheme%%_grp_key_init, /**< Initialize group keys. */
  &%%newscheme%%_grp_key_free, /**< Free group keys. */
  &%%newscheme%%_grp_key_copy, /**< Copy group keys. */
  &%%newscheme%%_grp_key_export, /**< Export group keys. */
  &%%newscheme%%_grp_key_import, /**< Import group keys. */
  &%%newscheme%%_grp_key_to_string, /**< Convert to printable strings. */
  &%%newscheme%%_grp_key_get_size_in_format, /**< Get size of key in format */
};

#endif

/* grp_key.h ends here */
