/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 11:21:21 2013 (+0100)
 * @version: 
 * Last-Updated: lun ago  5 15:07:24 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include "types.h"
#include "sys/mem.h"
#include "misc/misc.h"
#include "groupsig/%%newscheme%%/identity.h"

identity_t* %%newscheme%%_identity_init() {

  /* identity_t *id; */
  /* %%newscheme%%_identity_t *%%newscheme%%_id; */

  /* if(!(id = (identity_t *) mem_malloc(sizeof(identity_t)))) { */
  /*   LOG_ERRORCODE(&logger, __FILE__, "%%newscheme%%_identity_init", __LINE__, errno, LOGERROR); */
  /*   return NULL; */
  /* } */

  /* if(!(%%newscheme%%_id = (%%newscheme%%_identity_t *) mem_malloc(sizeof(%%newscheme%%_identity_t)))) { */
  /*   mem_free(id); id = NULL; */
  /*   LOG_ERRORCODE(&logger, __FILE__, "%%newscheme%%_identity_init", __LINE__, errno, LOGERROR); */
  /*   return NULL; */
  /* } */

  /* /\* A %%NEWSCHEME%% identity is the index pointing to an entry in the GML, we initialize  */
  /*    it to UINT64_MAX *\/ */
  /* *%%newscheme%%_id = UINT64_MAX; */
  
  /* id->scheme = GROUPSIG_%%NEWSCHEME%%_CODE; */
  /* id->id = %%newscheme%%_id; */

  /* return id; */

  return NULL;

}

int %%newscheme%%_identity_free(identity_t *id) {

  if(!id) {
    LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_identity_free", __LINE__,
  		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  /* if(id->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) { */
  /*   LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_identity_free", __LINE__, LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* /\* Currently, it is just an uint64_t* *\/ */
  /* mem_free((%%newscheme%%_identity_t *)id->id); id->id = NULL;   */
  /* mem_free(id); */

  /* return IOK; */

  return IERROR;

}

int %%newscheme%%_identity_copy(identity_t *dst, identity_t *src) {

  if(!dst || dst->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !src || src->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_identity_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  *((%%newscheme%%_identity_t *) dst->id) = *((%%newscheme%%_identity_t *) src->id);
  
  return IOK;

}

uint8_t %%newscheme%%_identity_cmp(identity_t *id1, identity_t *id2) {

  if(!id1 || !id2 || id1->scheme != id2->scheme || 
     id1->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_identity_cmp", __LINE__, LOGERROR);
    return UINT8_MAX;
  }

  /* if(*(uint64_t *) id1->id != *(uint64_t *) id2->id) return 1; */
  return 0;

}

char* %%newscheme%%_identity_to_string(identity_t *id) {

  if(!id || id->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_identity_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  /* /\* Currently, the %%NEWSCHEME%% identities are uint64_t's *\/ */
  /* return misc_uint642string(*((%%newscheme%%_identity_t *)id->id)); */

  return NULL;

}

identity_t* %%newscheme%%_identity_from_string(char *sid) {

  /* identity_t *id; */
  /* uint64_t uid; */

  if(!sid) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_identity_from_string", __LINE__, LOGERROR);
    return NULL;
  }

  /* if(!(id = %%newscheme%%_identity_init())) { */
  /*   return NULL; */
  /* } */

  /* /\* Currently, %%NEWSCHEME%% identities are uint64_t's *\/ */
  /* errno = 0; */
  /* uid = strtoul(sid, NULL, 10); */
  /* if(errno) { */
  /*   %%newscheme%%_identity_free(id); */
  /*   return NULL; */
  /* } */

  /* *((%%newscheme%%_identity_t *) id->id) = uid; */

  /* return id; */

  return NULL;

}

/* identity.c ends here */
