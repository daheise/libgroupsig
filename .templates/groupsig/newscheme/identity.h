/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.h
 * @brief: %%NEWSCHEME%% identities.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 11:16:11 2013 (+0100)
 * @version: 0.1
 * Last-Updated: lun ago  5 11:55:32 2013 (+0200)
 *           By: jesus
 *     Update #: 2
 * URL: bitbucket.org/jdiazvico/libgroupsig
 * @todo Identities should be made independent of group signature schemes, in 
 * order to allow different schemes to share the same identity implementation.
 */

#ifndef _%%NEWSCHEME%%_IDENTITY_H
#define _%%NEWSCHEME%%_IDENTITY_H

#include "include/identity.h"
#include "%%newscheme%%.h"

/**
 * %%NEWSCHEME%% identities.
 */
typedef uint64_t %%newscheme%%_identity_t;

/** 
 * @fn void* %%newscheme%%_identity_init()
 * @brief Allocates memory for a %%NEWSCHEME%% identity and sets its values to defaults.
 * 
 * @return A pointer to the allocated memory or NULL if error.
 */
identity_t* %%newscheme%%_identity_init();

/** 
 * @fn int %%newscheme%%_identity_free(void *id)
 * @brief Frees the memory allocated for a %%NEWSCHEME%% identity.
 *
 * @param[in,out] id The identity to free.
 * 
 * @return IOK.
 */
int %%newscheme%%_identity_free(identity_t *id);

/** 
 * @fn int %%newscheme%%_identity_copy(identity_t *dst, identity_t *src)
 * @brief Copies the source identity into the destination identity.
 *
 * @param[in,out] dst The destination identity. Initialized by the caller.
 * @param[in] src The source identity.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_identity_copy(identity_t *dst, identity_t *src);

/** 
 * @fn uint8_t %%newscheme%%_identity_cmp(identity_t *id1, identity_t *id2);
 * @brief Returns 0 if both ids are the same, != 0 otherwise.
 *
 * @param[in] id1 The first id to compare. 
 * @param[in] id2 The second id to compare.
 * 
 * @return 0 if both ids are the same, != otherwise. In case of error,
 *  errno is set consequently.
 */
uint8_t %%newscheme%%_identity_cmp(identity_t *id1, identity_t *id2);

/** 
 * @fn char* %%newscheme%%_identity_to_string(identity_t *id)
 * @brief Converts the given %%NEWSCHEME%% id into a printable string.
 *
 * @param[in] id The ID to convert.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
char* %%newscheme%%_identity_to_string(identity_t *id);

/** 
 * @fn identity_t* %%newscheme%%_identity_from_string(char *sid)
 * @brief Parses the given string as  %%NEWSCHEME%% identity.
 *
 * @param[in] sid The string containing the %%NEWSCHEME%% identity.
 * 
 * @return A pointer to the retrieved %%NEWSCHEME%% identity or NULL if error.
 */
identity_t* %%newscheme%%_identity_from_string(char *sid);

/**
 * @var %%newscheme%%_identity_handle
 * @brief Set of functions to manage %%NEWSCHEME%% identities.
 */
static const identity_handle_t %%newscheme%%_identity_handle = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< Scheme code. */
  &%%newscheme%%_identity_init, /**< Identity initialization. */
  &%%newscheme%%_identity_free, /**< Identity free.*/
  &%%newscheme%%_identity_copy, /**< Copies identities. */
  &%%newscheme%%_identity_cmp, /**< Compares identities. */
  &%%newscheme%%_identity_to_string, /**< Converts identities to printable strings. */
  &%%newscheme%%_identity_from_string /**< Imports identities from strings. */
};

#endif /* _%%NEWSCHEME%%_IDENTITY_H */

/* identity.h ends here */
