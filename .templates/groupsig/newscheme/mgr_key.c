/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié may  9 18:44:54 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 15:10:46 2013 (+0200)
 *           By: jesus
 *     Update #: 4
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/mgr_key.h"
#include "misc/misc.h"
#include "wrappers/base64.h"
#include "sys/mem.h"

/* private functions */

/** 
 * @fn static int _is_supported_format(groupsig_key_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_key_format_t format) {

  int i;

  for(i=0; i<%%NEWSCHEME%%_SUPPORTED_KEY_FORMATS_N; i++) {
    if(%%NEWSCHEME%%_SUPPORTED_KEY_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

/** 
 * @fn static int _mgr_key_export_file_null_b64(%%newscheme%%_mgr_key_t *key)
 * @brief Creates a representation of the given key as a string 
 *  "<field>=<value>..." and converts it to base64.
 *
 * @param[in] key The key to export.
 * 
 * @return A pointer to the produced base64 string or NULL in case of error.
 */
static int _mgr_key_export_file_null_b64(%%newscheme%%_mgr_key_t *key, void *dst) {

  if(!key || !dst) {
    LOG_EINVAL(&logger, __FILE__, "_grp_key_export_null_b64", __LINE__, LOGERROR);
    return IERROR;   
  }

  return IERROR;

}

/** 
 * @fn static int _mgr_key_import_file_null_b64(char *source, %%newscheme%%_mgr_key_t *key)
 * @brief Imports the key in source, a string encoded using the 
 *  _grp_key_export_null_base64 function, and initializes the given key structure
 *  accordingly.
 *
 * @param[in] source The base64 "null" encoding of the key.
 * @param[in,out] key Will be set to the imported key.
 * 
 * @return IOK or IERROR
 */
static groupsig_key_t* _mgr_key_import_file_null_b64(char *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_mgr_key_import_file_null_b64", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

/* public functions */

groupsig_key_t* %%newscheme%%_mgr_key_init() {

  return NULL;

}

int %%newscheme%%_mgr_key_free(groupsig_key_t *key) {

  return IERROR;

}

int %%newscheme%%_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src) {

  if(!dst || dst->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !src || src->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_mgr_key_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Add your code here */

  return IOK;

}

int %%newscheme%%_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst) {

  if(!key || key->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_mgr_key_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_mgr_key_export", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* /\* Apply the specified conversion *\/ */
  /* switch(format) { */
  /* case GROUPSIG_KEY_FORMAT_FILE_NULL_B64: */
  /*   return _mgr_key_export_file_null_b64(key->key, dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_mgr_key_export", __LINE__, */
  /* 		   "Unknown format.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;
  
}

groupsig_key_t* %%newscheme%%_mgr_key_import(groupsig_key_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_mgr_key_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "mgr_key_import", __LINE__,
		   "The specified format is not supported.", LOGERROR);
    return NULL;    
  }

  /* /\* Apply the specified conversion *\/ */
  /* switch(format) { */
  /* case GROUPSIG_KEY_FORMAT_FILE_NULL_B64: */
  /*   return _mgr_key_import_file_null_b64(source); */

  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "mgr_key_import", __LINE__, */
  /* 		   "Unexpected format.", LOGERROR); */
  /*   return NULL; */
  /* } */

  return NULL;

}

char* %%newscheme%%_mgr_key_to_string(groupsig_key_t *key) {

  if(!key || key->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_mgr_key_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

/* mgr_key.c ends here */
