/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key.h
 * @brief: %%NEWSCHEME%% Manager keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié may  9 17:11:58 2012 (+0200)
 * @version: 0.1
 * Last-Updated: vie ago 23 11:00:33 2013 (+0200)
 *           By: jesus
 *     Update #: 6
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _%%NEWSCHEME%%_MGR_KEY_H
#define _%%NEWSCHEME%%_MGR_KEY_H

#include <stdint.h>
#include "types.h"
#include "sysenv.h"
#include "%%newscheme%%.h"
#include "include/mgr_key.h"

/**
 * @def %%NEWSCHEME%%_MGR_KEY_BEGIN_MSG
 * @brief Begin string to prepend to headers of files containing %%NEWSCHEME%% group keys
 */
#define %%NEWSCHEME%%_MGR_KEY_BEGIN_MSG "BEGIN %%NEWSCHEME%% MANAGERKEY"

/**
 * @def %%NEWSCHEME%%_MGR_KEY_END_MSG
 * @brief End string to prepend to headers of files containing %%NEWSCHEME%% group keys
 */
#define %%NEWSCHEME%%_MGR_KEY_END_MSG "END %%NEWSCHEME%% MANAGERKEY"

/**
 * @struct %%newscheme%%_mgr_key_t
 * @brief Defineme.
 */
typedef struct {
  void *defineme;
} %%newscheme%%_mgr_key_t;

/** 
 * @fn groupsig_key_t* %%newscheme%%_mgr_key_init()
 * @brief Creates a new %%NEWSCHEME%% manager key
 *
 * @return The created manager key or NULL if error.
 */
groupsig_key_t* %%newscheme%%_mgr_key_init();

/** 
 * @fn int %%newscheme%%_mgr_key_free(groupsig_key_t *key)
 * @brief Frees the variables of the given manager key.
 *
 * @param[in,out] key The manager key to initialize.
 * 
 * @return IOK or IERROR
 */
int %%newscheme%%_mgr_key_free(groupsig_key_t *key);

/** 
 * @fn int %%newscheme%%_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies the source key into the destination key (which must be initialized by 
 * the caller).
 *
 * @param[in,out] dst The destination key.
 * @param[in] src The source key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int %%newscheme%%_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, 
 *                              void *dst)
 * @brief Exports the given manager key to the specified destination, using the
 *  specified format.
 *
 * @param[in] key The key to export.
 * @param[in] format The format to use. The supported formats for %%NEWSCHEME%% keys are
 *  specified in @ref %%newscheme%%.h.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);

/** 
 * @fn groupsig_key_t* %%newscheme%%_mgr_key_import(groupsig_key_format_t format, 
 *                                          void *source)
 * @brief Imports a %%NEWSCHEME%% manager key from the specified source, of the specified
 *  format.

 * @param[in] format The format of <i>source</i>. The supported formats for %%NEWSCHEME%%
 *  keys are defined in @ref %%newscheme%%.h.
 * @param[in] source The source information.
 * 
 * @return A pointer to the imported manager key, or NULL if error.
 */
groupsig_key_t* %%newscheme%%_mgr_key_import(groupsig_key_format_t format, void *source);

/** 
 * @fn char* %%newscheme%%_mgr_key_to_string(mgr_key_t *key)
 * @brief Creates a printable string of the given manager key.
 *
 * @param[in] key The manager key.
 * 
 * @return The created string or NULL if error.
 */
char* %%newscheme%%_mgr_key_to_string(groupsig_key_t *key);

/**
 * @var %%newscheme%%_mgr_key_handle
 * @brief Set of functions for %%NEWSCHEME%% manager keys management.
 */
static const mgr_key_handle_t %%newscheme%%_mgr_key_handle = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< The scheme code. */
  &%%newscheme%%_mgr_key_init, /**< Initializes manager keys. */
  &%%newscheme%%_mgr_key_free, /**< Frees manager keys. */
  &%%newscheme%%_mgr_key_copy, /**< Copies manager keys. */
  &%%newscheme%%_mgr_key_export, /**< Exports manager keys. */
  &%%newscheme%%_mgr_key_import, /**< Imports manager keys. */
  &%%newscheme%%_mgr_key_to_string, /**< Converts manager keys to printable strings. */
};

#endif

/* mgr_key.h ends here */
