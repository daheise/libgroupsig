/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: open.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:53:13 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 15:10:56 2013 (+0200)
 *           By: jesus
 *     Update #: 4
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "types.h"
#include "sysenv.h"
#include "bigz.h"
#include "sys/mem.h"
#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/grp_key.h"
#include "groupsig/%%newscheme%%/mgr_key.h"
#include "groupsig/%%newscheme%%/signature.h"
#include "groupsig/%%newscheme%%/gml.h"
#include "groupsig/%%newscheme%%/identity.h"

int %%newscheme%%_open(identity_t *id, groupsig_signature_t *sig, groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml) {

  if(!id || !sig || sig->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !gml) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_open", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

/* open.c ends here */
