/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: prove_equality
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 15:19:39 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 14:26:10 2013 (+0200)
 *           By: jesus
 *     Update #: 4
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <openssl/sha.h> /** @todo This should not be! */

#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/proof.h"
#include "groupsig/%%newscheme%%/mem_key.h"
#include "groupsig/%%newscheme%%/grp_key.h"
#include "groupsig/%%newscheme%%/signature.h"
#include "bigz.h"
#include "sys/mem.h"

/* Private functions */


/* Public functions */

int %%newscheme%%_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			 groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs) {
  
  if(!proof || proof->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !memkey || memkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !sigs || !n_sigs) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_prove_equality", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;
   
}

/* prove_equality.c ends here */
