/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: reveal.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:32:51 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 11:48:37 2013 (+0200)
 *           By: jesus
 *     Update #: 2
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "sysenv.h"
#include "bigz.h"
#include "sys/mem.h"
#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/mem_key.h"
#include "groupsig/%%newscheme%%/gml.h"
#include "groupsig/%%newscheme%%/crl.h"
#include "groupsig/%%newscheme%%/trapdoor.h"

int %%newscheme%%_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index) {

  %%newscheme%%_crl_entry_t *crl_entry;
  %%newscheme%%_gml_entry_t *gml_entry;
  bigz_t trapdoor;

  if(!trap || trap->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !gml || gml->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     (crl && crl->scheme != GROUPSIG_%%NEWSCHEME%%_CODE)) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  trapdoor = *(bigz_t *) trap->trap;

  /* The tracing trapdoor for the i-th member is the x field of its member key */
  if(!(gml_entry = ((%%newscheme%%_gml_entry_t *) gml_get(gml, index)))) {
    return IERROR;
  }

  if(bigz_set(trapdoor, *(%%newscheme%%_trapdoor_t *) gml_entry->trapdoor->trap) == IERROR) {
    return IERROR;
  }

  /* If we have received a CRL, update it with the "revoked" member */
  if(crl) {

    if(!(crl_entry = %%newscheme%%_crl_entry_init())) {
      return IERROR;
    }
    
    bigz_set(crl_entry->trapdoor, trapdoor);
    
    if(%%newscheme%%_identity_copy(crl_entry->id, gml_entry->id) == IERROR) {
      %%newscheme%%_crl_entry_free(crl_entry);
      return IERROR;
    }

    if(%%newscheme%%_crl_insert(crl, crl_entry) == IERROR) {
      %%newscheme%%_crl_entry_free(crl_entry);
      return IERROR;      
    }

  }

  return IOK;
}

/* reveal.c ends here */
