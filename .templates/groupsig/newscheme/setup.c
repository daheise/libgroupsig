/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: setup.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié may  9 13:30:04 2012 (+0200)
 * @version: 
 * Last-Updated: jue ago 22 20:39:09 2013 (+0200)
 *           By: jesus
 *     Update #: 4
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/grp_key.h"
#include "groupsig/%%newscheme%%/mgr_key.h"
#include "groupsig/%%newscheme%%/gml.h"
#include "math/nt.h"
#include "sys/mem.h"

/** 
 * @fn static int _setup_parameters_check(uint64_t k, uint64_t primesize, double epsilon)
 * @brief Checks the input parameters of the setup function
 *
 * @param[in] k The security parameter.
 * @param[in] primesize The Sophie Germain primes' size.
 * @param[in] epsilon The epsilon parameter for statistical indistinguishability.
 * 
 * @return IOK if the parameters are valid, IERROR otherwise
 */
static int _setup_parameters_check(/*specify input parameters here*/) {

  return IERROR;

}

groupsig_config_t* %%NEWSCHEME%%_config_init() {
  
  groupsig_config_t *cfg;

  if(!(cfg = (groupsig_config_t *) mem_malloc(sizeof(groupsig_config_t)))) {
    return NULL;
  }

  cfg->scheme = GROUPSIG_%%NEWSCHEME%%_CODE;
  if(!(cfg->config = (%%newscheme%%_config_t *) mem_malloc(sizeof(%%newscheme%%_config_t)))) {
    mem_free(cfg); cfg = NULL;
    return NULL;
  }

  return cfg;

}

int %%NEWSCHEME%%_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, groupsig_config_t *config) {

  if(!grpkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !mgrkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !gml ||
     !config || config->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%NEWSCHEME%%_setup", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get imput parameters from config->config */

  if(_setup_parameters_check(/**/) == IERROR) {
    return IERROR;
  }

  return IERROR;

}

/* setup.c ends here */
