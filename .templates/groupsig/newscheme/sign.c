/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: sign.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 15:19:39 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 15:12:20 2013 (+0200)
 *           By: jesus
 *     Update #: 5
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <limits.h>
#include <openssl/sha.h> /** @todo This should not be! */

#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/grp_key.h"
#include "groupsig/%%newscheme%%/mem_key.h"
#include "groupsig/%%newscheme%%/signature.h"
#include "bigz.h"

/* Private functions */

int %%newscheme%%_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
	       groupsig_key_t *grpkey, unsigned int seed) {

  /* Here we won't follow the typical C programming conventions for naming variables.
     Instead, we will name the variables as in the %%NEWSCHEME%% paper (with the exception 
     of doubling a letter when a ' is used, e.g. k' ==> kk). Auxialiar variables 
     that are not specified in the paper but helpful or required for its 
     implementation will be named aux_<name> */
  
  if(!sig || !msg || 
     !memkey || memkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_sign", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;
  
}

/* sign.c ends here */
