/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie may 18 11:48:37 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 15:12:56 2013 (+0200)
 *           By: jesus
 *     Update #: 5
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <gmp.h>
#include <openssl/sha.h>

#include "types.h"
#include "sysenv.h"
#include "sys/mem.h"
#include "wrappers/base64.h"
#include "misc/misc.h"
#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/signature.h"

/* Private functions */
/** 
 * @fn static int _is_supported_format(groupsig_signature_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_signature_format_t format) {

  int i;

  for(i=0; i<%%NEWSCHEME%%_SUPPORTED_SIG_FORMATS_N; i++) {
    if(%%NEWSCHEME%%_SUPPORTED_SIG_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

static int _signature_get_size_string_null_b64(%%newscheme%%_signature_t *sig) {

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "_signature_get_size_string_null_b64", __LINE__, LOGERROR);
    return -1;
  }

  return -1;
  
}


static int _signature_export_string_null_b64(%%newscheme%%_signature_t *sig, char *dst) {

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "_signature_export_string_null_b64", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

static int _signature_export_message_null_b64(%%newscheme%%_signature_t *sig, message_t *dst) {

  if(!sig || !dst) {
    LOG_EINVAL(&logger, __FILE__, "_signature_export_message_null_b64", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

static int _signature_export_file_null_b64(%%newscheme%%_signature_t *sig, char *dst) {

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "_signature_export_file_null_b64", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

static groupsig_signature_t* _signature_import_string_null_b64(char *b64) {

  if(!b64) {
    LOG_EINVAL(&logger, __FILE__, "_signature_import_string_null_b64", __LINE__,
	       LOGERROR);
    return NULL;
  }
    
  return NULL;

}

static groupsig_signature_t* _signature_import_message_null_b64(message_t *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_signature_import_message_null_b64", __LINE__,
	       LOGERROR);
    return NULL;
  }

  /* The message bytes are a Base64 representation of the group signature */
  return _signature_import_string_null_b64((char *) source->bytes);

}

static groupsig_signature_t* _signature_import_file_null_b64(char *source) {

  groupsig_signature_t *sig;

  /* Begin - this should be deleted once the key source types are implemented */
  char *b64;
  uint64_t b64_len;
  /* End  -  this should be deleted once the key source types are implemented */

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_signature_import_file_null_b64", __LINE__, LOGERROR);
    return NULL;
  }

  sig = NULL;

  /* 1) Parse from base64 to binary */

  /* Begin - this should be deleted once the key source types are implemented */  
  b64=NULL;
  b64_len=0;
  if(misc_read_file_to_bytestring((char *) source, (byte_t **) &b64, &b64_len) == IERROR) {
    return NULL;
  }
  /* End  -  this should be deleted once the key source types are implemented */

  if(!(sig = _signature_import_string_null_b64(b64))) {
    mem_free(b64); b64 = NULL;
    return NULL;
  }

  mem_free(b64); b64 = NULL;

  return sig;
  
}

/* Public functions */
groupsig_signature_t* %%newscheme%%_signature_init() {

  return NULL;

}

int %%newscheme%%_signature_free(groupsig_signature_t *sig) {

  %%newscheme%%_signature_t *%%newscheme%%_sig;

  if(!sig || sig->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_signature_free", __LINE__,
		   "Nothing to free.", LOGWARN);    
    return IOK;
  }

  return IERROR;

}

int %%newscheme%%_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src) {

  if(!dst || dst->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !src || src->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_signature_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

char* %%newscheme%%_signature_to_string(groupsig_signature_t *sig) {

  if(!sig || sig->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "signature_to_string", __LINE__, LOGERROR);
    return NULL;
  }
  
  return NULL;

}

int %%newscheme%%_signature_get_size_in_format(groupsig_signature_t *sig, groupsig_signature_format_t format) {

  if(!sig || sig->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_signature_get_size_in_format", __LINE__, LOGERROR);
    return -1;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_signature_get_size_in_format", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return -1; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64: */
  /*   return _signature_get_size_string_null_b64(sig->sig); */
  /* case GROUPSIG_SIGNATURE_FORMAT_STRING_NULL_B64: */
  /*   return _signature_get_size_string_null_b64(sig->sig); */
  /* case GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL_B64: */
  /*   return _signature_get_size_string_null_b64(sig->sig); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_signature_get_size_in_format", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return -1; */
  /* } */

  return -1;

}

int %%newscheme%%_signature_export(groupsig_signature_t *sig, groupsig_signature_format_t format, void *dst) { 

  if(!sig || sig->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_signature_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_signature_export", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64: */
  /*   return _signature_export_file_null_b64(sig->sig, dst); */
  /* case GROUPSIG_SIGNATURE_FORMAT_STRING_NULL_B64: */
  /*   return _signature_export_string_null_b64(sig->sig, dst); */
  /* case GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL_B64: */
  /*   return _signature_export_message_null_b64(sig->sig, dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_signature_export", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;

}

groupsig_signature_t* %%newscheme%%_signature_import(groupsig_signature_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "signature_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "signature_import", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return NULL; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64: */
  /*   return _signature_import_file_null_b64((char *) source);     */
  /* case GROUPSIG_SIGNATURE_FORMAT_STRING_NULL_B64: */
  /*   return _signature_import_string_null_b64((char *) source); */
  /* case GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL_B64: */
  /*   return _signature_import_message_null_b64((message_t *) source); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "signature_import", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return NULL; */
  /* } */

  return NULL;  

}

/* signature.c ends here */
