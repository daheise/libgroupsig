/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 11:21:21 2013 (+0100)
 * @version: 
 * Last-Updated: lun ago  5 14:43:05 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include "types.h"
#include "sys/mem.h"
#include "misc/misc.h"
#include "groupsig/%%newscheme%%/trapdoor.h"

trapdoor_t* %%newscheme%%_trapdoor_init() {

  return NULL;

}

int %%newscheme%%_trapdoor_free(trapdoor_t *trap) {

  if(!trap) {
    LOG_EINVAL_MSG(&logger, __FILE__, "%%newscheme%%_trapdoor_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  return IERROR;

}

int %%newscheme%%_trapdoor_copy(trapdoor_t *dst, trapdoor_t *src) {

  if(!dst || dst->scheme != GROUPSIG_%%NEWSCHEME%%_CODE ||
     !src || src->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_trapdoor_copy", __LINE__, LOGERROR);
    return IERROR;
  }
  
  return IERROR;

}

char* %%newscheme%%_trapdoor_to_string(trapdoor_t *trap) {

  if(!trap || trap->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_trapdoor_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

trapdoor_t* %%newscheme%%_trapdoor_from_string(char *strap) {

  if(!strap) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_trapdoor_from_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;
  
}

/* identity.c ends here */
