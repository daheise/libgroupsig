/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: verify.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 17:20:57 2012 (+0200)
 * @version: 
 * Last-Updated: lun ago  5 14:44:09 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <openssl/sha.h> /** @todo This should not be! */

#include "%%newscheme%%.h"
#include "groupsig/%%newscheme%%/grp_key.h"
#include "groupsig/%%newscheme%%/signature.h"
#include "bigz.h"
#include "sys/mem.h"

/* Private functions */

/* Public functions */
int %%newscheme%%_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, groupsig_key_t *grpkey) {
  
  if(!ok || !sig || !msg || 
     !grpkey || grpkey->scheme != GROUPSIG_%%NEWSCHEME%%_CODE) {
    LOG_EINVAL(&logger, __FILE__, "%%newscheme%%_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;

}

/* verify.c ends here */
