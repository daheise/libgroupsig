/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: %%newscheme%%.h
 * @brief: Main definitions for the %%NEWSCHEME%% group signature scheme.
 *
 * The %%NEWSCHEME%% scheme is an implementation of the scheme XXX
 *
 * @author: jesus
 * Maintainer: jesus
 * @date: vie jul  6 11:59:40 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun ago  5 15:31:12 2013 (+0200)
 *           By: jesus
 *     Update #: 129
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _%%NEWSCHEME%%_H
#define _%%NEWSCHEME%%_H

/**
 * @def GROUPSIG_%%NEWSCHEME%%_CODE
 * @brief %%NEWSCHEME%% scheme code.
 */
#define GROUPSIG_%%NEWSCHEME%%_CODE 0

/**
 * @def GROUPSIG_%%NEWSCHEME%%_NAME
 * @brief %%NEWSCHEME%% scheme name.
 */
#define GROUPSIG_%%NEWSCHEME%%_NAME "%%NEWSCHEME%%"

#include "key.h"
#include "gml.h"
#include "crl.h"
#include "signature.h"
#include "proof.h"
#include "grp_key.h"
#include "mgr_key.h"
#include "mem_key.h"
#include "groupsig.h"

/** 
 * @def %%NEWSCHEME%%_SUPPORTED_KEY_FORMATS_N
 * @brief Number of supported key formats supported by %%NEWSCHEME%%.
 */
#define %%NEWSCHEME%%_SUPPORTED_KEY_FORMATS_N 3

/**
 * @var %%NEWSCHEME%%_SUPPORTED_KEY_FORMATS
 * @brief Codes of the key formats supported by %%NEWSCHEME%%.
 */
static const int %%NEWSCHEME%%_SUPPORTED_KEY_FORMATS[%%NEWSCHEME%%_SUPPORTED_KEY_FORMATS_N] = { 
  GROUPSIG_KEY_FORMAT_FILE_NULL_B64,
  GROUPSIG_KEY_FORMAT_STRING_NULL_B64,
  GROUPSIG_KEY_FORMAT_MESSAGE_NULL_B64,
};

/**
 * @var %%newscheme%%_description
 * @brief %%NEWSCHEME%%'s description.
 */
static const groupsig_description_t %%newscheme%%_description = {
  GROUPSIG_%%NEWSCHEME%%_CODE, /**< %%NEWSCHEME%%'s scheme code. */
  GROUPSIG_%%NEWSCHEME%%_NAME /**< %%NEWSCHEME%%'s scheme name. */
};

/** 
 * @struct %%newscheme%%_config_t
 * @brief The configuration information for the %%NEWSCHEME%% scheme.
 * Defineme.
 */
typedef struct {
  void *defineme;
} %%newscheme%%_config_t;

/*
 * @def %%NEWSCHEME%%_CONFIG_SET_DEFAULTS
 * @brief Sets the configuration structure to the default values for
 *  %%NEWSCHEME%%.
 */
#define %%NEWSCHEME%%_CONFIG_SET_DEFAULTS(cfg) \
	((%%newscheme%%_config_t *) cfg) = NULL;

/**
 * @struct %%newscheme%%_sysenv_t
 * @brief Global information specific to the %%NEWSCHEME%% scheme, useful for saving
 *  communications and/or computation costs.
 */
typedef struct {
  void *defineme;
} %%newscheme%%_sysenv_t;

/** 
 * @fn groupsig_config_t* %%newscheme%%_config_init()
 * @brief Allocates memory for a %%NEWSCHEME%% config structure.
 * 
 * @return A pointer to the allocated structure or NULL if error.
 */
groupsig_config_t* %%newscheme%%_config_init();

/** 
 * @fn int %%newscheme%%_sysenv_update(void *data)
 * @brief Sets the %%NEWSCHEME%% internal environment data, i.e., the PBC params and pairings.
 *
 * @param data A %%newscheme%%_sysenv_t structure containing the PBC params and pairings.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_sysenv_update(void *data);

/** 
 * @fn void* %%newscheme%%_sysenv_get()
 * @brief Returns the %%NEWSCHEME%% specific environment data.
 * 
 * @return A pointer to the %%NEWSCHEME%% specific environment data or NULL if error.
 */
void* %%newscheme%%_sysenv_get();

/** 
 * @fn int %%newscheme%%_sysenv_free()
 * @brief Frees the %%NEWSCHEME%% internal environment.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_sysenv_free();

/** 
 * @fn int %%newscheme%%_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, 
 *                     gml_t *gml, groupsig_config_t *config)
 * @brief The setup function for the %%NEWSCHEME%% scheme.
 *
 * @param[in,out] grpkey An initialized group key, will be updated with the newly
 *   created group's group key.
 * @param[in,out] mgrkey An initialized manager key, will be updated with the
 *   newly created group's manager key.
 * @param[in,out] gml An initialized GML, will be set to an empty GML.
 * @param[in] config A %%NEWSCHEME%% configuration structure.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, groupsig_config_t *config);

/** 
 * @fn int %%newscheme%%_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Executes the member-side join of the %%NEWSCHEME%% scheme.
 *
 * @param[in,out] memkey An initialized member key. Will be updated with the
 * member-side generated key information.
 * @param[in,out] grpkey The group's key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey);

/** 
 * @fn int %%newscheme%%_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey)
 * @brief Executes the manager-side join of the join procedure.
 *
 * @param[in,out] gml The group's GML. Upon successful executions, a new entry
 *  corresponding to the new member will be added.
 * @param[in,out] memkey A member key in which the member-side join has already
 *  been executed. Upon successful execution, will be completed with the information
 *  generated by the Group Manager.
 * @param[in] mgrkey The Group Manager private key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey);

/** 
 * @fn int %%newscheme%%_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
 *	              groupsig_key_t *grpkey, unsigned int seed)
 * @brief Issues %%NEWSCHEME%% group signatures.
 *
 * Using the specified member and group keys, issues a signature for the specified
 * message.
 *
 * @param[in,out] sig An initialized %%NEWSCHEME%% group signature. Will be updated with
 *  the generated signature data.
 * @param[in] msg The message to sign.
 * @param[in] memkey The member key to use for signing.
 * @param[in] grpkey The group key.
 * @param[in] seed The seed. If it is set to UINT_MAX, the current system PRNG
 *  will be used normally. Otherwise, it will be reseeded with the specified
 *  seed before issuing the signature. 
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
	       groupsig_key_t *grpkey, unsigned int seed);

/** 
 * @fn int %%newscheme%%_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
 *		        groupsig_key_t *grpkey);
 * @brief Verifies a %%NEWSCHEME%% group signature.
 *
 * @param[in,out] ok Will be set to 1 if the verification succeeds, to 0 if
 *  it fails.
 * @param[in] sig The signature to verify.
 * @param[in] msg The corresponding message.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
		 groupsig_key_t *grpkey);

/** 
 * @fn int %%newscheme%%_open(identity_t *id, groupsig_signature_t *sig, groupsig_key_t *grpkey, 
 *	              groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Opens a %%NEWSCHEME%% group signature.
 * 
 * Opens the specified group signature, obtaining the signer's identity.
 *
 * @param[in,out] id An initialized identity. Will be updated with the signer's
 *  real identity.
 * @param[in] sig The signature to open.
 * @param[in] grpkey The group key.
 * @param[in] mgrkey The manager's key.
 * @param[in] gml The GML.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_open(identity_t *id, groupsig_signature_t *sig, groupsig_key_t *grpkey, 
	       groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @fn int %%newscheme%%_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index)
 * @brief Reveals the tracing trapdoor of the GML entry with the specified index.
 *
 * Reveals the tracing trapdoor of the GML entry with the specified index. If
 * a CRL is also specified, a new entry corresponding to the retrieved trapdoor
 * will be added. 
 *
 * @param[in,out] trap An initialized trapdoor. Will be updated with the trapdoor
 *  associated to the group member with the given index within the GML.
 * @param[in,out] crl Optional. If not NULL, must be an initialized CRL, and will
 *  be updated with a new entry corresponding to the obtained trapdoor.
 * @param[in] gml The GML.
 * @param[in] index The index of the GML from which the trapdoor is to be obtained.
 *  In %%NEWSCHEME%%, this matches the real identity of the group members.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index);

/** 
 * @fn int %%newscheme%%_trace(uint8_t *ok, groupsig_signature_t *sig, 
 *                     groupsig_key_t *grpkey, crl_t *crl)
 * @brief Determines whether or not the given signature has been issued by a 
 *  (unlinkability) revoked member.
 *
 * If the specified signature has been issued by a group member whose tracing
 * trapdoor is included in the CRL, ok will be set to 1. Otherwise, it will
 * be set to 0.
 *
 * @param[in,out] ok Will be set to 1 if the signature has been issued by a 
 *  group member with revoked unlinkability. To 0 otherwise.
 * @param[in] sig The signature to use for tracing.
 * @param[in] grpkey The group key.
 * @param[in] crl The CRL.
 * @param[in] mgrkey The manager key.
 * @param[in] gml The GML.
 *
 * @return IOK or IERROR.
 */
int %%newscheme%%_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, crl_t *crl, groupsig_key_t *mgrky, gml_t *gml);

/** 
 * @fn int %%newscheme%%_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *		       groupsig_key_t *grpkey, groupsig_signature_t *sig)
 * @brief Issues a proof demonstrating that the member with the specified key is
 *  the issuer of the specified signature.
 * 
 * @param[in,out] proof An initialized %%NEWSCHEME%% proof. Will be updated with the
 *  contents of the proof.
 * @param[in] memkey The member key of the issuer of the <i>sig</i> parameter.
 * @param[in] grpkey The group key.
 * @param[in] sig The signature.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
		groupsig_key_t *grpkey, groupsig_signature_t *sig);

/** 
 * @fn int %%newscheme%%_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *		              groupsig_signature_t *sig, groupsig_key_t *grpkey)
 * @brief Verifies a claim produced by the function <i>%%newscheme%%_claim</i>.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] sig The signature associated to the proof.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
		       groupsig_signature_t *sig, groupsig_key_t *grpkey);

/** 
 * @fn int %%newscheme%%_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *			        groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
 *                              uint16_t n_sigs)
 * @brief Creates a proof demonstrating that the given set of group signatures
 *  have all been issued by the same member.
 *
 * @param[in,out] proof An initialized proof. Will be updated with the produced
 *  proof.
 * @param[in] memkey The member key of the issuer of the given set of signatures.
 * @param[in] grpkey The group key.
 * @param[in] sigs The set of signatures, issued by the member with key <i>memkey</i>
 *  to be used for proof generation.
 * @param[in] n_sigs The number of signatures in <i>sigs</i> 
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			 groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs);

/** 
 * @fn int %%newscheme%%_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *                                     groupsig_key_t *grpkey,
 * 				       groupsig_signature_t **sigs, uint16_t n_sigs)
 * @brief Verifies the received proof, demonstrating that the given set of 
 *  signatures have been issued by the same group member.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] grpkey The group key.
 * @param[in] sigs The signatures that have allegedly been issued by the same member.
 * @param[in] n_sigs The number of signatures in <i>sigs</i>.
 * 
 * @return IOK or IERROR.
 */
int %%newscheme%%_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, groupsig_key_t *grpkey,
				groupsig_signature_t **sigs, uint16_t n_sigs);

/**
 * @var %%newscheme%%_groupsig_bundle
 * @brief The set of functions to manage %%NEWSCHEME%% groups.
 */
static const groupsig_t %%newscheme%%_groupsig_bundle = {
  &%%newscheme%%_description, /**< Contains the %%NEWSCHEME%% scheme description. */
  &%%newscheme%%_config_init, /**< Initializes a %%NEWSCHEME%% config structure. */
  &%%newscheme%%_sysenv_update, /**< Updates %%NEWSCHEME%%'s environment data, if any. */
  &%%newscheme%%_sysenv_get, /**< Gets %%NEWSCHEME%%'s environment data, if any. */
  &%%newscheme%%_sysenv_free, /**< Frees %%NEWSCHEME%%'s environment data, if any. */
  &%%newscheme%%_setup, /**< Sets up %%NEWSCHEME%% groups. */
  &%%newscheme%%_join_mem, /**< Executes member-side joins. */
  &%%newscheme%%_join_mgr, /**< Executes maanger-side joins. */
  &%%newscheme%%_sign, /**< Issues %%NEWSCHEME%% signatures. */
  &%%newscheme%%_verify, /**< Verifies %%NEWSCHEME%% signatures. */
  &%%newscheme%%_open, /**< Opens %%NEWSCHEME%% signatures. */
  &%%newscheme%%_reveal, /**< Reveals the tracing trapdoor from %%NEWSCHEME%% signatures. */
  &%%newscheme%%_trace, /**< Traces the issuer of a signature. */ 
  &%%newscheme%%_claim, /**< Claims, in ZK, "ownership" of a signature. */
  &%%newscheme%%_claim_verify, /**< Verifies claims. */
  &%%newscheme%%_prove_equality, /**< Issues "same issuer" ZK proofs for several signatures. */
  &%%newscheme%%_prove_equality_verify, /**< Verifies "same issuer" ZK proofs. */
};

#endif /* _%%NEWSCHEME%%_H */

/* %%newscheme%%.h ends here */
