FROM ubuntu:trusty

# Update and misc installs
RUN apt-get update 
RUN apt-get install -y --force-yes build-essential wget git autotools-dev autoconf libtool

# Install dependencies available from apt: libglib2.0-dev, libssl-dev, libgmp-dev and flex and bison (for libpbc)
RUN apt-get install -y --force-yes libglib2.0-dev libssl-dev libgmp-dev flex bison

# Install libpbc, not available from apt
WORKDIR /opt/libpbc
RUN wget https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz	
RUN tar -xzvf pbc-0.5.14.tar.gz
WORKDIR /opt/libpbc/pbc-0.5.14
RUN ./configure; make; make install

# Finally install libgroupsig

# Create directory for the library
WORKDIR /opt/libgroupsig

#  Clone from git 
RUN git clone https://bitbucket.org/jdiazvico/libgroupsig.git .
RUN ./configure; make; make check; make install
RUN ldconfig

# Re-set workdir
WORKDIR /root
ENTRYPOINT ["/bin/bash"]
