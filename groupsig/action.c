/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: action.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié ago  1 15:23:34 2012 (+0200)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:23 2013 (-0400)
 *           By: jesus
 *     Update #: 10
 * URL: 
 */
#include "config.h"
#include "action.h"
#include "types.h"

int action_code_from_str(char *str, uint8_t *action) {

  uint8_t i;

  if(!str || !action) {
    LOG_EINVAL(&logger, __FILE__, "action_code_from_str", __LINE__, LOGERROR);
    return IERROR;
  }

  for(i=0; i<ACTIONS_N; i++) {
    if(!strcasecmp(str, ACTIONS[i].name)) {
      *action = ACTIONS[i].code;
      return IOK;
    }
  }

  return IFAIL;

}

/* action.c ends here */
