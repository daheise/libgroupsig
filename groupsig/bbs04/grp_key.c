/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: grp_key.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié may  9 18:44:54 2012 (+0200)
 * @version: 
 * Last-Updated: mar ago 26 00:24:11 2014 (+0200)
 *           By: jesus
 *     Update #: 42
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

#include "sysenv.h"
#include "sys/mem.h"
#include "misc/misc.h"
#include "exim.h"
#include "wrappers/base64.h"
#include "wrappers/pbc_ext.h"

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"

/* This breaks encapsulation, but is needed to get the number of bits of
   the elements in the group. */
/* From $PBC/ecc/d_param.c */
struct d_param_s {
  mpz_t q;       // curve defined over F_q
  mpz_t n;       // has order n (= q - t + 1) in F_q
  mpz_t h;       // h * r = n, r is prime
  mpz_t r;
  mpz_t a, b;    // curve equation is y^2 = x^3 + ax + b
  int k;         // embedding degree
  mpz_t nk;      // order of curve over F_q^k
  mpz_t hk;      // hk * r^2 = nk
  mpz_t *coeff;  // coefficients of polynomial used to extend F_q by k/2
  mpz_t nqr;     // a quadratic nonresidue in F_q^d that lies in F_q
};

typedef struct d_param_s *d_param_ptr;

/* static (private) functions */

/** 
 * @fn static int _is_supported_format(groupsig_key_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_key_format_t format) {

  int i;

  for(i=0; i<BBS04_SUPPORTED_KEY_FORMATS_N; i++) {
    if(BBS04_SUPPORTED_KEY_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

/**
 * @fn static int _get_size_bytearray_null(exim_t *obj)
 * @brief Returns the size in bytes of the exim wrapped object. The size will be
 * equal to the size of bytearray output by _export_fd() or created by
 * _import_fd().
 *
 * @param[in] obj The object to be sized.
 *
 * @return The size in bytes of the object contained in obj.
 */
static int _get_size_bytearray_null(exim_t *obj){
  int size;
  if(!obj || !obj->eximable){
    return -1;
  }
  bbs04_grp_key_t* key = (bbs04_grp_key_t*)obj->eximable;
  bbs04_sysenv_t *bbs04_sysenv;
  bbs04_sysenv = sysenv->data;
  // We include the overhead for the generic group key type and the specific
  // bbs04_grp_key_t struct
  size = sizeof(uint8_t)*2 + sizeof(bbs04_grp_key_t) +
      element_length_in_bytes(key->g1) +
      element_length_in_bytes(key->g2) +
      element_length_in_bytes(key->h) +
      element_length_in_bytes(key->u) +
      element_length_in_bytes(key->v) +
      element_length_in_bytes(key->w) +
      element_length_in_bytes(key->hw) +
      element_length_in_bytes(key->hg2) +
      element_length_in_bytes(key->g1g2) +
      pbcext_get_dump_param_size(bbs04_sysenv->param);
  return size;
}

/**
 * @fn static int _export_fd(exim_t* obj, FILE *fd)
 * @brief Writes a bytearray representation of the given exim object to a
 * file descriptor with format:
 *
 *  | BBS04_CODE | KEYTYPE | size_params | params | size_g1 | g1 | size_g2 | g2 | size_h | h | size_u | u | size_v | v |
 *
 * @param[in] key The key to export.
 * @param[in, out] fd An open filestream to write to.
 *
 * @return IOK or IERROR
 */
static int _export_fd(exim_t* obj, FILE *fd) {
  bbs04_grp_key_t *key;
  bbs04_sysenv_t *bbs04_sysenv;
  uint8_t code, type;

  if(!obj || !obj->eximable || !fd) {
    LOG_EINVAL(&logger, __FILE__, "_export_fd", __LINE__, LOGERROR);
    return IERROR;
  }
  key = obj->eximable;

  bbs04_sysenv = sysenv->data;

  /* Dump GROUPSIG_BBS04_CODE */
  code = GROUPSIG_BBS04_CODE;
  if(fwrite(&code, sizeof(uint8_t), 1, fd) != 1) {
    return IERROR;
  }

  /* Dump key type */
  type = GROUPSIG_KEY_GRPKEY;
  if(fwrite(&type, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_export_fd", __LINE__,
          errno, LOGERROR);
    return IERROR;
  }

  /* Dump params */
  if(pbcext_dump_param_fd(bbs04_sysenv->param, fd) == IERROR) {
    return IERROR;
  }

  /* Dump g1 */
  if(pbcext_dump_element_fd(key->g1, fd) == IERROR) {
    return IERROR;
  }

  /* Dump g2 */
  if(pbcext_dump_element_fd(key->g2, fd) == IERROR) {
    return IERROR;
  }

  /* Dump h */
  if(pbcext_dump_element_fd(key->h, fd) == IERROR) {
    return IERROR;
  }

  /* Dump u */
  if(pbcext_dump_element_fd(key->u, fd) == IERROR) {
    return IERROR;
  }

  /* Dump v */
  if(pbcext_dump_element_fd(key->v, fd) == IERROR) {
    return IERROR;
  }

  /* Dump w */
  if(pbcext_dump_element_fd(key->w, fd) == IERROR) {
    return IERROR;
  }

  /* Dump hw */
  if(pbcext_dump_element_fd(key->hw, fd) == IERROR) {
    return IERROR;
  }

  /* Dump hg2 */
  if(pbcext_dump_element_fd(key->hg2, fd) == IERROR) {
    return IERROR;
  }

  /* Dump g1g2 */
  if(pbcext_dump_element_fd(key->g1g2, fd) == IERROR) {
    return IERROR;
  }

  return IOK;

}

/**
 * @fn static int _import_fd(FILE *fd, exim_t* obj)
 * @brief Import a representation of the given key from a file descriptor.
 * Expects the same format as the output from _export_fd().
 *
 * @return IOK or IERROR
 */
static int _import_fd(FILE *fd, exim_t* obj) {
  groupsig_key_t *key;
  bbs04_grp_key_t *bbs04_key;
  bbs04_sysenv_t *bbs04_sysenv;
  uint8_t type, scheme;

  if(!(key = bbs04_grp_key_init())) {
    return IERROR;
  }

  bbs04_key = key->key;

  if(fread(&scheme, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_import_fd", __LINE__,
          errno, LOGERROR);
    bbs04_grp_key_free(key); key = NULL;
    return IERROR;
  }

  if(scheme != key->scheme) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "_import_fd", __LINE__,
              EDQUOT, "Unexpected key scheme.", LOGERROR);
    bbs04_grp_key_free(key); key = NULL;
    return IERROR;
  }

  if(fread(&type, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_import_fd", __LINE__,
          errno, LOGERROR);
    bbs04_grp_key_free(key); key = NULL;
    return IERROR;
  }

  if(type != GROUPSIG_KEY_GRPKEY) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "_import_fd", __LINE__,
              EDQUOT, "Unexpected key scheme.", LOGERROR);
    bbs04_grp_key_free(key); key = NULL;
    return IERROR;
  }

  /* Get the params if sysenv->data is uninitialized */
  if(!sysenv->data) {

    /* Copy the param and pairing to the BBS04 internal environment */
    /* By setting the environment, we avoid having to keep a copy of params
       and pairing in manager/member keys and signatures, crls, gmls... */
    if(!(bbs04_sysenv = (bbs04_sysenv_t *) mem_malloc(sizeof(bbs04_sysenv_t)))) {
      bbs04_grp_key_free(key); key = NULL;
      return IERROR;
    }

    if(pbcext_get_param_fd(bbs04_sysenv->param, fd) == IERROR) {
      bbs04_grp_key_free(key); key = NULL;
      return IERROR;
    }

    pairing_init_pbc_param(bbs04_sysenv->pairing, bbs04_sysenv->param);

    if(bbs04_sysenv_update(bbs04_sysenv) == IERROR) {
      bbs04_grp_key_free(key); key = NULL;
      pbc_param_clear(bbs04_sysenv->param);
      mem_free(bbs04_sysenv); bbs04_sysenv = NULL;
      return IERROR;
    }

  } else { /* Else, skip it */

    if (pbcext_skip_param_fd(fd) == IERROR) {
      bbs04_grp_key_free(key); key = NULL;
    }

    bbs04_sysenv = sysenv->data;

  }

  /* Get g1 */
  element_init_G1(bbs04_key->g1, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->g1, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get g2 */
  element_init_G2(bbs04_key->g2, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->g2, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get h */
  element_init_G1(bbs04_key->h, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->h, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get u */
  element_init_G1(bbs04_key->u, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->u, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get v */
  element_init_G1(bbs04_key->v, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->v, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get w */
  element_init_G2(bbs04_key->w, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->w, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get hw */
  element_init_GT(bbs04_key->hw, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->hw, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }
  /* Get hg2 */
  element_init_GT(bbs04_key->hg2, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->hg2, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  /* Get g1g2 */
  element_init_GT(bbs04_key->g1g2, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->g1g2, fd) == IERROR) {
    bbs04_grp_key_free(key); key = NULL;
    bbs04_sysenv_free();
    return IERROR;
  }

  obj->eximable = (void*) key;
  return IOK;
}

/* Export/import handle definition */

static exim_handle_t _exim_h = {
  &_get_size_bytearray_null,
  &_export_fd,
  &_import_fd,
};

/* "Public" functions */

groupsig_key_t* bbs04_grp_key_init() {

  groupsig_key_t *key;

  if(!(key = (groupsig_key_t *) mem_malloc(sizeof(groupsig_key_t)))) {
    return NULL;
  }

  if(!(key->key = (bbs04_grp_key_t *) mem_malloc(sizeof(bbs04_grp_key_t)))) {
    mem_free(key); key = NULL;
    return NULL;
  }

  key->scheme = GROUPSIG_BBS04_CODE;
  
  return key;
  
}

int bbs04_grp_key_free(groupsig_key_t *key) {

  bbs04_grp_key_t *bbs04_key;

  if(!key) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_grp_key_free", __LINE__, 
		   "Nothing to free.", LOGWARN);
    return IOK;  
  }

  if(key->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_free", __LINE__, LOGERROR);
    return IERROR;	       
  }

  if(key->key) {

    bbs04_key = key->key;
    element_clear(bbs04_key->g1);
    element_clear(bbs04_key->g2);
    element_clear(bbs04_key->h);
    element_clear(bbs04_key->u);
    element_clear(bbs04_key->v);
    element_clear(bbs04_key->w);
    element_clear(bbs04_key->hg2);
    element_clear(bbs04_key->hw);
    element_clear(bbs04_key->g1g2);

    mem_free(key->key); key->key = NULL;
  }

  mem_free(key); key = NULL;

  return IOK;

}

int bbs04_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src) {

  bbs04_grp_key_t *bbs04_dst, *bbs04_src;

  if(!dst || dst->scheme != GROUPSIG_BBS04_CODE ||
     !src || src->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_dst = dst->key;
  bbs04_src = src->key;

  /* Copy the elements */
  element_init_same_as(bbs04_dst->g1, bbs04_src->g1);
  element_set(bbs04_dst->g1, bbs04_src->g1);
  element_init_same_as(bbs04_dst->g2, bbs04_src->g2);
  element_set(bbs04_dst->g2, bbs04_src->g2);
  element_init_same_as(bbs04_dst->h, bbs04_src->h);
  element_set(bbs04_dst->h, bbs04_src->h);  
  element_init_same_as(bbs04_dst->u, bbs04_src->u);
  element_set(bbs04_dst->u, bbs04_src->u);
  element_init_same_as(bbs04_dst->v, bbs04_src->v);
  element_set(bbs04_dst->v, bbs04_src->v);
  element_init_same_as(bbs04_dst->w, bbs04_src->w);
  element_set(bbs04_dst->w, bbs04_src->w);    

  return IOK;

}

int bbs04_grp_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format) {
  if(!key || key->scheme != GROUPSIG_BBS04_CODE ||
     !_is_supported_format(format)) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_get_size_in_format", __LINE__, LOGERROR);
    return -1;
  }
  exim_t wrap = {key->key, &_exim_h };
  return exim_get_size_in_format(&wrap, format);

}

int bbs04_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst) {
  if(!key || key->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_grp_key_export", __LINE__,
  		   "The specified format is not supported.", LOGERROR);
    return IERROR;
  }

  /* Apply the specified conversion */
  exim_t wrap = {key->key, &_exim_h };
  return exim_export(&wrap, format, dst);
  
}

groupsig_key_t* bbs04_grp_key_import(groupsig_key_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_grp_key_import", __LINE__,
  		   "The specified format is not supported.", LOGERROR);
    return NULL;
  }

  /* Apply the specified conversion */
  exim_t wrap = {NULL, &_exim_h };
  if(exim_import(format, source, &wrap) == IOK){
    return wrap.eximable;
  }

  return NULL;

}

char* bbs04_grp_key_to_string(groupsig_key_t *key) { 

  struct stat buf;
  uint64_t len;
  size_t b;
  bbs04_sysenv_t *bbs04_sysenv;
  FILE *fd;
  char *skey, *tmpnm, *pbc_str;

  if(!key) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  bbs04_sysenv = sysenv->data;

  /* Get the size of the key (determined by the q value of the PBC struct) */
  b = mpz_sizeinbase(((d_param_ptr) bbs04_sysenv->param->data)->q, 2);

  /* PBC only supports direct dumping of the PBC parameters to a file... 
     We'll dump them to a file and then read the file to a string...
     @todo Probably reading the fields directly would be better, although
      it would break encapsulation... */
  // @todo tmpnam is deprecated!!! use mkstemp instead
  if(!(tmpnm = tmpnam(NULL))) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, 
		  errno, LOGERROR);
    return NULL;
    }
  
  if(!(fd = fopen(tmpnm, "w"))) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, 
		  errno, LOGERROR);
    return NULL;
  }

  /* Dump the pbc params to fd */
  pbc_param_out_str(fd, bbs04_sysenv->param);
  fclose(fd); fd = NULL;

  if(stat(tmpnm, &buf) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, 
		  errno, LOGERROR);
    return NULL;    
  }

  len = buf.st_size+10+strlen("Group key:  bits\n");

  /* Read the file to a string */
  if(!(skey = (char *) 
       mem_malloc(sizeof(char)*(len+1)))) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, 
		  errno, LOGERROR);
    return NULL;        
  }

  sprintf(skey, "Group key: %ld bits\n", b);

  pbc_str = NULL;
  if(misc_read_file_to_string(tmpnm, (char **) &pbc_str, &len) == IERROR) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_grp_key_to_string", __LINE__, 
		  errno, LOGERROR);
    return NULL;        
  }

  sprintf(&skey[strlen(skey)], "%s", pbc_str);
  mem_free(pbc_str); pbc_str = NULL;

  unlink(tmpnm);

  return skey;

}

/* grp_key.c ends here */
