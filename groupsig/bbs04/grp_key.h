/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: grp_key.h
 * @brief: BBS04 group keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié may  9 17:11:58 2012 (+0200)
 * @version: 0.1 
 * Last-Updated: mié oct  2 21:08:27 2013 (+0200)
 *           By: jesus
 *     Update #: 11
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BBS04_GRP_KEY_H
#define _BBS04_GRP_KEY_H

#include <stdint.h>
#include <pbc/pbc.h>
#include "types.h"
#include "sysenv.h"
#include "bbs04.h"
#include "include/grp_key.h"

/**
 * @struct bbs04_grp_key_t
 * @brief Structure for BBS04 group keys.
 *
 * BBS04 group keys. 
 */
typedef struct {
  element_t g1; /**< Tr(g2) */
  element_t g2; /**< Random generator of G2 */
  element_t h; /**< Random element in G1 \ 1 */
  element_t u; /**< h^(xi1^-1) @see bbs04_mgr_key_t */
  element_t v; /**< h^(xi2^-1) @see bbs04_mgr_key_t */
  element_t w; /**< g2^gamma @see bbs04_mgr_key_t */
  element_t hw; /**< Precompute e(h,w) **/
  element_t hg2; /**<Precompute e(h,g2) **/
  element_t g1g2; /**< Precompute e(g1,g2) **/
} bbs04_grp_key_t;

/**
 * @def BBS04_GRP_KEY_BEGIN_MSG
 * @brief Begin string to prepend to headers of files containing BBS04 group keys
 */
#define BBS04_GRP_KEY_BEGIN_MSG "BEGIN BBS04 GROUPKEY"

/**
 * @def BBS04_GRP_KEY_END_MSG
 * @brief End string to prepend to headers of files containing BBS04 group keys
 */
#define BBS04_GRP_KEY_END_MSG "END BBS04 GROUPKEY"

/** 
 * @fn groupsig_key_t* bbs04_grp_key_init()
 * @brief Creates a new group key.
 *
 * @return A pointer to the initialized group key or NULL in case of error.
 */
groupsig_key_t* bbs04_grp_key_init();

/** 
 * @fn int bbs04_grp_key_free(groupsig_key_t *key)
 * @brief Frees the variables of the given group key.
 *
 * @param[in,out] key The group key to initialize.
 * 
 * @return IOK or IERROR
 */
int bbs04_grp_key_free(groupsig_key_t *key);

/** 
 * @fn int bbs04_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies a group key.
 *
 * Copies the source key into the destination key (which must be initialized by 
 * the caller).
 *
 * @param[in,out] dst The destination key.
 * @param[in] src The source key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int bbs04_grp_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format)
 * @brief Returns the size that the given key would require in order to be
 *  represented using the specified format.
 *
 * @param[in] key The key.
 * @param[in] format The format. The list of supported key formats in the BBS04
 *  scheme are defined in @ref bbs04.h.
 *
 * @return The required number of bytes, or -1 if error.
 */
int bbs04_grp_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format);

/**
 * @fn int bbs04_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst)
 * @brief Exports the given group key.
 *
 * Exports the given BBS04 group key, to the specified destination, using the given format.
 *
 * @param[in] key The group key to export.
 * @param[in] format The format to use for exporting the key. The available key 
 *  formats in BBS04 are defined in @ref bbs04.h.
 * @param[in] dst The destination's description.
 * 
 * @return IOK or IERROR.
 */
int bbs04_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);

/** 
 * @fn groupsig_key_t* bbs04_grp_key_import(groupsig_key_format_t format, void *source)
 * @brief Imports a group key.
 *
 * Imports a BBS04 group key from the specified source, of the specified format.
 * 
 * @param[in] format The source format. The available key formats in BBS04 are
 *  defined in @ref bbs04.h.
 * @param[in] source The source's description.
 * 
 * @return A pointer to the imported key, or NULL if error.
 */
groupsig_key_t* bbs04_grp_key_import(groupsig_key_format_t format, void *source);

/** 
 * @fn char* bbs04_grp_key_to_string(groupsig_key_t *key)
 * @brief Converts the key to a printable string.
 *
 * Returns a printable string associated to the given key.
 *
 * @param[in] key The key to convert.
 * 
 * @return The printable string associated to the key, or NULL if error.
 */
char* bbs04_grp_key_to_string(groupsig_key_t *key);

/**
 * @var bbs04_grp_key_handle
 * @brief The set of functions to manage BBS04 group keys.
 */
static const grp_key_handle_t bbs04_grp_key_handle = {
  GROUPSIG_BBS04_CODE, /**< Scheme. */
  &bbs04_grp_key_init, /**< Initialize group keys. */
  &bbs04_grp_key_free, /**< Free group keys. */
  &bbs04_grp_key_copy, /**< Copy group keys. */
  &bbs04_grp_key_export, /**< Export group keys. */
  &bbs04_grp_key_import, /**< Import group keys. */
  &bbs04_grp_key_to_string, /**< Convert to printable strings. */
  &bbs04_grp_key_get_size_in_format,
};

#endif

/* grp_key.h ends here */
