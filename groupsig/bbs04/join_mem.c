/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: join.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue may 10 11:12:56 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 20:46:32 2013 (+0200)
 *           By: jesus
 *     Update #: 5
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mem_key.h"
#include "bigz.h"
#include "sys/mem.h"

/** 
 * @fn int bbs04_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Member side join procedure.
 * 
 * The original proposal does not include a "join" procedure. Instead, it is the
 * private-key issuer generates and distributes the member keys, and requires a
 * predefined group size. We adopt this approach to allow dynamic addition of group
 * members.
 *
 * @param[in,out] memkey Will be set to the produced member key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey) {

  bbs04_mem_key_t* bbs04_memkey;
  bbs04_grp_key_t* bbs04_grpkey;
  bbs04_sysenv_t *bbs04_sysenv;

  if(!memkey || memkey->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_join_mem", __LINE__, LOGERROR);
    return IERROR;
  }

  /** @todo Currently this is a dummy function that does nothing. In a real world
      scenario, a communication should be established, and provably a secure two 
      party computation for adaptive chosing of random powers should be executed 
      (see KTY04). */  

  /* By convention here, we will set x and A to 0 to mark that they have not
       been set... (@todo is this a mathematical stupidity?)
       NOTE: this is needed by some external applications (e.g. caduceus)
  */
  bbs04_memkey = (bbs04_mem_key_t *) memkey->key;
  bbs04_grpkey = (bbs04_grp_key_t *) grpkey->key;
  bbs04_sysenv = (bbs04_sysenv_t *) sysenv->data;

  element_init_Zr(bbs04_memkey->x, bbs04_sysenv->pairing);
  element_set0(bbs04_memkey->x);

  element_init_G1(bbs04_memkey->A, bbs04_sysenv->pairing);
  element_set0(bbs04_memkey->A);

  return IOK;

}

/* join.c ends here */
