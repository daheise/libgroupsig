/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: join_mgr.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 16:50:13 2013 (+0100)
 * @version: 
 * Last-Updated: mié sep 25 20:48:06 2013 (+0200)
 *           By: jesus
 *     Update #: 34
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mgr_key.h"
#include "groupsig/bbs04/mem_key.h"
#include "groupsig/bbs04/gml.h"
#include "groupsig/bbs04/identity.h"
#include "groupsig/bbs04/trapdoor.h"
#include "wrappers/pbc_ext.h"
#include "sys/mem.h"

int bbs04_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey) {

  bbs04_mem_key_t *bbs04_memkey;
  bbs04_mgr_key_t *bbs04_mgrkey;
  bbs04_grp_key_t *bbs04_grpkey;
  bbs04_gml_entry_t *bbs04_entry;
  bbs04_sysenv_t *bbs04_sysenv;
  bbs04_trapdoor_t *bbs04_trap;
  element_t gammax;

  if(!gml || gml->scheme != GROUPSIG_BBS04_CODE ||
     !memkey || memkey->scheme != GROUPSIG_BBS04_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_join_mgr", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_memkey = (bbs04_mem_key_t *) memkey->key;
  bbs04_mgrkey = (bbs04_mgr_key_t *) mgrkey->key;
  bbs04_grpkey = (bbs04_grp_key_t *) grpkey->key;
  bbs04_sysenv = (bbs04_sysenv_t *) sysenv->data;

  /* Select memkey->x randomly in Z_p^* */
  element_init_Zr(bbs04_memkey->x, bbs04_sysenv->pairing);
  element_random(bbs04_memkey->x);

  /* Compute memkey->A = g_1^(1/(mgrkey->gamma+memkey->x)) */
  element_init_Zr(gammax, bbs04_sysenv->pairing);
  element_add(gammax, bbs04_mgrkey->gamma, bbs04_memkey->x);

  element_init_G1(bbs04_memkey->A, bbs04_sysenv->pairing);
  element_set(bbs04_memkey->A, bbs04_grpkey->g1);
  element_invert(gammax, gammax);
  element_pow_zn(bbs04_memkey->A, bbs04_memkey->A, gammax);
  //element_invert(bbs04_memkey->A, bbs04_memkey->A);

  /* Optimization */
  element_init_GT(bbs04_memkey->Ag2, bbs04_sysenv->pairing);
  element_pairing(bbs04_memkey->Ag2, bbs04_memkey->A, bbs04_grpkey->g2);

  element_clear(gammax);
  
  /* Update the gml, if any */
  if(gml) {
    
    /* Initialize the GML entry */
    if(!(bbs04_entry = bbs04_gml_entry_init()))
      return IERROR;

    bbs04_trap = (bbs04_trapdoor_t *) bbs04_entry->trapdoor->trap;
    element_init_same_as(bbs04_trap->open, bbs04_memkey->A);
    element_set(bbs04_trap->open, bbs04_memkey->A);

    /* Currently, BBS04 identities are just uint64_t's */
    *(bbs04_identity_t *) bbs04_entry->id->id = gml->n;
    
    if(gml_insert(gml, bbs04_entry) == IERROR) {
      bbs04_gml_entry_free(bbs04_entry); bbs04_entry = NULL;
      return IERROR;      
    }
    
  }

  return IOK;

}

/* join_mgr.c ends here */
