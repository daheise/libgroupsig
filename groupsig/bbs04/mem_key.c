/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mem_key.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié may  9 18:44:54 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 11 17:45:01 2013 (+0200)
 *           By: jesus
 *     Update #: 26
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

#include "bbs04.h"
#include "groupsig/bbs04/mem_key.h"
#include "wrappers/base64.h"
#include "wrappers/pbc_ext.h"
#include "misc/misc.h"
#include "exim.h"
#include "sys/mem.h"

/* Private functions */

/** 
 * @fn static int _is_supported_format(groupsig_key_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_key_format_t format) {

  int i;

  for(i=0; i<BBS04_SUPPORTED_KEY_FORMATS_N; i++) {
    if(BBS04_SUPPORTED_KEY_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

/**
 * @fn static int _get_size_bytearray_null(exim_t *obj)
 * @brief Returns the size in bytes of the exim wrapped object. The size will be
 * equal to the size of bytearray output by _export_fd() or created by
 * _import_fd().
 *
 * @param[in] obj The object to be sized.
 *
 * @return The size in bytes of the object contained in obj.
 */
static int _get_size_bytearray_null(exim_t *obj){
  int size;
  if(!obj || !obj->eximable){
    return -1;
  }
  bbs04_mem_key_t *key = obj->eximable;
  // This is a little dirty, but checks to see if the data has been initialized
  // to something other than null
  if(!key->x[0].data || !key->A[0].data){
    LOG_EINVAL(&logger, __FILE__, "_get_size_bytearray_null", __LINE__, LOGERROR);
    return -1;
  }

  bbs04_sysenv_t *bbs04_sysenv;
  bbs04_sysenv = sysenv->data;
  // We include the overhead for the generic group key type and the specific
  // bbs04_mem_key_t struct
  size = sizeof(uint8_t)*2 + sizeof(bbs04_mem_key_t) +
      element_length_in_bytes(key->A) +
      element_length_in_bytes(key->x) +
      element_length_in_bytes(key->Ag2) +
      pbcext_get_dump_param_size(bbs04_sysenv->param);
  return size;
}

/**
 * @fn static int _export_fd(exim_t* obj, FILE *fd)
 * @brief Writes a bytearray representation of the given exim object to a
 * file descriptor with format:
 *
 *   BBS04_CODE | KEYTYPE | size params | params | size x | x | size A | A |
 *
 * @param[in] key The key to export.
 * @param[in, out] fd An open filestream to write to.
 *
 * @return IOK or IERROR
 */
static int _export_fd(exim_t *obj, FILE *fd) {
  bbs04_mem_key_t *key;
  bbs04_sysenv_t *bbs04_sysenv;
  uint8_t code, type;

  if(!obj || !obj->eximable || !fd) {
    LOG_EINVAL(&logger, __FILE__, "_export_fd", __LINE__, LOGERROR);
    return IERROR;
  }
  key = obj->eximable;
  // This is a little dirty, but checks to see if the data has been initialized
  // to something other than null
  if(!key->x[0].data || !key->A[0].data){
    LOG_EINVAL(&logger, __FILE__, "_export_fd", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_sysenv = sysenv->data;

  /* Dump GROUPSIG_BBS04_CODE */
  code = GROUPSIG_BBS04_CODE;
  if(fwrite(&code, sizeof(uint8_t), 1, fd) != 1) {
    return IERROR;
  }

  /* Dump key type */
  type = GROUPSIG_KEY_MEMKEY;
  if(fwrite(&type, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_export_fd", __LINE__,
          errno, LOGERROR);
    return IERROR;
  }

  /* Dump params */
  if(pbcext_dump_param_fd(bbs04_sysenv->param, fd) == IERROR) {
    return IERROR;
  }

  /* Dump x */
  if(pbcext_dump_element_fd(key->x, fd) == IERROR) {
    return IERROR;
  }

  /* Dump A */
  if(pbcext_dump_element_fd(key->A, fd) == IERROR) {
    return IERROR;
  }

  /* Dump e(A, g2) */
  if(pbcext_dump_element_fd(key->Ag2, fd) == IERROR) {
    return IERROR;
  }

  return IOK;

}

/**
 * @fn static int _import_fd(FILE *fd, exim_t* obj)
 * @brief Import a representation of the given key from a file descriptor.
 * Expects the same format as the output from _export_fd().
 *
 * @return IOK or IERROR
 */
static int _import_fd(FILE *fd, exim_t* obj) {

  groupsig_key_t *key;
  bbs04_mem_key_t *bbs04_key;
  bbs04_sysenv_t *bbs04_sysenv;
  uint8_t type, scheme;

  if(!(key = bbs04_mem_key_init())) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  bbs04_key = key->key;

  /* First sizeof(int) bytes: scheme */
  if(fread(&scheme, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_import_fd", __LINE__,
          errno, LOGERROR);
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  if(scheme != key->scheme) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "_import_fd", __LINE__,
              EDQUOT, "Unexpected key scheme.", LOGERROR);
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  /* Next sizeof(int) bytes: key type */
  if(fread(&type, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_import_fd", __LINE__,
          errno, LOGERROR);
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  if(type != GROUPSIG_KEY_MEMKEY) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "_import_fd", __LINE__,
              EDQUOT, "Unexpected key scheme.", LOGERROR);
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  /* Get the params if sysenv->data is uninitialized */
  if(!sysenv->data) {

    /* Copy the param and pairing to the BBS04 internal environment */
    /* By setting the environment, we avoid having to keep a copy of params
       and pairing in manager/member keys and signatures, crls, gmls... */
    if(!(bbs04_sysenv = (bbs04_sysenv_t *) mem_malloc(sizeof(bbs04_sysenv_t)))) {
      bbs04_mem_key_free(key); key = NULL;
      return IERROR;
    }

    if(pbcext_get_param_fd(bbs04_sysenv->param, fd) == IERROR) {
      bbs04_mem_key_free(key); key = NULL;
      return IERROR;
    }

    pairing_init_pbc_param(bbs04_sysenv->pairing, bbs04_sysenv->param);

    if(bbs04_sysenv_update(bbs04_sysenv) == IERROR) {
      bbs04_mem_key_free(key); key = NULL;
      pbc_param_clear(bbs04_sysenv->param);
      mem_free(bbs04_sysenv); bbs04_sysenv = NULL;
      return IERROR;
    }

  } else { /* Else, skip it */

    if (pbcext_skip_param_fd(fd) == IERROR) {
      bbs04_mem_key_free(key); key = NULL;
    }

    bbs04_sysenv = sysenv->data;

  }

  /* Get x */
  element_init_Zr(bbs04_key->x, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->x, fd) == IERROR) {
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  /* Get A */
  element_init_G1(bbs04_key->A, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->A, fd) == IERROR) {
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  /* Get A */
  element_init_GT(bbs04_key->Ag2, bbs04_sysenv->pairing);
  if(pbcext_get_element_fd(bbs04_key->Ag2, fd) == IERROR) {
    bbs04_mem_key_free(key); key = NULL;
    return IERROR;
  }

  obj->eximable = (void*) key;
  return IOK;
}

/* static groupsig_key_t* _mem_key_import_message_null_b64(message_t *source) { */

/*   if(!source) { */
/*     LOG_EINVAL(&logger, __FILE__, "_mem_key_import_message_null_b64", __LINE__, */
/* 	       LOGERROR); */
/*     return NULL; */
/*   } */

/*   return _mem_key_import_string_null_b64((char *) source->bytes); */

/* } */

/* Export/import handle definition */

static exim_handle_t _exim_h = {
  &_get_size_bytearray_null,
  &_export_fd,
  &_import_fd,
};

/* Public functions */

groupsig_key_t* bbs04_mem_key_init() {
  
  groupsig_key_t *key;

  if(!(key = (groupsig_key_t *) mem_malloc(sizeof(groupsig_key_t)))) {
    return NULL;
  }

  if(!(key->key = (bbs04_mem_key_t *) mem_malloc(sizeof(bbs04_mem_key_t)))) {
    mem_free(key); key = NULL;
    return NULL;
  }

  key->scheme = GROUPSIG_BBS04_CODE;

  return key;

}

int bbs04_mem_key_free(groupsig_key_t *key) {

  bbs04_mem_key_t *bbs04_key;

  if(!key) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_mem_key_free", __LINE__, 
		   "Nothing to free.", LOGWARN);
    return IOK;  
  }

  if(key->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_free", __LINE__, LOGERROR);
    return IERROR;	       
  }

  if(key->key) {
    bbs04_key = key->key;
    if(bbs04_key->x[0].data) element_clear(bbs04_key->x);
    if(bbs04_key->A[0].data) element_clear(bbs04_key->A);
    if(bbs04_key->A[0].data) element_clear(bbs04_key->Ag2);

    mem_free(key->key); key->key = NULL;
    key->key = NULL;
  }
  
  mem_free(key); key = NULL;

  return IOK;

}

int bbs04_mem_key_copy(groupsig_key_t *dst, groupsig_key_t *src) {

  bbs04_mem_key_t *bbs04_dst, *bbs04_src;

  if(!dst || dst->scheme != GROUPSIG_BBS04_CODE ||
     !src || src->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_dst = dst->key;
  bbs04_src = src->key;

  /* Copy the elements */
  element_init_same_as(bbs04_dst->x, bbs04_src->x);
  element_set(bbs04_dst->x, bbs04_src->x);
  element_init_same_as(bbs04_dst->A, bbs04_src->A);
  element_set(bbs04_dst->A, bbs04_src->A);

  return IOK;

}

int bbs04_mem_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format) {

  if(!key || key->scheme != GROUPSIG_BBS04_CODE ||
     !_is_supported_format(format)) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_get_size_in_format", __LINE__, LOGERROR);
    return -1;
  }

  exim_t wrap = {key->key, &_exim_h };
  return exim_get_size_in_format(&wrap, format);

}

char* bbs04_mem_key_to_string(groupsig_key_t *key) {

  if(!key || key->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

int bbs04_mem_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst) {

  if(!key || key->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_mem_key_export", __LINE__,
		   "The specified format is not supported.", LOGERROR);
    return IERROR;
  }

  /* Apply the specified conversion */
  exim_t wrap = {key->key, &_exim_h };
  return exim_export(&wrap, format, dst);
  
}

groupsig_key_t* bbs04_mem_key_import(groupsig_key_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_mem_key_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_mem_key_import", __LINE__,
  		   "The specified format is not supported.", LOGERROR);
    return NULL;
  }

  /* Apply the specified conversion */
  exim_t wrap = {NULL, &_exim_h };
  if(exim_import(format, source, &wrap) == IOK){
    return wrap.eximable;
  }

  return NULL;

}

/* mem_key.c ends here */
