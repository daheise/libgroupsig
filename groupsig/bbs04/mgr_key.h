/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key.h
 * @brief: BBS04 Manager keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié may  9 17:11:58 2012 (+0200)
 * @version: 0.1
 * Last-Updated: mié sep 25 20:39:05 2013 (+0200)
 *           By: jesus
 *     Update #: 11
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BBS04_MGR_KEY_H
#define _BBS04_MGR_KEY_H

#include <stdint.h>
#include <pbc/pbc.h>
#include "types.h"
#include "sysenv.h"
#include "bbs04.h"
#include "include/mgr_key.h"

/**
 * @def BBS04_MGR_KEY_BEGIN_MSG
 * @brief Begin string to prepend to headers of files containing BBS04 group keys
 */
#define BBS04_MGR_KEY_BEGIN_MSG "BEGIN BBS04 MANAGERKEY"

/**
 * @def BBS04_MGR_KEY_END_MSG
 * @brief End string to prepend to headers of files containing BBS04 group keys
 */
#define BBS04_MGR_KEY_END_MSG "END BBS04 MANAGERKEY"

/**
 * @struct bbs04_mgr_key_t
 * @brief BBS04 manager key. 
 * 
 * In the BBS04 paper, the private key issuer role (the owner of the gamma field
 * below) is differentiated from that of the group manager (who can revoke members).
 * However, we combine both roles into the group manager, for simplicity.
 */
typedef struct {
  element_t xi1; /**< Exponent for tracing signatures. */
  element_t xi2; /**< Exponent for tracing signatures. */
  element_t gamma; /**< Exponent for generating member keys. */
} bbs04_mgr_key_t;

/** 
 * @fn groupsig_key_t* bbs04_mgr_key_init()
 * @brief Creates a new BBS04 manager key
 *
 * @return The created manager key or NULL if error.
 */
groupsig_key_t* bbs04_mgr_key_init();

/** 
 * @fn int bbs04_mgr_key_free(groupsig_key_t *key)
 * @brief Frees the variables of the given manager key.
 *
 * @param[in,out] key The manager key to initialize.
 * 
 * @return IOK or IERROR
 */
int bbs04_mgr_key_free(groupsig_key_t *key);

/** 
 * @fn int bbs04_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies the source key into the destination key (which must be initialized by 
 * the caller).
 *
 * @param[in,out] dst The destination key.
 * @param[in] src The source key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int bbs04_mgr_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format)
 * @brief Returns the size that the given key would require in order to be
 *  represented using the specified format.
 *
 * @param[in] key The key.
 * @param[in] format The format. The list of supported key formats in the BBS04
 *  scheme are defined in @ref bbs04.h.
 *
 * @return The required number of bytes, or -1 if error.
 */
int bbs04_mgr_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format);

/**
 * @fn int bbs04_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, 
 *                              void *dst)
 * @brief Exports the given manager key to the specified destination, using the
 *  specified format.
 *
 * @param[in] key The key to export.
 * @param[in] format The format to use. The supported formats for BBS04 keys are
 *  specified in @ref bbs04.h.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int bbs04_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);

/** 
 * @fn groupsig_key_t* bbs04_mgr_key_import(groupsig_key_format_t format, 
 *                                          void *source)
 * @brief Imports a BBS04 manager key from the specified source, of the specified
 *  format.

 * @param[in] format The format of <i>source</i>. The supported formats for BBS04
 *  keys are defined in @ref bbs04.h.
 * @param[in] source The source information.
 * 
 * @return A pointer to the imported manager key, or NULL if error.
 */
groupsig_key_t* bbs04_mgr_key_import(groupsig_key_format_t format, void *source);

/** 
 * @fn char* bbs04_mgr_key_to_string(mgr_key_t *key)
 * @brief Creates a printable string of the given manager key.
 *
 * @param[in] key The manager key.
 * 
 * @return The created string or NULL if error.
 */
char* bbs04_mgr_key_to_string(groupsig_key_t *key);

/**
 * @var bbs04_mgr_key_handle
 * @brief Set of functions for BBS04 manager keys management.
 */
static const mgr_key_handle_t bbs04_mgr_key_handle = {
  GROUPSIG_BBS04_CODE, /**< The scheme code. */
  &bbs04_mgr_key_init, /**< Initializes manager keys. */
  &bbs04_mgr_key_free, /**< Frees manager keys. */
  &bbs04_mgr_key_copy, /**< Copies manager keys. */
  &bbs04_mgr_key_export, /**< Exports manager keys. */
  &bbs04_mgr_key_import, /**< Imports manager keys. */
  &bbs04_mgr_key_to_string, /**< Converts manager keys to printable strings. */
  &bbs04_mgr_key_get_size_in_format,
};

#endif

/* mgr_key.h ends here */
