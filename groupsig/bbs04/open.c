/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: open.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:53:13 2012 (+0200)
 * @version: 
 * Last-Updated: lun jul 20 21:26:23 2015 (+0200)
 *           By: jesus
 *     Update #: 24
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "types.h"
#include "sysenv.h"
#include "bigz.h"
#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mgr_key.h"
#include "groupsig/bbs04/signature.h"
#include "groupsig/bbs04/gml.h"
#include "groupsig/bbs04/identity.h"
#include "groupsig/bbs04/trapdoor.h"

int bbs04_open(identity_t *id, groupsig_proof_t *proof, 
	       crl_t *crl, groupsig_signature_t *sig, 
	       groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml) {

  element_t A;
  bbs04_signature_t *bbs04_sig;
  bbs04_grp_key_t *bbs04_grpkey;
  bbs04_mgr_key_t *bbs04_mgrkey;
  bbs04_gml_entry_t *entry;
  bbs04_sysenv_t *bbs04_sysenv;
  uint64_t i;
  uint8_t match;

  if(!id || !sig || sig->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_BBS04_CODE ||
     !gml) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_open", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_sig = sig->sig;
  bbs04_grpkey = grpkey->key;
  bbs04_mgrkey = mgrkey->key;
  bbs04_sysenv = sysenv->data;

  /* In the paper, a signature verification process is included within the open
     procedure to check that the signature is valid. Here, we sepatarate the two
     processes (verify can always be called before opening...) */
  
  /* Recover the signer's A as: A = T3/(T1^xi1 * T2^xi2) */
  element_init_G1(A, bbs04_sysenv->pairing);
  element_pow2_zn(A, bbs04_sig->T1, bbs04_mgrkey->xi1, bbs04_sig->T2, bbs04_mgrkey->xi2);
  element_div(A, bbs04_sig->T3, A);

  /* Look up the recovered A in the GML */
  match = 0;
  for(i=0; i<gml->n; i++) {  

    if(!(entry = gml_get(gml, i))) {
      element_clear(A);
      return IERROR;
    }

    if(!element_cmp(((bbs04_trapdoor_t *)entry->trapdoor->trap)->open, A)) {

      /* Get the identity from the matched entry. */
      if(bbs04_identity_copy(id, entry->id) == IERROR) {
	element_clear(A);
	return IERROR;
      }

      match = 1;
      break;

    }

  }

  element_clear(A);

  /* No match: FAIL */
  if(!match) {
    return IFAIL;
  }

  /* /\* If we have received a CRL, update it with the "revoked" member *\/ */
  /* if(crl) { */

  /*   if(!(crl_entry = bbs04_crl_entry_init())) { */
  /*     return IERROR; */
  /*   } */
    
  /*   if(bbs04_identity_copy(crl_entry->id, gml_entry->id) == IERROR) { */
  /*     bbs04_crl_entry_free(crl_entry); */
  /*     return IERROR; */
  /*   } */
    
  /*   crl_entry->trapdoor = trap; */

  /*   if(bbs04_crl_insert(crl, crl_entry) == IERROR) { */
  /*     bbs04_crl_entry_free(crl_entry); */
  /*     return IERROR; */
  /*   } */

  /* } */

  return IOK;

}

/* open.c ends here */
