/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: proof.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun dic 10 21:24:27 2012 (-0500)
 * @version: 
 * Last-Updated: mié sep 25 20:36:52 2013 (+0200)
 *           By: jesus
 *     Update #: 10
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <gmp.h>
#include <openssl/sha.h>

#include "types.h"
#include "sysenv.h"
#include "sys/mem.h"
#include "wrappers/base64.h"
#include "misc/misc.h"
#include "bbs04.h"
#include "groupsig/bbs04/proof.h"

/* Private functions */
/** 
 * @fn static int _is_supported_format(groupsig_proof_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_proof_format_t format) {

  int i;

  for(i=0; i<BBS04_SUPPORTED_PROOF_FORMATS_N; i++) {
    if(BBS04_SUPPORTED_PROOF_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

static int _proof_get_size_string_null_b64(bbs04_proof_t *proof) {

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "_proof_get_size_string_null_b64", __LINE__, LOGERROR);
    return -1;
  }
  
  return -1;

}

static int _bbs04_proof_export_string_null_b64(bbs04_proof_t *proof, char *dst) {

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_export_string_null_b64", __LINE__, 
	       LOGERROR);
    return IERROR;
  }

  return IERROR;

}

static int _bbs04_proof_export_file_null_b64(bbs04_proof_t *proof, char *dst) {

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_export_file_null_b64", __LINE__, 
	       LOGERROR);
    return IERROR;
  }

  /* if((size = _proof_get_size_string_null_b64(proof)) == -1) { */
  /*   return IERROR; */
  /* } */

  return IERROR;

}

static int _bbs04_proof_export_message_null_b64(bbs04_proof_t *proof, message_t *dst) {

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_export_file_null_b64", __LINE__, 
	       LOGERROR);
    return IERROR;
  }

  /* if((size = _proof_get_size_string_null_b64(proof)) == -1) { */
  /*   return IERROR; */
  /* } */

  return IERROR;

}



static groupsig_proof_t* _bbs04_proof_import_string_null_b64(char *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_import_string_null_b64", 
	       __LINE__, LOGERROR);
    return NULL;
  }
    
  return NULL;

}

static groupsig_proof_t* _bbs04_proof_import_file_null_b64(char *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_import_file_null_b64", 
	       __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;

}

static groupsig_proof_t* _bbs04_proof_import_message_null_b64(message_t *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_bbs04_proof_import_message_null_b64", 
	       __LINE__, LOGERROR);
    return NULL;
  }

  return _bbs04_proof_import_string_null_b64((char *) source->bytes);

}

/* Public functions */
groupsig_proof_t* bbs04_proof_init() {

  return NULL;

}

int bbs04_proof_free(groupsig_proof_t *proof) {

  return IERROR;

}

int bbs04_proof_init_set_c(bbs04_proof_t *proof, bigz_t c) {

  if(!proof || !c) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_init_set_c", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(proof->c = bigz_init_set(c))) {
    return IERROR;
  }

  return IOK;

}

int bbs04_proof_init_set_s(bbs04_proof_t *proof, bigz_t s) {

  if(!proof || !s) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_init_set_s", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(proof->s = bigz_init_set(s))) {
    return IERROR;
  }

  return IOK;

}

char* bbs04_proof_to_string(groupsig_proof_t *proof) {

  if(!proof || proof->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_to_string", __LINE__, LOGERROR);
    return NULL;
  }
  
  return NULL;

}

int bbs04_proof_export(groupsig_proof_t *proof, groupsig_proof_format_t format, void *dst) { 

  /* bbs04_proof_t *bbs04_proof; */

  if(!proof || proof->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* bbs04_proof = (bbs04_proof_t *) proof->proof; */

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_export", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_PROOF_FORMAT_STRING_NULL_B64: */
  /*   return _bbs04_proof_export_string_null_b64(bbs04_proof, dst); */
  /* case GROUPSIG_PROOF_FORMAT_FILE_NULL_B64: */
  /*   return _bbs04_proof_export_file_null_b64(bbs04_proof, dst); */
  /* case GROUPSIG_PROOF_FORMAT_MESSAGE_NULL_B64: */
  /*   return _bbs04_proof_export_message_null_b64(bbs04_proof, dst); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_export", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return IERROR; */
  /* } */

  return IERROR;

}

groupsig_proof_t* bbs04_proof_import(groupsig_proof_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_import", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return NULL; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_PROOF_FORMAT_STRING_NULL_B64: */
  /*   return _bbs04_proof_import_string_null_b64((char *) source); */
  /* case GROUPSIG_PROOF_FORMAT_FILE_NULL_B64: */
  /*   return _bbs04_proof_import_file_null_b64((char *) source); */
  /* case GROUPSIG_PROOF_FORMAT_MESSAGE_NULL_B64: */
  /*   return _bbs04_proof_import_message_null_b64((message_t *) source); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_import", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return NULL; */
  /* } */
  
  return NULL;  
  
}

int bbs04_proof_get_size_in_format(groupsig_proof_t *proof, groupsig_proof_format_t format) {

  if(!proof || proof->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_proof_get_size_in_format", __LINE__, LOGERROR);
    return -1;
  }

  /* /\* See if the current scheme supports the given format *\/ */
  /* if(!_is_supported_format(format)) { */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_get_size_in_format", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return -1; */
  /* } */

  /* switch(format) { */
  /* case GROUPSIG_PROOF_FORMAT_FILE_NULL_B64: */
  /*   return _proof_get_size_string_null_b64(proof->proof); */
  /* case GROUPSIG_PROOF_FORMAT_STRING_NULL_B64: */
  /*   return _proof_get_size_string_null_b64(proof->proof); */
  /* case GROUPSIG_PROOF_FORMAT_MESSAGE_NULL_B64: */
  /*   return _proof_get_size_string_null_b64(proof->proof); */
  /* default: */
  /*   LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_proof_get_size_in_format", __LINE__, */
  /* 		   "The specified format is not supported.", LOGERROR); */
  /*   return -1; */
  /* } */

  return -1;

}

/* proof.c ends here */
