/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: prove_equality
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 15:19:39 2012 (+0200)
 * @version: 
 * Last-Updated: vie sep 27 18:39:50 2013 (+0200)
 *           By: jesus
 *     Update #: 6
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <openssl/sha.h> /** @todo This should not be! */

#include "bbs04.h"
#include "groupsig/bbs04/proof.h"
#include "groupsig/bbs04/mem_key.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/signature.h"
#include "bigz.h"
#include "sys/mem.h"

/* Private functions */


/* Public functions */

int bbs04_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			 groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs) {
  return IERROR;
 /*  bbs04_grp_key_t *gkey; */
 /*  bbs04_mem_key_t *mkey; */
 /*  groupsig_signature_t *sig;  */
 /*  bbs04_signature_t *bbs04_sig; */
 /*  bbs04_proof_t *bbs04_proof; */
 /*  byte_t aux_sc[SHA_DIGEST_LENGTH+1]; */
 /*  SHA_CTX aux_sha; */
 /*  char *aux_t7r, *aux_t7, *aux_n; */
 /*  bigz_t r, t7r; */
 /*  int rc; */
 /*  uint8_t i; */

 /*  if(!proof || proof->scheme != GROUPSIG_BBS04_CODE || */
 /*     !memkey || memkey->scheme != GROUPSIG_BBS04_CODE || */
 /*     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE || */
 /*     !sigs || !n_sigs) { */
 /*    LOG_EINVAL(&logger, __FILE__, "bbs04_prove_equality", __LINE__, LOGERROR); */
 /*    return IERROR; */
 /*  } */
   
 /*  r = NULL; t7r = NULL; */
 /*  rc = IOK; */
   
 /*  gkey = (bbs04_grp_key_t *) grpkey->key; */
 /*  mkey = (bbs04_mem_key_t *) memkey->key; */
 /*  bbs04_proof = (bbs04_proof_t *) proof->proof; */
   
 /*  if(!(r = bigz_init())) { */
 /*    LOG_ERRORCODE(&logger, __FILE__, "bbs04_prove_equality", __LINE__, errno, LOGERROR); */
 /*    return IERROR; */
 /*  } */

 /*  /\* Initialize the hashing environment *\/ */
 /*  /\** @todo Use EVP_* instead of SHA1_* *\/ */
 /*  if(!SHA1_Init(&aux_sha)) { */
 /*    LOG_ERRORCODE_MSG(&logger, __FILE__, "bbs04_prove_equality", __LINE__, EDQUOT, */
 /* 		      "SHA1_Init", LOGERROR); */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*  } */
   
 /*  /\* We get r in the same sphere as x' *\/ */
 /*  if(sphere_get_random(gkey->inner_lambda, r) == IERROR)  */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */
   
 /*  /\* To create the proof, we make use of the T6 and T7 objects (A[5] and */
 /*     A[6], if I remember correctly). The knowledge of the discrete logarithm */
 /*     of T6 to the base T7 is used in normal signature claims. In the same way, */
 /*     given two signatures (allegedly) issued by the same member, with corresponding */
 /*     objects T6, T7, T6' and T7', we prove here that the discrete logarithm */
 /*     of T6 to the base T7 is the same to that of T6' to the base T7'. *\/ */

 /*  /\* In a bbs04_signature_t, T6 is stored in A[12] and T7 in A[4] *\/ */
  
 /*  /\* (1) Raise the T7 field of each received signature to r, and put it into */
 /*     the hash. *\/ */
 /*  if(!(t7r = bigz_init()))  */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */
  
 /*  for(i=0; i<n_sigs; i++) { */

 /*    /\* Get the next signature in the line... *\/ */
 /*    sig = (groupsig_signature_t *) sigs[i]; */
 /*    if(sig->scheme != GROUPSIG_BBS04_CODE) { */
 /*      LOG_EINVAL(&logger, __FILE__, "bbs04_prove_equality", __LINE__, LOGERROR); */
 /*      GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*    } */

 /*    bbs04_sig = (bbs04_signature_t *) sig->sig; */
     
 /*    if(bigz_powm(t7r, bbs04_sig->A[4], r, gkey->n) == IERROR) */
 /*      GOTOENDRC(IERROR, bbs04_prove_equality); */
         
 /*    /\* Put the i-th element of the array *\/ */
 /*    if(!(aux_t7r = bigz_get_str(10, t7r))) GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*    if(!SHA1_Update(&aux_sha, aux_t7r, strlen(aux_t7r))) { */
 /*      LOG_ERRORCODE_MSG(&logger, __FILE__, "bbs04_prove_equality", __LINE__, EDQUOT, */
 /* 			"SHA1_Update", LOGERROR); */
 /*      GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*    }      */
 /*    free(aux_t7r); aux_t7r = NULL; */

 /*    /\* Put the also the base (the T7's) into the hash *\/ */
 /*    if(!(aux_t7 = bigz_get_str(10, bbs04_sig->A[4]))) GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*    if(!SHA1_Update(&aux_sha, aux_t7, strlen(aux_t7))) { */
 /*      LOG_ERRORCODE_MSG(&logger, __FILE__, "bbs04_prove_equality", __LINE__, EDQUOT, */
 /* 			"SHA1_Update", LOGERROR); */
 /*      GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*    } */
 /*    free(aux_t7); aux_t7 = NULL; */
    
 /*  } */

 /*  /\* And finally, put the modulus into the hash *\/ */
 /*  if(!(aux_n = bigz_get_str(10, gkey->n))) GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*  if(!SHA1_Update(&aux_sha, aux_n, strlen(aux_n))) { */
 /*    LOG_ERRORCODE_MSG(&logger, __FILE__, "bbs04_prove_equality", __LINE__, EDQUOT, */
 /* 		      "SHA1_Update", LOGERROR); */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*  } */
 /*  free(aux_n); aux_n = NULL;   */
    
 /*  /\* (2) Calculate c = hash(t7r[0] || t7[0] || ... || t7r[n-1] || t7[n-1] || mod ) *\/ */
 /*  memset(aux_sc, 0, SHA_DIGEST_LENGTH+1); */
 /*  if(!SHA1_Final(aux_sc, &aux_sha)) { */
 /*    LOG_ERRORCODE_MSG(&logger, __FILE__, "proof_equality", __LINE__, EDQUOT, */
 /* 		      "SHA1_Final", LOGERROR); */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */
 /*  } */

 /*  /\* Now, we have to get c as a bigz_t *\/ */
 /*  if(!(bbs04_proof->c = bigz_import(aux_sc,SHA_DIGEST_LENGTH)))  */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */

 /*  /\* (3) To end, get s = r - c*x *\/ */
 /*  if(!(bbs04_proof->s = bigz_init())) */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */

 /*  if(bigz_mul(bbs04_proof->s, bbs04_proof->c, mkey->xx) == IERROR) */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */

 /*  if(bigz_sub(bbs04_proof->s, r, bbs04_proof->s) == IERROR) */
 /*    GOTOENDRC(IERROR, bbs04_prove_equality); */

 /*  /\* Free resources and exit *\/ */
 /* bbs04_prove_equality_end: */
   
 /*  if(r) bigz_free(r); */
 /*  if(t7r) bigz_free(t7r); */
   
 /*  return rc; */
   
}

/* prove_equality.c ends here */
