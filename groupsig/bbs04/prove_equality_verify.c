/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: prove_equality_verify.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié ene 16 16:12:29 2013 (+0100)
 * @version: 
 * Last-Updated: mié sep 25 20:46:13 2013 (+0200)
 *           By: jesus
 *     Update #: 4
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <openssl/sha.h> /** @todo This should not be! */

#include "bbs04.h"
#include "groupsig/bbs04/proof.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/signature.h"
#include "bigz.h"
#include "sys/mem.h"

/* Private functions */


/* Public functions */

int bbs04_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, 
				groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
				uint16_t n_sigs) {
  
  if(!ok || !proof || proof->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE ||
     !sigs || !n_sigs) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_prove_equality_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  return IERROR;
   
}

/* prove_equality_verify.c ends here */
