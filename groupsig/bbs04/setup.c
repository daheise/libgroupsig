/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: setup.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié may  9 13:30:04 2012 (+0200)
 * @version: 
 * Last-Updated: mar ago 26 20:22:39 2014 (+0200)
 *           By: jesus
 *     Update #: 78
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <pbc/pbc.h>

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mgr_key.h"
#include "groupsig/bbs04/gml.h"
#include "math/nt.h"
#include "sys/mem.h"
#include "wrappers/pbc_ext.h"

/* @todo Global variable for quick modification. This should be passed 
   as parameter to _param_generated */
size_t bitlimit;

/** 
 * @fn static int _setup_parameters_check(int d, unsigned int bitlimit)
 * @brief Checks the input parameters of the setup function.
 *
 * @param[in] bitlimit The produced groups will be of order at most 2^bitlimit-1.
 * 
 * @return IOK if the parameters are valid, IERROR otherwise
 */
static int _setup_parameters_check(unsigned int bitlimit) {

  if(!bitlimit) {
    LOG_EINVAL(&logger, __FILE__, "_setup_parameters_check", __LINE__, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _param_generated(pbc_cm_t cm, void *data) {

  bbs04_genparam_t *genparam;

  /* Initialize params */
   genparam = (bbs04_genparam_t *) data;
   pbc_param_init_d_gen(genparam->param, cm);
   if(!(genparam->r = bigz_init_set(&cm->r)) ||
      bigz_sizeinbase(genparam->r, 2) < bitlimit ||
      bigz_sizeinbase(genparam->r, 2) > bitlimit + bitlimit/10) {
     bigz_free(genparam->r);
     pbc_param_clear(genparam->param);
     return 0;
   }

   return 1;
}

/**
 * @fn static int _f_param_generate(bbs04_genparam_t *genparam, int bitlimit)
 * @brief Find a Type F curve with at least 'bitlimit' bits in r.
 *
 * @param[in] bitlimit The produced groups will be of order at least 2^bitlimit-1.
 *
 * @return IOK if the parameters are valid, IERROR otherwise
 */
static int _f_param_generate(bbs04_genparam_t *genparam, int bitlimit) {

  pairing_t pairing;
  int attempt_bits = bitlimit;
  /* Initialize params */

  pbc_param_init_f_gen(genparam->param, attempt_bits);
  pairing_init_pbc_param(pairing, genparam->param);

  while(bigz_sizeinbase((bigz_t)pairing->r, 2) < bitlimit){
    pbc_param_clear(genparam->param);
    pairing_clear(pairing);
    attempt_bits++;
    pbc_param_init_f_gen(genparam->param, attempt_bits);
    pairing_init_pbc_param(pairing, genparam->param);
  }

  genparam->r = bigz_init_set((bigz_t)pairing->r);
  pairing_clear(pairing);
  return IOK;
}

/* static int _param_generated(pbc_cm_t cm, void *data) {                                                                               */
  
/*   bbs04_genparam_t *genparam; */
  
/*   /\* Initialize params *\/ */
/*    genparam = (bbs04_genparam_t *) data; */
/*    pbc_param_init_d_gen(genparam->param, cm); */
/*   if(!(genparam->q = bigz_init_set(&cm->q))) */
/*     return 0; */
  
/*   return 1; */
                                                                                                         
/* }                                                                                                                    */

/* @todo k is the embedding degree? (which for MNT curves is 6) */
/* @todo q is the prime defining the field? */
static int _trace_map(element_t tr, element_t e, bigz_t q, uint64_t k) {

  /* element_t _tr, aux; */
  bigz_t _tr, exp, aux;
  uint64_t i;

  /* From Ben Lynn's thesis: "on the implementation of pairing-based 
     cryptosystems", p. 28: 

     The trace map is defined by tr(P) = P + \Phi(P) + ... + \Phi^k(P),
     
     where Phi(P) is the Frobenius map P^q, where q is the characteristic of the field,
     and \Phi^k(P) = P^(q^k).

  */

  if(!(_tr = bigz_init())) {
    return IERROR;
  }

  if(!(exp = bigz_init())) {
    bigz_free(_tr);
    return IERROR;
  }

  if(!(aux = bigz_init())) {
    bigz_free(_tr); bigz_free(exp);
    return IERROR;
  }

  element_to_mpz(*_tr, e);
  element_to_mpz(*aux, e);

  for(i=1; i<k; i++) {

    /* exp = q^i */
    if(bigz_pow_ui(exp, q, i) == IERROR) {
      bigz_free(exp); bigz_free(_tr);
      bigz_free(aux);
      return IERROR;
    }

    /* aux = e^(q^i) */
    if(bigz_powm(aux, aux, exp, q) == IERROR) {
      bigz_free(exp); bigz_free(_tr);
      bigz_free(aux);
      return IERROR;
    }

    /* _tr = sum_0^k (\Phi^i(P)) */
    if(bigz_add(_tr, _tr, aux) == IERROR) {
      bigz_free(exp); bigz_free(_tr);
      bigz_free(aux);
      return IERROR;
    }

  }

  element_set_mpz(tr, *_tr);

  bigz_free(exp); bigz_free(_tr);
  bigz_free(aux);

  /* element_init_same_as(_tr, e); */
  /* element_set(_tr, e); /\* _tr = e *\/ */
  /* element_init_same_as(aux, e); */

  /* for(i=1; i<k; i++) { */

  /*   /\* exp = q^i *\/ */
  /*   if(bigz_pow_ui(exp, q, i) == IERROR) { */
  /*     bigz_free(exp); element_clear(_tr); */
  /*     element_clear(aux); */
  /*     return IERROR; */
  /*   } */

  /*   /\* aux = e^(q^i) *\/ */
  /*   element_pow_mpz(aux, aux, exp);  */

  /*   /\* _tr = sum_0^k (\Phi^i(P)) *\/ */
  /*   element_add(_tr, _tr, aux);  */

  /* } */

  /* bigz_free(exp); */
  /* element_clear(aux); */
  /* element_set(tr, _tr);  */

  return IOK;

}

groupsig_config_t* bbs04_config_init() {
  
  groupsig_config_t *cfg;

  if(!(cfg = (groupsig_config_t *) mem_malloc(sizeof(groupsig_config_t)))) {
    return NULL;
  }

  cfg->scheme = GROUPSIG_BBS04_CODE;
  if(!(cfg->config = (bbs04_config_t *) mem_malloc(sizeof(bbs04_config_t)))) {
    mem_free(cfg); cfg = NULL;
    return NULL;
  }

  BBS04_CONFIG_SET_DEFAULTS((bbs04_config_t *) cfg->config);

  return cfg;

}

int bbs04_config_free(groupsig_config_t *cfg) {

  if(!cfg) {
    return IOK;
  }

  mem_free(cfg->config); cfg->config = NULL;
  mem_free(cfg); cfg = NULL;
  return IOK;

}

int bbs04_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, groupsig_config_t *config) {

  bbs04_genparam_t genparam;
  bbs04_config_t *bbs04_config;
  bbs04_grp_key_t *gkey;
  bbs04_mgr_key_t *mkey;
  bbs04_sysenv_t *bbs04_sysenv;
  element_t inv;
  int status = 0;

  if(!grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_BBS04_CODE ||
     !config || config->scheme != GROUPSIG_BBS04_CODE ||
     !gml) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_setup", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get imput parameters from config->config */ 
  bbs04_config = (bbs04_config_t *) config->config;

  /* if(_setup_parameters_check(bbs04_config->bitlimit) == IERROR) { */
  /*   return IERROR; */
  /* } */

  gkey = grpkey->key;
  mkey = mgrkey->key;
  
  /* Look for pairings suitable for cryptography, if found, store them in param */
  /* Code based on pbc/gen/listmnt program */
  bitlimit = bbs04_config->bitlimit;
  status = _f_param_generate(&genparam, bitlimit);
  if(status != IOK){
    return IERROR;
  }

  /* First, copy the param and pairing to the BBS04 internal environment */
  if(!(bbs04_sysenv = (bbs04_sysenv_t *) mem_malloc(sizeof(bbs04_sysenv_t)))) {
    return IERROR;
  }

  if(pbcext_param_copy(bbs04_sysenv->param, genparam.param) == IERROR) {
    mem_free(bbs04_sysenv); bbs04_sysenv = NULL;
    return IERROR;
  }

  if(bbs04_sysenv_update(bbs04_sysenv) == IERROR) {
    pbc_param_clear(bbs04_sysenv->param);
    mem_free(bbs04_sysenv); bbs04_sysenv = NULL;
    return IERROR;
  }

  /* Initialize the pairing structure with the obtained params */
  pairing_init_pbc_param(bbs04_sysenv->pairing, bbs04_sysenv->param);

  /* Select random generator g2 in G2. Since G2 is a cyclic multiplicative group 
     of prime order, any element is a generator, so choose some random element. */
  element_init_G2(gkey->g2, bbs04_sysenv->pairing);
  element_random(gkey->g2);
  
  /* g1 is the isomorphism of g2. As suggested by Boneh et al, we use the trace
     map on the curve as isomorphism. @todo But they say that isomorphisms are
     only necessary for the correctness proofs! */
  element_init_G1(gkey->g1, bbs04_sysenv->pairing);
  /* _trace_map(gkey->g1, gkey->g2, genparam.q, 6); */
  // gkey->pairing->phi(gkey->g1, gkey->g2, gkey->pairing);
  element_random(gkey->g1);

  /* h random in G1 \ 1 */
  element_init_G1(gkey->h, bbs04_sysenv->pairing);
  do {
    element_random(gkey->h);
  } while(element_is1(gkey->h));
  
  /* xi1 and xi2 random in Z^*_p */
  element_init_Zr(mkey->xi1, bbs04_sysenv->pairing);
  element_random(mkey->xi1);
  element_init_Zr(mkey->xi2, bbs04_sysenv->pairing);
  element_random(mkey->xi2);

  /* u = h^(1/xi1) */
  element_init_same_as(inv, mkey->xi1);
  element_init_G1(gkey->u, bbs04_sysenv->pairing);
  element_invert(inv, mkey->xi1);
  element_pow_zn(gkey->u, gkey->h, inv);

  /* v = h^(1/xi2) */
  element_init_G1(gkey->v, bbs04_sysenv->pairing);
  element_invert(inv, mkey->xi2);
  element_pow_zn(gkey->v, gkey->h, inv);

  /* gamma random in Z^*_p */
  element_init_Zr(mkey->gamma, bbs04_sysenv->pairing);
  element_random(mkey->gamma);

  /* w = g_2^gamma */
  element_init_G2(gkey->w, bbs04_sysenv->pairing);
  element_pow_zn(gkey->w, gkey->g2, mkey->gamma);

  /* Optimizations */

  /* hw = e(h,w) */
  element_init_GT(gkey->hw, bbs04_sysenv->pairing);
  element_pairing(gkey->hw, gkey->h, gkey->w);

  /* hg2 = e(h, g2) */
  element_init_GT(gkey->hg2, bbs04_sysenv->pairing);
  element_pairing(gkey->hg2, gkey->h, gkey->g2);

  /* g1g2 = e(g2, g2) */
  element_init_GT(gkey->g1g2, bbs04_sysenv->pairing);
  element_pairing(gkey->g1g2, gkey->g1, gkey->g2);

  /* Clear data */
  element_clear(inv);
  bigz_free(genparam.r);
  pbc_param_clear(genparam.param);

  return IOK;

}

/* setup.c ends here */
