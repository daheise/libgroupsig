/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: sign.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 15:19:39 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 22:23:50 2013 (+0200)
 *           By: jesus
 *     Update #: 38
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <limits.h>

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mem_key.h"
#include "groupsig/bbs04/signature.h"
#include "bigz.h"
#include "wrappers/hash.h"
#include "wrappers/pbc_ext.h"
#include "sys/mem.h"

/* Private functions */

int bbs04_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
	       groupsig_key_t *grpkey, unsigned int seed) {

  /* Here we won't follow the typical C programming conventions for naming variables.
     Instead, we will name the variables as in the BBS04 paper (with the exception 
     of doubling a letter when a ' is used, e.g. k' ==> kk). Auxiliar variables 
     that are not specified in the paper but helpful or required for its 
     implementation will be named aux_<name>. */

  element_t R1, R2, R3, R4, R5, alpha, beta, delta1, delta2;
  element_t ralpha, rbeta, rx, rdelta1, rdelta2;
  element_t aux_e1, aux_e2, aux_e3, aux_Zr, aux_G1;
  element_t aux_o1;
  hash_t *aux_c;
  byte_t *aux_bytes;
  bbs04_signature_t *bbs04_sig;
  bbs04_grp_key_t *bbs04_grpkey;
  bbs04_mem_key_t *bbs04_memkey;
  bbs04_sysenv_t *bbs04_sysenv;
  int rc, aux_n;
  
  if(!sig || !msg || 
     !memkey || memkey->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_sign", __LINE__, LOGERROR);
    return IERROR;
  }

  rc = IOK;

  bbs04_sig = sig->sig;
  bbs04_grpkey = grpkey->key;
  bbs04_memkey = memkey->key;
  bbs04_sysenv = sysenv->data;

  /* alpha,beta \in_R Zp */
  element_init_Zr(alpha, bbs04_sysenv->pairing);
  element_random(alpha);
  element_init_Zr(beta, bbs04_sysenv->pairing);
  element_random(beta);

  /* Compute T1,T2,T3 */
  element_init_G1(bbs04_sig->T1, bbs04_sysenv->pairing);
  element_init_G1(bbs04_sig->T2, bbs04_sysenv->pairing);
  element_init_G1(bbs04_sig->T3, bbs04_sysenv->pairing);

  /* T1 = u^alpha */
  element_pow_zn(bbs04_sig->T1, bbs04_grpkey->u, alpha);

  /* T2 = v^beta */
  element_pow_zn(bbs04_sig->T2, bbs04_grpkey->v, beta);

  /* T3 = A*h^(alpha+beta) */
  element_pow2_zn(bbs04_sig->T3, bbs04_grpkey->h, alpha, bbs04_grpkey->h, beta);
  element_mul(bbs04_sig->T3, bbs04_memkey->A, bbs04_sig->T3);

  /* delta1 = x*alpha */
  element_init_Zr(delta1, bbs04_sysenv->pairing);
  element_mul(delta1, bbs04_memkey->x, alpha);

  /* delta2 = x*beta */
  element_init_Zr(delta2, bbs04_sysenv->pairing);
  element_mul(delta2, bbs04_memkey->x, beta);
  
  /* ralpha, rbeta, rx, rdelta1, rdelta2 \in_R Zp */
  element_init_Zr(ralpha, bbs04_sysenv->pairing);
  element_random(ralpha);
  element_init_Zr(rbeta, bbs04_sysenv->pairing);
  element_random(rbeta);
  element_init_Zr(rx, bbs04_sysenv->pairing);
  element_random(rx);
  element_init_Zr(rdelta1, bbs04_sysenv->pairing);
  element_random(rdelta1);
  element_init_Zr(rdelta2, bbs04_sysenv->pairing);
  element_random(rdelta2);

  /* Compute R1, R2, R3, R4, R5 */
  /* Optimized o1 = e(T3, g2) = e(A, g2) * e(h, g2) ^ alpha + beta */
  element_init_GT(aux_o1, bbs04_sysenv->pairing);
  element_pow2_zn(aux_o1, bbs04_grpkey->hg2, alpha, bbs04_grpkey->hg2, beta);
  element_mul(aux_o1, aux_o1, bbs04_memkey->Ag2);

  /* R1 = u^ralpha */
  element_init_G1(R1, bbs04_sysenv->pairing);
  element_pow_zn(R1, bbs04_grpkey->u, ralpha);

  /* R2 = v^rbeta */
  element_init_G1(R2, bbs04_sysenv->pairing);
  element_pow_zn(R2, bbs04_grpkey->v, rbeta);

  /* R3 = e(T3,g2)^rx * e(h,w)^(-ralpha-rbeta) * e(h,g2)^(-rdelta1-rdelta2) */
  element_init_GT(aux_e1, bbs04_sysenv->pairing);
  element_init_GT(aux_e2, bbs04_sysenv->pairing);
  element_init_GT(aux_e3, bbs04_sysenv->pairing);
  
  /* e1 = e(T3,g2)^rx */
  //element_pairing(aux_e1, bbs04_sig->T3, bbs04_grpkey->g2);
  element_pow_zn(aux_e1, aux_o1, rx);
  
  /* e2 = e(h,w)^(-ralpha-rbeta) */
  //element_pairing(aux_e2, bbs04_grpkey->h, bbs04_grpkey->w);
  element_init_Zr(aux_Zr, bbs04_sysenv->pairing);
  element_neg(aux_Zr, ralpha);
  element_sub(aux_Zr, aux_Zr, rbeta);
  element_pow_zn(aux_e2, bbs04_grpkey->hw, aux_Zr);

  /* e3 = e(h,g2)^(-rdelta1-rdelta2) */
  //element_pairing(aux_e3, bbs04_grpkey->h, bbs04_grpkey->g2);
  element_neg(aux_Zr, rdelta1);
  element_sub(aux_Zr, aux_Zr, rdelta2);
  element_pow_zn(aux_e3, bbs04_grpkey->hg2, aux_Zr);

  /* R3 = e1 * e2 * e3 */
  element_init_GT(R3, bbs04_sysenv->pairing);
  element_mul(R3, aux_e1, aux_e2);
  element_mul(R3, R3, aux_e3);

  /* R4 = T1^rx * u^-rdelta1 */
  element_init_G1(R4, bbs04_sysenv->pairing);
  element_pow_zn(R4, bbs04_sig->T1, rx);
  element_init_G1(aux_G1, bbs04_sysenv->pairing);
  element_neg(aux_Zr, rdelta1);
  element_pow_zn(aux_G1, bbs04_grpkey->u, aux_Zr);
  element_mul(R4, R4, aux_G1);

  /* R5 = T2^rx * v^-rdelta2 */
  element_init_G1(R5, bbs04_sysenv->pairing);
  element_pow_zn(R5, bbs04_sig->T2, rx);
  element_neg(aux_Zr, rdelta2);
  element_pow_zn(aux_G1, bbs04_grpkey->v, aux_Zr);
  element_mul(R5, R5, aux_G1);

  /* c = hash(M,T1,T2,T3,R1,R2,R3,R4,R5) \in Zp */
  if(!(aux_c = hash_init(HASH_SHA1))) GOTOENDRC(IERROR, bbs04_sign);

  /* Push the message */
  if(hash_update(aux_c, msg->bytes, msg->length) == IERROR) 
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push T1 */
  aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T1) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push T2 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T2) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push T3 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T3) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push R1 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R1) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push R2 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R2) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push R3 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R3) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push R4 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R4) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Push R5 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R5) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_sign);

  /* Finish the hash */
  if(hash_finalize(aux_c) == IERROR) GOTOENDRC(IERROR, bbs04_sign);

  /* Get c as the element associated to the obtained hash value */
  element_init_Zr(bbs04_sig->c, bbs04_sysenv->pairing);
  element_from_hash(bbs04_sig->c, aux_c->hash, aux_c->length);

  /* Calculate salpha, sbeta, sx, sdelta1, sdelta2 */

  /* salpha = ralpha + c*alpha */
  element_init_Zr(bbs04_sig->salpha, bbs04_sysenv->pairing);
  element_mul(bbs04_sig->salpha, bbs04_sig->c, alpha);
  element_add(bbs04_sig->salpha, bbs04_sig->salpha, ralpha);

  /* sbeta = rbeta + c*beta */
  element_init_Zr(bbs04_sig->sbeta, bbs04_sysenv->pairing);
  element_mul(bbs04_sig->sbeta, bbs04_sig->c, beta);
  element_add(bbs04_sig->sbeta, bbs04_sig->sbeta, rbeta);

  /* sx = rx + c*x */
  element_init_Zr(bbs04_sig->sx, bbs04_sysenv->pairing);
  element_mul(bbs04_sig->sx, bbs04_sig->c, bbs04_memkey->x);
  element_add(bbs04_sig->sx, bbs04_sig->sx, rx);

  /* sdelta1 = rdelta1 + c*delta1 */
  element_init_Zr(bbs04_sig->sdelta1, bbs04_sysenv->pairing);
  element_mul(bbs04_sig->sdelta1, bbs04_sig->c, delta1);
  element_add(bbs04_sig->sdelta1, bbs04_sig->sdelta1, rdelta1);

  /* sdelta2 = rdelta2 + c*delta2 */
  element_init_Zr(bbs04_sig->sdelta2, bbs04_sysenv->pairing);
  element_mul(bbs04_sig->sdelta2, bbs04_sig->c, delta2);
  element_add(bbs04_sig->sdelta2, bbs04_sig->sdelta2, rdelta2);

 bbs04_sign_end:
  
  element_clear(R1); element_clear(R2);
  element_clear(R3); element_clear(R4);
  element_clear(R5); element_clear(alpha);
  element_clear(beta); element_clear(delta1);
  element_clear(delta2); element_clear(ralpha);
  element_clear(rbeta); element_clear(rx);
  element_clear(rdelta1); element_clear(rdelta2);
  element_clear(aux_e1); element_clear(aux_e2);
  element_clear(aux_e3); element_clear(aux_Zr);
  element_clear(aux_G1);

  if(aux_bytes) { mem_free(aux_bytes); aux_bytes = NULL; }
  if(aux_c) { hash_free(aux_c); aux_c = NULL; }

  return rc;
  
}

/* sign.c ends here */
