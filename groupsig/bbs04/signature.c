/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie may 18 11:48:37 2012 (+0200)
 * @version: 
 * Last-Updated: jue sep 26 23:11:06 2013 (+0200)
 *           By: jesus
 *     Update #: 48
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <gmp.h>
#include <openssl/sha.h>

#include "types.h"
#include "sysenv.h"
#include "sys/mem.h"
#include "wrappers/base64.h"
#include "wrappers/pbc_ext.h"
#include "misc/misc.h"
#include "exim.h"
#include "bbs04.h"
#include "groupsig/bbs04/signature.h"

/* Private functions */
/** 
 * @fn static int _is_supported_format(groupsig_signature_format_t format)
 * @brief Returns 1 if the specified format is supported by this scheme. 0 if not.
 *
 * @param[in] format The format to be "tested"
 * 
 * @return 1 if the specified format is supported, 0 if not.
 */
static int _is_supported_format(groupsig_signature_format_t format) {

  int i;

  for(i=0; i<BBS04_SUPPORTED_SIG_FORMATS_N; i++) {
    if(BBS04_SUPPORTED_SIG_FORMATS[i] == format) {
      return 1;
    }
  }

  return 0;

}

/**
 * @fn static int _get_size_bytearray_null(exim_t *obj)
 * @brief Returns the size in bytes of the exim wrapped object. The size will be
 * equal to the size of bytearray output by _export_fd() or created by
 * _import_fd().
 *
 * @param[in] obj The object to be sized.
 *
 * @return The size in bytes of the object contained in obj.
 */
static int _get_size_bytearray_null(exim_t* obj){
  int size;
  bbs04_signature_t *sig;
  if(!obj || !obj->eximable){
    return -1;
  }
  sig = obj->eximable;
  size = sizeof(bbs04_signature_t) + element_length_in_bytes(sig->T1)+element_length_in_bytes(sig->T2)+
      element_length_in_bytes(sig->T3)+element_length_in_bytes(sig->c)+
      element_length_in_bytes(sig->salpha)+element_length_in_bytes(sig->sbeta)+
      element_length_in_bytes(sig->sx)+element_length_in_bytes(sig->sdelta1)+
      element_length_in_bytes(sig->sdelta2);
  return size;
}

/**
 * @fn static int _export_fd(exim_t* obj, FILE *fd)
 * @brief Writes a bytearray representation of the given exim object to a
 * file descriptor with format:
 *
 *  | BBS04_CODE | size_T1 | T1 | size_T2 | T2 | size_T3 | T3 | size_c | c |
 *                 size_salpha | salpha | size_sbeta | sbeta | size_sx | sx |
 *                 size_sdelta1 | sdelta1 | size_sdelta2 | sdelta2 |
 *
 * @param[in] obj The key to export.
 * @param[in, out] fd An open filestream to write to.
 *
 * @return IOK or IERROR
 */
static int _export_fd(exim_t *obj, FILE *fd) {
  uint8_t code;
  bbs04_signature_t *sig;

  if(!obj || !obj->eximable || !fd) {
    LOG_EINVAL(&logger, __FILE__, "_export_fd", __LINE__, LOGERROR);
    return IERROR;
  }
  sig = obj->eximable;

  /* Dump GROUPSIG_BBS04_CODE */
  code = GROUPSIG_BBS04_CODE;
  if(fwrite(&code, sizeof(uint8_t), 1, fd) != 1) {
    return IERROR;
  }

  /* Dump T1 */
  if(pbcext_dump_element_fd(sig->T1, fd) == IERROR) {
    return IERROR;
  }

  /* Dump T2 */
  if(pbcext_dump_element_fd(sig->T2, fd) == IERROR) {
    return IERROR;
  }

  /* Dump T3 */
  if(pbcext_dump_element_fd(sig->T3, fd) == IERROR) {
    return IERROR;
  }

  /* Dump c */
  if(pbcext_dump_element_fd(sig->c, fd) == IERROR) {
    return IERROR;
  }

  /* Dump salpha */
  if(pbcext_dump_element_fd(sig->salpha, fd) == IERROR) {
    return IERROR;
  }

  /* Dump sbeta */
  if(pbcext_dump_element_fd(sig->sbeta, fd) == IERROR) {
    return IERROR;
  }

  /* Dump sx */
  if(pbcext_dump_element_fd(sig->sx, fd) == IERROR) {
    return IERROR;
  }

  /* Dump sdelta1 */
  if(pbcext_dump_element_fd(sig->sdelta1, fd) == IERROR) {
    return IERROR;
  }

  /* Dump sdelta2 */
  if(pbcext_dump_element_fd(sig->sdelta2, fd) == IERROR) {
    return IERROR;
  }

  return IOK;

}

/**
 * @fn static int _import_fd(FILE *fd, exim_t* obj)
 * @brief Import a representation of the given key from a file descriptor.
 * Expects the same format as the output from _export_fd().
 *
 * @return IOK or IERROR
 */
static int _import_fd(FILE *fd, exim_t* obj) {

  groupsig_signature_t *sig;
  bbs04_signature_t *bbs04_sig;
  uint8_t scheme;
  struct pairing_s *pairing;

  if(!(sig = bbs04_signature_init())) {
    return IERROR;
  }

  bbs04_sig = sig->sig;

  pairing = ((bbs04_sysenv_t *) sysenv->data)->pairing;

  /* First sizeof(int) bytes: scheme */
  if(fread(&scheme, sizeof(uint8_t), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "_import_fd", __LINE__,
          errno, LOGERROR);
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  if(scheme != sig->scheme) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "_import_fd", __LINE__,
              EDQUOT, "Unexpected sig scheme.", LOGERROR);
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get T1 */
  element_init_G1(bbs04_sig->T1, pairing);
  if(pbcext_get_element_fd(bbs04_sig->T1, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get T2 */
  element_init_G1(bbs04_sig->T2, pairing);
  if(pbcext_get_element_fd(bbs04_sig->T2, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get T3 */
  element_init_G1(bbs04_sig->T3, pairing);
  if(pbcext_get_element_fd(bbs04_sig->T3, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get c */
  element_init_Zr(bbs04_sig->c, pairing);
  if(pbcext_get_element_fd(bbs04_sig->c, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get salpha */
  element_init_Zr(bbs04_sig->salpha, pairing);
  if(pbcext_get_element_fd(bbs04_sig->salpha, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get sbeta */
  element_init_Zr(bbs04_sig->sbeta, pairing);
  if(pbcext_get_element_fd(bbs04_sig->sbeta, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get sx */
  element_init_Zr(bbs04_sig->sx, pairing);
  if(pbcext_get_element_fd(bbs04_sig->sx, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get sdelta1 */
  element_init_Zr(bbs04_sig->sdelta1, pairing);
  if(pbcext_get_element_fd(bbs04_sig->sdelta1, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  /* Get sdelta2 */
  element_init_Zr(bbs04_sig->sdelta2, pairing);
  if(pbcext_get_element_fd(bbs04_sig->sdelta2, fd) == IERROR) {
    bbs04_signature_free(sig); sig = NULL;
    return IERROR;
  }

  obj->eximable = sig;
  return IOK;
}

/* Export/import handle definition */

static exim_handle_t _exim_h = {
  &_get_size_bytearray_null,
  &_export_fd,
  &_import_fd,
};

/* Public functions */
groupsig_signature_t* bbs04_signature_init() {

  groupsig_signature_t *sig;
  bbs04_signature_t *bbs04_sig;

  bbs04_sig = NULL;

  /* Initialize the signature contents */
  if(!(sig = (groupsig_signature_t *) mem_malloc(sizeof(groupsig_signature_t)))) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_signature_init", __LINE__, errno, 
		  LOGERROR);
  }

  if(!(bbs04_sig = (bbs04_signature_t *) mem_malloc(sizeof(bbs04_signature_t)))) {
    LOG_ERRORCODE(&logger, __FILE__, "bbs04_signature_init", __LINE__, errno, 
		  LOGERROR);
    return NULL;
  }

  sig->scheme = GROUPSIG_BBS04_CODE;
  sig->sig = bbs04_sig;

  /* Since we use the PBC library, to initialize the signature fields, we need
     the pairing. Hence, they will be initialized during the sign procedure. */

  return sig;

}

int bbs04_signature_free(groupsig_signature_t *sig) {

  bbs04_signature_t *bbs04_sig;

  if(!sig || sig->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_signature_free", __LINE__,
		   "Nothing to free.", LOGWARN);    
    return IOK;
  }

  if(sig->sig) {
    bbs04_sig = sig->sig;
    element_clear(bbs04_sig->T1);
    element_clear(bbs04_sig->T2);
    element_clear(bbs04_sig->T3);
    element_clear(bbs04_sig->c);
    element_clear(bbs04_sig->salpha);
    element_clear(bbs04_sig->sbeta);
    element_clear(bbs04_sig->sx);
    element_clear(bbs04_sig->sdelta1);
    element_clear(bbs04_sig->sdelta2);
    mem_free(bbs04_sig); bbs04_sig = NULL;
  }
  
  mem_free(sig); sig = NULL;

  return IOK;

}

int bbs04_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src) {

  bbs04_signature_t *bbs04_dst, *bbs04_src;

  if(!dst || dst->scheme != GROUPSIG_BBS04_CODE ||
     !src || src->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_signature_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_dst = dst->sig;
  bbs04_src = src->sig;

  /* Copy the elements */
  element_init_same_as(bbs04_dst->T1, bbs04_src->T1);
  element_set(bbs04_dst->T1, bbs04_src->T1);
  element_init_same_as(bbs04_dst->T2, bbs04_src->T2);
  element_set(bbs04_dst->T2, bbs04_src->T2);
  element_init_same_as(bbs04_dst->T3, bbs04_src->T3);
  element_set(bbs04_dst->T3, bbs04_src->T3);
  element_init_same_as(bbs04_dst->c, bbs04_src->c);
  element_set(bbs04_dst->c, bbs04_src->c);  
  element_init_same_as(bbs04_dst->salpha, bbs04_src->salpha);
  element_set(bbs04_dst->salpha, bbs04_src->salpha);
  element_init_same_as(bbs04_dst->sbeta, bbs04_src->sbeta);
  element_set(bbs04_dst->sbeta, bbs04_src->sbeta);
  element_init_same_as(bbs04_dst->sx, bbs04_src->sx);
  element_set(bbs04_dst->sx, bbs04_src->sx);
  element_init_same_as(bbs04_dst->sdelta1, bbs04_src->sdelta1);
  element_set(bbs04_dst->sdelta1, bbs04_src->sdelta1);
  element_init_same_as(bbs04_dst->sdelta2, bbs04_src->sdelta2);
  element_set(bbs04_dst->sdelta2, bbs04_src->sdelta2);

  return IOK;

}

char* bbs04_signature_to_string(groupsig_signature_t *sig) {

  if(!sig || sig->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "signature_to_string", __LINE__, LOGERROR);
    return NULL;
  }
  
  if(!sig->sig){
    LOG_EINVAL(&logger, __FILE__, "signature_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return NULL;
}

int bbs04_signature_get_size_in_format(groupsig_signature_t *sig, groupsig_signature_format_t format) {

  if(!sig || sig->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_signature_get_size_in_format", __LINE__, LOGERROR);
    return -1;
  }

   /* See if the current scheme supports the given format */
   if(!_is_supported_format(format)) {
     LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_signature_get_size_in_format", __LINE__,
   		   "The specified format is not supported.", LOGERROR);
     return -1;
   }

   exim_t wrap = {sig->sig, &_exim_h };
   return exim_get_size_in_format(&wrap, format);

}

int bbs04_signature_export(groupsig_signature_t *sig, groupsig_signature_format_t format, void *dst) { 

  if(!sig || sig->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_signature_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_signature_export", __LINE__,
  		   "The specified format is not supported.", LOGERROR);
    return IERROR;
  }

  exim_t wrap = {sig->sig, &_exim_h };
  return exim_export(&wrap, format, dst);

}

groupsig_signature_t* bbs04_signature_import(groupsig_signature_format_t format, void *source) {

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "signature_import", __LINE__, LOGERROR);
    return NULL;
  }

  /* See if the current scheme supports the given format */
  if(!_is_supported_format(format)) {
    LOG_EINVAL_MSG(&logger, __FILE__, "signature_import", __LINE__,
  		   "The specified format is not supported.", LOGERROR);
    return NULL;
  }

  exim_t wrap = {NULL, &_exim_h };
  if(exim_import(format, source, &wrap) == IOK){
    return wrap.eximable;
  }

  return NULL;

}

/* signature.c ends here */
