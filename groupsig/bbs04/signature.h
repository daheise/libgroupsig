/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature.h
 * @brief: BBS04 signatures
 * @author: jesus
 * Maintainer: jesus
 * @date: vie may 18 11:48:51 2012 (+0200)
 * @version: 0.1
 * Last-Updated: mié sep 25 22:21:58 2013 (+0200)
 *           By: jesus
 *     Update #: 13
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BBS04_SIGNATURE_H
#define _BBS04_SIGNATURE_H

#include <stdint.h>
#include <pbc/pbc.h>
#include "include/signature.h"
#include "bigz.h"
#include "bbs04.h"

/**
 * @def BBS04_SUPPORTED_SIG_FORMATS_N
 * @brief Number of supported signature formats in BBS04.
 */
#define BBS04_SUPPORTED_SIG_FORMATS_N 6

/**
 * @var BBS04_SUPPORTED_SIG_FORMATS
 * @brief List of supported signature formats in BBS04.
 */
static const int BBS04_SUPPORTED_SIG_FORMATS[BBS04_SUPPORTED_SIG_FORMATS_N] = { 
  GROUPSIG_SIGNATURE_FORMAT_FILE_NULL,
  GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64,
  GROUPSIG_SIGNATURE_FORMAT_BYTEARRAY,
  GROUPSIG_SIGNATURE_FORMAT_STRING_NULL_B64,
  GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL,
  GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL_B64,
};

/**
 * @struct bbs04_signature_t
 * @brief Defines the structure of a BBS04 signature.
 */
typedef struct {
  uint8_t scheme; /**< Metainformation: the gs scheme this key belongs to. */
  /* pbc_param_t param; /\**< PBC parameters. *\/ */
  /* pairing_t pairing; /\**< PBC pairing data. *\/ */
  element_t T1;
  element_t T2;
  element_t T3;
  element_t c;
  element_t salpha;
  element_t sbeta;
  element_t sx;
  element_t sdelta1;
  element_t sdelta2;
} bbs04_signature_t;

/** 
 * @fn groupsig_signature_t* bbs04_signature_init()
 * @brief Initializes the fields of a BBS04 signature.
 * 
 * @return A pointer to the allocated signature, or NULL if error.
 */
groupsig_signature_t* bbs04_signature_init();

/** 
 * @fn int bbs04_signature_free(groupsig_signature_t *sig)
 * @brief Frees the alloc'ed fields of the given BBS04 signature.
 *
 * @param[in,out] sig The signature to free.
 * 
 * @return IOK or IERROR
 */
int bbs04_signature_free(groupsig_signature_t *sig);

/** 
 * @fn int bbs04_signature_copy(groupsig_signature_t *dst, 
 *                              groupsig_signature_t *src)
 * @brief Copies the given source signature into the destination signature.
 *
 * @param[in,out] dst The destination signature. Initialized by the caller.
 * @param[in] src The signature to copy. 
 * 
 * @return IOK or IERROR.
 */
int bbs04_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src);

/** 
 * @fn int bbs04_signature_to_string(groupsig_signature_t *sig)
 * @brief Returns a printable string representing the current signature.
 *
 * @param[in] sig The signature o convert.
 * 
 * @return A pointer to the created string or NULL if error.
 */
char* bbs04_signature_to_string(groupsig_signature_t *sig);

/** 
 * @fn int bbs04_signature_get_size_in_format(groupsig_signature_t *sig, 
 *   groupsig_signature_format_t format)
 * Returns the size of the signature in the specified format. Useful when you have
 * to export the signature and pre-allocate the destination.
 *
 * @param[in] sig The signature.
 * @param[in] format The format.
 * 
 * @return -1 if error, the size that this signature would have in case of
 *  being exported to the specified format.
 */
int bbs04_signature_get_size_in_format(groupsig_signature_t *sig, 
				       groupsig_signature_format_t format);

/** 
 * @fn int bbs04_signature_export(groupsig_signature *signature,
 *                                groupsig_signature_format_t format,
 *                                void *dst)
 * @brief Exports the specified signature to the given destination using
 *  the specified format..
 *
 * @param[in] signature The signature to export.
 * @param[in] format The format to use.
 * @param[in,out] dst Details about the destination. Will depend on the
 *  specified parameter.
 * 
 * @return IOK or IERROR
 */
int bbs04_signature_export(groupsig_signature_t *signature, 
			   groupsig_signature_format_t format, 
			   void *dst);

/** 
 * @fn groupsig_signature_t* bbs04_signature_import(bbs04_groupsig_signature_format_t format, 
 *                                            void *source)
 * @brief Imports a signature according to the specified format.
 *
 * @param[in] format The format of the signature to import.
 * @param[in] source The signature to be imported.
 * 
 * @return A pointer to the imported signature.
 */
groupsig_signature_t* bbs04_signature_import(groupsig_signature_format_t format, void *source);

/**
 * @var bbs04_signature_handle
 * @brief Set of functions for managing BBS04 signatures.
 */
static const groupsig_signature_handle_t bbs04_signature_handle = {
  GROUPSIG_BBS04_CODE, /**< The scheme code. */
  &bbs04_signature_init,  /**< Initializes signatures. */
  &bbs04_signature_free, /**< Frees signatures. */
  &bbs04_signature_copy, /**< Copies signatures. */
  &bbs04_signature_get_size_in_format, /**< Gets the size in bytes of a signature
					  in a specific format. */
  &bbs04_signature_export, /**< Exports signatures. */
  &bbs04_signature_import, /**< Imports signatures. */
  &bbs04_signature_to_string, /**< Converts signatures to printable strings. */
};

#endif

/* signature.h ends here */
