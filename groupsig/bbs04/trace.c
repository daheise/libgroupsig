/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trace.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 14:55:35 2012 (+0200)
 * @version: 
 * Last-Updated: lun jul 20 21:27:53 2015 (+0200)
 *           By: jesus
 *     Update #: 19
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include "include/crl.h"
#include "bigz.h"
#include "bbs04.h"
#include "groupsig/bbs04/signature.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/mgr_key.h"
#include "groupsig/bbs04/crl.h"
#include "groupsig/bbs04/gml.h"
#include "groupsig/bbs04/trapdoor.h"
#include "groupsig/bbs04/identity.h"

int bbs04_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml) {

  bbs04_signature_t *bbs04_sig;
  bbs04_grp_key_t *gkey;
  bbs04_mgr_key_t *mkey;
  identity_t *id;
  trapdoor_t *trap, *trapi;
  uint64_t i;
  uint8_t revoked;

  if(!ok || !sig || sig->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_BBS04_CODE ||
     !gml || !crl) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  gkey = (bbs04_grp_key_t *) grpkey->key;
  mkey = (bbs04_mgr_key_t *) mgrkey->key;
  bbs04_sig = (bbs04_signature_t *) sig->sig;

  /* In BBS04, tracing implies opening the signature to get the signer's 
     identity, and using the signer's identity to get her A, which
     is then matched against those of the users in a CRL */
  if(!(id = bbs04_identity_init())) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Open the signature */
  if(bbs04_open(id, NULL, crl, sig, grpkey, mgrkey, gml) == IERROR) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
    identity_free(id); id = NULL;
    return IERROR;
  }

  if(!(trap = bbs04_trapdoor_init())) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
    identity_free(id); id = NULL;
    return IERROR;
  }

  /* We pass a NULL crl because we do not want to update it */
  if(bbs04_reveal(trap, NULL, gml, *(bbs04_identity_t *) id->id) == IERROR) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
    identity_free(id); id = NULL;
    trapdoor_free(trap); trap = NULL;
    return IERROR;
  }
  
  i = 0; revoked = 0;
  while(i < crl->n) {

    if(!(trapi = bbs04_trapdoor_init())) {
      LOG_EINVAL(&logger, __FILE__, "bbs04_trace", __LINE__, LOGERROR);
      identity_free(id); id = NULL;
      trapdoor_free(trap); trap = NULL;
      return IERROR;
    }

    /* Get the next trapdoor to test */
    trapi = ((bbs04_crl_entry_t *) crl_get(crl, i))->trapdoor;
  
    if(!element_cmp(((bbs04_trapdoor_t *) trap->trap)->open,
		    ((bbs04_trapdoor_t *) trapi->trap)->open)) {
      revoked = 1;
      break;
    }

    /* trapdoor_free(trapi); trapi = NULL; */

    i++;

  }

  *ok = revoked;

  identity_free(id); id = NULL;
  trapdoor_free(trap); trap = NULL;

  return IOK;


}

/* trace.c ends here */
