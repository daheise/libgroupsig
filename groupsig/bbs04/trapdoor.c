/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 11:21:21 2013 (+0100)
 * @version: 
 * Last-Updated: mar sep 24 23:45:20 2013 (+0200)
 *           By: jesus
 *     Update #: 21
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include "types.h"
#include "sys/mem.h"
#include "misc/misc.h"
#include "groupsig/bbs04/trapdoor.h"
#include "wrappers/pbc_ext.h"

trapdoor_t* bbs04_trapdoor_init() {
  
  trapdoor_t *trap;
  bbs04_trapdoor_t *bbs04_trap;
  
  if(!(trap = (trapdoor_t *) mem_malloc(sizeof(trapdoor_t)))) {
    return NULL;
  }

  if(!(bbs04_trap = (bbs04_trapdoor_t *) mem_malloc(sizeof(bbs04_trapdoor_t)))) {
    mem_free(trap); trap = NULL;
    return NULL;
  }
  
  /* BBS04 does not implement tracing */
  bbs04_trap->trace = NULL;
  
  /* The bbs04_trap->open field is of type element_t (pbc library) and the
     pairing is necessary to initialize it, hence, it must be initialized
     and set in the join_mgr function. */
  
  trap->scheme = GROUPSIG_BBS04_CODE;
  trap->trap = bbs04_trap;
  
  return trap;
  
}

int bbs04_trapdoor_free(trapdoor_t *trap) {
  
  bbs04_trapdoor_t *bbs04_trap;
  
  if(!trap || trap->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bbs04_trapdoor_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }
  
  if(trap->trap) {
    bbs04_trap = trap->trap;
    element_clear(bbs04_trap->open);
    mem_free(bbs04_trap); bbs04_trap = NULL;
  }
  
  mem_free(trap); trap = NULL;
  
  return IOK;
  
}

int bbs04_trapdoor_copy(trapdoor_t *dst, trapdoor_t *src) {
  
  if(!dst || dst->scheme != GROUPSIG_BBS04_CODE ||
     !src || src->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trapdoor_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  element_init_same_as(((bbs04_trapdoor_t *) dst->trap)->open,
		       ((bbs04_trapdoor_t *) src->trap)->open);

  element_set(((bbs04_trapdoor_t *) dst->trap)->open, 
	      ((bbs04_trapdoor_t *) src->trap)->open);  

  return IOK;

}

char* bbs04_trapdoor_to_string(trapdoor_t *trap) {
  
  char *b64;
  
  if(!trap || trap->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trapdoor_to_string", __LINE__, LOGERROR);
    return NULL;
  }
  
  /* BBS04 only has open trapdoors, with type bigz */
  if(!(b64 = pbcext_element_export_b64(((bbs04_trapdoor_t *)trap->trap)->open))) {
    return NULL;
  }
  
  return b64;
  
}

trapdoor_t* bbs04_trapdoor_from_string(char *strap) {
  
  trapdoor_t *trap;
  bbs04_sysenv_t *env;
  
  if(!strap) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trapdoor_from_string", __LINE__, LOGERROR);
    return NULL;
  }
  
  if(!(trap = bbs04_trapdoor_init())) {
    return NULL;
  }

  /* Open trapdoors are the A elements of the member keys */
  env = (bbs04_sysenv_t *) sysenv->data;
  element_init_G1(((bbs04_trapdoor_t *) trap->trap)->open, env->pairing);
  if(pbcext_element_import_b64(((bbs04_trapdoor_t *) trap->trap)->open, strap) == IERROR) {
    bbs04_trapdoor_free(trap); trap = NULL;
    return NULL;
  }
  
  return trap;
  
}

int bbs04_trapdoor_cmp(trapdoor_t *t1, trapdoor_t *t2) {
  
  if(!t1 || t1->scheme != GROUPSIG_BBS04_CODE ||
     !t2 || t2->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_trapdoor_cmp", __LINE__, LOGERROR);
    return IERROR;
  }

  return element_cmp(((bbs04_trapdoor_t *)t1->trap)->open, 
		     ((bbs04_trapdoor_t *)t2->trap)->open);

}

/* identity.c ends here */
