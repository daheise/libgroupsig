/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: verify.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  9 17:20:57 2012 (+0200)
 * @version: 
 * Last-Updated: jue sep  4 19:08:14 2014 (+0200)
 *           By: jesus
 *     Update #: 33
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include "bbs04.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/bbs04/signature.h"
#include "bigz.h"
#include "wrappers/pbc_ext.h"
#include "wrappers/hash.h"
#include "sys/mem.h"

/* Private functions */

/* Public functions */
int bbs04_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, groupsig_key_t *grpkey) {
  
  element_t R1, R2, R3, R4, R5, aux_neg, aux_e1, aux_e2, aux_e3, aux_e4, aux_e5, c;
  bbs04_signature_t *bbs04_sig;
  bbs04_grp_key_t *bbs04_grpkey;
  bbs04_sysenv_t *bbs04_sysenv;
  hash_t *aux_c;
  byte_t *aux_bytes;
  int aux_n, rc;

  if(!ok || !msg || !sig || sig->scheme != GROUPSIG_BBS04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_BBS04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "bbs04_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  bbs04_sig = sig->sig;
  bbs04_grpkey = grpkey->key;
  bbs04_sysenv = sysenv->data;
  rc = IOK;

  /* Re-derive R1, R2, R3, R4 and R5 from the signature */
  
  /* R1 = u^salpha * T1^(-c) */
  element_init_G1(R1, bbs04_sysenv->pairing);
  element_init_Zr(aux_neg, bbs04_sysenv->pairing);
  element_neg(aux_neg, bbs04_sig->c);
  element_pow2_zn(R1, bbs04_grpkey->u, bbs04_sig->salpha, bbs04_sig->T1, aux_neg);

  /* R2 = v^sbeta * T2^(-c) */
  element_init_G1(R2, bbs04_sysenv->pairing);
  element_pow2_zn(R2, bbs04_grpkey->v, bbs04_sig->sbeta, bbs04_sig->T2, aux_neg);

  /* R3 = e(T3,g2)^sx * e(h,w)^(-salpha-sbeta) * e(h,g2)^(-sdelta1-sdelta2) * (e(T3,w)/e(g1,g2))^c */
  /* Optimized R3 =  e(h,w)^(-salpha-sbeta) * e(h,g2)^(-sdelta1-sdelta2) * e(T3, w^c * g2 ^ sx) * e(g1, g2)^-c

  /* Optimized e1 = e(T3, w^c * g2 ^ sx) */
  element_init_GT(aux_e1, bbs04_sysenv->pairing);
  element_init_G2(aux_e5, bbs04_sysenv->pairing);
  element_pow2_zn(aux_e5, bbs04_grpkey->w, bbs04_sig->c,
                  bbs04_grpkey->g2, bbs04_sig->sx);
  element_pairing(aux_e1, bbs04_sig->T3, aux_e5);

  /* e2 = e(h,w)^(-salpha-sbeta) */
  element_init_GT(aux_e2, bbs04_sysenv->pairing);
  element_neg(aux_neg, bbs04_sig->salpha);
  element_sub(aux_neg, aux_neg, bbs04_sig->sbeta);
  element_pow_zn(aux_e2, bbs04_grpkey->hw, aux_neg);
  
  /* e3 = e(h,g2)^(-sdelta1-sdelta2) */
  element_init_GT(aux_e3, bbs04_sysenv->pairing);
  element_neg(aux_neg, bbs04_sig->sdelta1);
  element_sub(aux_neg, aux_neg, bbs04_sig->sdelta2);
  element_pow_zn(aux_e3, bbs04_grpkey->hg2, aux_neg);

  /* e4 = e(g1,g2)^-c */
  element_init_GT(aux_e4, bbs04_sysenv->pairing);
  element_pow_zn(aux_e4, bbs04_grpkey->g1g2, bbs04_sig->c);
  element_invert(aux_e4, aux_e4);

  /* R3 = e1 * e2 * e3 * e4 */
  element_init_GT(R3, bbs04_sysenv->pairing);
  element_mul(R3, aux_e1, aux_e2);
  element_mul(R3, R3, aux_e3);
  element_mul(R3, R3, aux_e4);
  element_clear(aux_e1); element_clear(aux_e2);
  element_clear(aux_e3); element_clear(aux_e4);
  element_clear(aux_e5);

  /* R4 = T1^sx * u^(-sdelta1) */
  element_init_G1(R4, bbs04_sysenv->pairing);
  element_neg(aux_neg, bbs04_sig->sdelta1);
  element_pow2_zn(R4, bbs04_sig->T1, bbs04_sig->sx, bbs04_grpkey->u, aux_neg); 

  /* R5 = T2^sx * v^(-sdelta2) */
  element_init_G1(R5, bbs04_sysenv->pairing);
  element_neg(aux_neg, bbs04_sig->sdelta2);
  element_pow2_zn(R5, bbs04_sig->T2, bbs04_sig->sx, bbs04_grpkey->v, aux_neg);
  element_clear(aux_neg);

  /* Recompute the hash-challenge c */

  /* c = hash(M,T1,T2,T3,R1,R2,R3,R4,R5) \in Zp */
  if(!(aux_c = hash_init(HASH_SHA1))) GOTOENDRC(IERROR, bbs04_verify);

  /* Push the message */
  if(hash_update(aux_c, msg->bytes, msg->length) == IERROR) 
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push T1 */
  aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T1) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push T2 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T2) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push T3 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, bbs04_sig->T3) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push R1 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R1) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push R2 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R2) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push R3 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R3) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push R4 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R4) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Push R5 */
  mem_free(aux_bytes); aux_bytes = NULL;
  if(pbcext_element_export_bytes(&aux_bytes, &aux_n, R5) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);
  
  if(hash_update(aux_c, aux_bytes, aux_n) == IERROR)
    GOTOENDRC(IERROR, bbs04_verify);

  /* Finish the hash */
  if(hash_finalize(aux_c) == IERROR) GOTOENDRC(IERROR, bbs04_verify);

  /* Get c as the element associated to the obtained hash value */
  element_init_Zr(c, bbs04_sysenv->pairing);
  element_from_hash(c, aux_c->hash, aux_c->length);

  /* Compare the result with the received challenge */
  if(element_cmp(bbs04_sig->c, c)) { /* Different: sig fail */
    *ok = 0;
  } else { /* Same: sig OK */
    *ok = 1;
  }

 bbs04_verify_end:

  if(aux_bytes) { mem_free(aux_bytes); aux_bytes = NULL; }
  if(aux_c) { hash_free(aux_c); aux_c = NULL; }

  return rc;

}

/* verify.c ends here */
