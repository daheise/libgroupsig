/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: claim_verify.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 15:54:02 2013 (+0100)
 * @version: 
 * Last-Updated: vie oct  4 14:20:35 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: 
 */
#include "config.h"
#include "sys/mem.h"
#include "cpy06.h"

int cpy06_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
		       groupsig_signature_t *sig, groupsig_key_t *grpkey) {
  
  groupsig_signature_t **sigs;
  int rc;

  if(!ok || 
     !proof || proof->scheme != GROUPSIG_CPY06_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_CPY06_CODE ||
     !sig || sig->scheme != GROUPSIG_CPY06_CODE) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_claim_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  /* A claim is just similar to proving "equality" of N sigature, but just 
     for 1 signature */
  if(!(sigs = (groupsig_signature_t **) mem_malloc(sizeof(groupsig_signature_t *)))) {
    LOG_ERRORCODE(&logger, __FILE__, "cpy06_claim", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  sigs[0] = sig;

  rc = cpy06_prove_equality_verify(ok, proof, grpkey, sigs, 1);
  mem_free(sigs); sigs = NULL;

  return rc;

}

/* claim_verify.c ends here */
