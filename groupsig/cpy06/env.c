/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *

 * @file: env.c
 * @brief: Manages the CPY06 specific environment information, namely, the PBC
 * params and pairings.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie sep  6 13:43:35 2013 (+0200)
 * @version: 0.1
 * Last-Updated: mié oct  9 21:30:42 2013 (+0200)
 *           By: jesus
 *     Update #: 16
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#include "cpy06.h"
#include "sysenv.h"
#include "sys/mem.h"
#include "wrappers/pbc_ext.h"

int cpy06_sysenv_update(void *data) {
 
  if(!data) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_sysenv_update", __LINE__, LOGERROR);
    return IERROR;
  }

  sysenv->data = data;

  return IOK;

}

void* cpy06_sysenv_get() {
  return sysenv->data;
}

int cpy06_sysenv_free() {

  if (sysenv->data) {

    pairing_clear(((cpy06_sysenv_t *) sysenv->data)->pairing);
    pbc_param_clear(((cpy06_sysenv_t *) sysenv->data)->param);
    mem_free(sysenv->data); sysenv->data = NULL;

  }
  
  return IOK;
  
}

/* env.c ends here */
