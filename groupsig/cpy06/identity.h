/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.h
 * @brief: CPY06 identities.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 11:16:11 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 14:16:21 2013 (+0200)
 *           By: jesus
 *     Update #: 3
 * URL: bitbucket.org/jdiazvico/libgroupsig
 * @todo Identities should be made independent of group signature schemes, in 
 * order to allow different schemes to share the same identity implementation.
 */

#ifndef _CPY06_IDENTITY_H
#define _CPY06_IDENTITY_H

#include "include/identity.h"
#include "cpy06.h"

/**
 * CPY06 identities.
 */
typedef uint64_t cpy06_identity_t;

/** 
 * @fn void* cpy06_identity_init()
 * @brief Allocates memory for a CPY06 identity and sets its values to defaults.
 * 
 * @return A pointer to the allocated memory or NULL if error.
 */
identity_t* cpy06_identity_init();

/** 
 * @fn int cpy06_identity_free(void *id)
 * @brief Frees the memory allocated for a CPY06 identity.
 *
 * @param[in,out] id The identity to free.
 * 
 * @return IOK.
 */
int cpy06_identity_free(identity_t *id);

/** 
 * @fn int cpy06_identity_copy(identity_t *dst, identity_t *src)
 * @brief Copies the source identity into the destination identity.
 *
 * @param[in,out] dst The destination identity. Initialized by the caller.
 * @param[in] src The source identity.
 * 
 * @return IOK or IERROR.
 */
int cpy06_identity_copy(identity_t *dst, identity_t *src);

/** 
 * @fn uint8_t cpy06_identity_cmp(identity_t *id1, identity_t *id2);
 * @brief Returns 0 if both ids are the same, != 0 otherwise.
 *
 * @param[in] id1 The first id to compare. 
 * @param[in] id2 The second id to compare.
 * 
 * @return 0 if both ids are the same, != otherwise. In case of error,
 *  errno is set consequently.
 */
uint8_t cpy06_identity_cmp(identity_t *id1, identity_t *id2);

/** 
 * @fn char* cpy06_identity_to_string(identity_t *id)
 * @brief Converts the given CPY06 id into a printable string.
 *
 * @param[in] id The ID to convert.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
char* cpy06_identity_to_string(identity_t *id);

/** 
 * @fn identity_t* cpy06_identity_from_string(char *sid)
 * @brief Parses the given string as  CPY06 identity.
 *
 * @param[in] sid The string containing the CPY06 identity.
 * 
 * @return A pointer to the retrieved CPY06 identity or NULL if error.
 */
identity_t* cpy06_identity_from_string(char *sid);

/**
 * @var cpy06_identity_handle
 * @brief Set of functions to manage CPY06 identities.
 */
static const identity_handle_t cpy06_identity_handle = {
  GROUPSIG_CPY06_CODE, /**< Scheme code. */
  &cpy06_identity_init, /**< Identity initialization. */
  &cpy06_identity_free, /**< Identity free.*/
  &cpy06_identity_copy, /**< Copies identities. */
  &cpy06_identity_cmp, /**< Compares identities. */
  &cpy06_identity_to_string, /**< Converts identities to printable strings. */
  &cpy06_identity_from_string /**< Imports identities from strings. */
};

#endif /* _CPY06_IDENTITY_H */

/* identity.h ends here */
