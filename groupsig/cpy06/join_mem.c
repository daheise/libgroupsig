/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: join.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue may 10 11:12:56 2012 (+0200)
 * @version: 
 * Last-Updated: mar oct  8 21:13:27 2013 (+0200)
 *           By: jesus
 *     Update #: 12
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "cpy06.h"
#include "groupsig/cpy06/grp_key.h"
#include "groupsig/cpy06/mem_key.h"
#include "bigz.h"
#include "sys/mem.h"

/** 
 * @fn int cpy06_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Member side join procedure.
 * 
 * The original proposal does not include a "join" procedure. Instead, it is the
 * private-key issuer generates and distributes the member keys, and requires a
 * predefined group size. We adopt this approach to allow dynamic addition of group
 * members.
 *
 * @param[in,out] memkey Will be set to the produced member key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int cpy06_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey) {

  cpy06_sysenv_t *cpy06_sysenv;
  cpy06_mem_key_t *cpy06_memkey;
  cpy06_grp_key_t *cpy06_grpkey;

  if(!memkey || memkey->scheme != GROUPSIG_CPY06_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_CPY06_CODE) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_join_mem", __LINE__, LOGERROR);
    return IERROR;
  }

  cpy06_sysenv = (cpy06_sysenv_t*) sysenv->data;
  cpy06_memkey = (cpy06_mem_key_t *) memkey->key;
  cpy06_grpkey = (cpy06_grp_key_t *) grpkey->key;
  cpy06_sysenv = sysenv->data;

  /** @todo A provably secure two party computation for adaptive chosing of 
      random powers should be executed here (see KTY04). */  
  /* x \in_R Z^*_p */
  element_init_Zr(cpy06_memkey->x, cpy06_sysenv->pairing);
  element_random(cpy06_memkey->x);

  /* By convention here, we will set t and A to 0 to mark that they have not
     been set... (@todo is this a mathematical stupidity?) 
     NOTE: this is needed by some external applications (e.g. caduceus)
  */
  
  element_init_Zr(cpy06_memkey->t, cpy06_sysenv->pairing);
  element_set0(cpy06_memkey->t);
  element_init_G1(cpy06_memkey->A, cpy06_sysenv->pairing);
  element_set0(cpy06_memkey->A);

  return IOK;

}

/* join.c ends here */
