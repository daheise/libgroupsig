/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: join_mgr.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 16:50:13 2013 (+0100)
 * @version: 
 * Last-Updated: mar oct  8 21:35:11 2013 (+0200)
 *           By: jesus
 *     Update #: 11
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "cpy06.h"
#include "groupsig/cpy06/grp_key.h"
#include "groupsig/cpy06/mgr_key.h"
#include "groupsig/cpy06/mem_key.h"
#include "groupsig/cpy06/gml.h"
#include "groupsig/cpy06/identity.h"
#include "groupsig/cpy06/trapdoor.h"
#include "wrappers/pbc_ext.h"
#include "sys/mem.h"

int cpy06_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey) {

  cpy06_mem_key_t *cpy06_memkey;
  cpy06_mgr_key_t *cpy06_mgrkey;
  cpy06_grp_key_t *cpy06_grpkey;
  cpy06_gml_entry_t *cpy06_entry;
  cpy06_trapdoor_t *cpy06_trap;
  cpy06_sysenv_t *cpy06_sysenv;
  element_t gammat,c;

  if(!gml || gml->scheme != GROUPSIG_CPY06_CODE ||
     !memkey || memkey->scheme != GROUPSIG_CPY06_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_CPY06_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_CPY06_CODE) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_join_mgr", __LINE__, LOGERROR);
    return IERROR;
  }

  cpy06_memkey = (cpy06_mem_key_t *) memkey->key;
  cpy06_mgrkey = (cpy06_mgr_key_t *) mgrkey->key;
  cpy06_grpkey = (cpy06_grp_key_t *) grpkey->key;
  cpy06_sysenv = sysenv->data;

  /* /\* x \in_R Z^*_p (@todo Should be non-adaptively chosen by member) *\/ */
  /* element_init_Zr(cpy06_memkey->x, cpy06_grpkey->pairing); */
  /* element_random(cpy06_memkey->x); */

  /* t \in_R Z^*_p */
  element_init_Zr(cpy06_memkey->t, cpy06_sysenv->pairing);
  element_random(cpy06_memkey->t);

  /* A = (q*g_1^x)^(1/t+\gamma) */
  element_init_Zr(gammat, cpy06_sysenv->pairing);
  element_add(gammat, cpy06_mgrkey->gamma, cpy06_memkey->t);
  element_invert(gammat, gammat);
  element_init_G1(cpy06_memkey->A, cpy06_sysenv->pairing);
  element_pow_zn(cpy06_memkey->A, cpy06_grpkey->g1, cpy06_memkey->x);
  element_mul(cpy06_memkey->A, cpy06_memkey->A, cpy06_grpkey->q);
  element_pow_zn(cpy06_memkey->A,cpy06_memkey->A, gammat);
  element_clear(gammat);

  /* Update the gml, if any */
  if(gml) {

    /* Initialize the GML entry */
    if(!(cpy06_entry = cpy06_gml_entry_init()))
      return IERROR;

    cpy06_trap = (cpy06_trapdoor_t *) cpy06_entry->trapdoor->trap;

    /* Open trapdoor */
    element_init_same_as(cpy06_trap->open, cpy06_memkey->A);
    element_set(cpy06_trap->open, cpy06_memkey->A);

    /* Trace trapdoor */
    element_init_G1(cpy06_trap->trace, cpy06_sysenv->pairing);
    element_pow_zn(cpy06_trap->trace, cpy06_grpkey->g1, cpy06_memkey->x);

    /* Currently, CPY06 identities are just uint64_t's */
    *(cpy06_identity_t *) cpy06_entry->id->id = gml->n;
    
    if(gml_insert(gml, cpy06_entry) == IERROR) {
      cpy06_gml_entry_free(cpy06_entry); cpy06_entry = NULL;
      return IERROR;
    }
    
  }

  return IOK;

}

/* join_mgr.c ends here */
