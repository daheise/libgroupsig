/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: reveal.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:32:51 2012 (+0200)
 * @version: 
 * Last-Updated: vie oct  4 19:19:48 2013 (+0200)
 *           By: jesus
 *     Update #: 11
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "sysenv.h"
#include "bigz.h"
#include "cpy06.h"
#include "groupsig/cpy06/mem_key.h"
#include "groupsig/cpy06/gml.h"
#include "groupsig/cpy06/crl.h"
#include "groupsig/cpy06/trapdoor.h"

int cpy06_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index) {

  cpy06_crl_entry_t *crl_entry;
  cpy06_gml_entry_t *gml_entry;
  trapdoor_t* crl_trap;


  if(!trap || trap->scheme != GROUPSIG_CPY06_CODE ||
     !gml || gml->scheme != GROUPSIG_CPY06_CODE ||
     (crl && crl->scheme != GROUPSIG_CPY06_CODE)) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(crl_trap = trapdoor_init(trap->scheme))){
    LOG_EINVAL(&logger, __FILE__, "cpy06_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  /* The tracing trapdoor for the i-th member is the C value computed during join */
  if(!(gml_entry = ((cpy06_gml_entry_t *) gml_get(gml, index)))) {
    return IERROR;
  }

  if(cpy06_trapdoor_copy(trap, (trapdoor_t *) gml_entry->trapdoor) == IERROR) {
    return IERROR;
  }

  /* If we have received a CRL, update it with the "revoked" member */
  if(crl) {

    if(!(crl_entry = cpy06_crl_entry_init())) {
      return IERROR;
    }

    if(cpy06_identity_copy(crl_entry->id, gml_entry->id) == IERROR) {
      cpy06_crl_entry_free(crl_entry);
      return IERROR;
    }

    cpy06_trapdoor_copy(crl_trap, trap);
    crl_entry->trapdoor = crl_trap;

    if(cpy06_crl_insert(crl, crl_entry) == IERROR) {
      cpy06_crl_entry_free(crl_entry);
      return IERROR;
    }

  }

  return IOK;

}

/* reveal.c ends here */
