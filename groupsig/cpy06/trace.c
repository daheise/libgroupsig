/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trace.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 14:55:35 2012 (+0200)
 * @version: 
 * Last-Updated: jue nov 21 23:17:57 2013 (+0100)
 *           By: jesus
 *     Update #: 24
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include "include/crl.h"
#include "bigz.h"
#include "cpy06.h"
#include "groupsig/cpy06/signature.h"
#include "groupsig/cpy06/grp_key.h"
#include "groupsig/cpy06/mgr_key.h"
#include "groupsig/cpy06/crl.h"
#include "groupsig/cpy06/gml.h"
#include "groupsig/cpy06/trapdoor.h"
#include "groupsig/cpy06/identity.h"

int cpy06_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml) {

  cpy06_signature_t *cpy06_sig;
  cpy06_grp_key_t *gkey;
  //cpy06_mgr_key_t *mkey;
  cpy06_sysenv_t *cpy06_sysenv;
  trapdoor_t *trapi;
  element_t e;
  uint64_t i;
  uint8_t revoked;

  if(!ok || !sig || sig->scheme != GROUPSIG_CPY06_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_CPY06_CODE ||
     //!mgrkey || mgrkey->scheme != GROUPSIG_CPY06_CODE ||
     //!gml ||
     !crl) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  gkey = (cpy06_grp_key_t *) grpkey->key;
  //mkey = (cpy06_mgr_key_t *) mgrkey->key;
  cpy06_sig = (cpy06_signature_t *) sig->sig;
  cpy06_sysenv = sysenv->data;

  element_init_GT(e, cpy06_sysenv->pairing);

  i = 0; revoked = 0;
  while(i < crl->n) {

    // @bug Memory leak here... but uncommenting crashes the program
    /* if(!(trapi = cpy06_trapdoor_init())) { */
    /*   element_clear(e); */
    /*   return IERROR; */
    /* } */

    /* Get the next trapdoor to test */
    trapi = ((cpy06_crl_entry_t *) crl_get(crl, i))->trapdoor;
  
    /* Compute e(trapi->C, sig->T4) */
    element_pairing(e, ((cpy06_trapdoor_t *) trapi->trap)->trace, cpy06_sig->T4);
    if(!element_cmp(e, cpy06_sig->T5)) {
      revoked = 1;
    }

    /* trapdoor_free(trapi); trapi = NULL; */

    i++;

  }

  *ok = revoked;

  return IOK;


}

/* trace.c ends here */
