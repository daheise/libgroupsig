/*                               -*- Mode: C -*- 
 * @file: update_env.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie sep  6 13:43:35 2013 (+0200)
 * @version: 
 * Last-Updated: vie oct  4 14:21:50 2013 (+0200)
 *           By: jesus
 *     Update #: 2
 * URL: 
 */

int cpy06_update_env(void *data) {

  if(!data) {
    LOG_EINVAL(&logger, __FILE__, "cpy06_update_env", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Just update the sysenv->data pointer. */
  sysenv->data = data;

  return IOK;

}

/* update_env.c ends here */
