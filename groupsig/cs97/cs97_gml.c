/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_gml.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun may  7 09:59:16 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:04:36 2013 (+0200)
 *           By: jesus
 *     Update #: 54
 * URL: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "cs97_gml.h"

int cs97_gml_init(cs97_gml_t *cs97_gml) {

  if(!cs97_gml) {
    fprintf(stderr, "Error in cs97_gml_init (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  cs97_gml->n = 0;
  cs97_gml->gkey = NULL;
  cs97_gml->mkeys = NULL;
  
  return IOK;

}

int cs97_gml_free_keys(cs97_gml_t *cs97_gml) {

  uint64_t i;

  if(!cs97_gml) {
    return IOK;
  }

  if(cs97_gml->gkey) {
    cs97_keys_groupkey_free(cs97_gml->gkey);
  }

  for(i=0; i<cs97_gml->n; i++) {
    if(cs97_gml->mkeys[i]) {
      cs97_keys_memberkey_free(cs97_gml->mkeys[i]);
    }
  }

  return IOK;

}

int cs97_gml_free(cs97_gml_t *cs97_gml) {

  uint64_t i;

  if(!cs97_gml) {
    return IOK;
  }

  if(cs97_gml->mkeys) {

    for(i=0; i<cs97_gml->n; i++) {
      if(cs97_gml->mkeys[i]) {
	free(cs97_gml->mkeys[i]); cs97_gml->mkeys[i] = NULL;
      }
    }

    free(cs97_gml->mkeys); cs97_gml->mkeys = NULL;

  }

  return IOK;

}

int cs97_gml_set_gkey(cs97_groupkey_t *cs97_gkey, cs97_gml_t *cs97_gml) {

  if(!cs97_gkey || !cs97_gml) {
    fprintf(stderr, "Error in cs97_gml_set_gkey (%d): %s\n", __LINE__,
	    strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  cs97_gml->gkey = cs97_gkey;
  
  return IOK;

}

int cs97_gml_insert_mkey(cs97_memberkey_t *cs97_mkey, cs97_gml_t *cs97_gml) {

  if(!cs97_gml || !cs97_mkey) {
    fprintf(stderr, "Error in cs97_gml_insert_mkey (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Increment the size of the array of member keys */
  if(!(cs97_gml->mkeys = (cs97_memberkey_t **) 
       realloc(cs97_gml->mkeys, sizeof(cs97_memberkey_t *)*(cs97_gml->n+1)))) {
    fprintf(stderr, "Error in cs97_gml_insert_mkey (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  /* Point to the new key and update the number of keys */
  cs97_gml->mkeys[cs97_gml->n] = cs97_mkey;
  cs97_gml->n++;

  return IOK;

}

int cs97_gml_load(char *filename, cs97_gml_t *cs97_gml) {

  cs97_memberkey_t *cs97_mkey;
  cs97_groupkey_t *cs97_gkey;
  char *sn;
  uint64_t n, i;
  int fd, rc;
  uint8_t eof;

  if(!filename || !cs97_gml) {
    fprintf(stderr, "Error in cs97_gml_load (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Open the file for reading... */
  if(!(fd = open(filename, O_RDONLY) )) {
    fprintf(stderr, "Error in cs97_gml_load (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  /* First, read the group key */
  if(!(cs97_gkey = (cs97_groupkey_t *) malloc(sizeof(cs97_groupkey_t)))) {
    fprintf(stderr, "Error in cs97_gml_load (%d): %s\n",
	    __LINE__, strerror(errno));
      close(fd);
      return IERROR;    
  }

  if(cs97_keys_groupkey_init(cs97_gkey) == IERROR) {
    close(fd);
    free(cs97_gkey); cs97_gkey = NULL;
    return IERROR;
  }

  eof =  0;
  if(cs97_keys_groupkey_parse_b64(fd, cs97_gkey, &eof) != IOK) {
    close(fd);
    cs97_keys_groupkey_free(cs97_gkey);
    free(cs97_gkey); cs97_gkey = NULL;
    return IERROR;
  }

  /* Read the group member keys */
  while(!eof) {

    /* Allocate memory for a new member key */
    if(!(cs97_mkey = (cs97_memberkey_t *) malloc(sizeof(cs97_memberkey_t)))) {
      fprintf(stderr, "Error in cs97_gml_load (%d): %s\n",
	      __LINE__, strerror(errno));
      close(fd);
      cs97_keys_groupkey_free(cs97_gkey);
      free(cs97_gkey); cs97_gkey = NULL;
      return IERROR;
    }
    
    if(cs97_keys_memberkey_init(cs97_mkey) == IERROR) {
      close(fd);
      cs97_keys_groupkey_free(cs97_gkey);
      free(cs97_gkey); cs97_gkey = NULL;
      return IERROR;
    }

    /* Read a new member key */
    if(cs97_keys_memberkey_parse_b64(fd, cs97_mkey, &eof) == IERROR) {
      close(fd);
      cs97_keys_groupkey_free(cs97_gkey);
      free(cs97_gkey); cs97_gkey = NULL;
      return IERROR;
    } 
    
    /* Store it in the GML structure */
    if(cs97_gml_insert_mkey(cs97_mkey, cs97_gml) == IERROR) {
      close(fd);
      cs97_keys_groupkey_free(cs97_gkey);
      free(cs97_gkey); cs97_gkey = NULL;
      return IERROR;
    }

  }
  
  close(fd);
  cs97_gml->gkey = cs97_gkey;

  return IOK;
 
}

int cs97_gml_save(cs97_gml_t *cs97_gml, char *filename) {
  
  uint64_t i;
  FILE *fd;

  if(!cs97_gml || !filename) {
    fprintf(stderr, "Error in cs97_gml_save (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  if(!(fd = fopen(filename, "w"))) {
    fprintf(stderr, "Error in cs97_gml_save (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  /* Dump the group key */
  if(cs97_keys_groupkey_fprintf_b64(fd, cs97_gml->gkey) == IERROR) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  /* Dump all the member keys */
  for(i=0; i<cs97_gml->n; i++) {
    if(cs97_keys_memberkey_fprintf_b64(fd, cs97_gml->mkeys[i]) == IERROR) {
      fclose(fd); fd = NULL;
      return IERROR;
    }
  }

  return IOK;

}

int cs97_gml_fprintf(FILE *fd, cs97_gml_t *cs97_gml) {

  uint64_t i;

  if(!cs97_gml) {
    fprintf(stderr, "Error in cs97_gml_fprintf (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  fprintf(fd, " Group Membership List: \n");
  fprintf(fd, " ---------------------\n");

  if(cs97_keys_groupkey_fprintf(fd, cs97_gml->gkey) == IERROR) {
    return IERROR;
  }

  fprintf(fd, " Number of members: %lu\n", cs97_gml->n);
  fprintf(fd, " ----------------- \n");

  for(i=0; i<cs97_gml->n; i++) {
    if(cs97_keys_memberkey_fprintf(fd, cs97_gml->mkeys[i]) == IERROR) {
      return IERROR;
    }    
  }

  return IOK;

}

/* cs97_gml.c ends here */
