/*                               -*- Mode: C -*- 
 * @file: cs97_gml.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun may  7 09:59:41 2012 (+0200)
 * @version: 
 * Last-Updated: mar may  8 13:10:21 2012 (+0200)
 *           By: jesus
 *     Update #: 13
 * URL: 
 */

#ifndef _CS97_GML_H
#define _CS97_GML_H

#include "../include/types.h"
#include "../include/sysenv.h"
#include "cs97_keys.h"

/**
 * Struct for storing lists of CS97 group members. Used for revocation.
 */
typedef struct _cs97_gml_t {
  uint64_t n; /**< The number of members in the group. */
  cs97_groupkey_t *gkey; /**< The group key. */
  cs97_memberkey_t **mkeys;  /**< The (public part of) the members' keys 
				in the group. */
} cs97_gml_t;

/** 
 * @fn int cs97_gml_init(cs97_gml_t *cs97_gml)
 * @brief Initializes the fields of the given CS97 Group Members List structure.
 *
 * @param[in,out] cs97_gml The CS97 GML structure to initialize.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_init(cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_free_keys(cs97_gml_t *cs97_gml)
 * @brief Frees the group and member keys of the given Group Membership List.
 *  Note that the pointers to the structures are not freed, and must be freed
 *  using cs97_gml_free.
 *
 * @param[in,out] cs97_gml The GML whose keys are to be freed.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_free_keys(cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_free(cs97_gml_t *cs97_gml)
 * @brief Frees the memory allocated by cs97_gml_init for the fields of the given
 *  CS97 GML structure. Note that the memory allocated for the kesy is not
 *  freed. That must be done with cs97_gml_free_keys.
 *
 * @param[in,out] cs97_gml The CS97 GML structure to free.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_free(cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_set_gkey(cs97_groupkey_t *cs97_gkey, cs97_gml_t *cs97_gml)
 * @brief Sets the group key of the given GML to the given group key.
 *
 * @param[in] cs97_gkey The group key.
 * @param[in,out] cs97_gml The Group Members List to update.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_set_gkey(cs97_groupkey_t *cs97_gkey, cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_insert_mkey(cs97_memberkey_t *cs97_mkey, cs97_gml_t *cs97_gml)
 * @brief Inserts the given CS97 member key in the given CS97 GML structure.
 *
 * @param[in] cs97_mkey The member key to insert.
 * @param[in,out] cs97_gml The Group Members List.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_insert_mkey(cs97_memberkey_t *cs97_mkey, cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_load(char *filename, cs97_gml_t *cs97_gml)
 * @brief Loads the Group Members List stored in the given filename into the given 
 *  cs97 group members list structure.
 *
 * @param[in] filename The name of the file containing the Group Members List.
 * @param[in,out] cs97_gml The CS97 group members list to fill.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_load(char *filename, cs97_gml_t *cs97_gml);

/** 
 * @fn int cs97_gml_save(cs97_gml_t *cs97_gml, char *filename)
 * @brief Saves the given CS97 Group Members List structure into the given file.
 *
 * @param[in] cs97_gml The GML structure to save.
 * @param[in] filename The name of the file to store the GML in.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_save(cs97_gml_t *cs97_gml, char *filename);

/** 
 * @fn int cs97_gml_fprintf(FILE *fd, cs97_gml_t *cs97_gml)
 * @brief Prints in the given file the information contained in the received
 *  Group Membership List.
 *
 * @param[in] fd The file descriptor.
 * @param[in] cs97_gml The GML to print.
 * 
 * @return IOK or IERROR
 */
int cs97_gml_fprintf(FILE *fd, cs97_gml_t *cs97_gml);

#endif /* CS97_GML_H */

/* cs97_gml.h ends here */
