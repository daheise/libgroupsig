/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_join.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue abr 26 17:23:25 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:04:38 2013 (+0200)
 *           By: jesus
 *     Update #: 35
 * URL: 
 */

#include <stdio.h>
#include <errno.h>

#include "cs97_join.h"
#include "../include/sysenv.h"

int cs97_join_member(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem) {

  mpz_t x, y, z;

  if(!cs97_grp || !cs97_mem) {
    fprintf(stderr, "Error in cs97_join_member (%d): %s\n", __LINE__,
	    strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Generate the secret x, selected randomly from [0,2^lambda-1] */
  mpz_init(x);
  mpz_urandomb(x, sysenv->gmp_rand, cs97_grp->lambda);

  /* Compute y=a^x (mod n) */
  mpz_init(y);
  mpz_powm(y, cs97_grp->a, x, cs97_grp->n);

  /* Compute the membership key z=g^y (mod n) */
  mpz_init(z);
  mpz_powm(z, cs97_grp->g, y, cs97_grp->G);

  /** @todo Commit to y, for instance, by sign it */

  /** @todo Compute SK of the discrete logarithm of y to the base a (this can be
      done with techniques similar to those for signatures of knowledge of a 
      discrete logarithm, with the difference that the group order is unknown).
      See "Traceable signature"'s relation sets */

  /* Copy the results into the member key */
  mpz_set(cs97_mem->x, x);
  mpz_set(cs97_mem->y, y);
  mpz_set(cs97_mem->z, z);

  mpz_clear(x);
  mpz_clear(y);
  mpz_clear(z);
      
  return IOK;

}

int cs97_join_manager(cs97_managerkey_t *cs97_mgr, cs97_memberkey_t *cs97_mem) {

  mpz_t v, y1;

  if(!cs97_mgr || !cs97_mem) {
    fprintf(stderr, "Error in cs97_join_manager (%d): %s\n", __LINE__,
	    strerror(EINVAL));
    errno = EINVAL;
  }

  /** @todo Verify the signature/commitment to cs97_mem->y */

  /** @todo Verify SK{ alpha : y=a^alpha (mod n) & z=g^y} */

  /* If every previous check is ok, compute the membership certificate. */
  /** @todo We are using (y+1)^(1/e) (mod n). However, in Ateniese and Tsudik's
      "Some open issues and new directions in group signatures", they found an
      attack based on this '1', and suggest changing it by a random value! 
      Do it!*/
  mpz_init_set(y1, cs97_mem->y);
  mpz_add_ui(y1, y1, 1);
  mpz_init(v);

  mpz_powm(v, y1, cs97_mgr->rsa->d, cs97_mgr->rsa->n);

  mpz_init_set(cs97_mem->v, v);
  mpz_clear(v);
  mpz_clear(y1);
  
  return IOK;

}

/* cs97_join.c ends here */
