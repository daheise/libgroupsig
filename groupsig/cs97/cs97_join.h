/*                               -*- Mode: C -*- 
 * @file: cs97_join.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie abr 27 10:34:59 2012 (+0200)
 * @version: 
 * Last-Updated: lun abr 30 12:31:36 2012 (+0200)
 *           By: jesus
 *     Update #: 5
 * URL: 
 */

#ifndef _CS97_JOIN_H
#define _CS97_JOIN_H

#include "cs97_keys.h"

/** 
 * @fn int cs97_join_member(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem)
 * @brief Runs the group members' part of the CS97 join procedure.
 * 
 * @todo Deal with the sign(y) and SK parts of the procedure!
 *
 * @param cs97 The group key of the CS97 scheme.
 * @param cs97_mem The member key to be filled.
 * 
 * @return IOK or IERROR
 */
int cs97_join_member(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem);

/** 
 * @fn int cs97_join_manager(cs97_managerkey_t *cs97_mgr, cs97_memberkey_t *cs97_mem)
 * @brief Runs the group manager's part of the join procedure.
 *
 * @todo Deal with the sign(y) and SK parts of the procedure!
 *
 * @param cs97_mgr The manager keypair.
 * @param cs97_mem The member key to be filled.
 * 
 * @return IOK or IERROR
 */
int cs97_join_manager(cs97_managerkey_t *cs97_mgr, cs97_memberkey_t *cs97_mem);

#endif /* _CS97_JOIN_H */

/* cs97_join.h ends here */
