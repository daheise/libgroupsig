/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_keys.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie abr 27 12:24:45 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:04:42 2013 (+0200)
 *           By: jesus
 *     Update #: 331
 * URL: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "cs97_keys.h"
#include "misc.h"

int cs97_keys_groupkey_init(cs97_groupkey_t *cs97) {

  if(!cs97) {
    fprintf(stderr, "Error in cs97_groupkey_init (%d): %s\n", 
	    __LINE__, strerror(errno));
    return IERROR;
  }

  mpz_init(cs97->n);
  mpz_init(cs97->e);
  mpz_init(cs97->G);
  mpz_init(cs97->g);
  mpz_init(cs97->a);
  cs97->lambda = 0;
  cs97->epsilon = 0;

  return IOK;
}

int cs97_keys_groupkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group, 
				uint64_t lambda, uint64_t epsilon, 
				cs97_groupkey_t *cs97_grp) {

  if(!rsa || !group || !cs97_grp) {
    fprintf(stderr, "Error in cs97_keys_groupkey_init_set (%d): %s\n", __LINE__,
	    strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  mpz_init_set(cs97_grp->n, rsa->n);
  mpz_init_set(cs97_grp->e, rsa->e);
  mpz_init_set(cs97_grp->G, group->G);
  mpz_init_set(cs97_grp->g, group->g);
  mpz_init_set(cs97_grp->a, group->a);
  cs97_grp->lambda = lambda;
  cs97_grp->epsilon = epsilon;

  return IOK;

}

int cs97_keys_groupkey_free(cs97_groupkey_t *cs97) {

  if(!cs97) {
    return IOK;
  }

  mpz_clear(cs97->n);
  mpz_clear(cs97->e);
  mpz_clear(cs97->G);
  mpz_clear(cs97->g);
  mpz_clear(cs97->a);

  return IOK;

}

int cs97_keys_groupkey_fprintf(FILE *fd, cs97_groupkey_t *cs97)  {

  char *sn, *se, *sG, *sg, *sa;
  
  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  sn = mpz_get_str(NULL, 10, cs97->n);
  se = mpz_get_str(NULL, 10, cs97->e);
  sG = mpz_get_str(NULL, 10, cs97->G);
  sg = mpz_get_str(NULL, 10, cs97->g);
  sa = mpz_get_str(NULL, 10, cs97->a);

  fprintf(fd, " Group key:\n");
  fprintf(fd, " ---------\n");
  fprintf(fd, "     n: %s\n", sn);
  fprintf(fd, "     e: %s\n", se);
  fprintf(fd, "     G: %s\n", sG);
  fprintf(fd, "     g: %s\n", sg);
  fprintf(fd, "     a: %s\n", sa);
  fprintf(fd, "lambda: %lu\n", cs97->lambda);
  fprintf(fd, "epsilon: %lu\n\n", cs97->epsilon);

  free(sn); sn = NULL;
  free(se); se = NULL;
  free(sG); sG = NULL;
  free(sg); sg = NULL;
  free(sa); sa = NULL;

  return IOK;
}

int cs97_keys_groupkey_fprintf_b64(FILE *fd, cs97_groupkey_t *cs97) {

  gnutls_datum_t cs97_datum, *cs97_b64=NULL;
  byte_t *bn=NULL, *be=NULL, *bG=NULL, *bg=NULL, *ba=NULL, *bgroupkey=NULL;
  size_t sn, se, sG, sg, sa, sgroupkey, offset;
  uint32_t i;
  int rc;

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;   
  }

  rc = IOK;
  
  /* Export the variables to binary data */
  if(!(bn = mpz_export(NULL, &sn, 1, 1, 1, 0, cs97->n))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(be = mpz_export(NULL, &se, 1, 1, 1, 0, cs97->e))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bG = mpz_export(NULL, &sG, 1, 1, 1, 0, cs97->G))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bg = mpz_export(NULL, &sg, 1, 1, 1, 0, cs97->g))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(ba = mpz_export(NULL, &sa, 1, 1, 1, 0, cs97->a))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  /* To separate the different values, and be able to parse them later, we use
     the 'syntax': "'n='<n>'e='<e>'G='<G>'g='<g>'a='<a>'l='<lambda>'E='<epsilon>", 
     where the values between '' are printed in ASCII, and the <x> are the binary 
     data obtained above. Therefore, the total length of the group key will be 
     7*2+sn+se+sG+sg+sa+2*sizeof(uint64_t).
     @todo although does not seem very probable, it is possible that the binary 
     data of n, e, ... contains the ASCII codes of 'n=', 'e=', etc.. This will
     obviously lead to program malfunction...
  */

  sgroupkey = 7*2+sn+se+sG+sg+sa+2*sizeof(uint64_t);
  
  /* Copy everything into a unique array */
  if(!(bgroupkey = (byte_t *) malloc(sizeof(byte_t)*sgroupkey))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  memset(bgroupkey, 0, sgroupkey);

  offset = 0;

  bgroupkey[0] = 'n';
  bgroupkey[1] = '=';
  memcpy(&bgroupkey[2], bn, sn);
  offset += 2+sn;

  bgroupkey[offset] = 'e';
  bgroupkey[offset+1] = '=';
  memcpy(&bgroupkey[offset+2], be, se);
  offset += 2+se;

  bgroupkey[offset] = 'G';
  bgroupkey[offset+1] = '=';
  memcpy(&bgroupkey[offset+2], bG, sG);
  offset += 2+sG;

  bgroupkey[offset] = 'g';
  bgroupkey[offset+1] = '=';
  memcpy(&bgroupkey[offset+2], bg, sg);
  offset += 2+sg;

  bgroupkey[offset] = 'a';
  bgroupkey[offset+1] = '=';
  memcpy(&bgroupkey[offset+2], ba, sa);
  offset += 2+sa;

  /* For the previous fields, we have specified big endian order (most significant 
     bytes first), we follow that ordering here too. */
  bgroupkey[offset] = 'l';
  bgroupkey[offset+1] = '=';
  offset += 2;
  for(i=0; i<sizeof(uint64_t); i++) {
    bgroupkey[offset] = (cs97->lambda >> ((sizeof(uint64_t)-i-1)*8)) & 0xFF;
    offset++;
  }

  bgroupkey[offset] = 'E';
  bgroupkey[offset+1] = '=';
  offset += 2;
  for(i=0; i<sizeof(uint64_t); i++) {
    bgroupkey[offset] = (cs97->epsilon >> ((sizeof(uint64_t)-i-1)*8)) & 0xFF;
    offset++;
  }

  /* Convert it to a base64 string prepended with CS97_KEYS_GROUPKEY_MSG */
  if(sgroupkey != offset) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "Conversion failure\n");
    rc = IERROR;
    goto error;
  }

  cs97_datum.data = bgroupkey;
  cs97_datum.size = sgroupkey;

  if(!(cs97_b64 = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  if((rc = gnutls_pem_base64_encode_alloc(CS97_KEYS_GROUPKEY_MSG,
					  &cs97_datum,
					  cs97_b64)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_groupkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, gnutls_strerror(rc));
    rc = IERROR;
    goto error;    
  }
  
  /* We have the associated base64 string in cs97_b64. Print it and we are done. */
  fprintf(fd, "%s", cs97_b64->data);

  gnutls_free(cs97_b64);

 error:

  if(bn) { free(bn); bn = NULL; }
  if(be) { free(be); be = NULL; }
  if(bG) { free(bG); bG = NULL; }
  if(bg) { free(bg); bg = NULL; }
  if(ba) { free(ba); ba = NULL; }
  if(bgroupkey) { free(bgroupkey); bgroupkey = NULL; }

  return rc;

}

int cs97_keys_groupkey_parse_b64(int fd, cs97_groupkey_t *cs97, uint8_t *eof) {

  struct stat buf;
  gnutls_datum_t cs97_datum_b64, *cs97_datum;
  mpz_t n, e, G, g, a;
  byte_t *cs97_str_b64, *bn, *be, *bG, *bg, *ba;
  uint64_t i, offset, uread, lambda, epsilon;
  ssize_t sread;
  int rc;
  uint8_t finish;

  if(!cs97 || !eof) {
    fprintf(stderr, "Error in cs97_keys_groupkey_parse_b64 (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Get the next group key */
  if((rc = cs97_keys_key_get_next_b64(fd, CS97_KEYS_GROUPKEY_BEGIN_MSG,
				      CS97_KEYS_GROUPKEY_END_MSG, (char **) &cs97_str_b64,
				      eof)) == IERROR) {
    return IERROR;
  }

  /* Group key not found */
  if(rc == IFAIL) {
    return rc;
  }

  cs97_datum_b64.data = cs97_str_b64;
  cs97_datum_b64.size = strlen((char *)cs97_str_b64);

  /* 2) Parse from base64 to binary */
  if(!(cs97_datum = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_groupkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    return IERROR;    
  }

  if((rc = gnutls_pem_base64_decode_alloc(CS97_KEYS_GROUPKEY_MSG,
					  &cs97_datum_b64,
					  cs97_datum)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_groupkey_parse_b64 (%d): %s\n",
	    __LINE__, gnutls_strerror(rc));
    return IERROR;
  }

  /* The 'syntax' is: "'n='<n>'e='<e>'G='<G>'g='<g>'a='<a>'l='<lambda>'E='<epsilon>", 
     where the values between '' are printed in ASCII, and the <x> are the binary 
     data representing the corresponding parameters. Therefore, the total length of 
     the group key will be 7*2+sn+se+sG+sg+sa+2*sizeof(uint64_t). */
  offset = 0;
  if(cs97_datum->data[0] != 'n' && cs97_datum->data[1] != '=') {
    fprintf(stderr, "Error in cs97_keys_groupkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    return IERROR;
  }
  offset += 2;

  /* Read the 'n' data until we find the 'e=' */
  bn = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "e=", 2,
			     &bn, &uread, &finish) == IERROR) {
    return IERROR;    
  }

  mpz_init(n);
  mpz_import(n, uread, 1, 1, 1, 0, bn);
  offset += (uread+2);

  /* Read the 'e' data until we find the 'G=' */
  be = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "G=", 2, 
			     &be, &uread, &finish) == IERROR) {
    mpz_clear(n);
    return IERROR;    
  }

  mpz_init(e);
  mpz_import(e, uread, 1, 1, 1, 0, be);
  offset += (uread+2);

  /* Read the 'G' data until we find the 'g=' */
  bG = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "g=", 2, 
			     &bG, &uread, &finish) == IERROR) {
    mpz_clear(n);
    mpz_clear(e);
    return IERROR;    
  }

  mpz_init(G);
  mpz_import(G, uread, 1, 1, 1, 0, bG);
  offset += (uread+2);

  /* Read the 'g' data until we find the 'a=' */
  bg = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "a=", 2, 
			     &bg, &uread, &finish) == IERROR) {
    mpz_clear(n);
    mpz_clear(e);
    mpz_clear(G);
    return IERROR;    
  }

  mpz_init(g);
  mpz_import(g, uread, 1, 1, 1, 0, bg);
  offset += (uread+2);

  /* Read the 'a' data until we find the EOS */
  ba = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "l=", 2, 
			     &ba, &uread, &finish) == IERROR) {
    mpz_clear(n);
    mpz_clear(e);
    mpz_clear(G);
    mpz_clear(g);
    return IERROR;    
  }

  mpz_init(a);
  mpz_import(a, uread, 1, 1, 1, 0, ba);
  offset += (uread+2);

  /* For the previous fields, we have specified big endian order (most significant 
     bytes first), we follow that ordering here too. */
  lambda = 0;
  for(i=0; i<sizeof(uint64_t); i++) {
    lambda += (cs97_datum->data[offset] << ((sizeof(uint64_t)-i-1)*8));
    offset++;
  }
  
  if(cs97_datum->data[offset] != 'E' ||
     cs97_datum->data[offset+1] != '=') {
    fprintf(stderr, "Error in cs97_keys_groupkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    mpz_clear(n);
    mpz_clear(e);
    mpz_clear(G);
    mpz_clear(g);
    mpz_clear(a);
    return IERROR;
  }

  offset += 2;
  epsilon = 0;
  for(i=0; i<sizeof(uint64_t); i++) {
    epsilon += (cs97_datum->data[offset] << ((sizeof(uint64_t)-i-1)*8));
    offset++;
  }

  mpz_set(cs97->n, n); mpz_clear(n);
  mpz_set(cs97->e, e); mpz_clear(e);
  mpz_set(cs97->G, G); mpz_clear(G);
  mpz_set(cs97->g, g); mpz_clear(g);
  mpz_set(cs97->a, a); mpz_clear(a);
  cs97->lambda = lambda;
  cs97->epsilon = epsilon;
    
  return IOK;

}

int cs97_keys_managerkey_init(cs97_managerkey_t *cs97) {

  if(!cs97) {
    fprintf(stderr, "Error in cs97_managerkey_init (%d): %s\n", 
	    __LINE__, strerror(errno));
    return IERROR;
  }

  if(!(cs97->rsa = (rsa_keypair_t *) malloc(sizeof(rsa_keypair_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_init (%d): %s\n", 
	    __LINE__, strerror(errno));
    return IERROR;
  }

  if(rsa_keypair_init(cs97->rsa) == IERROR) {
    free(cs97->rsa); cs97->rsa = NULL;
    return IERROR;
  }

  if(!(cs97->group = (cs97_group_t *) malloc(sizeof(cs97_group_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_init (%d): %s\n", 
	    __LINE__, strerror(errno));
    rsa_keypair_free(cs97->rsa);
    free(cs97->rsa); cs97->rsa = NULL;
    return IERROR;
  }

  mpz_init(cs97->group->G);
  mpz_init(cs97->group->g);
  mpz_init(cs97->group->a);
  cs97->lambda = 0;
  cs97->epsilon = 0;

  return IOK;

}

int cs97_keys_managerkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group, 
				  uint64_t lambda, uint64_t epsilon, 
				  cs97_managerkey_t *cs97_mgr) {

  if(!rsa || !group || !cs97_mgr) {
    fprintf(stderr, "Error in cs97_keys_managerkey_init_set (%d): %s\n", __LINE__,
	    strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Set RSA keys */
  if(!(cs97_mgr->rsa = (rsa_keypair_t *) malloc(sizeof(rsa_keypair_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_init_set (%d): %s\n", __LINE__,
	    strerror(errno));
    return IERROR;
  }

  mpz_init_set(cs97_mgr->rsa->p, rsa->p);
  mpz_init_set(cs97_mgr->rsa->q, rsa->q);
  mpz_init_set(cs97_mgr->rsa->n, rsa->n);
  mpz_init_set(cs97_mgr->rsa->phin, rsa->phin);
  mpz_init_set(cs97_mgr->rsa->e, rsa->e);
  mpz_init_set(cs97_mgr->rsa->d, rsa->d);

  /* Set group */
  if(!(cs97_mgr->group = (cs97_group_t *) malloc(sizeof(cs97_group_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_init_set (%d): %s\n", __LINE__,
	    strerror(errno));
    free(cs97_mgr->rsa); cs97_mgr->rsa = NULL;
    mpz_clear(cs97_mgr->rsa->p);
    mpz_clear(cs97_mgr->rsa->q);
    mpz_clear(cs97_mgr->rsa->n);
    mpz_clear(cs97_mgr->rsa->phin);
    mpz_clear(cs97_mgr->rsa->e);
    mpz_clear(cs97_mgr->rsa->d);
    return IERROR;
  }

  mpz_init_set(cs97_mgr->group->G, group->G);
  mpz_init_set(cs97_mgr->group->g, group->g);
  mpz_init_set(cs97_mgr->group->a, group->a);
  cs97_mgr->lambda = lambda;
  cs97_mgr->epsilon = epsilon;

  return IOK;

}

int cs97_keys_managerkey_free(cs97_managerkey_t *cs97) {

  int rc;

  if(!cs97) {
    return IOK;
  }

  if((rc = rsa_keypair_free(cs97->rsa)) == IOK) {
    cs97->rsa = NULL;
  }
  
  if(cs97_setup_group_free(cs97->group) == IERROR) {
    return IERROR;
  }

  cs97->group = NULL;

  return rc;

}

int cs97_keys_managerkey_fprintf(FILE *fd, cs97_managerkey_t *cs97)  {

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  fprintf(fd, " Manager key:\n");
  fprintf(fd, " -----------\n\n");

  if(rsa_keypair_fprintf(fd, cs97->rsa) == IERROR) {
    return IERROR;
  }

  if(cs97_setup_group_fprintf(fd, cs97->group) ==  IERROR) {
    return IERROR;
  }

  fprintf(stderr, "lambda: %lu\n", cs97->lambda);
  fprintf(stderr, "epsilon: %lu\n\n", cs97->epsilon);

  return IOK;

}

int cs97_keys_managerkey_fprintf_b64(FILE *fd, cs97_managerkey_t *cs97) {

  gnutls_datum_t cs97_datum, *cs97_b64=NULL;
  byte_t *bp=NULL, *bq=NULL, *bn=NULL, *bphin=NULL, *be=NULL, *bd=NULL;
  byte_t *bG=NULL, *bg=NULL, *ba=NULL, *bmanagerkey=NULL;
  size_t sp, sq, sn, sphin, se, sd, sG, sg, sa, smanagerkey, offset;
  uint32_t i;
  int rc;

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;   
  }

  rc = IOK;

  /* Export the variables to binary data */
  if(!(bp = mpz_export(NULL, &sp, 1, 1, 1, 0, cs97->rsa->p))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(bq = mpz_export(NULL, &sq, 1, 1, 1, 0, cs97->rsa->q))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(bn = mpz_export(NULL, &sn, 1, 1, 1, 0, cs97->rsa->n))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(bphin = mpz_export(NULL, &sphin, 1, 1, 1, 0, cs97->rsa->phin))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(be = mpz_export(NULL, &se, 1, 1, 1, 0, cs97->rsa->e))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bd = mpz_export(NULL, &sd, 1, 1, 1, 0, cs97->rsa->d))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bG = mpz_export(NULL, &sG, 1, 1, 1, 0, cs97->group->G))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bg = mpz_export(NULL, &sg, 1, 1, 1, 0, cs97->group->g))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(ba = mpz_export(NULL, &sa, 1, 1, 1, 0, cs97->group->a))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  /* To separate the different values, and be able to parse them later, we use
     the 'syntax': "'p='<p>'q='<q>'n='<n>'f='<phin>'e='<e>'d='<d>'G='<G>'g='<g>
     'a='<a>'l='<lambda>'E='<epsilon>", where the values between '' are printed 
     in ASCII, and the <x> are the binary data obtained above. Therefore, the 
     total length of the manager key will be 11*2+sp+sq+sn+sphin+se+sd+sG+sg+sa+2*sizeof(uint64_t).
     @todo although does not seem very probable, it is possible that the binary 
     data of n, e, ... contains the ASCII codes of 'n=', 'e=', etc.. This will
     obviously lead to program malfunction...
  */

  smanagerkey = 11*2+sp+sq+sn+sphin+se+sd+sG+sg+sa+2*sizeof(uint64_t);
  
  /* Copy everything into a unique array */
  if(!(bmanagerkey = (byte_t *) malloc(sizeof(byte_t)*smanagerkey))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  memset(bmanagerkey, 0, smanagerkey);

  offset = 0;

  bmanagerkey[0] = 'p';
  bmanagerkey[1] = '=';
  memcpy(&bmanagerkey[2], bp, sp);
  offset += 2+sp;

  bmanagerkey[offset] = 'q';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bq, sq);
  offset += 2+sq;

  bmanagerkey[offset] = 'n';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bn, sn);
  offset += 2+sn;

  bmanagerkey[offset] = 'f';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bphin, sphin);
  offset += 2+sphin;

  bmanagerkey[offset] = 'e';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], be, se);
  offset += 2+se;

  bmanagerkey[offset] = 'd';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bd, sd);
  offset += 2+sd;

  bmanagerkey[offset] = 'G';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bG, sG);
  offset += 2+sG;

  bmanagerkey[offset] = 'g';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], bg, sg);
  offset += 2+sg;

  bmanagerkey[offset] = 'a';
  bmanagerkey[offset+1] = '=';
  memcpy(&bmanagerkey[offset+2], ba, sa);
  offset += 2+sa;

  /* For the previous fields, we have specified big endian order (most significant 
     bytes first), we follow that ordering here too. */
  bmanagerkey[offset] = 'l';
  bmanagerkey[offset+1] = '=';
  offset += 2;
  for(i=0; i<sizeof(uint64_t); i++) {
    bmanagerkey[offset] = (cs97->lambda >> ((sizeof(uint64_t)-i-1)*8)) & 0xFF;
    offset++;
  }

  bmanagerkey[offset] = 'E';
  bmanagerkey[offset+1] = '=';
  offset += 2;
  for(i=0; i<sizeof(uint64_t); i++) {
    bmanagerkey[offset] = (cs97->epsilon >> ((sizeof(uint64_t)-i-1)*8)) & 0xFF;
    offset++;
  }

  /* Convert it to a base64 string prepended with CS97_KEYS_MANAGERKEY_MSG */
  if(smanagerkey != offset) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "Conversion failure\n");
    rc = IERROR;
    goto error;
  }

  cs97_datum.data = bmanagerkey;
  cs97_datum.size = smanagerkey;

  if(!(cs97_b64 = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  if((rc = gnutls_pem_base64_encode_alloc(CS97_KEYS_MANAGERKEY_MSG,
					  &cs97_datum,
					  cs97_b64)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_managerkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, gnutls_strerror(rc));
    rc = IERROR;
    goto error;    
  }
  
  /* We have the associated base64 string in cs97_b64. Print it and we are done. */
  fprintf(fd, "%s", cs97_b64->data);

  gnutls_free(cs97_b64);

 error:

  if(bp) { free(bp); bp = NULL; }
  if(bq) { free(bq); bq = NULL; }
  if(bn) { free(bn); bn = NULL; }
  if(bphin) { free(bphin); bphin = NULL; }
  if(be) { free(be); be = NULL; }
  if(bd) { free(bd); bd = NULL; }
  if(bG) { free(bG); bG = NULL; }
  if(bg) { free(bg); bg = NULL; }
  if(ba) { free(ba); ba = NULL; }
  if(bmanagerkey) { free(bmanagerkey); bmanagerkey = NULL; }

  return rc;

}

int cs97_keys_managerkey_parse_b64(int fd, cs97_managerkey_t *cs97, uint8_t *eof) {

  struct stat buf;
  gnutls_datum_t cs97_datum_b64, *cs97_datum;
  mpz_t p, q, n, phin, e, d, G, g, a;
  byte_t *cs97_str_b64, *bp, *bq, *bn, *bphin, *be, *bd, *bG, *bg, *ba;
  uint64_t i, offset, uread, lambda, epsilon;
  ssize_t sread;
  int rc;
  uint8_t finish;

  if(!cs97 || !eof) {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Get the next manager key */
  if((rc = cs97_keys_key_get_next_b64(fd, CS97_KEYS_MANAGERKEY_BEGIN_MSG,
				      CS97_KEYS_MANAGERKEY_END_MSG, (char **) &cs97_str_b64,
				      eof)) == IERROR) {
    return IERROR;
  }

  /* Manager key not found */
  if(rc == IFAIL) {    
    return rc;
  }

  cs97_datum_b64.data = cs97_str_b64;
  cs97_datum_b64.size = strlen((char *)cs97_str_b64);

  /* 2) Parse from base64 to binary */
  if(!(cs97_datum = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    return IERROR;    
  }

  if((rc = gnutls_pem_base64_decode_alloc(CS97_KEYS_MANAGERKEY_MSG,
					  &cs97_datum_b64,
					  cs97_datum)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, gnutls_strerror(rc));
    return IERROR;
  }

  /* The 'syntax' is: "'p='<p>'q='<q>'n='<n>'f='<phin>'e='<e>'d='<d>'G='<G>'g='<g>
     'a='<a>'l='<lambda>'E='<epsilon>", where the values between '' are printed in 
     ASCII, and the <x> are the binary data representing the corresponding 
     parameters. Therefore, the total length of the manager key will be 
     11*2+sp+sq+sn+sphin+se+sd+sG+sg+sa+2*sizeof(uint64_t). */
  offset = 0;
  if(cs97_datum->data[0] != 'p' && cs97_datum->data[1] != '=') {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    return IERROR;
  }
  offset += 2;

  /* Read the 'p' data until we find the 'q=' */
  bp = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "q=", 2,
			     &bp, &uread, &finish) == IERROR) {
    return IERROR;    
  }

  mpz_init(p);
  mpz_import(p, uread, 1, 1, 1, 0, bp);
  offset += (uread+2);

  /* Read the 'q' data until we find the 'n=' */
  bq = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "n=", 2,
			     &bq, &uread, &finish) == IERROR) {
    mpz_clear(p);
    return IERROR;    
  }

  mpz_init(q);
  mpz_import(q, uread, 1, 1, 1, 0, bq);
  offset += (uread+2);

  /* Read the 'n' data until we find the 'f=' */
  bn = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "f=", 2,
			     &bn, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    return IERROR;    
  }

  mpz_init(n);
  mpz_import(n, uread, 1, 1, 1, 0, bn);
  offset += (uread+2);

  /* Read the 'f' data until we find the 'e=' */
  bphin = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "e=", 2,
			     &bphin, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n);
    return IERROR;    
  }

  mpz_init(phin);
  mpz_import(phin, uread, 1, 1, 1, 0, bphin);
  offset += (uread+2);

  /* Read the 'e' data until we find the 'd=' */
  be = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "d=", 2, 
			     &be, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    return IERROR;    
  }

  mpz_init(e);
  mpz_import(e, uread, 1, 1, 1, 0, be);
  offset += (uread+2);

  /* Read the 'd' data until we find the 'G=' */
  bd = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "G=", 2, 
			     &bd, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e);
    return IERROR;    
  }

  mpz_init(d);
  mpz_import(d, uread, 1, 1, 1, 0, bd);
  offset += (uread+2);

  /* Read the 'G' data until we find the 'g=' */
  bG = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "g=", 2, 
			     &bG, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e); mpz_clear(d);
    return IERROR;    
  }

  mpz_init(G);
  mpz_import(G, uread, 1, 1, 1, 0, bG);
  offset += (uread+2);

  /* Read the 'g' data until we find the 'a=' */
  bg = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "a=", 2, 
			     &bg, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e); mpz_clear(d);
    mpz_clear(G);
    return IERROR;    
  }

  mpz_init(g);
  mpz_import(g, uread, 1, 1, 1, 0, bg);
  offset += (uread+2);

  /* Read the 'a' data until we find the 'l=' */
  ba = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "l=", 2, 
			     &ba, &uread, &finish) == IERROR) {
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e); mpz_clear(d);
    mpz_clear(G); mpz_clear(g);
    return IERROR;    
  }

  mpz_init(a);
  mpz_import(a, uread, 1, 1, 1, 0, ba);
  offset += (uread+2);

  /* For the previous fields, we have specified big endian order (most significant 
     bytes first), we follow that ordering here too. */
  lambda = 0;
  for(i=0; i<sizeof(uint64_t); i++) {
    lambda += (cs97_datum->data[offset] << ((sizeof(uint64_t)-i-1)*8));
    offset++;
  }
  
  if(cs97_datum->data[offset] != 'E' ||
     cs97_datum->data[offset+1] != '=') {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid group key file");
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e); mpz_clear(d);
    mpz_clear(G); mpz_clear(g); 
    mpz_clear(a);
    return IERROR;
  }

  offset += 2;
  epsilon = 0;
  for(i=0; i<sizeof(uint64_t); i++) {
    epsilon += (cs97_datum->data[offset] << ((sizeof(uint64_t)-i-1)*8));
    offset++;
  }

  /* Initialize and set the RSA keypair */
  if(!(cs97->rsa = (rsa_keypair_t *) malloc(sizeof(rsa_keypair_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    mpz_clear(p); mpz_clear(q);
    mpz_clear(n); mpz_clear(phin);
    mpz_clear(e); mpz_clear(d);
    mpz_clear(G); mpz_clear(g); 
    mpz_clear(a);
    return IERROR;
  }

  mpz_init_set(cs97->rsa->p, p); mpz_clear(p);
  mpz_init_set(cs97->rsa->q, q); mpz_clear(q);
  mpz_init_set(cs97->rsa->n, n); mpz_clear(n);
  mpz_init_set(cs97->rsa->phin, phin); mpz_clear(phin);
  mpz_init_set(cs97->rsa->e, e); mpz_clear(e);
  mpz_init_set(cs97->rsa->d, d); mpz_clear(d);

  /* Initialize and set the group */
  if(!(cs97->group = (cs97_group_t *) malloc(sizeof(cs97_group_t)))) {
    fprintf(stderr, "Error in cs97_keys_managerkey_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    rsa_keypair_free(cs97->rsa);
    cs97->rsa = NULL;
    return IERROR;
  }

  mpz_init_set(cs97->group->G, G); mpz_clear(G);
  mpz_init_set(cs97->group->g, g); mpz_clear(g);
  mpz_init_set(cs97->group->a, a); mpz_clear(a);
  cs97->lambda = lambda;
  cs97->epsilon = epsilon;
    
  return IOK;

}

int cs97_keys_memberkey_init(cs97_memberkey_t *cs97) {

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_memberkey_init (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  mpz_init(cs97->x);
  mpz_init(cs97->y);
  mpz_init(cs97->z);
  mpz_init(cs97->v);
  cs97->member = NULL;

  return IOK;

}

int cs97_keys_memberkey_copy(cs97_memberkey_t *dst, cs97_memberkey_t *src) {
  
  if(!dst || !src) {
    fprintf(stderr, "Error in cs97_keys_memberkey_copy (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  mpz_set(dst->x, src->x);
  mpz_set(dst->y, src->y);
  mpz_set(dst->z, src->z);
  mpz_set(dst->v, src->v);
  if(src->member) {
    if(!(dst->member = strdup(src->member))) {
      fprintf(stderr, "Error in cs97_keys_memberkey_copy (%d): %s\n", 
	      __LINE__, strerror(errno));
      return IERROR;
    }
  }

  return IOK;

}

int cs97_keys_memberkey_free(cs97_memberkey_t *cs97) {

  if(!cs97) {
    return IOK;
  }

  mpz_clear(cs97->x);
  mpz_clear(cs97->y);
  mpz_clear(cs97->z);
  mpz_clear(cs97->v);
  if(cs97->member) { free(cs97->member); cs97->member = NULL; }

  return IOK;

}

int cs97_keys_memberkey_fprintf(FILE *fd, cs97_memberkey_t *cs97) {
  
  char *sx, *sy, *sz, *sv;

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  sx = mpz_get_str(NULL, 10, cs97->x);
  sy = mpz_get_str(NULL, 10, cs97->y);
  sz = mpz_get_str(NULL, 10, cs97->z);
  sv = mpz_get_str(NULL, 10, cs97->v);

  fprintf(fd, " Group member key:\n");
  fprintf(fd, " ----------------\n");
  if(cs97->member) {
    fprintf(fd, " Member: %s\n", cs97->member);
  }
  fprintf(fd, " x: %s\n", sx);
  fprintf(fd, " y: %s\n", sy);
  fprintf(fd, " z: %s\n", sz);
  fprintf(fd, " v: %s\n\n", sv);
  
  free(sx); sx = NULL;
  free(sy); sy = NULL;
  free(sz); sz = NULL;
  free(sv); sv = NULL;

  return IOK;

}

int cs97_keys_memberkey_fprintf_b64(FILE *fd, cs97_memberkey_t *cs97) {

  gnutls_datum_t cs97_datum, *cs97_b64=NULL;
  byte_t *bx=NULL, *by=NULL, *bz=NULL, *bv=NULL, *bmemberkey=NULL;
  size_t sx, sy, sz, sv, smemberkey, offset;
  uint32_t i;
  int rc;

  if(!cs97) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;   
  }

  rc = IOK;
  
  /* Export the variables to binary data */
  if(!(bx = mpz_export(NULL, &sx, 1, 1, 1, 0, cs97->x))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  if(!(by = mpz_export(NULL, &sy, 1, 1, 1, 0, cs97->y))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bz = mpz_export(NULL, &sz, 1, 1, 1, 0, cs97->z))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  /* During the process of member key generation, v may be 0, and mpz_export
     does not print anything in this case. We just skip it.*/
  if(!mpz_cmp_ui(cs97->v, 0)) {
    sv = 0;
  } else {
    if(!(bv = mpz_export(NULL, &sv, 1, 1, 1, 0, cs97->v))) {
      fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	      __LINE__, "mpz_export.");
      rc = IERROR;
      goto error;
    }
  }

  /* To separate the different values, and be able to parse them later, we use
     the 'syntax': "'x='<x>'y='<y>'z='<z>'v='<v>", 
     where the values between '' are printed in ASCII, and the <a> are the binary 
     data obtained above. Therefore, the total length of the member key will be 
     4*2+sx+sy+sz+sv.
     @todo although does not seem very probable, it is possible that the binary 
     data of x, y, ... contains the ASCII codes of 'x=', 'y=', etc.. This will
     obviously lead to program malfunction...
  */

  smemberkey = 4*2+sx+sy+sz+sv+3;
  
  /* Copy everything into a unique array */
  if(!(bmemberkey = (byte_t *) malloc(sizeof(byte_t)*smemberkey))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  memset(bmemberkey, 0, smemberkey);

  offset = 0;

  bmemberkey[0] = 'x';
  bmemberkey[1] = '=';
  memcpy(&bmemberkey[2], bx, sx);
  offset += 2+sx;

  bmemberkey[offset] = 'y';
  bmemberkey[offset+1] = '=';
  memcpy(&bmemberkey[offset+2], by, sy);
  offset += 2+sy;

  bmemberkey[offset] = 'z';
  bmemberkey[offset+1] = '=';
  memcpy(&bmemberkey[offset+2], bz, sz);
  offset += 2+sz;

  bmemberkey[offset] = 'v';
  bmemberkey[offset+1] = '=';
  if(sv) {
    memcpy(&bmemberkey[offset+2], bv, sv);
  }
  offset += 2+sv; 

  /** @todo Change this botch! */
  bmemberkey[offset++] = 'E';
  bmemberkey[offset++] = 'O';
  bmemberkey[offset++] = 'S';

  /* Convert it to a base64 string prepended with CS97_KEYS_MEMBERKEY_MSG */
  if(smemberkey != offset) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n",
	    __LINE__, "Conversion failure\n");
    rc = IERROR;
    goto error;
  }

  cs97_datum.data = bmemberkey;
  cs97_datum.size = smemberkey;

  if(!(cs97_b64 = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  if((rc = gnutls_pem_base64_encode_alloc(CS97_KEYS_MEMBERKEY_MSG,
					  &cs97_datum,
					  cs97_b64)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_memberkey_fprintf_b64 (%d): %s\n", 
	    __LINE__, gnutls_strerror(rc));
    rc = IERROR;
    goto error;    
  }
  
  /* We have the associated base64 string in cs97_b64. Print it and we are done. */
  fprintf(fd, "%s", cs97_b64->data);

  gnutls_free(cs97_b64);

 error:

  if(bx) { free(bx); bx = NULL; }
  if(by) { free(by); by = NULL; }
  if(bz) { free(bz); bz = NULL; }
  if(bv) { free(bv); bv = NULL; }
  if(bmemberkey) { free(bmemberkey); bmemberkey = NULL; }

  return rc;

}

int cs97_keys_memberkey_parse_b64(int fd, cs97_memberkey_t *cs97, uint8_t *eof) {

  struct stat buf;
  gnutls_datum_t cs97_datum_b64, *cs97_datum;
  mpz_t x, y, z, v;
  byte_t *cs97_str_b64, *bx, *by, *bz, *bv;
  uint64_t i, offset, uread;
  ssize_t sread;
  int rc;
  uint8_t finish;

  if(!cs97 || !eof) {
    fprintf(stderr, "Error in cs97_keys_memberkey_parse_b64 (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Get the next member key */
  if((rc = cs97_keys_key_get_next_b64(fd, CS97_KEYS_MEMBERKEY_BEGIN_MSG,
				      CS97_KEYS_MEMBERKEY_END_MSG, (char **) &cs97_str_b64,
				      eof)) == IERROR) {
    return IERROR;
  }

  /* Member key not found */
  if(rc == IFAIL) {
    return rc;
  }

  cs97_datum_b64.data = cs97_str_b64;
  cs97_datum_b64.size = strlen((char *)cs97_str_b64);

  /* 2) Parse from base64 to binary */
  if(!(cs97_datum = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_keys_memberkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid member key file");
    return IERROR;    
  }

  if((rc = gnutls_pem_base64_decode_alloc(CS97_KEYS_MEMBERKEY_MSG,
					  &cs97_datum_b64,
					  cs97_datum)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_keys_memberkey_parse_b64 (%d): %s\n",
	    __LINE__, gnutls_strerror(rc));
    return IERROR;
  }

  /* The 'syntax' is: "'x='<x>'y='<y>'z='<z>'v='<v>", 
     where the values between '' are printed in ASCII, and the <a> are the binary 
     data representing the corresponding parameters. Therefore, the total length of 
     the member key will be 4*2+sx+sy+sz+sv. */
  offset = 0;
  if(cs97_datum->data[0] != 'x' && cs97_datum->data[1] != '=') {
    fprintf(stderr, "Error in cs97_keys_memberkey_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid member key file");
    return IERROR;
  }
  offset += 2;

  /* Read the 'x' data until we find the 'y=' */
  bx = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "y=", 2, 
			     &bx, &uread, &finish) == IERROR) {
    return IERROR;    
  }

  mpz_init(x);
  mpz_import(x, uread, 1, 1, 1, 0, bx);
  offset += (uread+2);

  /* Read the 'y' data until we find the 'z=' */
  by = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "z=", 2, 
			     &by, &uread, &finish) == IERROR) {
    mpz_clear(x);
    return IERROR;    
  }

  mpz_init(y);
  mpz_import(y, uread, 1, 1, 1, 0, by);
  offset += (uread+2);

  /* Read the 'z' data until we find the 'v=' */
  bz = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "v=", 2, 
			     &bz, &uread, &finish) == IERROR) {
    mpz_clear(x);
    mpz_clear(y);
    return IERROR;    
  }

  mpz_init(z);
  mpz_import(z, uread, 1, 1, 1, 0, bz);
  offset += (uread+2);

  /* Read the 'v' data until we find the 'EOS' */
  bv = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "EOS", 3, 
			     &bv, &uread, &finish) == IERROR) {
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(z);
    return IERROR;    
  }

  mpz_init(v);
  mpz_import(v, uread, 1, 1, 1, 0, bv);
  offset += (uread+3);

  mpz_set(cs97->x, x); mpz_clear(x);
  mpz_set(cs97->y, y); mpz_clear(y);
  mpz_set(cs97->z, z); mpz_clear(z);
  mpz_set(cs97->v, v); mpz_clear(v);
    
  return IOK;

}

int cs97_keys_key_get_next_b64(int fd, char *begin, char *end, 
			       char **key_b64, uint8_t *eof) {

  struct stat buf;
  char *haystack, *bneedle, *eneedle, *key;
  size_t haystacklen, bneedle_start, eneedle_start, key_b64_len, trail_len;
  size_t prepend_len;
  ssize_t sread;
  off_t cur;

  if(!begin || !end || !key_b64 || !eof) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Get the size of the file minus the current position of the file descriptor: 
     that will be the maximum size of the string */
  if(fstat(fd, &buf) == -1) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  if((cur = lseek(fd, 0, SEEK_CUR)) == (off_t) -1) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  haystacklen = buf.st_size - cur;

  /* We might well already be at the end of the file... */
  if(!haystacklen) {
    *eof = MISC_EOF;
    return IFAIL;
  }

  /* Allocate memory for the "haystack" */
  if(!(haystack = (char *) malloc(sizeof(char)*haystacklen+1))) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }
  
  memset(haystack, 0, haystacklen+1);

  /* Fill the haystack */
  if((sread = read(fd, haystack, haystacklen)) == -1) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    free(haystack); haystack = NULL;
    return IERROR;    
  }
  
  if((size_t) sread != haystacklen) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    free(haystack); haystack = NULL;
    return IERROR;    
  }

  /* Look for the next occurrence of the string in begin, that will be the 
     line were the current group key begins. */
  if(!(bneedle = strstr(haystack, begin))) {
    free(haystack); haystack = NULL;
    *eof = MISC_EOF;
    return IFAIL;
  }

  /* Look for the next occurrence of the string in end, that will be the 
     line were the current group key ends. */
  if(!(eneedle = strstr(haystack, end))) {
    free(haystack); haystack = NULL;
    *eof = MISC_EOF;
    return IFAIL;
  }

  /* To the "BEGIN CS97 {GROUP|MANAGER|MEMBER}KEY" message, we have to add
     the prepending "-"s. Currently, GNUTLS uses exactly 5 "-". Nevertheless
     it seems more robust to me to read until the previous '\n' or '\r' or
     SOF. */
  prepend_len = 1;
  bneedle_start = strlen(haystack)-strlen(bneedle);  
  while(bneedle_start-prepend_len >= 1 && 
	haystack[bneedle_start-prepend_len] != '\n' &&
	haystack[bneedle_start-prepend_len] != '\r') {
    prepend_len++;
  }

  /* Once we have found the "END CS97 {GROUP|MANAGER|MEMBER}KEY" message, we have
     to skip the trailing "-----". Currently, GNUTLS uses exactly 5 "-". Nevertheless
     it seems more robust to me to read until the next '\n' or '\r' */
  trail_len = 0;
  eneedle_start = strlen(haystack)-strlen(eneedle);
  while(haystack[eneedle_start+strlen(end)+trail_len] != '\n' &&
	haystack[eneedle_start+strlen(end)+trail_len] != '\r') {
  /* while(haystack[strlen(eneedle)+trail_len-1] != '\n' && */
  /* 	haystack[strlen(eneedle)+trail_len-1] != '\r') { */
    trail_len++;
  }
  
  /* Include the '\n' or '\r' */
  trail_len++;

  /* Now we now that a (potentially) well encoded group key is present, 
     dump JUST IT into a string. */
  key_b64_len = prepend_len + strlen(bneedle) - strlen(eneedle) + 
    strlen(end) + trail_len;
  
  if(!(key = (char *) malloc(sizeof(char)*key_b64_len+1))) {
    fprintf(stderr, "Error in cs97_keys_key_get_next_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    free(haystack); haystack = NULL;
    return IERROR;
  }

  memset(key, 0, key_b64_len+1);
  memcpy(key, &haystack[bneedle_start-prepend_len], key_b64_len);

  free(haystack); haystack = NULL;

  /* Position the fd immediatly after the group key */
  if(lseek(fd, cur+bneedle_start+key_b64_len-prepend_len, SEEK_SET) == (off_t) -1) {
    return IERROR;
  }

  /*See if we have reached EOF */
  if((off_t)(cur+bneedle_start+key_b64_len-prepend_len) == buf.st_size) {
    *eof = MISC_EOF;
  } else {
    *eof = 0;
  }  

  *key_b64 = key;

  return IOK;

}

int cs97_keys_get_keyfile_type(char *filename, int *type) {

  struct stat buf;
  char *filestr, *grpstr, *mgrstr, *memstr;
  ssize_t r;
  size_t grp_len, mgr_len, mem_len;
  int fd;

  if(!filename || !type) {
    fprintf(stderr, "Error in cs97_keys_get_keyfile_type (%d): %s\n",
	    __LINE__, strerror(errno));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Dump the file into a string */
  if((fd = open(filename, O_RDONLY)) == -1) {
    fprintf(stderr, "Error in cs97_keys_getkeyfile_type (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }
  
  if(fstat(fd, &buf) == -1) {
    fprintf(stderr, "Error in cs97_keys_getkeyfile_type (%d): %s\n",
	    __LINE__, strerror(errno));
    close(fd);
    return IERROR;
  }

  if(!(filestr = (char *) malloc(sizeof(char)*buf.st_size+1))) {
    fprintf(stderr, "Error in cs97_keys_getkeyfile_type (%d): %s\n",
	    __LINE__, strerror(errno));
    close(fd);
    return IERROR;
  }

  memset(filestr, 0, buf.st_size+1);

  if((r = read(fd, filestr, buf.st_size)) == -1) {
    fprintf(stderr, "Error in cs97_keys_getkeyfile_type (%d): %s\n",
	    __LINE__, strerror(errno));
    free(filestr); filestr = NULL;
    close(fd);
    return IERROR;
  }

  if(r != buf.st_size) {
    fprintf(stderr, "Error in cs97_keys_getkeyfile_type (%d): %s\n",
	    __LINE__, strerror(EBADF));
    errno = EBADF;
    free(filestr); filestr = NULL;
    close(fd);
    return IERROR;
  }
  
  close(fd);

  /* 2) Look for the first key message in the file */
  grpstr = strstr(filestr, CS97_KEYS_GROUPKEY_MSG);
  mgrstr = strstr(filestr, CS97_KEYS_MANAGERKEY_MSG);  
  memstr = strstr(filestr, CS97_KEYS_MEMBERKEY_MSG);

  if(!grpstr && !mgrstr && !memstr) {
    free(filestr); filestr = NULL;
    return IFAIL;
  }

  if(grpstr || mgrstr || memstr) {

    grp_len = 0;
    mgr_len = 0;
    mem_len = 0;

    if(grpstr) grp_len = strlen(grpstr);
    if(mgrstr) mgr_len = strlen(mgrstr);
    if(memstr) mem_len = strlen(memstr);

    /* The larger will be the first */
    if(grp_len > mgr_len) {
      if(grp_len > mem_len) { /* grpstr */
	*type = CS97_GROUPKEY_TYPE;
      } else { /* memstr */
	*type = CS97_MEMBERKEY_TYPE;
      }
    } else {
      if(mgr_len > mem_len) { /* mgrstr */
	*type = CS97_MANAGERKEY_TYPE;
      } else { /* memstr */
	*type = CS97_MEMBERKEY_TYPE;
      }
    }

  }

  free(filestr); filestr = NULL;

  return IOK;

}

int cs97_keys_get_keyfilename(char *pattern, uint8_t cs97_keytype, char **keyfilename) {

  uint16_t len;

  if(!pattern || !keyfilename) {
    fprintf(stderr, "Error in cs97_keys_get_keyfilename (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  len = 0;

  switch(cs97_keytype) {

  case CS97_MANAGERKEY_TYPE: /* Manager key */
    if(!(*keyfilename)) {
      len = strlen(pattern)+strlen(CS97_MANAGERKEY_SUFFIX)+2;
      if(!(*keyfilename = (char *) malloc(sizeof(char*)*len))){
	fprintf(stderr, "Error in cs97_keys_get_keyfilename (%d): %s\n",
		__LINE__, strerror(errno));
	return IERROR;
      }
      memset(*keyfilename, 0, len);
    } 
    sprintf(*keyfilename, "%s%s", pattern, CS97_MANAGERKEY_SUFFIX);
    break;

  case CS97_GROUPKEY_TYPE: /* Group key */
    if(!(*keyfilename)) {
      len = strlen(pattern)+strlen(CS97_GROUPKEY_SUFFIX)+2;
      if(!(*keyfilename = (char *) malloc(sizeof(char*)*len))){
	fprintf(stderr, "Error in cs97_keys_get_keyfilename (%d): %s\n",
		__LINE__, strerror(errno));
	return IERROR;
      }
      memset(*keyfilename, 0, len);
    } 
    sprintf(*keyfilename, "%s%s", pattern, CS97_GROUPKEY_SUFFIX);
    break;

  case CS97_MEMBERKEY_TYPE: /* Member key */
    if(!(*keyfilename)) {
      len = strlen(pattern)+strlen(CS97_MEMBERKEY_SUFFIX)+2;
      if(!(*keyfilename = (char *) malloc(sizeof(char*)*len))){
	fprintf(stderr, "Error in cs97_keys_get_keyfilename (%d): %s\n",
		__LINE__, strerror(errno));
	return IERROR;
      }
      memset(*keyfilename, 0, len);
    } 
    sprintf(*keyfilename, "%s%s", pattern, CS97_MEMBERKEY_SUFFIX);
    break;

  default: /* Invalid type */
    errno = EINVAL;
    return IERROR;
  }

  return IOK;
}

/* cs97_keys.c ends here */
