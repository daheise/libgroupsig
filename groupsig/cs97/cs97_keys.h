/*                               -*- Mode: C -*- 
 * @file: cs97_keys.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar abr 24 17:21:39 2012 (+0200)
 * @version: 
 * Last-Updated: mar may  8 14:23:24 2012 (+0200)
 *           By: jesus
 *     Update #: 68
 * URL: 
 */

#ifndef _CS97_KEYS_H
#define _CS97_KEYS_H

#include <stdio.h>

#include "rsa.h"

/**
 * CS97_KEYS_GROUPKEY_MSG
 * The string to prepend to headers of files containing C97 group keys
 */
#define CS97_KEYS_GROUPKEY_MSG "CS97 GROUPKEY"

/**
 * CS97_KEYS_GROUPKEY_BEGIN_MSG
 * Begin string to prepend to headers of files containing C97 group keys
 */
#define CS97_KEYS_GROUPKEY_BEGIN_MSG "BEGIN CS97 GROUPKEY"

/**
 * CS97_KEYS_GROUPKEY_END_MSG
 * End string to prepend to headers of files containing C97 group keys
 */
#define CS97_KEYS_GROUPKEY_END_MSG "END CS97 GROUPKEY"

/**
 * CS97_KEYS_MANAGER_MSG
 * The string to prepend to headers of files containing C97 manager keys
 */
#define CS97_KEYS_MANAGERKEY_MSG "CS97 MANAGERKEY"

/**
 * CS97_KEYS_MANAGER_BEGIN_MSG
 * Begin string to prepend to headers of files containing C97 manager keys
 */
#define CS97_KEYS_MANAGERKEY_BEGIN_MSG "BEGIN CS97 MANAGERKEY"

/**
 * CS97_KEYS_MANAGER_END_MSG
 * End string to prepend to headers of files containing C97 manager keys
 */
#define CS97_KEYS_MANAGERKEY_END_MSG "END CS97 MANAGERKEY"

/**
 * CS97_KEYS_MEMBERKEY_MSG
 * The string to prepend to headers of files containing C97 member keys
 */
#define CS97_KEYS_MEMBERKEY_MSG "CS97 MEMBERKEY"

/**
 * CS97_KEYS_MEMBERKEY_BEGIN_MSG
 * Begin string to prepend to headers of files containing C97 member keys
 */
#define CS97_KEYS_MEMBERKEY_BEGIN_MSG "BEGIN CS97 MEMBERKEY"

/**
 * CS97_KEYS_MEMBERKEY_END_MSG
 * End string to prepend to headers of files containing C97 member keys
 */
#define CS97_KEYS_MEMBERKEY_END_MSG "END CS97 MEMBERKEY"

/**
 * CS97_KEYS_DEFAULT_KEYLEN_B2
 * The default key length (in bits) when represented (it is just used for parsing 
 * key files, for now). I just hate using not constant-defined numbers in function 
 * calls...
 */
#define CS97_KEYS_DEFAULT_KEYLEN_B2 1024

/**
 * CS97_KEYS_MAX_KEYLEN
 * Specifies the maximum (default) key length for group, manager, and member keys.
 * This will be used for calling gnutls_pem_base64_decode and encode functions.
 */
#define CS97_KEYS_MAX_KEYLEN 16384

/**
 * @def CS97_MANAGERKEY_TYPE 
 * Defines the type for CS97 manager keys.
 */
#define CS97_MANAGERKEY_TYPE 0

/**
 * @def CS97_GROUPKEY_TYPE 
 * Defines the type for CS97 group keys.
 */
#define CS97_GROUPKEY_TYPE 1

/**
 * @def CS97_MEMBERKEY_TYPE 
 * Defines the type for CS97 member keys.
 */
#define CS97_MEMBERKEY_TYPE 2

/**
 * @def CS97_MANAGERKEY_SUFFIX
 * Defines the suffix for CS97 manager keys.
 */
#define CS97_MANAGERKEY_SUFFIX ".Mkey"

/**
 * @def CS97_GROUPKEY_SUFFIX
 * Defines the suffix for CS97 group keys.
 */
#define CS97_GROUPKEY_SUFFIX ".gkey"

/**
 * @def CS97_MEMBERKEY_SUFFIX
 * Defines the suffix for CS97 member keys.
 */
#define CS97_MEMBERKEY_SUFFIX ".mkey"

/*
 * Reminder of gnutls_datum_t (defined in <gnutls/gnutls.h>)
 * typedef struct {
 *   unsigned char *data;
 *   unsigned int size;
 * } gnutls_datum_t;
 */

/**
 * Defines the structure to use for CS97 groups.
 */
typedef struct _cs97_group_t {
  mpz_t G; /**< The group modulus. */
  mpz_t g; /**< The group generator (The 'g' base). */
  mpz_t a; /**< An element a in Z_n* (The 'a' base). */
} cs97_group_t;

/**
 * Defines a structure containing all the elements generated during the setup
 * of a CS97 group. 
 */
typedef struct _cs97_managerkey_t {
  rsa_keypair_t *rsa; /**< The generated rsa keypair. */
  cs97_group_t *group; /**< The generated group parameters. */
  uint64_t lambda; /**< The lambda parameter for SKLOGLOG */
  uint64_t epsilon; /**< The epsilon parameter for SKLOGLOG. */
} cs97_managerkey_t;

/**
 * Defines the structure for representing group keys in the CS97 scheme.
 */
typedef struct _cs97_groupkey_t {
  mpz_t n; /**< The subgroup order (equals the RSA modulus) */
  mpz_t e; /**< The 'e' RSA exponent. */
  mpz_t G; /**< The element defining the group (i.e., the prime defining the
              group containing the subgroup of order n) */
  mpz_t g; /**< The first base, generator of the subgroup. */
  mpz_t a; /**< An element in [1,n], used as second base (base 'a') in some
	      signatures of knowledge. */
  uint64_t lambda; /**< The lambda parameter for SKLOGLOG. */
  uint64_t epsilon; /**< The epsilon parameter for SKLOGLOG. */
} cs97_groupkey_t;

/**
 * Defines the structure of CS97 group member keys. 
 * @todo Should we also include here the sign(y) and 
 *  SK{ alpha : y = a^alpha (mod n) & z = g^y }?
 */
typedef struct _cs97_memberkey_t {
  mpz_t x; /**< The member secret key. */
  mpz_t y; /**< y = a^x (mod n), where a,n are part of the group key. */
  mpz_t z; /**< z = g^y (mod |G|), where g is part of the group key.
	      This is called in CS97 the "membership key". */
  mpz_t v; /**< v = (y+r)^(1/e) (mod |G|), where r is a random integer.
	      This is called in CS97 the "membership certificate". */
  char *member; /**< This is just "for aesthetics". Mainly, this name
                   will be printed when opening a signature. */
} cs97_memberkey_t;

/** 
 * @fn int cs97_keys_groupkey_init(cs97_groupkey_t *cs97)
 * @brief Allocates memory for the variables of the received CS97 group key.
 * 
 * @param[in] cs97 The CS97 group key to initialize.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_groupkey_init(cs97_groupkey_t *cs97);

/** 
 * @fn int cs97_keys_groupkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group,
 *                                     uint64_t lambda, uint64_t epsilon,
 *                                     cs97_groupkey_t *cs97_grp)
 * @brief Copies the information in cs97_kp corresponding to the group key and
 *  stores it in a group key.
 * 
 * @param[in] rsa The RSA keypair.
 * @param[in] group The CS97 group.
 * @param[in] lambda The lambda parameter for SKLOGLOG.
 * @param[in] epsilon The epsilon parameter for SKLOGLOG.
 * @param[in,out] cs97_grp The group key to fill.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_groupkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group, 
				uint64_t lambda, uint64_t epsilon, 
				cs97_groupkey_t *cs97_grp);

/** 
 * @fn int cs97_keys_groupkey_free(cs97_groupkey_t *cs97)
* @brief Frees the memory allocated for the received CS97 group key
 * 
 * @param[in] cs97 The CS97 group key to free.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_groupkey_free(cs97_groupkey_t *cs97);

/** 
 * @fn int cs97_keys_group_fprintf(FILE *fd, cs97_groupkey_t *cs97)
 * @brief Prints the given CS97 group key into the specified file descriptor.
 *
 * @param[in] fd The file descriptor.
 * @param[in] cs97 The CS97 group key.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_groupkey_fprintf(FILE *fd, cs97_groupkey_t *cs97);

/** 
 * @fn int cs97_keys_groupkey_fprintf_b64(FILE *fd, cs97_groupkey_t *cs97)
 * @brief Prints into the specified file descriptor the given group key,
 *  in base64 encoding.
 * 
 * @param[in] fd The output file descriptor.
 * @param[in] cs97 The group key.
 * 
 * @todo Adapt this function to DER encoding!
 * 
 * @return IOK or IERROR
 */
int cs97_keys_groupkey_fprintf_b64(FILE *fd, cs97_groupkey_t *cs97);

/** 
 * @fn int cs97_keys_groupkey_parse_b64(int fd, cs97_groupkey_t *cs97, uint8_t *eof)
 * @brief Parses the given file, which must contain a valid representation
 *  of a CS97 group key (i.e., it must follow the format produced in 
 *  cs97_keys_groupkey_fprintf_b64) and stores the result in the given
 *  structure.
 *
 * @param[in] fd The file descriptor.
 * @param[in,out] cs97 The CS97 group key structure to fill.
 * @param[in,out] eof Will be set to MISC_EOF if EOF is reached
 * 
 * @todo Adapt this function to DER encoding!
 * 
 * @return IOK with eof = 0 if a key was found but EOF was not reached, or
 *  eof = MISC_EOF if a key was found and EOF was reached. IERROR if an error
 *  occurred. IFAIL with eof = MISC_EOF if no key was found.
 */
int cs97_keys_groupkey_parse_b64(int fd, cs97_groupkey_t *cs97, uint8_t *eof);

/** 
 * @fn int cs97_keys_managerkey_init(cs97_managerkey_t *cs97)
 * @brief Initializes the fields of a CS97 manager key.
 * 
 * @param[in,out] cs97 The manager key to initialize.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_managerkey_init(cs97_managerkey_t *cs97);

/** 
 * @fn int cs97_keys_managerkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group, 
 *			                 uint64_t lambda, uint64_t epsilon, 
 *	   			         cs97_managerkey_t *cs97_mgr;
 * @brief Initializes a CS97 manager key and sets its fields to the received 
 *  values.
 *
 * @param[in] rsa The RSA keypair.
 * @param[in] group The CS97 group.
 * @param[in] lambda The lambda parameter for SKLOGLOG.
 * @param[in] epsilon The epsilon parameter for SKLOGLOG.
 * @param[in,out] cs97_mgr The CS97 manager key to initialize and set.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_managerkey_init_set(rsa_keypair_t *rsa, cs97_group_t *group, 
				  uint64_t lambda, uint64_t epsilon, 
				  cs97_managerkey_t *cs97_mgr);

/** 
 * @fn int cs97_keys_managerkey_free(cs97_managerkey_t *cs97)
 * @brief  Frees the memory allocated for the variables of a CS97 manager key.
 *
 * @param[in,out] cs97 The CS97 manager key to free.
 * 
 * @return IOK or IERROR 
 */
int cs97_keys_managerkey_free(cs97_managerkey_t *cs97);

/** 
 * @fn int cs97_keys_managerkey_fprintf(FILE *fd, cs97_managerkey_t *cs97)
 * @brief Prints into the given file descriptor the given CS97 manager key.
 * 
 * @param[in] fd The file descriptor.
 * @param[in] cs97 The CS97 manager key.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_managerkey_fprintf(FILE *fd, cs97_managerkey_t *cs97);

/** 
 * @fn int cs97_keys_managerkey_fprintf_b64(FILE *fd, cs97_managerkey_t *cs97)
 * @brief Prints into the given file descriptor the given CS97 manager key
 *  encoded as a base64 string.
 *
 * @param[in] fd The file descriptor.
 * @param[in] cs97 The CS97 manager key.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_managerkey_fprintf_b64(FILE *fd, cs97_managerkey_t *cs97);

/** 
 * @fn int cs97_keys_managerkey_parse_b64(int fd, cs97_managerkey_t *cs97, uint8_t *eof)
 * @brief Parses the given file descriptor, which must contain a base64 encoding
 *  of a CS97 manager key.
 *
 * @param[in] fd The file descriptor.
 * @param[in,out] cs97 The CS97 manager key to fill.
 * @param[in,out] eof Will be set to MISC_EOF if EOF is reached.
 * 
 * @return IOK with eof = 0 if a key was found but EOF was not reached, or
 *  eof = MISC_EOF if a key was found and EOF was reached. IERROR if an error
 *  occurred. IFAIL with eof = MISC_EOF if no key was found.
 */
int cs97_keys_managerkey_parse_b64(int fd, cs97_managerkey_t *cs97, uint8_t *eof);

/** 
 * @fn int cs97_keys_memberkey_init(cs97_memberkey_t *cs97)
 * @brief Initializes the memory for the variables of the given cs97_memberkey_t
 * 
 * @param[in,out] cs97 The cs97_memberkey_t to initialize
 * 
 * @return IOK or IERROR
 */
int cs97_keys_memberkey_init(cs97_memberkey_t *cs97);

/** 
 * @fn int cs97_keys_memberkey_copy(cs97_memberkey_t *dst, cs97_memberkey_t *src)
 * @brief Copies the information of the memberkey in src to the memberkey in dst.
 *  Memory for dst must have been previously initialized (using 
 *  cs97_keys_memberkey_init).
 *
 * @param[in,out] dst The memberkey to set.
 * @param[in] src The origin memberkey.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_memberkey_copy(cs97_memberkey_t *dst, cs97_memberkey_t *src);

/** 
 * @fn int cs97_keys_memberkey_free(cs97_memberkey_t *cs97)
 * @brief Frees the allocated the memory of the variables of the given 
 *  cs97_memberkey_t
 * 
 * @param[in,out] cs97 The cs97_memberkey_t to free
 * 
 * @return IOK or IERROR
 */
int cs97_keys_memberkey_free(cs97_memberkey_t *cs97);

/** 
 * @fn int cs97_keys_memberkey_fprintf(FILE *fd, cs97_memberkey_t *cs97)
 * @brief Prints the given CS97 member key into the specified file descriptor.
 *
 * @param[in] fd The file descriptor.
 * @param[in] cs97 The CS97 member key.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_memberkey_fprintf(FILE *fd, cs97_memberkey_t *cs97);

/** 
 * @fn int cs97_keys_memberkey_fprintf_b64(FILE *fd, cs97_memberkey_t *cs97)
 * @brief Prints into the specified file descriptor the given member key,
 *  in base64 encoding.
 * 
 * @param[in] fd The output file descriptor.
 * @param[in] cs97 The member key.
 * 
 * @todo Adapt this function to DER encoding!
 * 
 * @return IOK or IERROR
 */
int cs97_keys_memberkey_fprintf_b64(FILE *fd, cs97_memberkey_t *cs97);

/** 
 * @fn int cs97_keys_memberkey_parse_b64(int fd, cs97_memberkey_t *cs97, uint8_t *eof)
 * @brief Parses the given file, which must contain a valid representation
 *  of a CS97 member key (i.e., it must follow the format produced in 
 *  cs97_keys_memberkey_fprintf_b64) and stores the result in the given
 *  structure.
 *
 * @param[in] fd The file descriptor.
 * @param[in,out] cs97 The CS97 member key structure to fill.
 * @param[in,out] eof Will be set to MISC_EOF if EOF is reached.
 * 
 * @todo Adapt this function to DER encoding!
 *
 * @return IOK with eof = 0 if a key was found but EOF was not reached, or
 *  eof = MISC_EOF if a key was found and EOF was reached. IERROR if an error
 *  occurred. IFAIL with eof = MISC_EOF if no key was found.
 */
int cs97_keys_memberkey_parse_b64(int fd, cs97_memberkey_t *cs97, uint8_t *eof);

/** 
 * @fn int cs97_keys_key_get_next_b64(int fd, char *begin, char *end, 
 *                                    char **key_b64, uint8_t *eof)
 * @brief Reads the next Base64 encoded key in the given file.
 *
 * Starting at the current position of the file descriptor, this function looks
 * for the next CS97 key, which must be between the headers begin and end (which 
 * shall be one of CS97_KEYS_{BEGIN|END}_{GROUP|MANAGER|MEMBER}KEY. The complete 
 * key, including the headers, will be stored in *key_b64, in case it is found. 
 * In case no match is produced, IFAIL will be returned, and eof set to MISC_EOF. 
 * If a match is produced, the file descriptor will be set immediatly after the 
 * parsed CS97 key.
 * 
 * @param[in] fd The file descriptor. 
 * @param[in] begin The begin header to look for.
 * @param[in] end The end header to look for.
 * @param[in,out] key_b64 Will be set to the parsed key, if found.
 * @param[in,out] eof Will be set to MISC_EOF if EOF is reached.
 * 
 * @return IOK with eof = 0 if a key was found but EOF was not reached, or
 *  eof = MISC_EOF if a key was found and EOF was reached. IERROR if an error
 *  occurred. IFAIL with eof = MISC_EOF if no key was found.
 */
int cs97_keys_key_get_next_b64(int fd, char *begin, char *end, 
			       char **key_b64, uint8_t *eof);

/** 
 * @fn int cs97_keys_get_keyfile_type(char *filename, int *type)
 * @brief Returns the type (Group, Manager or Member) of the first key found
 *  in the specified file.
 *
 * @param[in] filename The name of the file to inspect.
 * @param[in,out] type Will be set to the type of key first found:
 *  CS97_GROUPKEY_TYPE, CS97_MANAGERKEY_TYPE or CS97_MEMBERKEY_TYPE
 * 
 * @return IOK or IERROR, or IFAIL if no key was found
 */
int cs97_keys_get_keyfile_type(char *filename, int *type);

/** 
 * @fn int cs97_keys_get_keyfilename(char *pattern, uint8_t cs97_keytype, char **keyfilename)
 * @brief Returns a keyfile name depending on the key type. 
 * 
 *  The keyfile name will depend on the key type, and it will be the result of 
 *  appending a suffix to the given pattern. If *keyfilename is NULL, a string of
 *  the required size will be allocated internally. Otherwise, it must be long
 *  enough.
 * 
 * @param[in] pattern The pattern to use to create the filenames.
 * @param[in] cs97_keytype The key type.
 * @param[in,out] keyfilename The string to store the generated keyfile name.
 * 
 * @return IOK or IERROR
 */
int cs97_keys_get_keyfilename(char *pattern, uint8_t cs97_keytype, char **keyfilename);

#endif

/* cs97_keys.h ends here */
