/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_open.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun may  7 09:34:04 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:06:11 2013 (+0200)
 *           By: jesus
 *     Update #: 19
 * URL: 
 */

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "cs97_open.h"
#include "cs97_gml.h"

int cs97_open_open_signature(char *cs97_gml_filename, char *sigfile, cs97_memberkey_t *mkey, uint8_t *found) {

  cs97_gml_t gml;
  cs97_signature_t signature;
  mpz_t ggyp;
  uint64_t i;
  int fd;
  uint8_t match;
  
  if(!cs97_gml_filename || !sigfile || !mkey || !found) {
    fprintf(stderr, "Error in cs97_open_open_signature (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* Initialize the CS97 GML structure */
  if(cs97_gml_init(&gml) == IERROR) {
    return IERROR;
  }

  /* Load the group members list */
  if(cs97_gml_load(cs97_gml_filename, &gml) == IERROR) {
    cs97_gml_free(&gml);
    return IERROR;
  }

#ifdef DEBUG
  cs97_gml_fprintf(stdout, &gml);
#endif

  /* Load the signature */
  if(cs97_signature_init(&signature) == IERROR) {
    cs97_gml_free(&gml);
    return IERROR;        
  }

  if((fd = open(sigfile, O_RDONLY)) == -1) {
    fprintf(stderr, "Error in cs97_open_open_signature (%d): %s\n",
	    __LINE__, strerror(errno));
    cs97_gml_free(&gml);
    return IERROR;
  }

  if(cs97_signature_parse_b64(fd, &signature) == IERROR) {
    cs97_gml_free(&gml);
    cs97_signature_free(&signature);
    close(fd);
    return IERROR;    
  }

#ifdef DEBUG
  cs97_signature_fprintf_signature(stdout, &signature);
#endif

  close(fd);

  /* Compare the gg^(y_i) with the zz */
  mpz_init(ggyp);
  match = 0;

  for(i=0; i<gml.n; i++) {
    mpz_powm(ggyp, signature.gg, gml.mkeys[i]->y, gml.gkey->G);
    if(!mpz_cmp(ggyp, signature.zz)) {
      match = 1;
      break;
    }
  }

  mpz_clear(ggyp);

  /* Prepare the output */
  *found = match;
  if(match) {
    if(cs97_keys_memberkey_copy(mkey, gml.mkeys[i]) == IERROR) {
      cs97_gml_free_keys(&gml);
      cs97_gml_free(&gml);
      cs97_signature_free(&signature);
    }
  }

  cs97_gml_free_keys(&gml);
  cs97_gml_free(&gml);
  cs97_signature_free(&signature);

  return IOK;
}

/* cs97_open.c ends here */
