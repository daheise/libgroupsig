/*                               -*- Mode: C -*- 
 * @file: cs97_open.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun may  7 09:34:16 2012 (+0200)
 * @version: 
 * Last-Updated: lun may  7 16:15:30 2012 (+0200)
 *           By: jesus
 *     Update #: 7
 * URL: 
 */

#ifndef _CS97_OPEN_H
#define _CS97_OPEN_H

#include "../include/types.h"
#include "../include/sysenv.h"
#include "cs97_signature.h"

/** 
 * @fn int cs97_open_open_signature(char *gml, char *sigfile, cs97_memberkey_t *mkey,
 *                                  uint8_t *found)
 * @brief Opens the signature stored in the specified file, using the given Group
 *  Members List.
 *
 * @param[in] gml The name of the Group Members List file.
 * @param[in] sigfile The name of the signature file.
 * @param[in,out] mkey The CS97 member key to store the data of the signer.
 * @param[in,out] found Will be set to 1 if a match is found, to 0 otherwise.
 * 
 * @return IOK or IERROR
 */
int cs97_open_open_signature(char *cs97_gml_filename, char *sigfile, 
			     cs97_memberkey_t *mkey, uint8_t *found);

#endif /* _CS97_OPEN_H */

/* cs97_open.h ends here */
