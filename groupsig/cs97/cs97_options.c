/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs_97options.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié abr 18 21:40:51 2012 (+0200)
 * @version: 
 * Last-Updated: sáb may 11 13:11:36 2013 (+0200)
 *           By: jesus
 *     Update #: 119
 * URL: 
 */


#include <stdio.h> 
#include <errno.h>
#include <getopt.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>
#include <openssl/sha.h>
#include "cs97_options.h"

struct option long_options[] = {
  {"help", no_argument, 0, 0},
  {"grpkey", required_argument, 0, 0},
  {"mgrkey", required_argument, 0, 0},
  {"usrkey", required_argument, 0, 0},
  {"input", required_argument, 0, 0},
  {"output", required_argument, 0, 0},
  {"setup", no_argument, 0, 0},
  {"mjoin", no_argument, 0, 0},
  {"Mjoin", no_argument, 0, 0},
  {"sign", no_argument, 0, 0},
  {"verify", no_argument, 0, 0},
  {"open", no_argument, 0, 0},
  {"digest", required_argument, 0, 0},
  {"primesize", required_argument, 0, 0},
  {"lambda", required_argument, 0, 0},
  {"epsilon", required_argument, 0, 0},
  {"mname", required_argument, 0, 0},
  {"gml", required_argument, 0, 0},
  {"print-key", required_argument, 0, 0},
  {NULL,0,0,0}
};

int print_usage(char *argv[]) {

  fprintf(stderr, "\n%s version %s %s\n\n"
	  "Usage: %s [options] -i <inputfile> -o <outputfile>\n\n"
	  "OPTIONS:\n"
	  " General:\n"
	  "  -h | --help\n"
	  "       Prints this help menu.\n\n"
	  "  -p <string> | --print-key <string>.\n\n"
	  "       Reads the specified file, which must contain a valid group, manager\n"
	  "       or member key, and prints it into the standard output.\n\n"
	  /* "  -L | --list-procedures @TODO\n" */
	  /* "       Lists the available procedures.\n\n", */
	  &argv[0][2], VERSION, COPYRIGHT, argv[0]);
  fprintf(stderr,
	  " Procedures:\n"
	  "  -S | --setup\n"
	  "       Generates group public key based on the specified parameters.\n\n"
	  "  -m | --mjoin\n"
	  "       Executes the member part of the join procedure.\n\n"
	  "  -M | --Mjoin\n"
	  "       Executes the Manager part of the join procedure.\n\n"
	  "  -s | --sign\n"
	  "       Executes signing procedure.\n\n"
	  "  -v | --verify\n"
	  "       Executes the verification procedure.\n\n"
	  "  -O | --open\n"
	  "       Executes the open procedure.\n\n");
  fprintf(stderr, 
	  " Setup procedure options:\n"
	  "  -k <uint> | --security <uint>\n"
	  "       Specifies the desired size for the RSA modulus primes, in bits.\n\n"
	  "  -l <uint> | --lambda <uint>\n"
	  "       Specifies the desired size, in bits, for the length of SKLOGLOG\n"
	  "       secret keys.\n\n"
	  "  -e <uint> | --epsilon <uint>\n"
	  "       Specifies the constant epsilon for SKLOGLOG\n\n"
	  "  -L <string> | --gml <string>\n"
	  "       Specifies the name to use for the Group Membership List\n\n"); 
  fprintf(stderr,
	  "  -g <string> | --grpkey <string>\n"
	  "       Specifies the file name for the generated group key.\n\n"
	  "  -G <string> | --mgrkey <string>\n"
	  "       Specifies the file name for the generated manager key.\n\n");
  fprintf(stderr, 
	  " Member join procedure options:\n"
	  "  -g <string> | --grpkey <string>\n"
	  "       Specifies the CS97 group key filename.\n\n"
	  "  -u <string> | --usrkey <string>\n"
	  "       Specifies the filename for storing the generated user key.\n\n"
	  "  -n <string> | --name <string>\n"
	  "       Specifies the name of the member. It does not need to be unique.\n\n");
  fprintf(stderr, 
	  " Manager join procedure options:\n"
	  "  -G <string> | --mgrkey <string>\n"
	  "       Specifies the filename of the CS97 group manager key file.\n\n"
	  "  -u <string> | --usrkey <string>\n"
	  "       Specifies the filename of the CS97 group user key file to update.\n\n");
  fprintf(stderr, 
	  " Sign procedure options:\n"
	  "  -i <string> | --input <string>\n"
	  "       Specifies the name of the file containing the data to sign\n\n"
	  "  -u <string> | --usrkey <string>\n"
	  "       Specifies the filename of the CS97 user key file.\n\n"
	  "  -g <string> | --grpkey <string>\n"
	  "       Specifies the filename of the CS97 group key file.\n\n"
	  "  -o <string> | --output <string>\n"
	  "       Specifies the file name for storing the resulting signature.\n\n");
  fprintf(stderr, 
	  " Verify procedure options:\n"
	  "  -i <string> | --input <string>\n"
	  "       Specifies the name of the file containing the data associated to\n"
	  "       the signature.\n\n"
	  "  -d <string> | --digest <string>\n"
	  "       Specifies the name of the file containing the CS97 signature.\n\n"
	  "  -g <string> | --grpkey <string>\n"
	  "       Specifies the name of the CS97 group key file.\n\n");
  fprintf(stderr, 
	  " Open procedure options:\n"
	  "  -i <string> | --input <string>\n"
	  "       Specifies the name of the file containing the Group Members List\n\n"
	  "  -d <string> | --digest <string>\n"
	  "       Specifies the name of the file containing the CS97 signature to open.\n\n");
  exit(IOK);
}

int parse_params(int argc, char **argv, cs97_options_t *opt) {

  int rc, ret, option_index;

  if(argc <= 1) {
    print_usage(argv);
    return IOK;
  }

  if(!argv) {
    fprintf(stderr, "Error in _parse_params (%d): Wrong input parameters\n", 
	    __LINE__);
    errno = EINVAL;
    return IERROR;
  }

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, "hSmMsvOd:u:g:G:i:o:k:l:e:L:p:", long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	print_usage(argv);
      } else if(!strcmp(long_options[option_index].name, "input")) { /* input */
	if(!(opt->input = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}	
      } else if(!strcmp(long_options[option_index].name, "output")) { /* output */
	if(!(opt->output = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "grpkey")) { /* grpkey */
	if(!(opt->grpkey = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "mgrkey")) { /* mgrkey */
	if(!(opt->mgrkey = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "usrkey")) { /* usrkey */
	if(!(opt->usrkey = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "setup")) { /* setup */
	opt->procedure = CS97_SETUP;
      } else if(!strcmp(long_options[option_index].name, "mjoin")) { /* member join */
	opt->procedure = CS97_JOIN_MEMBER;
      } else if(!strcmp(long_options[option_index].name, "Mjoin")) { /* manager join */
	opt->procedure = CS97_JOIN_MANAGER;
      } else if(!strcmp(long_options[option_index].name, "sign")) { /* sign */
	opt->procedure = CS97_SIGN;
      } else if(!strcmp(long_options[option_index].name, "verify")) { /* verify */
	opt->procedure = CS97_VERIFY;
      } else if(!strcmp(long_options[option_index].name, "open")) { /* open */
	opt->procedure = CS97_OPEN;
      } else if(!strcmp(long_options[option_index].name, "digest")) { /* digest */
	if(!(opt->digest = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "security")) { /* primesize */
	errno = 0;
	opt->primesize = strtoul(optarg, NULL, 10);
	if(errno) {
	  fprintf(stderr, "Error: wrong primesize\n");
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "lambda")) { /* lambda */
	errno = 0;
	opt->lambda = strtoul(optarg, NULL, 10);
	if(errno) {
	  fprintf(stderr, "Error: wrong lambda\n");
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "epsilon")) { /* epsilon */
	errno = 0;
	opt->epsilon = strtoul(optarg, NULL, 10);
	if(errno) {
	  fprintf(stderr, "Error: wrong epsilon\n");
	  return IERROR;
	}
      } else if(!strcmp(long_options[option_index].name, "name")) { /* member name */
	if(!(opt->member = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}	
      } else if(!strcmp(long_options[option_index].name, "gml")) { /* Group Membership List */
	if(!(opt->gml = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}	
      } else if(!strcmp(long_options[option_index].name, "print-key")) { /* print-key */
	opt->procedure = CS97_PRINT;
	if(!(opt->print = strdup(optarg))) {
	  fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		  strerror(errno));
	  return IERROR;
	}
      } else { /* Wat? */
	fprintf(stderr, "Error: Unknown option\n");
	return IERROR;
      } 
      break;
    case 'h': /* help */
      print_usage(argv);
      return IOK;
    case 'i': /* input */
      if(!(opt->input = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'o': /* output */
      if(!(opt->output = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'g': /* grpkey */
      if(!(opt->grpkey = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'G': /* mgrkey */
      if(!(opt->mgrkey = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'u': /* usrkey */
      if(!(opt->usrkey = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'S': /* setup */
      opt->procedure = CS97_SETUP;
      break;
    case 'm': /* member join */
      opt->procedure = CS97_JOIN_MEMBER;
      break;
    case 'M': /* manager join */
      opt->procedure = CS97_JOIN_MANAGER;
      break;
    case 's': /* sign */
      opt->procedure = CS97_SIGN;
      break;
    case 'v': /* verify */
      opt->procedure = CS97_VERIFY;
      break;
    case 'O': /* open */
      opt->procedure = CS97_OPEN;
      break;
    case 'd': /* digest */
      opt->digest = strdup(optarg);
      break;
    case 'k': /* primesize */
      errno = 0;
      opt->primesize = strtoul(optarg, NULL, 10);
      if(errno) {
	fprintf(stderr, "Error: wrong primesize\n");
	return IERROR;
      }
      break;
    case 'l': /* lambda */
      errno = 0;
      opt->lambda = strtoul(optarg, NULL, 10);
      if(errno) {
	fprintf(stderr, "Error: wrong lambda\n");
	return IERROR;
      }
      break;
    case 'e': /* epsilon */
      errno = 0;
      opt->epsilon = strtoul(optarg, NULL, 10);
      if(errno) {
	fprintf(stderr, "Error: wrong epsilon\n");
	return IERROR;
      }
      break;
    case 'n': /* member name */
      if(!(opt->member = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'L': /* Group Membership List */
      if(!(opt->gml = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }	
      break;
    case 'p': /* Print key */
      opt->procedure = CS97_PRINT;
      if(!(opt->print = strdup(optarg))) {
	fprintf(stderr, "Error in _parse_params (%d): %s\n", __LINE__,
		strerror(errno));
	return IERROR;
      }
      break;
    case '?': /* Character not included in optstring */
      return IERROR;
    default:
      fprintf(stderr, "Error: Unknown option -%c\n", ret);
      return IERROR;
    }
  }

  if(rc == IERROR) {
    fprintf(stderr, "Error: unknown error while parsing parameters\n");
    return IERROR;
  }

  /* Further checks */
  switch(opt->procedure) {

  case CS97_SETUP:

    if(opt->primesize <= 1) {
      fprintf(stderr, "Error: the prime size must be greater than 1\n");
      return IERROR;
    }
    if(!opt->lambda) {
      fprintf(stderr, "Error: lambda cannot be 0\n");
      return IERROR;
    }
    if(opt->epsilon <= 1) {
      fprintf(stderr, "Error: epsilon must be greater than 1\n");
      return IERROR;
    }
    if(!opt->grpkey) {
      fprintf(stderr, "Error: an output file for the group key is required (grpkey or g).\n");
      return IERROR;
    }
    if(!opt->mgrkey) {
      fprintf(stderr, "Error: an output file for the group key is required (mgrkey or G).\n");
      return IERROR;
    }
    if(!opt->gml) {
      fprintf(stderr, "Error: an output file for the Group Membership List is required (gml or L).\n");
      return IERROR;
    }
    break;

  case CS97_JOIN_MEMBER:

    if(!opt->grpkey) {
      fprintf(stderr, "Error: an input file containing the group key is required (grpkey or g).\n");
      return IERROR;
    }
    if(!opt->usrkey) {
      fprintf(stderr, "Error: an output file for the group key is required (usrkey or u).\n");
      return IERROR;
    }

    break;

  case CS97_JOIN_MANAGER:

    if(!opt->mgrkey) {
      fprintf(stderr, "Error: an input file containing the manager key is required (mgrkey or G).\n");
      return IERROR;
    }
    if(!opt->usrkey) {
      fprintf(stderr, "Error: an input/output file for the group key is required (usrkey or u).\n");
      return IERROR;
    }
    if(!opt->gml) {
      fprintf(stderr, "Error: an output file for the Group Membership List is required (gml or L).\n");
      return IERROR;
    }
    break;

  case CS97_SIGN:
    
    if(!opt->input) {
      fprintf(stderr, "Error: an input file containing data to sign is required.\n");
      return IERROR;
    }
    if(!opt->grpkey) {
      fprintf(stderr, "Error: an input file containing the group key is required (grpkey or g).\n");
      return IERROR;
    }
    if(!opt->usrkey) {
      fprintf(stderr, "Error: an input file containing the user key is required (usrkey or u).\n");
      return IERROR;
    }
    if(!opt->output) {
      fprintf(stderr, "Error: an output file to store the signature is required.\n");
      return IERROR;
    }
    break;

  case CS97_VERIFY:
    
    if(!opt->input) {
      fprintf(stderr, "Error: an input file containing data to be verified is required.\n");
      return IERROR;
    }
    if(!opt->digest) {
      fprintf(stderr, "Error: an input file containing the signature to verify is required.\n");
      return IERROR;
    }
    if(!opt->grpkey) {
      fprintf(stderr, "Error: an input file containing the group key is required (grpkey or g).\n");
      return IERROR;
    }
    break;

  case CS97_OPEN:

    if(!opt->digest) {
      fprintf(stderr, "Error: an input file containing the signature to be opened is required.\n");
      return IERROR;
    }
    if(!opt->gml) {
      fprintf(stderr, "Error: an output file for the Group Membership List is required (gml or L).\n");
      return IERROR;
    }
    break;
    
  case CS97_PRINT:
    
    if(!opt->print) {
      fprintf(stderr, "Error: an input file containing the key to be printed is required.\n");
      return IERROR;      
    }
    break;

  default:
    fprintf(stderr, "Error: unknown procedure.\n");
    return IERROR;    
  }
  return rc;
    
}

int free_params(cs97_options_t *opt) {
  
  if(!opt) {
    errno = EINVAL;
    return IERROR;
  }

  if(opt->input) { free(opt->input); opt->input = NULL; }
  if(opt->output) { free(opt->output); opt->output = NULL; }
  if(opt->member) { free(opt->member); opt->member = NULL; }
  if(opt->digest) { free(opt->digest); opt->digest = NULL; }
  if(opt->print) { free(opt->print); opt->print = NULL; }

  return IOK;

}


/* cs_97options.c ends here */
