/*                               -*- Mode: C -*- 
 * @file: cs97_options.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié abr 18 21:55:00 2012 (+0200)
 * @version: 
 * Last-Updated: lun may 21 17:51:51 2012 (+0200)
 *           By: jesus
 *     Update #: 23
 * URL: 
 */

#ifndef _CS97OPTIONS_H
#define _CS97OPTIONS_H

#include <stdint.h>
#include "../include/types.h"
#include "../include/sysenv.h"

extern char *optarg;
extern int optind, opterr, optopt;

/**
 * @def CS97_SETUP
 * Constant for indicating that a CS97 Setup procedure has
 * to be executed.
 */
#define CS97_SETUP 0

/**
 * @def CS97_JOIN_MEMBER
 * Constant for indicating that a CS97 member Join procedure has
 * to be executed.
 */
#define CS97_JOIN_MEMBER 1

/**
 * @def CS97_JOIN_MANAGER
 * Constant for indicating that a CS97 manager Join procedure has
 * to be executed.
 */
#define CS97_JOIN_MANAGER 2

/**
 * @def CS97_SIGN
 * Constant for indicating that a CS97 Sign procedure has to be executed.
 */
#define CS97_SIGN 3

/**
 * @def CS97_VERIFY
 * Constant for indicating that a CS97 Verify procedure has to be executed.
 */
#define CS97_VERIFY 4

/**
 * @def CS97_OPEN
 * Constant for indicating that a CS97 Open procedure has to be executed.
 */
#define CS97_OPEN 5

/**
 * @def CS97_PRINT
 * Constant for indicating that a CS97 key file is to be printed.
 */
#define CS97_PRINT 6

/**
 * @struct options_t
 * @brief Stores the parsed input options for the main program
 */
typedef struct {
  char *input;
  char *output;
  /* char *data; */
  char *digest;
  char *grpkey;
  char *mgrkey;
  char *usrkey;
  uint8_t procedure;
  uint64_t primesize;
  uint64_t lambda;
  uint64_t epsilon;
  char *member;
  char *gml;
  char *print;
} cs97_options_t;

/**
 * @fn int print_usage(char *argv[])
 * @brief Prints the help menu to stderr.
 *
 * @param[in] argv Program parameters
 * @return IOK
 */
int print_usage(char *argv[]);

/**
 * @fn int parse_params(int argc, char **argv, cs97_options_t *opt)
 * @brief Parses the command line parameters
 *
 * Parses the command line parameters, returning ERROR when the
 * parameters are not valid and OK when they are. If an
 * unexpected error occurs, errno will be consequently updated.
 *
 * @param[in] argc Number of arguments in argv
 * @param[in] argv Parameters
 * @param[out] options Configuration parameters structure initialized to the 
 *  read values
 *
 * @return IOK when the parameters are valid, IERROR when they are not. If an 
 *  unexpected error occurs, errno will be consequently updated.
 *
 */
int parse_params(int argc, char **argv, cs97_options_t *opt);

/** 
 * @fn int free_params(cs97_options_t *opt)
 * Frees the memory allocated for the options.
 * 
 * @param opt The options structure
 * 
 * @return IOK or IERROR
 */
int free_params(cs97_options_t *opt);

#endif

/* cs_97options.h ends here */
