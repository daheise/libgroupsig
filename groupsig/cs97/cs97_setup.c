/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_setup.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar abr 24 12:33:36 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:06:19 2013 (+0200)
 *           By: jesus
 *     Update #: 116
 * URL: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "cs97_setup.h"
#include "numbers.h"

int cs97_setup_group_gen(rsa_keypair_t *rsa, cs97_group_t *group) {

  mpz_t p, phip, g, g0, gcd, r, exp, order;
  factor_list_t factors;
  uint32_t i;
  
  if(!rsa || !group) {
    fprintf(stderr, "Error in cs97_setup_groupgen (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  /* Generate random prime p, larger than the RSA modulus */
  /* Generate a random prime p, such that rsa->n | (p-1). We use our modified
     version of Maurer's algorithm for the generation of proven primes. */
  if(numbers_factor_list_init(&factors) == IERROR) {
    return IERROR;
  }

  mpz_init(p);
  if(numbers_mov97_alg462_mod(rsa->p, rsa->q, &p, &factors) == IERROR) {
    numbers_factor_list_free(&factors);
    return IERROR;
  }

#ifdef DEBUG
  numbers_factor_list_fprintf(stdout, &factors);
#endif

  mpz_init(phip);
  mpz_sub_ui(phip, p, 1);

  /* Obtain a generator for a subgroup of G with high order. See p.8 of
     Ivan Damgard's "Discrete Log Cryptosystems" notes. */
  
  /* Let q be the largest prime factor in p-1, and G0 the Z_p* group of order p-1. 
     There is exactly one subgroup G of G0 with order q. Moreover, if one generator 
     of G0 is g0, then, g=g0^((p-1)/q) is a generator of G. But we want a group of
     order rsa->p*rsa->q, so substitute q before for (rsa->p*rsa->q)=rsa->n. Given
     how we have generated p, note that rsa->p | (p-1). */
  
  /* Randomly choose a generator for the group */
  mpz_init(g0);
  if(numbers_get_generator(p, &factors, &g0) == IERROR) {
    mpz_clear(phip);
    numbers_factor_list_free(&factors);
    return IERROR;
  }

  /* Once we have the generator of the group, obtain a generator of the subgroup */
  mpz_init(g);
  mpz_init(exp);
  mpz_divexact(exp, phip, rsa->n);
  mpz_powm(g, g0, exp, p); 
  mpz_clear(exp);

  mpz_init(order);
  if(numbers_get_elem_order(p, g, &factors, &order) == IERROR) {
    mpz_clear(phip);
    mpz_clear(g0);
    mpz_clear(g);
    mpz_clear(order);
    return IERROR;
  }

  if(mpz_cmp(order,rsa->n)) {
    fprintf(stderr, "Error in cs97_setup_group_gen (%d): %s\n", __LINE__,
	    "Subgroup generation failure.\n");
    fprintf(stderr, "XXX  order: %s\n", mpz_get_str(NULL, 10, order));
    fprintf(stderr, "XXX rsa->n: %s\n", mpz_get_str(NULL, 10, rsa->n));
    fprintf(stderr, "XXX      p: %s\n", mpz_get_str(NULL, 10, p));
    if(mpz_divisible_p(order, rsa->n)) {
      mpz_t q;
      mpz_init(q);
      mpz_divexact(q, order, rsa->n);
      fprintf(stderr, "XXX order/rsa->n: %s\n", mpz_get_str(NULL, 10, q));
      mpz_clear(q);
    }
    mpz_clear(phip);
    mpz_clear(g0);
    mpz_clear(g);
    mpz_clear(order);
    return IERROR;
  }

  mpz_clear(order);
  
  /* We also choose here the second base for the group, the 'a' base in CS97 */

  /**
   * @todo In CS97 it is advised to choose a random a in Z_n* with large
   *  multiplicative order modulo both RSA primes. For now, we just choose
   *  a generator of the group.
   *  See Section 5.2 of "Discrete Log Based Cryptosystems", by Ivan Damgard,
   *  for instructions on how to generate this kind of groups.
   *  See section 4.4.4 and algorithm 4.62 from Menezes et al.'s "Handbook of
   *  applied cryptography" for generation of primes p with known 'partial'
   *  factorization of p-1.
   */

  mpz_init(r);  
  mpz_init(gcd);
  do { /* With this we get a number between 0 and (n-1), 
	  we want one between 1 and n ... */
    mpz_urandomm(r, sysenv->gmp_rand, rsa->n);
    mpz_gcd(gcd, r, rsa->n);
  } while(mpz_cmp_ui(gcd, 1));

  mpz_add_ui(r, r, 1); /* ... so, add 1 */
  mpz_init_set(group->G, p);
  mpz_init_set(group->g, g);
  mpz_init_set(group->a, r);

  mpz_clear(p);
  mpz_clear(phip);
  mpz_clear(g0);
  mpz_clear(gcd);
  mpz_clear(r);
  mpz_clear(g);

  /* We won't need the factors anymore... do we? */
  numbers_factor_list_free(&factors);

  return IOK;

}

int cs97_setup_group_free(cs97_group_t *group) {

  if(!group) {
    return IOK;
  }

  mpz_clear(group->G);
  mpz_clear(group->g);
  mpz_clear(group->a);

  return IOK;
}

int cs97_setup_group_fprintf(FILE *fd, cs97_group_t *group) {

  char *sG, *sg, *sa;

  if(!group) {
    fprintf(stderr, "Error in cs97_setup_group_fprintf (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  sG = mpz_get_str(NULL, 10, group->G);
  sg = mpz_get_str(NULL, 10, group->g);
  sa = mpz_get_str(NULL, 10, group->a);

  fprintf(stderr, " CS97 group info:\n");
  fprintf(stderr, " ----------------\n");	  
  fprintf(stderr, "     G: %s\n", sG);
  fprintf(stderr, "     g: %s\n", sg);
  fprintf(stderr, "     a: %s\n\n", sa);

  free(sG); sG = NULL;
  free(sg); sg = NULL;
  free(sa); sa = NULL;

  return IOK;

}

/* cs97_setup.c ends here */
