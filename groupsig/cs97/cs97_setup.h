/*                               -*- Mode: C -*- 
 * @file: cs97_setup.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar abr 24 12:33:42 2012 (+0200)
 * @version: 
 * Last-Updated: vie abr 27 12:26:19 2012 (+0200)
 *           By: jesus
 *     Update #: 25
 * URL: 
 */

#ifndef _CS97_SETUP
#define _CS97_SETUP

#include "../include/types.h"
#include "../include/sysenv.h"
#include "rsa.h"
#include "cs97_keys.h"

/** 
 * @fn int cs97_setup_group_gen(rsa_keypair_t *rsa, cs97_group_t *group)
 * @brief Generates a group according to the requirements in CS97.
 *
 * @param[in] rsa A pointer to the rsa keypair to base the generation on.
 * @param[in,out] group A pointer to the structure to store the group.
 * 
 * @return IOK or IERROR
 */
int cs97_setup_group_gen(rsa_keypair_t *rsa, cs97_group_t *group);

/** 
 * @fn int cs97_setup_group_free(cs97_group_t *group)
 * @brief Frees the memory allocated for the given group.
 * 
 * @param[in] group The CS97 group to free.
 * 
 * @return IOK or IERROR
 */
int cs97_setup_group_free(cs97_group_t *group);

/** 
 * @fn int cs97_setup_group_fprintf(FILE *fd, cs97_group_t *group)
 * @brief Prints the group info in the specified output.
 *
 * @param fd The file descriptor to print to.
 * @param group The group to print.
 * 
 * @return IOK or IERROR
 */
int cs97_setup_group_fprintf(FILE *fd, cs97_group_t *group);

#endif /* _CS97_SETUP */

/* cs97_setup.h ends here */
