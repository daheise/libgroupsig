/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: cs97_signature.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun abr 30 14:00:35 2012 (+0200)
 * @version: 
 * Last-Updated: jue may  2 19:06:22 2013 (+0200)
 *           By: jesus
 *     Update #: 123
 * URL: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "../include/types.h"
#include "../include/sysenv.h"
#include "cs97_signature.h"

int cs97_signature_init(cs97_signature_t *signature) {

  if(!signature) {
    fprintf(stderr, "Error in cs97_signature_init (%d): %s\n",
	    __LINE__, EINVAL);
    return IERROR;
  }

  mpz_init(signature->gg);
  mpz_init(signature->zz);
  signature->v1 = NULL;
  signature->v1_len = 0;
  signature->v2 = NULL;
  signature->v2_len = 0;

  return IOK;

}

int cs97_signature_free(cs97_signature_t *signature) {
  
  if(!signature) {
    return IOK;
  }

  mpz_clear(signature->gg);
  mpz_clear(signature->zz);
  if(signature->v1) { free(signature->v1); signature->v1 = NULL; }
  if(signature->v2) { free(signature->v2); signature->v2 = NULL; }

  return IOK;

}

int cs97_signature_sign(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem,
			byte_t *message, uint64_t m_len, 
			cs97_signature_t *signature) {

  skloglog_signature_t v1;
  mpz_t r, gg, zz, gcd;
  uint64_t sig_len;


  if(!cs97_grp || !cs97_mem || !message || !signature) {
    fprintf(stderr, "Error in cs97_signature_sign (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Get gg = g^r (mod G), for r chosen at random from Z_n* */
  mpz_init(r);
  mpz_init(gcd);
  do { /* With this we get a number between 0 and (n-1), 
	  we want one between 1 and n ... */
    mpz_urandomm(r, sysenv->gmp_rand, cs97_grp->n);
    mpz_gcd(gcd, r, cs97_grp->n);
  } while(mpz_cmp_ui(gcd, 1));
  
  mpz_init(gg);
  mpz_powm(gg, cs97_grp->g, r, cs97_grp->G);
  mpz_clear(gcd);

  mpz_clear(r);

  /* 2) Get zz = gg^y (mod G) */
  mpz_init(zz);
  mpz_powm(zz, gg, cs97_mem->y, cs97_grp->G);

  mpz_set(signature->gg, gg);
  mpz_set(signature->zz, zz);

  /* 3) Get V1 = SKLOGLOG{ alpha: zz = gg^a^alpha }(message) */
  signature->v1 = NULL;

  /** @todo Make the security parameter k configurable in SKLOGLOG. */
  if(skloglog_sign_sc(message, m_len, gg, cs97_grp->a,
		      cs97_grp->G, cs97_mem->x, zz, cs97_grp->n,
		      cs97_grp->lambda, cs97_grp->epsilon, 
		      SKLOGLOG_K, &signature->v1,
		      &sig_len) == IERROR) {
    mpz_clear(gg);
    mpz_clear(zz);
    return IERROR;    
  }

  signature->v1_len = sig_len;

  /* 4) Get V2 = SKROOTLOG{ beta: zz*gg = gg^beta^exp }(message) */

  /** @todo Make the security parameter k configurable in SKROOTLOG. */

  mpz_mul(zz, zz, gg);
  if(skrootlog_sign_sc(message, m_len, gg, cs97_mem->v,
  		       cs97_grp->G, cs97_grp->n, cs97_grp->e, zz,
  		       SKROOTLOG_K, &signature->v2,
  		       &sig_len) == IERROR) {
    mpz_clear(gg);
    mpz_clear(zz);
    return IERROR;
  }

  signature->v2_len = sig_len;

  mpz_clear(gg);
  mpz_clear(zz);
  
  return IOK;

}

int cs97_signature_verify(cs97_groupkey_t *cs97_grp, byte_t *message, uint64_t m_len, 
			  cs97_signature_t *signature, uint8_t *fail) {

  mpz_t zzgg;

  if(!cs97_grp || !message || !signature || !fail) {
    fprintf(stderr, "Error in cs97_signature_verify (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Check v1 */

  /** @todo Make the security parameter k configurable in SKLOGLOG. */
  if(skloglog_verify_sc(message, m_len, signature->v1, signature->v1_len,
			signature->gg, cs97_grp->a, cs97_grp->G, signature->zz, 
			cs97_grp->n, cs97_grp->lambda, cs97_grp->epsilon,
			SKLOGLOG_K, fail) == IERROR) {
    return IERROR;
  }

  /* If the signature is already invalid, return */
  if(*fail) {
    return IOK;
  }

#ifdef DEBUG
  fprintf(stdout, "SKLOGLOG OK\n");
#endif

  /* 2) Check v2 */

  /** @todo Make the security parameter k configurable in SKROOTLOG. */
  mpz_init(zzgg);
  mpz_mul(zzgg, signature->zz, signature->gg);
  if(skrootlog_verify_sc(message, m_len, signature->v2, signature->v2_len,
  			 signature->gg, cs97_grp->G, cs97_grp->n, cs97_grp->e,
  			 zzgg, SKROOTLOG_K, fail) == IERROR) {
    return IERROR;
  }

  return IOK;

}

int cs97_signature_fprintf_signature_b64(FILE *fd, cs97_signature_t *signature) {

  gnutls_datum_t cs97_datum, *cs97_b64=NULL;
  byte_t *bgg=NULL, *bzz=NULL, *bv1=NULL, *bv2=NULL, *bsig=NULL;
  size_t sgg, szz, ssig, offset;
  uint32_t i;
  int rc;

  if(!signature) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;   
  }

  rc = IOK;
  
  /* Export the variables to binary data */
  if(!(bgg = mpz_export(NULL, &sgg, 1, 1, 1, 0, signature->gg))) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    rc = IERROR;
    goto error;
  }

  if(!(bzz = mpz_export(NULL, &szz, 1, 1, 1, 0, signature->zz))) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n",
	    __LINE__, "mpz_export.");
    return IERROR;
  }

  /* To separate the different values, and be able to parse them later, we use
     the 'syntax': "'g='<gg>'z='<zz>'v1='<v1>'v2='<v2>", 
     where the values between '' are printed in ASCII, and the <x> are the binary 
     data obtained above. Therefore, the total length of the group key will be 
     2*2+2*3+sgg+szz+v1_len+v2_len.
     @todo although does not seem very probable, it is possible that the binary 
     data of n, e, ... contains the ASCII codes of 'n=', 'e=', etc.. This will
     obviously lead to program malfunction...
  */

  ssig = 2*2+2*3+sgg+szz+signature->v1_len+signature->v2_len+3;
  
  /* Copy everything into a unique array */
  if(!(bsig = (byte_t *) malloc(sizeof(byte_t)*ssig))) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  memset(bsig, 0, ssig);

  offset = 0;

  bsig[0] = 'g';
  bsig[1] = '=';
  memcpy(&bsig[2], bgg, sgg);
  offset += 2+sgg;

  bsig[offset] = 'z';
  bsig[offset+1] = '=';
  memcpy(&bsig[offset+2], bzz, szz);
  offset += 2+szz;

  bsig[offset] = 'v';
  bsig[offset+1] = '1';
  bsig[offset+2] = '=';
  memcpy(&bsig[offset+3], signature->v1, signature->v1_len);
  offset += 3+signature->v1_len;

  bsig[offset] = 'v';
  bsig[offset+1] = '2';
  bsig[offset+2] = '=';
  memcpy(&bsig[offset+3], signature->v2, signature->v2_len);
  offset += 3+signature->v2_len;

  /** @todo Change this botch! */
  bsig[offset] = 'E';
  bsig[offset+1] = 'O';
  bsig[offset+2] = 'S';
  offset += 3;

  /* Convert it to a base64 string prepended with CS97_KEYS_SIG_MSG */
  if(ssig != offset) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n",
	    __LINE__, "Conversion failure\n");
    rc = IERROR;
    goto error;
  }

  cs97_datum.data = bsig;
  cs97_datum.size = ssig;

  if(!(cs97_b64 = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n", 
	    __LINE__, strerror(errno));
    rc = IERROR;
    goto error;
  }

  if((rc = gnutls_pem_base64_encode_alloc(CS97_SIGNATURE_MSG,
					  &cs97_datum,
					  cs97_b64)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature_b64 (%d): %s\n", 
	    __LINE__, gnutls_strerror(rc));
    rc = IERROR;
    goto error;    
  }
  
  /* We have the associated base64 string in cs97_b64. Print it and we are done. */
  fprintf(fd, "%s", cs97_b64->data);

  gnutls_free(cs97_b64);

 error:

  if(bgg) { free(bgg); bgg = NULL; }
  if(bzz) { free(bzz); bzz = NULL; }
  if(bsig) { free(bsig); bsig = NULL; }

  return rc;

}

int cs97_signature_parse_b64(int fd, cs97_signature_t *signature) {

  struct stat buf;
  gnutls_datum_t cs97_datum_b64, *cs97_datum;
  mpz_t gg, zz;
  byte_t *cs97_str_b64, *bgg, *bzz;
  skloglog_signature_t *bv1;
  skrootlog_signature_t *bv2;
  uint64_t i, offset, uread;
  ssize_t sread;
  int rc;
  uint8_t finish;

  if(!signature) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n", 
	    __LINE__, strerror(EINVAL));
    errno = EINVAL;
    return IERROR;
  }

  /* 1) Dump the file into a string */

  /* Get the size of the file: that will be the size of the string */
  if(fstat(fd, &buf) == -1) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  if(!(cs97_str_b64 = (byte_t *) malloc(sizeof(byte_t)*buf.st_size))) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;
  }

  if((sread = read(fd, cs97_str_b64, buf.st_size)) == -1) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    return IERROR;   
  }

  if(sread != buf.st_size) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid signature file");
    return IERROR;
  }

  cs97_datum_b64.data = cs97_str_b64;
  cs97_datum_b64.size = buf.st_size;

  /* 2) Parse from base64 to binary */
  if(!(cs97_datum = (gnutls_datum_t *) malloc(sizeof(gnutls_datum_t)))) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid signature file");
    return IERROR;    
  }

  if((rc = gnutls_pem_base64_decode_alloc(CS97_SIGNATURE_MSG,
					  &cs97_datum_b64,
					  cs97_datum)) != GNUTLS_E_SUCCESS) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, gnutls_strerror(rc));
    return IERROR;
  }

  /* The 'syntax' is: "'g='<gg>'z='<zz>'v1='<v1>'v2='<v2>", 
     where the values between '' are printed in ASCII, and the <x> are the binary 
     data representing the corresponding parameters. Therefore, the total length of 
     the group key will be 2*2+2*3+sgg+szz+sv1+sv2. */
  offset = 0;
  if(cs97_datum->data[0] != 'g' && cs97_datum->data[1] != '=') {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, "Invalid signature file");
    return IERROR;
  }
  offset += 2;

  /* Read the 'g' data until we find the 'z=' */
  bgg = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "z=", 2,
			     &bgg, &uread, &finish) == IERROR) {
    return IERROR;    
  }

  mpz_init(gg);
  mpz_import(gg, uread, 1, 1, 1, 0, bgg);
  offset += (uread+2);

  /* Read the 'z' data until we find the 'v1=' */
  bzz = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "v1=", 2, 
			     &bzz, &uread, &finish) == IERROR) {
    mpz_clear(gg);
    return IERROR;    
  }

  mpz_init(zz);
  mpz_import(zz, uread, 1, 1, 1, 0, bzz);
  offset += (uread+3);

  /* Read the 'v1' data until we find the 'v2=' */
  bv1 = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], (byte_t *) "v2=", 3, 
			     &bv1, &uread, &finish) == IERROR) {
    mpz_clear(zz);
    mpz_clear(gg);
    return IERROR;    
  }

  if(!(signature->v1 = malloc(sizeof(byte_t)*uread))) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    mpz_clear(zz);
    mpz_clear(gg);
    return IERROR;
  }

  memcpy(signature->v1, bv1, uread);
  signature->v1_len = uread;
  offset += (uread+3);

  /* Read the 'v2' data until we find the EOS */
  bv2 = NULL; finish = 0; uread = 0;
  if(misc_read_bstring_until(&cs97_datum->data[offset], "EOS", 3, 
			     &bv2, &uread, &finish) == IERROR) {
    mpz_clear(gg);
    mpz_clear(zz);
    return IERROR;    
  }

  if(!(signature->v2 = malloc(sizeof(byte_t)*uread))) {
    fprintf(stderr, "Error in cs97_signature_parse_b64 (%d): %s\n",
	    __LINE__, strerror(errno));
    mpz_clear(zz);
    mpz_clear(gg);
    return IERROR;
  }

  memcpy(signature->v2, bv2, uread);
  signature->v2_len = uread;
  offset += (uread+3);

  mpz_set(signature->gg, gg); mpz_clear(gg);
  mpz_set(signature->zz, zz); mpz_clear(zz);
    
  return IOK;


}

int cs97_signature_fprintf_signature(FILE *fd, cs97_signature_t *signature) {

  char *sgg, *szz;

  if(!fd || !signature) {
    fprintf(stderr, "Error in cs97_signature_fprintf_signature (%d): %s\n",
	    __LINE__, strerror(EINVAL));
    return IERROR;
  }

  sgg = mpz_get_str(NULL, 10, signature->gg);
  szz = mpz_get_str(NULL, 10, signature->zz);
  
  fprintf(fd, " CS97 signature:\n");
  fprintf(fd, " --------------\n");
  fprintf(fd, " g: %s\n", sgg);
  fprintf(fd, " z: %s\n", szz);

  free(sgg); sgg = NULL;
  free(szz); szz = NULL;

  if(signature->v1) {
    fprintf(fd, " v1: ");
    if(misc_fprintf_bytestring(fd, signature->v1, signature->v1_len)  == IERROR) {
      return IERROR;
    }
    fprintf(fd, "\n");
  }
   
  if(signature->v2) {
    fprintf(fd, " v2: ");
    if(misc_fprintf_bytestring(fd, signature->v2, signature->v2_len)  == IERROR) {
      return IERROR;
    }
    fprintf(fd, "\n");
  }
  
  return IOK;

}

/* cs97_signature.c ends here */
