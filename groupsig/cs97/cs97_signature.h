/*                               -*- Mode: C -*- 
 * @file: cs97_signature.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun abr 30 14:23:51 2012 (+0200)
 * @version: 
 * Last-Updated: lun may 21 17:37:24 2012 (+0200)
 *           By: jesus
 *     Update #: 26
 * URL: 
 */

#ifndef _CS97_SIGNATURE_H
#define _CS97_SIGNATURE_H

#include <stdint.h>
#include "cs97_keys.h"
#include "skloglog.h"
#include "skrootlog.h"

/**
 * CS97_SIGNATURE_MSG
 * The string to prepend to headers of files containing CS97 group signature
 */
#define CS97_SIGNATURE_MSG "CS97 SIGNATURE"

/**
 * @struct cs97_signature
 *  A CS97 signature.
 */
typedef struct _cs97_signature_t {
  mpz_t gg; /**< A value g^r (mod n), for r in Z_n*, and
	      g and n are the base and modulus of a CS97
	      group. */
  mpz_t zz; /**< gg^y, where y is a^x (mod n), and a and
	       x are the 'a' base and exponent of a CS97
	       group and member key, respectively. */
  byte_t *v1; /**< A skloglog signature, namely:
			       SKLOGLOG[ alpha : zz = gg^a^alpha ]*/
  uint64_t v1_len; /**< The length, in bytes, of v1. */
  byte_t *v2; /**< A skrootlog signature, namely:
	         SKROOTLOG [ beta : zz*gg=gg^beta^e ] */
  uint64_t v2_len; /**< The length, in bytes, of v2. */
} cs97_signature_t;

/** 
 * @fn int cs97_signature_init(cs97_signature_t *signature)
 * @brief Initializes the elements of a CS97 signature
 *
 * @param[in,out] signature A pointer to the signature to initialize.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_init(cs97_signature_t *signature);

/** 
 * @fn int cs97_signature_free(cs97_signature_t *signature)
 * @brief Frees the variables of a CS97 signature.
 *
 * @param[in,out] signature A pointer to the signature to free.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_free(cs97_signature_t *signature);

/** 
 * @fn int cs97_signature_sign(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem,
 *	             byte_t *message, uint64_t m_len, cs97_signature_t *signature)
 * @brief Signs the message contained in fd_in using the received CS97 group and
 *  member keys, and writes the resulting signature in fd_out.
 * 
 * @param[in] cs97_grp The CS97 group key.
 * @param[in] cs97_mem The CS97 member key.
 * @param[in] message The message to sign.
 * @param[in] m_len The message length, in bytes.
 * @param[in,out] signature The obtained signature.
 * 
 * @return  IOK or IERROR
 */
int cs97_signature_sign(cs97_groupkey_t *cs97_grp, cs97_memberkey_t *cs97_mem,
			byte_t *message, uint64_t m_len, 
			cs97_signature_t *signature);

/** 
 * @fn int cs97_signature_verify(cs97_groupkey_t *cs97_grp, 
 *		byte_t *message, uint64_t m_len, cs97_signature_t *signature, 
 *		uint8_t *fail)
 * @brief Verifies the received signature with the received keys, and updates
 *  fail consequently.
 *
 * @param cs97_grp The group key to use for verification.
 * @param message The message.
 * @param m_len The length of message, in bytes.
 * @param signature The CS97 signature to verify.
 * @param fail Will be set to 0 if the verification succeeds or to 1 otherwhise.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_verify(cs97_groupkey_t *cs97_grp, byte_t *message, uint64_t m_len, 
			  cs97_signature_t *signature, uint8_t *fail);

/** 
 * @fn int cs97_signature_fprintf_signature_b64(FILE *fd, cs97_signature_t *signature)
 * @brief Prints into the given file descriptor the given CS97 signature, using
 *  base64 encoding.
 *
 * @param[in] fd The file descriptor.
 * @param[in] signature The CS97 signature.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_fprintf_signature_b64(FILE *fd, cs97_signature_t *signature);

/** 
 * @fn int cs97_signature_parse_b64(int fd, cs97_signature_t *signature)
 * @brief Parses a CS97 signature, printed by cs97_signature_fprintf_signature_b64,
 *  and stored in filename.
 *
 * @param[in] The file descriptor.
 * @param[in,out] signature The CS97 signature structure to fill.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_parse_b64(int fd, cs97_signature_t *signature);

/** 
 * @fn int cs97_signature_fprintf_signature(FILE *fd, cs97_signature_t *signature)
 * @brief Prints to the specified file descriptor the given CS97 signature.
 * 
 * @param fd The file descriptor.
 * @param signature The CS97 signature to print.
 * 
 * @return IOK or IERROR
 */
int cs97_signature_fprintf_signature(FILE *fd, cs97_signature_t *signature);

#endif /* _CS97_SIGNATURE_H */

/* cs97_signature.h ends here */
