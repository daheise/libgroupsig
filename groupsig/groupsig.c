/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: groupsig.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié jul 11 16:12:38 2012 (+0200)
 * @version: 
 * Last-Updated: lun jul 20 21:58:11 2015 (+0200)
 *           By: jesus
 *     Update #: 259
 * URL: 
 */
#include "config.h"
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "sysenv.h"
#include "sys/mem.h"
#include "include/groupsig.h"
#include "include/registered_groupsigs.h"

#define GROUPSIG_REGISTERED_GROUPSIGS_N 3
static const groupsig_t *GROUPSIG_REGISTERED_GROUPSIGS[GROUPSIG_REGISTERED_GROUPSIGS_N] = {
  &kty04_groupsig_bundle,
  &bbs04_groupsig_bundle,
  &cpy06_groupsig_bundle,
};

uint8_t groupsig_is_supported_scheme(uint8_t code) {

  int i;

  for(i=0; i<GROUPSIG_REGISTERED_GROUPSIGS_N; i++) {
    if(code == GROUPSIG_REGISTERED_GROUPSIGS[i]->desc->code) return 1;
  }

  return 0;

}

const groupsig_t* groupsig_get_groupsig_from_str(char *str) {

  uint8_t i;

  if(!str) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_get_groupsig_from_str", __LINE__, LOGERROR);
    return NULL;
  }

  for(i=0; i<GROUPSIG_REGISTERED_GROUPSIGS_N; i++) {
    if(!strcasecmp(str, GROUPSIG_REGISTERED_GROUPSIGS[i]->desc->name)) {
      return GROUPSIG_REGISTERED_GROUPSIGS[i];
    }
  }

  return NULL;

}

const groupsig_t* groupsig_get_groupsig_from_code(uint8_t code) {

  uint8_t i;

  for(i=0; i<GROUPSIG_REGISTERED_GROUPSIGS_N; i++) {
    if(code == GROUPSIG_REGISTERED_GROUPSIGS[i]->desc->code) {
      return GROUPSIG_REGISTERED_GROUPSIGS[i];
    }
  }

  return NULL;

}

int groupsig_init(unsigned int seed) {

  /* Initialize system environment: right now, only seed the PRNGs */
  if(!(sysenv = sysenv_init(seed))) {
    return IERROR;
  }

  return IOK;

}

/* int groupsig_clear() { */

/*   if(sysenv_free(sysenv) == IERROR) { */
/*     return IERROR; */
/*   } */

/*   return IOK; */

/* } */

int groupsig_clear(uint8_t code) {

  return groupsig_sysenv_free(code);

}

groupsig_config_t* groupsig_config_init(uint8_t code) {

  const groupsig_t *gs;

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(code))) {
    return NULL;
  }  

  /* Run the CONFIG INIT action */
  return gs->config_init();

}

int groupsig_config_free(groupsig_config_t *cfg) {

  const groupsig_t *gs;

  if(!cfg) {
    return IOK;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(cfg->scheme))) {
    return IERROR;
  }  

  /* Run the CONFIG INIT action */
  return gs->config_free(cfg);

}

int groupsig_sysenv_update(uint8_t code, void *data) {

  const groupsig_t *gs;

  if(!data) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_sysenv_update", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(code))) {
    return IERROR;
  }

  /* For some schemes, there might not be and env_update action set. */
  if(!gs->sysenv_update) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_sysenv_update", __LINE__,
		   "It is not possible to update the environment for this scheme.",
		   LOGERROR);
    return IERROR;
  }

  /* Run the CONFIG INIT action */
  return gs->sysenv_update(data);

}

void* groupsig_sysenv_get(uint8_t code) {

  const groupsig_t *gs;

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(code))) {
    return NULL;
  }

  /* For some schemes, there might not be and env_update action set. */
  if(!gs->sysenv_get) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_sysenv_get", __LINE__,
		   "It is not possible to get the environment for this scheme.",
		   LOGERROR);
    return NULL;
  }

  /* Run the CONFIG INIT action */
  return gs->sysenv_get();

}

int groupsig_sysenv_free(uint8_t code) {

  const groupsig_t *gs;

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(code))) {
    return IERROR;
  }

  if(!sysenv) {
    return IOK;
  }

  /* For some schemes, there might not be and env_free action set. */
  if(!gs->sysenv_free) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_sysenv_free", __LINE__,
		   "It is not possible to update the environment for this scheme.",
		   LOGWARN);
  } else {

    /* Run the CONFIG INIT action */
    if(gs->sysenv_free() == IERROR) {
      return IERROR;
    }

  }

  if(sysenv) {
    mem_free(sysenv); sysenv = NULL;
  }

  return IOK;

}

int groupsig_setup(uint8_t code, groupsig_key_t *grpkey, 
		   groupsig_key_t *mgrkey, gml_t *gml, groupsig_config_t *config) {

  const groupsig_t *gs;

  /* The only mandatory parameters are grpkey and mgrkey; gml and config depend on
     the specific scheme (although they would probably be required too). */
  if(!grpkey || !mgrkey) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_setup", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(code))) {
    return IERROR;
  }  

  /* Run the SETUP action */
  if(gs->setup(grpkey, mgrkey, gml, config) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey) {

  const groupsig_t *gs;

  if(!memkey || !grpkey ||
     memkey->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_join_mem", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the JOINMEM action */
  if(gs->join_mem(memkey, grpkey) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey) {

  const groupsig_t *gs;

  /* The mandatory parameters at this point are the member key (of which some 
     attributes will probably be unset) and the manager and group keys; the gml,
     even though it is an [in,out] parameter, may be omitted in schemes that do not
     keep a transcript of joins (a.k.a. membership list). */
  if(!memkey || !mgrkey || !grpkey ||
     memkey->scheme != mgrkey->scheme || mgrkey->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_join_mgr", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the JOINMGR action */
  if(gs->join_mgr(gml, memkey, mgrkey, grpkey) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
		  groupsig_key_t *grpkey, unsigned int seed) {

  const groupsig_t *gs;

  if(!sig || !msg || !memkey || !grpkey ||
     sig->scheme != memkey->scheme || memkey->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_sign", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the SIGN action */
  if(gs->sign(sig, msg, memkey, grpkey, seed) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, groupsig_key_t *grpkey) {

  const groupsig_t *gs;

  if(!ok || !sig || !msg || !grpkey ||
     sig->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the VERIFY action */
  if(gs->verify(ok, sig, msg, grpkey) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_open(identity_t *id, groupsig_proof_t *proof, crl_t *crl, 
		  groupsig_signature_t *sig, groupsig_key_t *grpkey, 
		  groupsig_key_t *mgrkey, gml_t *gml) {

  const groupsig_t *gs;
  
  /* All the parameters are mandatory except the gml, which will depend on the
     specific scheme. Also, the type of ID will probably depend both on the
     scheme and the external application using the library. */
  if(!id || !sig || !grpkey || !mgrkey ||
     sig->scheme != grpkey->scheme || grpkey->scheme != mgrkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_open", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the OPEN action */
  return gs->open(id, proof, crl, sig, grpkey, mgrkey, gml);

}

int groupsig_open_verify(uint8_t *ok, identity_t *id,
			 groupsig_proof_t *proof, 
			 groupsig_signature_t *sig, 
			 groupsig_key_t *grpkey) {
  
  const groupsig_t *gs;
  
  /* All the parameters are mandatory. */
  if(!id || !proof || !sig || !grpkey || sig->scheme != grpkey->scheme ||
     proof->scheme != sig->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_open_verify", __LINE__, LOGERROR);
    return IERROR;
  }
  
  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  
  
  /* Run the OPEN VERIFY action */
  return gs->open_verify(ok, id, proof, sig, grpkey);

}

int groupsig_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index) {

  const groupsig_t *gs;

  /* All the parameters but the CRL here are mandatory, although the type of 
     trapdoor will depend on the scheme and application. */
  if(!trap || !gml) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(trap->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Run the REVEAL action */
  if(gs->reveal(trap, crl, gml, index) == IERROR){
    LOG_EINVAL(&logger, __FILE__, "groupsig_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  return IOK;

}

int groupsig_trace(uint8_t *ok, groupsig_signature_t *sig, 
		   groupsig_key_t *grpkey, crl_t *crl,
		   groupsig_key_t *mgrkey, gml_t *gml) {

  const groupsig_t *gs;

  /* Only ok, sig, grpkey, and crl are mandatory */
  if(!ok || !sig || !grpkey || !crl ||
     sig->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  /* If we have a manager  key its scheme needs to match the group key and we
   * must have a gml */
  if(mgrkey && (!gml || grpkey->scheme != mgrkey->scheme)){
    LOG_EINVAL(&logger, __FILE__, "groupsig_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the TRACE action */
  if(gs->trace(ok, sig, grpkey, crl, mgrkey, gml) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
		   groupsig_key_t *grpkey, groupsig_signature_t *sig) {

  const groupsig_t *gs;

  /* All parameters are mandatory here */
  if(!proof || !memkey || !grpkey || !sig ||
     proof->scheme != sig->scheme ||
     memkey->scheme != grpkey->scheme || grpkey->scheme != sig->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_claim", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the CLAIM action */
  if(gs->claim(proof, memkey, grpkey, sig) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
			  groupsig_signature_t *sig, groupsig_key_t *grpkey) {

  const groupsig_t *gs;

  /* All parameters are mandatory */
  if(!ok || !proof || !sig || !grpkey ||
     proof->scheme != sig->scheme || sig->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_claim_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the CLAIMVER action */
  if(gs->claim_verify(ok, proof, sig, grpkey) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			    groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs) {

  const groupsig_t *gs;

  /* All parameters are mandatory */
  if(!proof || !memkey || !grpkey || !sigs || !n_sigs ||
     proof->scheme != memkey->scheme || memkey->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_prove_equality", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the PROVEQCOMP action */
  if(gs->prove_equality(proof, memkey, grpkey, sigs, n_sigs) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, groupsig_key_t *grpkey,
				   groupsig_signature_t **sigs, uint16_t n_sigs) {

  const groupsig_t *gs;

  /* All parameters are mandatory */
  if(!ok || !proof || !grpkey || !sigs || !n_sigs ||
     proof->scheme != grpkey->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_prove_equality_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the group signature scheme from its code */
  if(!(gs = groupsig_get_groupsig_from_code(grpkey->scheme))) {
    return IERROR;
  }  

  /* Run the PROVEQVER action */
  if(gs->prove_equality_verify(ok, proof, grpkey, sigs, n_sigs) == IERROR) 
    return IERROR;

  return IOK;

}

int groupsig_get_code_from_str(uint8_t *code, char *name) {

  uint8_t i;

  if(!code || !name) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_get_groupsig_from_str", __LINE__, LOGERROR);
    return IERROR;
  }

  for(i=0; i<GROUPSIG_REGISTERED_GROUPSIGS_N; i++) {
    if(!strcasecmp(name, GROUPSIG_REGISTERED_GROUPSIGS[i]->desc->name)) {
      *code = GROUPSIG_REGISTERED_GROUPSIGS[i]->desc->code;
      return IOK;
    }
  }

  return IFAIL;

}

/* void* groupsig_options_parse(uint8_t gs_code, uint8_t action, int argc, char **argv) { */

/*   const groupsig_t *gs; */
/*   void *opt; */

/*   if(!argv) { */
/*     LOG_EINVAL(&logger, __FILE__, "groupsig_options_parse", __LINE__, LOGERROR); */
/*     return NULL; */
/*   } */

/*   /\* Get the group signature scheme from its code *\/ */
/*   if(!(gs = groupsig_get_groupsig_from_code(gs_code))) { */
/*     return NULL; */
/*   } */

/*   opt = gs->parse_options(action, argc, argv); */

/*   return opt; */

/* } */

/* int groupsig_options_free(void *opt, uint8_t gs_code) { */

/*   if(!opt) { */
/*     LOG_EINVAL(&logger, __FILE__, "groupsig_options_free", __LINE__, LOGERROR); */
/*     return IERROR; */
/*   } */

/*   switch(gs_code) { */
/*   case GROUPSIG_KTY04_CODE: */
/*     return kty04_options_free(opt); */
/*   default: */
/*     LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_options_free", __LINE__, */
/* 		   "Unknown groupsig.", LOGERROR); */
/*     return IERROR; */
/*   } */
  
/*   return IERROR; */
  
/* } */

/* groupsig.c ends here */
