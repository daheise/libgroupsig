/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun ene 21 16:01:57 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:34 2013 (-0400)
 *           By: jesus
 *     Update #: 19
 * URL: 
 */
#include "config.h"
#include "include/identity_handles.h"

const identity_handle_t* identity_handle_from_code(uint8_t code) {

  int i;

  for(i=0; i<IDENTITY_HANDLES_N; i++) {
    if(code == IDENTITY_HANDLES[i]->scheme) 
      return IDENTITY_HANDLES[i];
  }

  return NULL;

}

identity_t* identity_init(uint8_t code) {

  const identity_handle_t *idh;

  if(!(idh = identity_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "identity_init", __LINE__, LOGERROR);
    return NULL;
  }

  return idh->init();

}

int identity_free(identity_t *id) {

  const identity_handle_t *idh;

  if(!(idh = identity_handle_from_code(id->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "identity_free", __LINE__, LOGERROR);
    return IERROR;
  }

  return idh->free(id);

}

int identity_copy(identity_t *dst, identity_t *src) {

  const identity_handle_t *idh;

  if(dst->scheme != src->scheme) {
    LOG_EINVAL(&logger, __FILE__, "identity_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(idh = identity_handle_from_code(dst->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "identity_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  return idh->copy(dst, src);

}

uint8_t identity_cmp(identity_t *id1, identity_t *id2) {

 const identity_handle_t *idh;

 if(!id1 || !id2 || id1->scheme != id2->scheme) {
   LOG_EINVAL(&logger, __FILE__, "identity_cmp", __LINE__, LOGERROR);
   return UINT8_MAX;
 }

  if(!(idh = identity_handle_from_code(id1->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "identity_cmp", __LINE__, LOGERROR);
    return UINT8_MAX;
  }

  return idh->cmp(id1, id2);

}

char* identity_to_string(identity_t *id) {

  const identity_handle_t *idh;

  if(!(idh = identity_handle_from_code(id->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "identity_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return idh->to_string(id);

}

identity_t* identity_from_string(uint8_t code, char *sid) {

  const identity_handle_t *idh;

  if(!(idh = identity_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "identity_from_string", __LINE__, LOGERROR);
    return NULL;
  }

  return idh->from_string(sid);

}


/* identity.c ends here */
