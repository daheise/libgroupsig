/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: join.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue may 10 11:12:56 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 20:44:50 2013 (+0200)
 *           By: jesus
 *     Update #: 160
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "kty04.h"
#include "groupsig/kty04/sphere.h"
#include "groupsig/kty04/grp_key.h"
#include "groupsig/kty04/mem_key.h"
#include "bigz.h"
#include "sys/mem.h"

/**
 * @todo The Join procedure includes a protocol for non-adaptive drawing of 
 * random powers such that the group member gets x, and the group manager gets
 * b^x (mod n). For now, and for testing purposes, we just let the user choose 
 * a random x and send it to the manager, but we must implement it as soon as
 * everything is working correctly.
 */

/* static int _join_mem_draw_random_pow(kty04_grp_key_t *grpkey, kty04_mem_key_t *memkey) { */
/*   return IERROR; */
/* } */

int kty04_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey) {

  kty04_grp_key_t *gkey;
  kty04_mem_key_t *mkey;

  if(!memkey || memkey->scheme != GROUPSIG_KTY04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_KTY04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "kty04_join_mem", __LINE__, LOGERROR);
    return IERROR;
  }
 
  gkey = (kty04_grp_key_t *) grpkey->key;
  mkey = (kty04_mem_key_t *) memkey->key;

  /* Get a random power in the inner sphere of Lambda */
#ifdef DEBUG
  log_printf(&logger, LOGDEBUG,
	     "@todo Warning: This should be done with a protocol for non-adaptive"
	  " drawing of random powers!\n");
#endif

  if(sphere_get_random(gkey->inner_lambda, mkey->xx) == IERROR) {
    return IERROR;
  }
  
  /* Set C = b^xx */
  if(bigz_powm(mkey->C, gkey->b, mkey->xx, gkey->n) == IERROR) {
    return IERROR;
  }

  return IOK;

}

/* join.c ends here */
