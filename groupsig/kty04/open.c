/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: open.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:53:13 2012 (+0200)
 * @version: 
 * Last-Updated: lun jul 20 21:26:09 2015 (+0200)
 *           By: jesus
 *     Update #: 91
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "types.h"
#include "sysenv.h"
#include "bigz.h"
#include "kty04.h"
#include "groupsig/kty04/grp_key.h"
#include "groupsig/kty04/mgr_key.h"
#include "groupsig/kty04/signature.h"
#include "groupsig/kty04/gml.h"
#include "groupsig/kty04/identity.h"

int kty04_open(identity_t *id, groupsig_proof_t *proof, 
	       crl_t *crl, groupsig_signature_t *sig, 
	       groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml) {

  kty04_signature_t *kty04_sig;
  kty04_mgr_key_t *mkey;
  kty04_grp_key_t *gkey;
  bigz_t Ai, T1;  
  uint64_t i;
  uint8_t match;

  if(!id || !sig || sig->scheme != GROUPSIG_KTY04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_KTY04_CODE ||
     !mgrkey || mgrkey->scheme != GROUPSIG_KTY04_CODE ||
     !gml) {
    LOG_EINVAL(&logger, __FILE__, "kty04_open", __LINE__, LOGERROR);
    return IERROR;
  }

  kty04_sig = (kty04_signature_t *) sig->sig;
  mkey = (kty04_mgr_key_t *) mgrkey->key;
  gkey = (kty04_grp_key_t *) grpkey->key;

  /* Get T2^(-x)*T1. Note that sig->A[2] = T2^(-1) and sig->A[6] = T1^(-1) */
  if(!(T1 = bigz_init())) return IERROR;
  if(bigz_invert(T1, kty04_sig->A[6], gkey->n) == IERROR) { 
    bigz_free(T1); 
    return IERROR;
  }

  if(!(Ai = bigz_init())) {
    bigz_free(T1);
    return IERROR;
  }

  if(bigz_powm(Ai, kty04_sig->A[2], mkey->x, gkey->n) == IERROR) {
    bigz_free(T1); bigz_free(Ai);
    return IERROR;
  }
  
  if(bigz_mul(Ai, Ai, T1) == IERROR) {
    bigz_free(T1); bigz_free(Ai);
    return IERROR;    
  }

  if(bigz_mod(Ai, Ai, gkey->n) == IERROR) {
    bigz_free(T1); bigz_free(Ai);
    return IERROR;
  }

  bigz_free(T1);

  /* Go through all the member keys in gml looking for a match memkey->A == Ai */
  match = 0;
  for(i=0; i<gml->n; i++) {  

    errno = 0;

    if(!bigz_cmp(((kty04_gml_entry_t *) gml_get(gml, i))->A, Ai)) {

      if(errno) {
	bigz_free(Ai);
	return IERROR;
      }

      /* Get the identity from the matched entry. */
      if(kty04_identity_copy(id, ((kty04_gml_entry_t *) gml_get(gml, i))->id) == IERROR) {
	bigz_free(Ai);
	return IERROR;
      }

      match = 1;
      break;

    }

    if(errno) {
      bigz_free(Ai);
      return IERROR;
    }

  }

  bigz_free(Ai);

  /* No match: FAIL */
  if(!match) {
    return IFAIL;
  }

  /* /\* If we have received a CRL, update it with the "revoked" member *\/ */
  /* if(crl) { */

  /*   if(!(crl_entry = kty04_crl_entry_init())) { */
  /*     return IERROR; */
  /*   } */
    
  /*   if(kty04_identity_copy(crl_entry->id, gml_entry->id) == IERROR) { */
  /*     kty04_crl_entry_free(crl_entry); */
  /*     return IERROR; */
  /*   } */
    
  /*   crl_entry->trapdoor = trap; */

  /*   if(kty04_crl_insert(crl, crl_entry) == IERROR) { */
  /*     kty04_crl_entry_free(crl_entry); */
  /*     return IERROR; */
  /*   } */

  /* } */

  return IOK;

}

/* open.c ends here */
