/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: reveal.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 12:32:51 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 20:45:21 2013 (+0200)
 *           By: jesus
 *     Update #: 103
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "sysenv.h"
#include "bigz.h"
#include "kty04.h"
#include "groupsig/kty04/mem_key.h"
#include "groupsig/kty04/gml.h"
#include "groupsig/kty04/crl.h"
#include "groupsig/kty04/trapdoor.h"

int kty04_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index) {

  kty04_crl_entry_t *crl_entry;
  kty04_gml_entry_t *gml_entry;
  bigz_t trapdoor;

  if(!trap || trap->scheme != GROUPSIG_KTY04_CODE ||
     !gml || gml->scheme != GROUPSIG_KTY04_CODE ||
     (crl && crl->scheme != GROUPSIG_KTY04_CODE)) {
    LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  trapdoor = *(bigz_t *) trap->trap;

  /* The tracing trapdoor for the i-th member is the x field of its member key */
  if(!(gml_entry = ((kty04_gml_entry_t *) gml_get(gml, index)))) {
    LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  if(bigz_set(trapdoor, *(kty04_trapdoor_t *) gml_entry->trapdoor->trap) == IERROR) {
    LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  /* If we have received a CRL, update it with the "revoked" member */
  if(crl) {

    if(!(crl_entry = kty04_crl_entry_init())) {
      LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
      return IERROR;
    }

    bigz_set(crl_entry->trapdoor, trapdoor);

    if(kty04_identity_copy(crl_entry->id, gml_entry->id) == IERROR) {
      LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
      kty04_crl_entry_free(crl_entry);
      return IERROR;
    }

    if(kty04_crl_insert(crl, crl_entry) == IERROR) {
      LOG_EINVAL(&logger, __FILE__, "kty04_reveal", __LINE__, LOGERROR);
      kty04_crl_entry_free(crl_entry);
      return IERROR;
    }

  }

  return IOK;
}

/* reveal.c ends here */
