/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trace.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jun 11 14:55:35 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 20:44:41 2013 (+0200)
 *           By: jesus
 *     Update #: 92
 * URL: 
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include "include/crl.h"
#include "bigz.h"
#include "kty04.h"
#include "groupsig/kty04/signature.h"
#include "groupsig/kty04/grp_key.h"
#include "groupsig/kty04/crl.h"

int kty04_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml) {

  kty04_signature_t *kty04_sig;
  kty04_grp_key_t *gkey;
  bigz_t a3t, aux;
  uint64_t i;
  uint8_t revoked;

  if(!ok || !sig || sig->scheme != GROUPSIG_KTY04_CODE ||
     !grpkey || grpkey->scheme != GROUPSIG_KTY04_CODE ||
     !crl) {
    LOG_EINVAL(&logger, __FILE__, "kty04_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  gkey = (kty04_grp_key_t *) grpkey->key;
  kty04_sig = (kty04_signature_t *) sig->sig;

  /* To test whether the signature has been issued with the member with tracing
     trapdoor "trapdoor", we have to check if sig->A[3]^trapdoor == sig->A[11]. */
  if(!(a3t = bigz_init())) return IERROR;

  i = 0; revoked = 0;
  while(i < crl->n) {

    /* Get the next trapdoor to test */
    aux = ((kty04_crl_entry_t *) crl_get(crl, i))->trapdoor;

    if(bigz_powm(a3t, kty04_sig->A[3], aux, gkey->n)) {
      bigz_free(a3t);
      return IERROR;
    }
  
    errno = 0;
    if(!bigz_cmp(a3t, kty04_sig->A[11])) {
      if(errno) {
	bigz_free(a3t);
	return IERROR;
      }
      revoked = 1;
      break;
    } else {
      if(errno) {
	bigz_free(a3t);
	return IERROR;
      }
    }

    i++;

  }

  *ok = revoked;

  bigz_free(a3t);

  return IOK;

}

/* trace.c ends here */
