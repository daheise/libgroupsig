/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 11:21:21 2013 (+0100)
 * @version: 
 * Last-Updated: mié ago 21 20:27:05 2013 (+0200)
 *           By: jesus
 *     Update #: 29
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include "types.h"
#include "sys/mem.h"
#include "misc/misc.h"
#include "groupsig/kty04/trapdoor.h"

trapdoor_t* kty04_trapdoor_init() {

  trapdoor_t *trap;
  kty04_trapdoor_t *kty04_trap;

  if(!(trap = (trapdoor_t *) mem_malloc(sizeof(trapdoor_t)))) {
    LOG_ERRORCODE(&logger, __FILE__, "kty04_trapdoor_init", __LINE__, errno, LOGERROR);
    return NULL;
  }

  if(!(kty04_trap = (kty04_trapdoor_t *) mem_malloc(sizeof(kty04_trapdoor_t)))) {
    mem_free(trap); trap = NULL;
    LOG_ERRORCODE(&logger, __FILE__, "kty04_trapdoor_init", __LINE__, errno, LOGERROR);
    return NULL;
  }

  /* A KTY04 identity is the index pointing to an entry in the GML, we initialize 
     it to UINT64_MAX */
  *kty04_trap = bigz_init();
  
  trap->scheme = GROUPSIG_KTY04_CODE;
  trap->trap = kty04_trap;

  return trap;

}

int kty04_trapdoor_free(trapdoor_t *trap) {

  if(!trap) {
    LOG_EINVAL_MSG(&logger, __FILE__, "kty04_trapdoor_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  if(trap->scheme != GROUPSIG_KTY04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "kty04_trapdoor_free", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Currently, it is just an uint64_t* */
  bigz_free(*((kty04_trapdoor_t *) trap->trap));
  mem_free((kty04_trapdoor_t *) trap->trap); trap->trap = NULL;  
  mem_free(trap);

  return IOK;

}

int kty04_trapdoor_copy(trapdoor_t *dst, trapdoor_t *src) {

  if(!dst || dst->scheme != GROUPSIG_KTY04_CODE ||
     !src || src->scheme != GROUPSIG_KTY04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "kty04_trapdoor_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  bigz_set(*((kty04_trapdoor_t *) dst->trap), *((kty04_trapdoor_t *) src->trap));

  return IOK;

}

char* kty04_trapdoor_to_string(trapdoor_t *trap) {

  if(!trap || trap->scheme != GROUPSIG_KTY04_CODE) {
    LOG_EINVAL(&logger, __FILE__, "kty04_trapdoor_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  /* Currently, the KTY04 trapdoors are bigz_t's */
  return bigz_get_str(10, *((kty04_trapdoor_t *)trap->trap));

}

trapdoor_t* kty04_trapdoor_from_string(char *strap) {

  trapdoor_t *trap;

  if(!strap) {
    LOG_EINVAL(&logger, __FILE__, "kty04_trapdoor_from_string", __LINE__, LOGERROR);
    return NULL;
  }

  if(!(trap = kty04_trapdoor_init())) {
    return NULL;
  }

  /* Currently, KTY04 identities are bigz_t's */
  if(bigz_set_str(*(kty04_trapdoor_t *) trap->trap, strap, 10) == IERROR) {
    kty04_trapdoor_free(trap);
    return NULL;
  }

  return trap;

}

/* identity.c ends here */
