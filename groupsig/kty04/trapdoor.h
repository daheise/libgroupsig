/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trapdoor.h
 * @brief: KTY04 trapdoors. 
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 11:16:11 2013 (+0100)
 * @version: 0.1
 * Last-Updated: mié sep 25 20:49:09 2013 (+0200)
 *           By: jesus
 *     Update #: 21
 * URL: bitbucket.org/jdiazvico/libgroupsig
 * @todo Trapdoors should be made independent of the group signature scheme so
 *  that several schemes can share the same type of trapdoor.
 */

#ifndef _KTY04_TRAPDOOR_H
#define _KTY04_TRAPDOOR_H

#include "bigz.h"
#include "include/trapdoor.h"
#include "kty04.h"

/**
 * KTY04 trapdoors.
 */
typedef bigz_t kty04_trapdoor_t;

/** 
 * @fn void* kty04_trapdoor_init()
 * @brief Allocates memory for a KTY04 trapdoor and sets its values to defaults.
 * 
 * @return A pointer to the allocated memory or NULL if error.
 */
trapdoor_t* kty04_trapdoor_init();

/** 
 * @fn int kty04_trapdoor_free(void *trap)
 * @brief Frees the memory allocated for a KTY04 trapdoor.
 *
 * @param[in,out] id The trapdoor to free.
 * 
 * @return IOK.
 */
int kty04_trapdoor_free(trapdoor_t *trap);

/** 
 * @fn int kty04_trapdoor_copy(trapdoor_t *dst, trapdoor_t *src)
 * @brief Copies the source trapdoor into the destination trapdoor.
 *
 * @param[in,out] dst The destination trapdoor. Initialized by the caller.
 * @param[in] src The source trapdoor.
 * 
 * @return IOK or IERROR.
 */
int kty04_trapdoor_copy(trapdoor_t *dst, trapdoor_t *src);

/** 
 * @fn char* kty04_trapdoor_to_string(trapdoor_t *trap)
 * @brief Converts the given KTY04 id into a printable string.
 *
 * @param[in] trap The trapdoor to convert
 * 
 * @return A pointer to the produced string or NULL if error.
 */
char* kty04_trapdoor_to_string(trapdoor_t *trap);

/** 
 * @fn trapdoor_t* kty04_trapdoor_from_string(char *strap)
 * @brief Parses the given string as  KTY04 trapdoor.
 *
 * @param[in] strap The string containing the KTY04 trapdoor.
 * 
 * @return A pointer to the retrieved KTY04 trapdoor or NULL if error.
 */
trapdoor_t* kty04_trapdoor_from_string(char *strap);

/**
 * @var kty04_trapdoor_handle
 * @brief Set of functions to manage KTY04 trapdoors.
 */
static const trapdoor_handle_t kty04_trapdoor_handle = {
  GROUPSIG_KTY04_CODE, /**< The scheme code. */
  &kty04_trapdoor_init, /**< Initializes trapdoors. */
  &kty04_trapdoor_free, /**< Frees trapdoors. */
  &kty04_trapdoor_copy, /**< Copies trapdoors. */
  &kty04_trapdoor_to_string, /**< Converts trapdoors to printable strings. */
  &kty04_trapdoor_from_string /**< Gets trapdoors from printable strings. */
};

#endif /* _KTY04_TRAPDOOR_H */

/* trapdoor.h ends here */
