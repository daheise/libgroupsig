/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar ene 15 14:40:19 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:41 2013 (-0400)
 *           By: jesus
 *     Update #: 19
 * URL: 
 */
#include "config.h"
#include "include/mgr_key_handles.h"
#include "include/key.h"

const mgr_key_handle_t* groupsig_mgr_key_handle_from_code(uint8_t code) {

  int i;

  for(i=0; i<GROUPSIG_MGR_KEY_HANDLES_N; i++) {
    if(GROUPSIG_MGR_KEY_HANDLES[i]->code == code)
      return GROUPSIG_MGR_KEY_HANDLES[i];
  }

  return NULL;

}

groupsig_key_t* groupsig_mgr_key_init(uint8_t code) {

  const mgr_key_handle_t *gkh;

  if(!(gkh = groupsig_mgr_key_handle_from_code(code))) {
    return NULL;
  }  

  return gkh->init();

}

int groupsig_mgr_key_free(groupsig_key_t *key) {

  const mgr_key_handle_t *gkh;

  if(!key) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_mgr_key_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(key->scheme))) {
    return IERROR;
  }
    
  gkh->free(key);
  
  return IOK;

}

int groupsig_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src) {

  const mgr_key_handle_t *gkh;

  if(!dst || !src ||
     dst->scheme != src->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_mgr_key_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(dst->scheme))) {
    return IERROR;
  }

  return gkh->copy(dst, src);

}

int groupsig_mgr_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format) {

  const mgr_key_handle_t *gkh;

  if(!key) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_mgr_key_", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(key->scheme))) {
    return IERROR;
  }

  return gkh->get_size_in_format(key, format);

}

int groupsig_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst) {

  const mgr_key_handle_t *gkh;

  if(!key || !dst) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_mgr_key_export", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(key->scheme))) {
    return IERROR;
  }

  return gkh->export(key, format, dst);

}

groupsig_key_t* groupsig_mgr_key_import(uint8_t code, groupsig_key_format_t format, void *source) {

  const mgr_key_handle_t *gkh;

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_mgr_key_import", __LINE__, LOGERROR);
    return NULL;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(code))) {
    return NULL;
  }

  return gkh->import(format, source);

}

char* groupsig_mgr_key_to_string(groupsig_key_t *key) {

  const mgr_key_handle_t *gkh;

  if(!key) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_mgr_key_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  if(!(gkh = groupsig_mgr_key_handle_from_code(key->scheme))) {
    return NULL;
  }

  return gkh->to_string(key);

}

/* mgr_key.c ends here */
