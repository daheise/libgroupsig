/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: proof.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar ene 15 14:41:35 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:51 2013 (-0400)
 *           By: jesus
 *     Update #: 12
 * URL: 
 */
#include "config.h"
#include "include/proof_handles.h"

const groupsig_proof_handle_t* groupsig_proof_handle_from_code(uint8_t code) {

  int i;

  for(i=0; i<GROUPSIG_PROOF_HANDLES_N; i++) {
    if(code == GROUPSIG_PROOF_HANDLES[i]->scheme) {
      return GROUPSIG_PROOF_HANDLES[i];
    }
  }

  return NULL;

}


groupsig_proof_t* groupsig_proof_init(uint8_t code) {

  const groupsig_proof_handle_t *gph;

  if(!(gph = groupsig_proof_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_init", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gph->init();

}

int groupsig_proof_free(groupsig_proof_t *proof) {

  const groupsig_proof_handle_t *gph;

  if(!proof) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_proof_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }
  
  if(!(gph = groupsig_proof_handle_from_code(proof->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_free", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gph->free(proof);
  
}

int groupsig_proof_get_size_in_format(groupsig_proof_t *proof, groupsig_proof_format_t format) {

  const groupsig_proof_handle_t *gph;

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_get_size_in_format", __LINE__,
	       LOGERROR);
    return IERROR;
  }
  
  if(!(gph = groupsig_proof_handle_from_code(proof->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_get_size_in_format", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gph->get_size_in_format(proof, format);

}

int groupsig_proof_export(groupsig_proof_t *proof, 
			    groupsig_proof_format_t format, void *dst) {

  const groupsig_proof_handle_t *gph;

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_export", __LINE__,
	       LOGERROR);
    return IERROR;
  }
  
  if(!(gph = groupsig_proof_handle_from_code(proof->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_export", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gph->export(proof, format, dst);

}

groupsig_proof_t* groupsig_proof_import(uint8_t code, 
					groupsig_proof_format_t format, 
					void *source) {

  const groupsig_proof_handle_t *gph;

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_import", __LINE__,
	       LOGERROR);
    return NULL;
  }

  if(!(gph = groupsig_proof_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_import", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gph->import(format, source);

}

char* groupsig_proof_to_string(groupsig_proof_t *proof) {

  const groupsig_proof_handle_t *gph;

  if(!proof) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_to_string", __LINE__,
	       LOGERROR);
    return NULL;
  }

  if(!(gph = groupsig_proof_handle_from_code(proof->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_proof_to_string", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gph->to_string(proof);

}

/* proof.c ends here */
