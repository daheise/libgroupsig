/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar ene 15 14:42:03 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:55 2013 (-0400)
 *           By: jesus
 *     Update #: 19
 * URL: 
 */
#include "config.h"
#include "include/signature_handles.h"

const groupsig_signature_handle_t* groupsig_signature_handle_from_code(uint8_t code) {

  int i;

  for(i=0; i<GROUPSIG_SIGNATURE_HANDLES_N; i++) {
    if(GROUPSIG_SIGNATURE_HANDLES[i]->scheme == code)
      return GROUPSIG_SIGNATURE_HANDLES[i];
  }

  return NULL;

}

groupsig_signature_t* groupsig_signature_init(uint8_t code) {

  const groupsig_signature_handle_t *gsh;

  if(!(gsh = groupsig_signature_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_init", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gsh->init();

}

int groupsig_signature_free(groupsig_signature_t *sig) {

  const groupsig_signature_handle_t *gsh;

  if(!sig) {
    LOG_EINVAL_MSG(&logger, __FILE__, "groupsig_signature_free", __LINE__,
		   "Nothing to free.", LOGWARN);
    return IOK;
  }
  
  if(!(gsh = groupsig_signature_handle_from_code(sig->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_free", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gsh->free(sig);
  
}

int groupsig_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src) {

  const groupsig_signature_handle_t *gsh;

  if(!dst || !src ||
     dst->scheme != src->scheme) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(gsh = groupsig_signature_handle_from_code(dst->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_copy", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gsh->copy(dst, src);

}

int groupsig_signature_get_size_in_format(groupsig_signature_t *sig, 
					  groupsig_signature_format_t format) {

  const groupsig_signature_handle_t *gsh;

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_get_size_in_format", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  if(!(gsh = groupsig_signature_handle_from_code(sig->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_get_size_in_format", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gsh->get_size_in_format(sig, format);

}


int groupsig_signature_export(groupsig_signature_t *sig, 
			      groupsig_signature_format_t format, void *dst) {

  const groupsig_signature_handle_t *gsh;

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_export", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  if(!(gsh = groupsig_signature_handle_from_code(sig->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_export", __LINE__,
	       LOGERROR);
    return IERROR;
  }

  return gsh->export(sig, format, dst);

}

groupsig_signature_t* groupsig_signature_import(uint8_t code, 
						groupsig_signature_format_t format, 
						void *source) {

  const groupsig_signature_handle_t *gsh;

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_import", __LINE__,
	       LOGERROR);
    return NULL;
  }

  if(!(gsh = groupsig_signature_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_import", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gsh->import(format, source);

}

char* groupsig_signature_to_string(groupsig_signature_t *sig) {

  const groupsig_signature_handle_t *gsh;

  if(!sig) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_to_string", __LINE__,
	       LOGERROR);
    return NULL;
  }

  if(!(gsh = groupsig_signature_handle_from_code(sig->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "groupsig_signature_to_string", __LINE__,
	       LOGERROR);
    return NULL;
  }

  return gsh->to_string(sig);

}


/* signature.c ends here */
