/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trapdoor.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun ene 21 16:01:57 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:14:59 2013 (-0400)
 *           By: jesus
 *     Update #: 15
 * URL: 
 */
#include "config.h"
#include "include/trapdoor_handles.h"

const trapdoor_handle_t* trapdoor_handle_from_code(uint8_t code) {

  int i;

  for(i=0; i<TRAPDOOR_HANDLES_N; i++) {
    if(code == TRAPDOOR_HANDLES[i]->scheme) 
      return TRAPDOOR_HANDLES[i];
  }

  return NULL;

}

trapdoor_t* trapdoor_init(uint8_t code) {

  const trapdoor_handle_t *tdh;

  if(!(tdh = trapdoor_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_init", __LINE__, LOGERROR);
    return NULL;
  }

  return tdh->init();

}

int trapdoor_free(trapdoor_t *trap) {

  const trapdoor_handle_t *tdh;

  if(!(tdh = trapdoor_handle_from_code(trap->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_free", __LINE__, LOGERROR);
    return IERROR;
  }

  return tdh->free(trap);

}

int trapdoor_copy(trapdoor_t *dst, trapdoor_t *src) {

  const trapdoor_handle_t *tdh;

  if(dst->scheme != src->scheme) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(tdh = trapdoor_handle_from_code(dst->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_copy", __LINE__, LOGERROR);
    return IERROR;
  }

  return tdh->copy(dst, src);

}

char* trapdoor_to_string(trapdoor_t *trap) {

  const trapdoor_handle_t *tdh;

  if(!(tdh = trapdoor_handle_from_code(trap->scheme))) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return tdh->to_string(trap);

}

trapdoor_t* trapdoor_from_string(uint8_t code, char *strap) {

  const trapdoor_handle_t *tdh;

  if(!(tdh = trapdoor_handle_from_code(code))) {
    LOG_EINVAL(&logger, __FILE__, "trapdoor_to_string", __LINE__, LOGERROR);
    return NULL;
  }

  return tdh->from_string(strap);

}

/* trapdoor.c ends here */
