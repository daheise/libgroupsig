/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: action.h
 * @brief: Codes, names and functions for conversion among themselves for 
 *  supported actions.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié ago  1 14:33:56 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun may 13 23:10:17 2013 (+0200)
 *           By: jesus
 *     Update #: 40
 * URL: bitbucket.org/jdiazvico/libgroupsig 
 */

#ifndef _ACTION_H
#define _ACTION_H

#include <stdint.h>

/**
 * @struct action_description_t
 * @brief Holds together an action's code and the corresponding
 *  name.
 */
typedef struct {
  uint8_t code; /**< The code. */
  char name[20]; /**< The name. */
} action_description_t;

/**
 * @def ACTION_SETUP_CODE
 * @brief Code for the setup functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_SETUP_CODE 0

/**
 * @def ACTION_SETUP_NAME
 * @brief Name for the setup functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_SETUP_NAME "SETUP"

/**
 * @def ACTION_JOIN_MEM_CODE
 * @brief Code for the member join functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_JOIN_MEM_CODE 1

/**
 * @def ACTION_JOIN_MEM_NAME
 * @brief Name for the member join functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_JOIN_MEM_NAME "JOINMEM"

/**
 * @def ACTION_JOIN_MGR_CODE
 * @brief Code for the manager join functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_JOIN_MGR_CODE 2

/**
 * @def ACTION_JOIN_MGR_NAME
 * @brief Name for the manager join functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_JOIN_MGR_NAME "JOINMGR"

/**
 * @def ACTION_SIGN_CODE
 * @brief Code for the signing functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_SIGN_CODE 3

/**
 * @def ACTION_SIGN_NAME
 * @brief Name for the signing functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_SIGN_NAME "SIGN"

/**
 * @def ACTION_VERIFY_CODE
 * @brief Code for the signature verification functionality of the implemented 
 *  group signature schemes.
 */
#define ACTION_VERIFY_CODE 4

/**
 * @def ACTION_VERIFY_NAME
 * @brief Name for the signature verification functionality of the implemented 
 *  group signature schemes.
 */
#define ACTION_VERIFY_NAME "VERIFY"

/**
 * @def ACTION_OPEN_CODE
 * @brief Code for the signature opening functionality of the implemented group 
 *  signature schemes.
 */
#define ACTION_OPEN_CODE 5

/**
 * @def ACTION_OPEN_NAME
 * @brief Name for the signature opening functionality of the implemented group 
 *  signature schemes.
 */
#define ACTION_OPEN_NAME "OPEN"

/**
 * @def ACTION_REVEAL_CODE
 * @brief Code for the functionality for revealing the signers identities of the implemented 
 *  group signature schemes.
 */
#define ACTION_REVEAL_CODE 6

/**
 * @def ACTION_REVEAL_NAME
 * @brief Name for the functionality for revealing the signers identities of the implemented
 *  group signature schemes.
 */
#define ACTION_REVEAL_NAME "REVEAL"

/**
 * @def ACTION_TRACE_CODE
 * @brief Code for the signer tracing functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_TRACE_CODE 7

/**
 * @def ACTION_TRACE_NAME
 * @brief Name for the signer tracing functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_TRACE_NAME "TRACE"

/**
 * @def ACTION_CLAIM_CODE
 * @brief Code for the authorship claiming functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_CLAIM_CODE 8

/**
 * @def ACTION_CLAIM_NAME
 * @brief Name for the authorship claiming functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_CLAIM_NAME "CLAIM"

/**
 * @def ACTION_CLAIM_VERIFY_CODE
 * @brief Code for the claim verification functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_CLAIM_VERIFY_CODE 9

/**
 * @def ACTION_CLAIM_VERIFY_NAME
 * @brief Name for the claim verification functionality of the implemented group signature 
 *  schemes.
 */
#define ACTION_CLAIM_VERIFY_NAME "CLAIMVER"

/**
 * @def ACTION_PROVE_EQUALITY_CODE
 * @brief Code for the functionality for proving having issued several signatures.
 */
#define ACTION_PROVE_EQUALITY_CODE 10

/**
 * @def ACTION_PROVE_EQUALITY_NAME
 * @brief Name for the functionality for proving having issued several signatures.
 */
#define ACTION_PROVE_EQUALITY_NAME "PROVEQ"

/**
 * @def ACTION_PROVE_EQUALITY_VERIFY_CODE
 * @brief Code for the functionality for verification of the proves demonstrating
 *  issuance of several signatures.
 */
#define ACTION_PROVE_EQUALITY_VERIFY_CODE 11

/**
 * @def ACTION_SETUP_NAME
 * @brief Name for the functionality for verification of the proves demonstrating
 *  issuance of several signatures.
 */
#define ACTION_PROVE_EQUALITY_VERIFY_NAME "PROVEQVER"

/* MISCELLANEOUS ACTIONS */

/**
 * @def ACTION_PRINT_GRP_KEY_CODE
 * @brief Code for the functionality for printing group keys.
 *  schemes.
 */
#define ACTION_PRINT_GRP_KEY_CODE 12

/**
 * @def ACTION_PRINT_GRP_KEY_NAME
 * @brief Name for the functionality for printing group keys.
 */
#define ACTION_PRINT_GRP_KEY_NAME "PRINTGRPKEY"

/**
 * @def ACTION_PRINT_MGR_KEY_CODE
 * @brief Code for the functionality for printing group keys.
 */
#define ACTION_PRINT_MGR_KEY_CODE 13

/**
 * @def ACTION_PRINT_MGR_KEY_NAME
 * @brief Name for the functionality for printing manager keys.
 */
#define ACTION_PRINT_MGR_KEY_NAME "PRINTMGRKEY"

/**
 * @def ACTION_PRINT_MEM_KEY_CODE
 * @brief Code for the functionality for printing member keys.
 */
#define ACTION_PRINT_MEM_KEY_CODE 14

/**
 * @def ACTION_PRINT_MEM_KEY_NAME
 * @brief Name for the functionality for printing member keys.
 */
#define ACTION_PRINT_MEM_KEY_NAME "PRINTMEMKEY"

/**
 * @def ACTION_PRINT_SIG_CODE
 * @brief Code for the functionality for printing group signatures.
 */
#define ACTION_PRINT_SIG_CODE 15

/**
 * @def ACTION_PRINT_SIG_NAME
 * @brief Name for the functionality for printing group signatures.
 */
#define ACTION_PRINT_SIG_NAME "PRINTSIG"

/**
 * @def ACTIONS_N
 * @brief Number of predefined actions.
 */
#define ACTIONS_N 16

/**
 * @var ACTIONS
 * @brief Bundles all the predefined actions codes and names.
 */
static const action_description_t ACTIONS[ACTIONS_N] = {
  { ACTION_SETUP_CODE, ACTION_SETUP_NAME },
  { ACTION_JOIN_MEM_CODE, ACTION_JOIN_MEM_NAME },
  { ACTION_JOIN_MGR_CODE, ACTION_JOIN_MGR_NAME },
  { ACTION_SIGN_CODE, ACTION_SIGN_NAME },
  { ACTION_VERIFY_CODE, ACTION_VERIFY_NAME },
  { ACTION_OPEN_CODE, ACTION_OPEN_NAME },
  { ACTION_REVEAL_CODE, ACTION_REVEAL_NAME },
  { ACTION_TRACE_CODE, ACTION_TRACE_NAME },
  { ACTION_CLAIM_CODE, ACTION_CLAIM_NAME },
  { ACTION_CLAIM_VERIFY_CODE, ACTION_CLAIM_VERIFY_NAME },
  { ACTION_PROVE_EQUALITY_CODE, ACTION_PROVE_EQUALITY_NAME },
  { ACTION_PROVE_EQUALITY_VERIFY_CODE, ACTION_PROVE_EQUALITY_VERIFY_NAME },
  { ACTION_PRINT_GRP_KEY_CODE, ACTION_PRINT_GRP_KEY_NAME },
  { ACTION_PRINT_MGR_KEY_CODE, ACTION_PRINT_MGR_KEY_NAME },
  { ACTION_PRINT_MEM_KEY_CODE, ACTION_PRINT_MEM_KEY_NAME },
  { ACTION_PRINT_SIG_CODE, ACTION_PRINT_SIG_NAME },  
};

/** 
 * @fn int action_code_from_str(char *str, uint8_t *action)
 * @brief Sets <i>action</i> to the code associated to the given action name.
 *
 * @param[in] str The action name.
 * @param[in,out] action The action code.
 * 
 * @return IOK with action set to the action code. IFAIL if the name does not
 *  correspond to any action name, or IERROR if error.
 */
int action_code_from_str(char *str, uint8_t *action);

#endif /* _ACTION_H */

/* action.h ends here */
