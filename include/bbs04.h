/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: bbs04.h
 * @brief: Main definitions for the BBS04 group signature scheme.
 *
 * The BBS04 scheme is an implementation of the 2004 Boneh, Boyen and Shacham
 * paper "Short Group Signatures".
 *
 * @author: jesus
 * Maintainer: jesus
 * @date: vie jul  6 11:59:40 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun jul 20 21:21:34 2015 (+0200)
 *           By: jesus
 *     Update #: 166
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BBS04_H
#define _BBS04_H

/**
 * @def GROUPSIG_BBS04_CODE
 * @brief BBS04 scheme code.
 */
#define GROUPSIG_BBS04_CODE 1

/**
 * @def GROUPSIG_BBS04_NAME
 * @brief BBS04 scheme name.
 */
#define GROUPSIG_BBS04_NAME "BBS04"

#include <pbc/pbc.h>

#include "key.h"
#include "gml.h"
#include "crl.h"
#include "signature.h"
#include "proof.h"
#include "grp_key.h"
#include "mgr_key.h"
#include "mem_key.h"
#include "groupsig.h"
#include "bigz.h"

/**
 * @def BBS04_D_MAX
 * @brief Maximum discriminant for pairing generation.
 */
#define BBS04_D_MAX 1000000000

/** 
 * @def BBS04_SUPPORTED_KEY_FORMATS_N
 * @brief Number of supported key formats supported by BBS04.
 */
#define BBS04_SUPPORTED_KEY_FORMATS_N 6

/**
 * @var BBS04_SUPPORTED_KEY_FORMATS
 * @brief Codes of the key formats supported by BBS04.
 */
static const int BBS04_SUPPORTED_KEY_FORMATS[BBS04_SUPPORTED_KEY_FORMATS_N] = { 
  GROUPSIG_KEY_FORMAT_FILE_NULL,
  GROUPSIG_KEY_FORMAT_FILE_NULL_B64,
  GROUPSIG_KEY_FORMAT_BYTEARRAY,
  GROUPSIG_KEY_FORMAT_STRING_NULL_B64,
  GROUPSIG_KEY_FORMAT_MESSAGE_NULL,
  GROUPSIG_KEY_FORMAT_MESSAGE_NULL_B64,
};

/**
 * @var bbs04_description
 * @brief BBS04's description.
 */
static const groupsig_description_t bbs04_description = {
  GROUPSIG_BBS04_CODE, /**< BBS04's scheme code. */
  GROUPSIG_BBS04_NAME /**< BBS04's scheme name. */
};

/**
 * @struct bbs04_genparam_t
 * @brief Structure used for generation of BBS04 pairings.
 */
typedef struct {
  pbc_param_t param; /**< PBC parameters. */
  bigz_t r; /**< The characteristic of the generated field. */
} bbs04_genparam_t;

/** 
 * @struct bbs04_config_t
 * @brief The configuration information for the BBS04 scheme.
 */
typedef struct {
  unsigned int bitlimit; /**< The order of the created group will be of at 
			    most bitlimit bits. */
} bbs04_config_t;

#define BBS04_DEFAULT_BITLIMIT 160

#define BBS04_CONFIG_SET_DEFAULTS(cfg) \
  ((bbs04_config_t *) cfg)->bitlimit = BBS04_DEFAULT_BITLIMIT;

/**
 * @struct bbs04_sysenv_t
 * @brief Global information specific to the BBS04 scheme, useful for saving
 *  communications and/or computation costs.
 */
typedef struct {
  pbc_param_t param; /**< The pairing parameters. */
  pairing_t pairing; /**< The pairing. */
} bbs04_sysenv_t;

/** 
 * @fn groupsig_config_t* bbs04_config_init(void)
 * @brief Allocates memory for a BBS04 config structure.
 * 
 * @return A pointer to the allocated structure or NULL if error.
 */
groupsig_config_t* bbs04_config_init(void);

/** 
 * @fn int bbs04_config_free(groupsig_config_t *cfg)
 * @brief Frees the memory of a BBS04 config structure.
 * 
 * @param cfg The structure to free.
 *
 * @return A pointer to the allocated structure or NULL if error.
 */
int bbs04_config_free(groupsig_config_t *cfg);

/** 
 * @fn int bbs04_sysenv_update(void *data)
 * @brief Sets the BBS04 internal environment data, i.e., the PBC params and pairings.
 *
 * @param data A bbs04_sysenv_t structure containing the PBC params and pairings.
 * 
 * @return IOK or IERROR.
 */
int bbs04_sysenv_update(void *data);

/** 
 * @fn int bbs04_sysenv_free(void)
 * @brief Frees the BBS04 internal environment.
 * 
 * @return IOK or IERROR.
 */
int bbs04_sysenv_free(void);

/** 
 * @fn int bbs04_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, 
 *                     gml_t *gml, groupsig_config_t *config)
 * @brief The setup function for the BBS04 scheme.
 *
 * @param[in,out] grpkey An initialized group key, will be updated with the newly
 *   created group's group key.
 * @param[in,out] mgrkey An initialized manager key, will be updated with the
 *   newly created group's manager key.
 * @param[in,out] gml An initialized GML, will be set to an empty GML.
 * @param[in] config A BBS04 configuration structure.
 * 
 * @return IOK or IERROR.
 */
int bbs04_setup(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, groupsig_config_t *config);

/** 
 * @fn int bbs04_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Executes the member-side join of the BBS04 scheme.
 *
 * @param[in,out] memkey An initialized member key. Will be updated with the
 * member-side generated key information.
 * @param[in,out] grpkey The group's key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey);

/** 
 * @fn int bbs04_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey)
 * @brief Executes the manager-side join of the join procedure.
 *
 * @param[in,out] gml The group's GML. Upon successful executions, a new entry
 *  corresponding to the new member will be added.
 * @param[in,out] memkey A member key in which the member-side join has already
 *  been executed. Upon successful execution, will be completed with the information
 *  generated by the Group Manager.
 * @param[in] mgrkey The Group Manager private key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_join_mgr(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, groupsig_key_t *grpkey);

/** 
 * @fn int bbs04_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
 *	              groupsig_key_t *grpkey, unsigned int seed)
 * @brief Issues BBS04 group signatures.
 *
 * Using the specified member and group keys, issues a signature for the specified
 * message.
 *
 * @param[in,out] sig An initialized BBS04 group signature. Will be updated with
 *  the generated signature data.
 * @param[in] msg The message to sign.
 * @param[in] memkey The member key to use for signing.
 * @param[in] grpkey The group key.
 * @param[in] seed The seed. If it is set to UINT_MAX, the current system PRNG
 *  will be used normally. Otherwise, it will be reseeded with the specified
 *  seed before issuing the signature. 
 * 
 * @return IOK or IERROR.
 */
int bbs04_sign(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
	       groupsig_key_t *grpkey, unsigned int seed);

/** 
 * @fn int bbs04_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
 *		        groupsig_key_t *grpkey);
 * @brief Verifies a BBS04 group signature.
 *
 * @param[in,out] ok Will be set to 1 if the verification succeeds, to 0 if
 *  it fails.
 * @param[in] sig The signature to verify.
 * @param[in] msg The corresponding message.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
		 groupsig_key_t *grpkey);

/** 
 * @fn int bbs04_open(identity_t *id, groupsig_proof_t *proof, crl_t *crl, 
 *                    groupsig_signature_t *sig, groupsig_key_t *grpkey, 
 *	              groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Opens a BBS04 group signature.
 * 
 * Opens the specified group signature, obtaining the signer's identity.
 *
 * @param[in,out] id An initialized identity. Will be updated with the signer's
 *  real identity.
 * @param[in,out] proof BBS04 ignores this parameter.
 * @param[in,out] crl Optional. If not NULL, must be an initialized CRL, and will
 *  be updated with a new entry corresponding to the obtained trapdoor.
 * @param[in] sig The signature to open.
 * @param[in] grpkey The group key.
 * @param[in] mgrkey The manager's key.
 * @param[in] gml The GML.
 * 
 * @return IOK if it was possible to open the signature. IFAIL if the open
 *  trapdoor was not found, IERROR otherwise.
 */
int bbs04_open(identity_t *id, groupsig_proof_t *proof, crl_t *crl, groupsig_signature_t *sig, 
	       groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @fn int bbs04_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index)
 * @brief Reveals the tracing trapdoor of the GML entry with the specified index.
 *
 * Reveals the tracing trapdoor of the GML entry with the specified index. If
 * a CRL is also specified, a new entry corresponding to the retrieved trapdoor
 * will be added. 
 *
 * @param[in,out] trap An initialized trapdoor. Will be updated with the trapdoor
 *  associated to the group member with the given index within the GML.
 * @param[in,out] crl Optional. If not NULL, must be an initialized CRL, and will
 *  be updated with a new entry corresponding to the obtained trapdoor.
 * @param[in] gml The GML.
 * @param[in] index The index of the GML from which the trapdoor is to be obtained.
 *  In BBS04, this matches the real identity of the group members.
 * 
 * @return IOK or IERROR.
 */
int bbs04_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index);

/** 
 * @fn int bbs04_trace(uint8_t *ok, groupsig_signature_t *sig, 
 *                     groupsig_key_t *grpkey, crl_t *crl,
 *                     groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Determines whether or not the given signature has been issued by a 
 *  (unlinkability) revoked member.
 *
 * If the specified signature has been issued by a group member whose tracing
 * trapdoor is included in the CRL, ok will be set to 1. Otherwise, it will
 * be set to 0.
 *
 * @param[in,out] ok Will be set to 1 if the signature has been issued by a 
 *  group member with revoked unlinkability. To 0 otherwise.
 * @param[in] sig The signature to use for tracing.
 * @param[in] grpkey The group key.
 * @param[in] crl The CRL.
 * @param[in] mgrkey The manager key.
 * @param[in] gml The GML.
 * 
 * @return IOK or IERROR.
 */
int bbs04_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, 
		crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @fn int bbs04_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *		       groupsig_key_t *grpkey, groupsig_signature_t *sig)
 * @brief Issues a proof demonstrating that the member with the specified key is
 *  the issuer of the specified signature.
 * 
 * @param[in,out] proof An initialized BBS04 proof. Will be updated with the
 *  contents of the proof.
 * @param[in] memkey The member key of the issuer of the <i>sig</i> parameter.
 * @param[in] grpkey The group key.
 * @param[in] sig The signature.
 * 
 * @return IOK or IERROR.
 */
int bbs04_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
		groupsig_key_t *grpkey, groupsig_signature_t *sig);

/** 
 * @fn int bbs04_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *		              groupsig_signature_t *sig, groupsig_key_t *grpkey)
 * @brief Verifies a claim produced by the function <i>bbs04_claim</i>.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] sig The signature associated to the proof.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
int bbs04_claim_verify(uint8_t *ok, groupsig_proof_t *proof, 
		       groupsig_signature_t *sig, groupsig_key_t *grpkey);

/** 
 * @fn int bbs04_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *			        groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
 *                              uint16_t n_sigs)
 * @brief Creates a proof demonstrating that the given set of group signatures
 *  have all been issued by the same member.
 *
 * @param[in,out] proof An initialized proof. Will be updated with the produced
 *  proof.
 * @param[in] memkey The member key of the issuer of the given set of signatures.
 * @param[in] grpkey The group key.
 * @param[in] sigs The set of signatures, issued by the member with key <i>memkey</i>
 *  to be used for proof generation.
 * @param[in] n_sigs The number of signatures in <i>sigs</i> 
 * 
 * @return IOK or IERROR.
 */
int bbs04_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			 groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs);

/** 
 * @fn int bbs04_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *                                     groupsig_key_t *grpkey,
 * 				       groupsig_signature_t **sigs, uint16_t n_sigs)
 * @brief Verifies the received proof, demonstrating that the given set of 
 *  signatures have been issued by the same group member.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] grpkey The group key.
 * @param[in] sigs The signatures that have allegedly been issued by the same member.
 * @param[in] n_sigs The number of signatures in <i>sigs</i>.
 * 
 * @return IOK or IERROR.
 */
int bbs04_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, groupsig_key_t *grpkey,
				groupsig_signature_t **sigs, uint16_t n_sigs);

/**
 * @var bbs04_groupsig_bundle
 * @brief The set of functions to manage BBS04 groups.
 */
static const groupsig_t bbs04_groupsig_bundle = {
  &bbs04_description, /**< Contains the BBS04 scheme description. */
  &bbs04_config_init, /**< Initializes a BBS04 config structure. */
  &bbs04_config_free, /**< Frees a BBS04 config structure. */
  &bbs04_sysenv_update, /**< Sets the PBC params and pairing. */
  NULL, /**< Not yet iplemented... */
  &bbs04_sysenv_free, /**<  Frees the PBC params and pairing. */
  &bbs04_setup, /**< Sets up BBS04 groups. */
  &bbs04_join_mem, /**< Executes member-side joins. */
  &bbs04_join_mgr, /**< Executes maanger-side joins. */
  &bbs04_sign, /**< Issues BBS04 signatures. */
  &bbs04_verify, /**< Verifies BBS04 signatures. */
  &bbs04_open, /**< Opens BBS04 signatures. */
  NULL, /**< BBS04 does not create proofs of opening. */
  &bbs04_reveal, /**< Reveals the tracing trapdoor from BBS04 signatures. */
  &bbs04_trace, /**< Traces the issuer of a signature. */ 
  &bbs04_claim, /**< Claims, in ZK, "ownership" of a signature. */
  &bbs04_claim_verify, /**< Verifies claims. */
  &bbs04_prove_equality, /**< Issues "same issuer" ZK proofs for several signatures. */
  &bbs04_prove_equality_verify, /**< Verifies "same issuer" ZK proofs. */
};

#endif /* _BBS04_H */

/* bbs04.h ends here */
