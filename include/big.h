/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: big.h
 * @brief: General definitions for Big Numbers functionality wrapping.
 * 
 * Currently, the library being wrapped is GNU's libgmp. The current file
 * contains general types and definitions that will be shared by type-specific
 * submodules extending specific Big Numbers funcionality (e.g. Big Integers,
 * Big Floats, etc.).
 *
 * @author: jesus
 * Maintainer: jesus
 * @date: vie jul 13 15:05:01 2012 (+0200)
 * @version: 0.1
 * Last-Updated: dom may 12 20:11:55 2013 (+0200)
 *           By: jesus
 *     Update #: 8
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BIG_H
#define _BIG_H

#include <gmp.h>

/**
 * Big Integer type definition.
 */
typedef mpz_t *bigz_t;

/**
 * Big Float type definition.
 */
typedef mpf_t *bigf_t;

/**
 * Random state definition.
 */
typedef gmp_randstate_t bigz_randstate_t;

#endif /* _BIG_H */

/* big.h ends here */
