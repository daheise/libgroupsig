/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: crl_handles.h
 * @brief: Set of supported CRL implementations.
 * @author: jesus
 * Maintainer: 
 * @date: jue ene 17 19:17:04 2013 (+0100)
 * @version: 
 * Last-Updated: vie oct  4 14:18:39 2013 (+0200)
 *           By: jesus
 *     Update #: 18
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _CRL_HANDLES_H
#define _CRL_HANDLES_H

#include "crl.h"
#include "groupsig/kty04/crl.h"
#include "groupsig/bbs04/crl.h"
#include "groupsig/cpy06/crl.h"

/**
 * @def CRL_HANDLES_N
 * @brief Number of supported CRL implementations.
 */
#define CRL_HANDLES_N 3

/**
 * @var CRL_HANDLES
 * @brief List of handles of CRL implementations.
 */
const crl_handle_t *CRL_HANDLES[CRL_HANDLES_N] = {
  &kty04_crl_handle,
  &bbs04_crl_handle,
  &cpy06_crl_handle,
};

#endif /* _CRL_HANDLES_H */

/* crl_handles.h ends here */
