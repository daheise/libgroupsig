/*
 * exim.h
 *
 *  Created on: Jan 18, 2017
 *      Author: fbh
 */

#ifndef MISC_EXIM_H_
#define MISC_EXIM_H_
#include "message.h"
#include "types.h"

typedef enum {
  EXIM_FORMAT_FILE_NULL, /* For arbitrary bytes format in a file */
  EXIM_FORMAT_FILE_NULL_B64, /* For formats stored as base64 strings in a file */
  EXIM_FORMAT_STRING_NULL_B64, /* For formats stored as base64 strings */
  EXIM_FORMAT_MESSAGE_NULL, /* For formats stored as libmsg messages. */
  EXIM_FORMAT_MESSAGE_NULL_B64, /* For formats stored as base64 encoded libmsg
                              messages. */
  EXIM_FORMAT_BYTEARRAY_NULL, /* For formatting as a bytearray. Counterpart to
                            FORMAT_STRING_NULL_B64 */
} exim_format_t;

//Forward declaration to resolve name resolution issues
typedef struct exim_handle_t exim_handle_t;
typedef struct exim_t exim_t;



/* Size functions */
typedef int (*exim_get_size_bytearray_null_f)(exim_t* obj);

/* Export functions */
typedef int (*exim_export_bytearray_fd_f)(exim_t* obj, FILE* fd);

/* Import functions */
typedef int (*exim_import_bytearray_fd_f)(FILE *fd, exim_t* obj);

/*
 * This structure holds the three functions that must be defined by the client
 * to use the exim interface.
 */
struct exim_handle_t{
  /* Size functions */
  exim_get_size_bytearray_null_f get_size_bytearray_null; /**< Size of bytearray representation. */

  /* Export functions */
  exim_export_bytearray_fd_f export_bytearray_fd; /**< Write a bytearray to a file descriptor. */

  /* Import functions */
  exim_import_bytearray_fd_f import_bytearray_fd; /**< Read a bytearray from a file descriptor. */

};

struct exim_t{
  // A pointer to the data the exim functions can operate on
  void *eximable;
  // The exim functions for the data pointer to by eximable
  exim_handle_t *funcs;
};


/*
 * These functions must have a specialized worker defined and stored in a
 * exim_handle_t
 *
 * Declaring them here as status causes a function not implemented warning in
 * gcc. We declare them here so that are easy to #include for private declaration
 * in client code.
 *
 * TODO: These functions could (should) take/return a (void*) instead so that the
 * client function doesn't need to know about exim for these purposes.
 * static int _get_size_bytearray_null(void* obj);
 * static int _export_fd(void* obj, FILE *fd);
 * static void* _import_fd(FILE *fd);
 */

/**
 * @fn static int _get_size_bytearray_null(exim_t* obj)
 * @brief Calculates the size of a byte representation of the data contained in
 * obj.
 *
 * @param[in] obj An exim wrapper containing the data to be sized.
 *
 * @return The size in bytes of the data when exported as a byte array.
 *         -1 on error.
 */
//static int _get_size_bytearray_null(exim_t* obj);

// TODO: This should probably return the number of bytes written, which
// should be compared to _get_size_bytearray_null()
/**
 * @fn static int _export_fd(exim_t* obj, FILE *fd)
 * @brief Writes a byte representation of the data in obj to the file
 * descriptor fd. The file descriptor should be allocated by the client to accept
 * at least _get_size_bytearray_null() bytes.
 *
 * @param[in] obj An exim wrapper containing the data to be exported.
 * @param[in] fd The file descriptor to be written to.
 *
 * @return IOK or IERROR
 */
//static int _export_fd(exim_t* obj, FILE *fd);

// TODO: This should probably return the number of bytes read
/**
 * @fn static int _import_fd(FILE *fd, exim_t* obj)
 * @brief Reads a byte representation of the data in fd into the data field of
 * obj. The data returned in obj is newly allocated and it is the responsibility
 * of the caller to free it
 *
 * @param[in] fd The file descriptor to be read from.
 * @param[in] obj The exim wrapper to have the newly allocated data written to
 *
 * @return IOK or IERROR
 */
//static int _import_fd(FILE *fd, exim_t* obj);


/**
 * @fn int exim_get_size_in_format(exim_t *obj, exim_format_t format);
 * @brief Calculates the size of the representation of the data contained in
 * obj in the given format.
 *
 * @param[in] obj An exim wrapper containing the data to be sized.
 * @param[in] format The format the size is requested for.
 *
 * @return The size in bytes of the data when exported in format
 *         -1 on error.
 */
int exim_get_size_in_format(exim_t *obj, exim_format_t format);

// TODO: This should probably return the number of bytes written, which
// should be compared to exim_get_size_in_format()
/**
 * @fn int exim_export(exim_t* obj, exim_format_t format, void *dst)
 * @brief Writes a byte representation of the data in obj to dst.
 * The destination should be allocated by the client to accept
 * at least exim_get_size_in_format() bytes.
 *
 * The type of dst depends on format:
 * FORMAT_FILE_NULL: (char*) The filename to be written to.
 * FORMAT_FILE_NULL_B64: (char*) The filename to be written to.
 * FORMAT_STRING_NULL_B64: (char*) The C string to be written to.
 * FORMAT_MESSAGE_NULL: (message_t) A libmsg object to be written to.
 * FORMAT_MESSAGE_NULL_B64: (message_t) A libmsg object to be written to.
 * FORMAT_BYTEARRAY_NULL: (byte_t) A bytearray to be written to.
 *
 * @param[in] obj An exim wrapper containing the data to be exported.
 * @param[in] format The format to export the data in obj as.
 * @param[in] dst The destination to write to.
 *
 * @return IOK or IERROR
 */
int exim_export(exim_t* obj, exim_format_t format, void *dst);

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import(exim_format_t format, void *source, exim_t* obj);
 * @brief Reads the data from source into the eximable field of obj based on
 * the given format. The data returned in obj is newly allocated and it is
 * the responsibility of the caller to free it
 *
 * The type of sourve depends on format:
 * FORMAT_FILE_NULL: (char*) The filename to be read from.
 * FORMAT_FILE_NULL_B64: (char*) The filename to be read from.
 * FORMAT_STRING_NULL_B64: (char*) The C string to be read from.
 * FORMAT_MESSAGE_NULL: (message_t) A libmsg object to be read from.
 * FORMAT_MESSAGE_NULL_B64: (message_t) A libmsg object to be read from.
 * FORMAT_BYTEARRAY_NULL: (byte_t) A bytearray to be read from.
 *
 * @param[in] format The format the data in fd is stored as
 * @param[in] source The source to be read from.
 * @param[in] obj The exim wrapper to have the newly allocated data written to
 *
 * @return IOK or IERROR
 */
int exim_import(exim_format_t format, void *source, exim_t* obj);

#endif /* MISC_EXIM_H_ */
