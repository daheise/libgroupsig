/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: gml_handles.h
 * @brief: Set of handles of the supported GML implementations.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 19:17:04 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 14:18:46 2013 (+0200)
 *           By: jesus
 *     Update #: 14
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _GML_HANDLES_H
#define _GML_HANDLES_H

#include "gml.h"
#include "groupsig/kty04/gml.h"
#include "groupsig/bbs04/gml.h"
#include "groupsig/cpy06/gml.h"

/**
 * @def GML_HANDLES_N
 * @brief Number of known GML implementation handles.
 */
#define GML_HANDLES_N 3

/**
 * @var GML_HANDLES
 * @brief Set of handles of known GML implementations.
 */
const gml_handle_t *GML_HANDLES[GML_HANDLES_N] = {
  &kty04_gml_handle,
  &bbs04_gml_handle,
  &cpy06_gml_handle,
};

#endif /* _GML_HANDLES_H */

/* gml_handles.h ends here */
