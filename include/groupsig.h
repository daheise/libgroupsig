/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: groupsig.h
 * @brief: Basic declarations of function types and data structures for managing
 *  group signature schemes.
 * @author: jesus
 * Maintainer: jesus
 * @date: lun jul  2 15:44:34 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun jul 20 21:30:40 2015 (+0200)
 *           By: jesus
 *     Update #: 236
 * URL: bitbucket.org/libgroupsig/jdiazvico
 */
 
#ifndef _GROUPSIG_H
#define _GROUPSIG_H

#include <stdint.h>

#include "key.h"
#include "signature.h"
#include "gml.h"
#include "crl.h"
#include "grp_key.h"
#include "mgr_key.h"
#include "mem_key.h"
#include "proof.h"
#include "identity.h"
#include "trapdoor.h"
#include "sysenv.h"
#include "message.h"

/**
 * @struct groupsig_description_t
 * @brief Stores the basic description of a group signature scheme.
 */
typedef struct {
  uint8_t code; /**< The scheme's code. Each code uniquely identifies a group
		   signature scheme. The registered group signature schemes are
		   linked in the file registered_groupsigs.h, and their
		   corresponding unique codes are defined in the therein
		   included files. */
  char name[10]; /**< The scheme's name. */
} groupsig_description_t;

/**
 * @struct groupsig_config_t
 * @brief Stores the basic information necessary to "instantiate" a group
 *  signature scheme (e.g. security parameters, limitations, etc.)
 */
typedef struct {
  uint8_t scheme; /**< The group signature scheme's code. */
  void *config; /**< An opaque pointer to the scheme's specific configuration. */
} groupsig_config_t;

/** 
 * @typedef groupsig_config_t* (*config_init_f)(void);
 * @brief Functions for initializing config structures.
 *
 * @return An allocated config structure or NULL if error.
 */
typedef groupsig_config_t* (*config_init_f)(void);

/** 
 * @typedef int (*config_free_f)(groupsig_config_t *cfg);
 * @brief Functions for freeing config structures.
 *
 * @param cfg The config structure to free.
 *
 * @return IOK or IERROR.
 */
typedef int (*config_free_f)(groupsig_config_t *cfg);

/** 
 * @typedef int (*sysenv_update_f)(void *data);
 * @brief Sets the environment of the specific scheme to the received data.
 *
 *  Sets the <i>data</i> field of the global sysenv structure. This function is
 *  useful for setting information that saves computation and/or communication
 *  costs, but is specific to each scheme.
 *
 * @return IOK or IERROR.
 */
typedef int (*sysenv_update_f)(void * data);

/** 
 * @typedef (void *) (*sysenv_get_f)(void);
 * @brief Returns the current environment data.
 *
 * @return A pointer to the current environment data or NULL if error.
 */
typedef void* (*sysenv_get_f)(void);

/**
 * @typedef int (*sysenv_free_f)(void);
 * @brief Frees the scheme specific environment information.
 *
 * @return IOK or IERROR.
 */
typedef int (*sysenv_free_f)(void);

/**
 * @typedef int (*setup_f)(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, 
 *		       groupsig_config_t *config)
 * @brief Type for setup functions.
 *
 * All schemes' setup functions must follow this scheme. By using the specified
 * configuration information, functions following this prototype will create
 * a specific scheme instance, setting the group and manager keys, and the GML.
 *
 * @param[in,out] grpkey An initialized group key, will be updated with the
 *  generated group key.
 * @param[in,out] mgrkey An initialized manager key, will be updated with the
 *  generated manager key.
 * @param[in,out] gml An initialized GML, will be updated with the generated
 *  GML.
 * @param[in] config The scheme's specific configuration information.
 *
 * @return IOK or IERROR.
 */
typedef int (*setup_f)(groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml, 
		       groupsig_config_t *config);

/** 
 * @typedef int (*join_mem_f)(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Type for functions implementing the member join functionality.
 *
 * Functions of this type are executed by entities who want to be included in a
 * group. They perform all the necessary computations to create a partial key
 * that can be later processed by the group manager to create a final member
 * key.
 *
 * @param[in,out] memkey An initialized group member key. Must have been
 *  initialized by the caller.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
typedef int (*join_mem_f)(groupsig_key_t *memkey, groupsig_key_t *grpkey);

/** 
 * @typedef int (*join_mgr_f)(gml_t *gml, groupsig_key_t *memkey, 
 *                            groupsig_key_t *mgrkey, 
 *			      groupsig_key_t *grpkey)
 * @brief Type for functions implementing the manager join functionality.
 *
 * Functions of this type are executed by group managers. From a partial member
 * key, as produced by the corresponding join_mem_f function, these functions
 * create a complete member key, adding the new member to any necessary component
 * (e.g. GMLs).
 *
 * @param[in,out] gml The group membership list that may be updated with
 *  information related to the new member.
 * @param[in,out] memkey The partial member key to be completed by the group
 *  manager.
 * @param[in] mgrkey The group manager key.
 * @param[in] grpkey The group key.
 * 
 * @return 
 */
typedef int (*join_mgr_f)(gml_t *gml, groupsig_key_t *memkey, groupsig_key_t *mgrkey, 
			  groupsig_key_t *grpkey);

/** 
 * @typedef int (*sign_f)(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
 *		      groupsig_key_t *grpkey, unsigned int seed)
 * @brief Type of functions for signing messages.
 *
 * @param[in,out] sig An initialized group signature structure. Will be set to the
 *  produced signature.
 * @param[in] msg The message to sign.
 * @param[in] memkey The member key for signing.
 * @param[in] grpkey The group key.
 * @param[in] seed When set to a value different to UINT_MAX, the system's pseudo
 *  random number generator will be reseeded with the specified value (allowing to
 *  re-generate signatures). Otherwise, the random number generator state will not
 *  be modified. In any case, the random generator will be randomly reseeded after
 *  signing.
 * 
 * @return IOK or IERROR.
 */
typedef int (*sign_f)(groupsig_signature_t *sig, message_t *msg, groupsig_key_t *memkey, 
		      groupsig_key_t *grpkey, unsigned int seed);

/** 
 * @typedef int (*verify_f)(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
 *			groupsig_key_t *grpkey)
 * @brief Type of functions for verifying group signatures. 
 * 
 * @param[in,out] Will be set to 1 if the signature is valid, to 0 otherwise.
 * @param[in] sig The signature to be verified.
 * @param[in] msg The message associated to <i>sig</i>.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
typedef int (*verify_f)(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
			groupsig_key_t *grpkey);

/** 
 * @typedef int (*reveal_f)(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index)
 * @brief Type of functions for revealing the tracing trapdoor of group members.7
 *
 * Functions of this type return the tracing trapdoor of the member at the position
 * <i>index</i> within the given GML. If the received CRL is not NULL, a new 
 * entry will be updated to it, corresponding to the member with the specified index.
 *
 * @param[in,out] trap Will be set to the retrieved trapdoor. Must have been
 *  initialized by the caller.
 * @param[in] crl If not NULL, a new entry will be added to the CRL, associated
 *  to the member with the given index.
 * @param[in] gml The Group Membership List.
 * @param[in] index The index of the member whose trapdoor is to be revealed.
 * 
 * @return IOK or IERROR.
 *
 * @todo Using <i>index</i>es to refer to specific members may not be general
 *  enough (e.g. for dynamic acccumulator based schemes).
 */
typedef int (*reveal_f)(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index);

/** 
 * @typedef int (*open_f)(identity_t *id, groupsig_proof_t *proof, crl_t *crl, 
 *                        groupsig_signature_t *sig, groupsig_key_t *grpkey, 
 *		          groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Type of functions for opening group signatures, revealing the identity
 *  of the issuer of the given signature.
 *
 * @param[in,out] id Will be set to the identity of the member who issued the
 *  signature.
 * @param[in,out] proof If opening proofs are produced, this parameter will be set
 *  to a proof of correctness of the opening.
 * @param[in] crl If not NULL, a new entry will be added to the CRL, associated
 *  to the member with the given index.
 * @param[in] sig The group signature to be opened.
 * @param[in] grpkey The group key.
 * @param[in] mgrkey The group manager key.
 * @param[in] gml The group membership list.
 * 
 * @return IOK if it was possible to open the signature. IFAIL if the open
 *  trapdoor was not found, IERROR otherwise.
 */
typedef int (*open_f)(identity_t *id, groupsig_proof_t *proof, crl_t *crl, 
		      groupsig_signature_t *sig, groupsig_key_t *grpkey, 
		      groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @typedef typedef int (*open_verify_f)(uint8_t *ok, identity_t *id,
 *                       groupsig_proof_t *proof, 
 *                       groupsig_signature_t *sig, groupsig_key_t *grpkey)
 * 
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 *  signature.
 * @param[in] id The identity produced by the open algorithm.
 * @param[in] proof The proof of opening.
 * @param[in] sig The group signature associated to the proof.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR
 */
typedef int (*open_verify_f)(uint8_t *ok, identity_t *id, groupsig_proof_t *proof, 
			     groupsig_signature_t *sig, groupsig_key_t *grpkey);

/** 
 * @typedef int (*trace_f)(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, 
 *		           crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Type of functions for tracing group signatures.
 * 
 * Functions of this type set <i>ok</i> to 1 if the given group signature has been
 * issued by any of the group members in the specified CRL (otherwise, <i>ok</i> is
 * set to 0).
 * 
 * @param[in,out] ok Will be set to 1 if the group signature has been issued by
 *  a member in the CRL, to 0 otherwise.
 * @param[in] The group signature to be traced.
 * @param[in] grpkey The group key.
 * @param[in] CRL the Certificate Revocation List to use for tracing.
 * @param[in] mgrkey Schemes that do not include native support for tracing may
 *  "emulate" it by opening group signatures. Hence, they will need the manager
 *  key. In traceable schemes, this parameter may be ignored.
 * @param[in] gml Schemes that do not include native support for tracing may
 *  "emulate" it by opening group signatures. Hence, they will need the GML.
 *  In traceable schemes, this parameter may be ignored.
 * 
 * @return IOK or IERROR.
 */
typedef int (*trace_f)(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, 
		       crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @typedef int (*claim_f)(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *		           groupsig_key_t *grpkey, groupsig_signature_t *sig)
 * @brief Type of functions for issuing (Zero Knowledge) proofs for claiming 
 *  authorship of a signature.
 *
 * @param[in,out] proof The proof to be created. Must have been initialized by
 *  the caller.
 * @param[in] memkey The member key of the issuer of the signature.
 * @param[in] grpkey The group key.
 * @param[in] sig The group signature.
 * 
 * @return IOK or IERROR.
 */
typedef int (*claim_f)(groupsig_proof_t *proof, groupsig_key_t *memkey, 
		       groupsig_key_t *grpkey, groupsig_signature_t *sig);

/** 
 * @typedef int (*claim_verify_f)(uint8_t *ok, groupsig_proof_t *proof, 
 *			          groupsig_signature_t *sig, groupsig_key_t *grpkey)
 * @brief Type of functions for verifying claim proofs. 
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] sig The signature that the proof claims ownership of.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 */
typedef int (*claim_verify_f)(uint8_t *ok, groupsig_proof_t *proof, 
			      groupsig_signature_t *sig, groupsig_key_t *grpkey);

/** 
 * @typedef int (*prove_equality_f)(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *				    groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
 *				    uint16_t n_sigs)
 * @brief Type of functions for proving that all the signatures in <i>sigs</i> have
 *  been issued by the owner of <i>memkey</i>.
 * 
 * @param[in,out] proof Will be set to the generated proof. Must have been
 *  initialized by the caller.
 * @param[in] memkey The group member key. Must be the issuer of all the signatures.
 * @param[in] grpkey The group key.
 * @param[in] sigs The signatures to be proved.
 * @param[in] n_sigs The number of signatures in <i>sigs</i>.
 * 
 * @return IOK or IERROR.
 */
typedef int (*prove_equality_f)(groupsig_proof_t *proof, groupsig_key_t *memkey, 
				groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
				uint16_t n_sigs);

/** 
 * @typedef int (*prove_equality_verify_f)(uint8_t *ok, groupsig_proof_t *proof, 
 *				           groupsig_key_t *grpkey,
 *			                   groupsig_signature_t **sigs, uint16_t n_sigs)
 * @brief Type of functions for verifying equality proofs.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct. To 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] grpkey The group key.
 * @param[in] sigs The group signatures related to the proof.
 * @param[in] n_sigs The number of signatures in <i>n_sigs</i>.
 * 
 * @return IOK or IERROR.
 */
typedef int (*prove_equality_verify_f)(uint8_t *ok, groupsig_proof_t *proof, 
				       groupsig_key_t *grpkey,
				       groupsig_signature_t **sigs, uint16_t n_sigs);

/** 
 * @struct groupsig_t
 * @brief Defines the structure for group signature scheme handles.
 */
typedef struct {
  const groupsig_description_t *desc; /**< The scheme's description. */
  config_init_f config_init; /**< The config struct initialization function.  */
  config_free_f config_free; /**< The config struct free function.  */
  sysenv_update_f sysenv_update; /**< Function for initializing scheme specific
			      environment information. */
  sysenv_get_f sysenv_get; /**< Gets the current environment specific data. */
  sysenv_free_f sysenv_free; /**< Frees the scheme specific environment info. */
  setup_f setup; /**< The schemes setup function. */
  join_mem_f join_mem; /**< The member side join function. */
  join_mgr_f join_mgr; /**< The manager side join function. */
  sign_f sign; /**< Signs messages. */
  verify_f verify; /**< Verifies group signatures. */
  open_f open; /**< Opens group signatures. */
  open_verify_f open_verify; /**< Verifies proofs of opening. */
  reveal_f reveal; /**< Reveals tracing trapdoors. */
  trace_f trace; /**< Traces signers. */
  claim_f claim; /**< Issues proofs of ownership. */
  claim_verify_f claim_verify; /**< Verifies proofs of ownership. */
  prove_equality_f prove_equality; /**< Proves that several signatures have
				      been issued by the same member. */
  prove_equality_verify_f prove_equality_verify; /**< Verifies proofs demonstrating
						    "prove_equalities". */
} groupsig_t;

/* Function implementations */

/** 
 * @fn uint8_t groupsig_is_supported_scheme(uint8_t code)
 * @brief Returns 1 if a group signature scheme with the given code is supported. 
 * Returns 0 otherwise.
 *
 * @param[in] code The code to check.
 * 
 * @return 1 or 0
 */
uint8_t groupsig_is_supported_scheme(uint8_t code);

/** 
 * @fn const groupsig_t* groupsig_get_groupsig_from_str(char *str, groupsig_t *gs)
 * @brief Returns the bundle associated to the given groupsig name.
 *
 * @param[in] str The groupsig name.
 * 
 * @return The associated groupsig bundle or NULL.
 */
const groupsig_t* groupsig_get_groupsig_from_str(char *str);

/** 
 * @fn const groupsig_t* groupsig_get_groupsig_from_code(uint8_t code)
 * @brief Returns the bundle associated to the given groupsig code.
 *
 * @param[in] code The groupsig code.
 * 
 * @return The associated groupsig bundle or NULL.
 */
const groupsig_t* groupsig_get_groupsig_from_code(uint8_t code);

/** 
 * @fn int groupsig_init(unsigned int seed)
 * @brief Initializes the group signature environment (random number generators, 
 *  etc.).
 *
 * @param[in] seed The seed to use for the [P]RNG.
 * 
 * @return IOK or IERROR.
 */
int groupsig_init(unsigned int seed);


/** 
 * @fn groupsig_clear(uint8_t code)
 * @brief Frees all the memory allocated for a group signature scheme environment.
 * 
 * @param[in] code The groupsig code.
 *
 * @return IOK or IERROR.
 */
int groupsig_clear(uint8_t code);

/** 
 * @fn groupsig_config_t* groupsig_config_init(uint8_t code)
 * @brief Allocates memory for a config structure of the given scheme.
 *
 * @param[in] code The scheme's code.
 * 
 * @return A pointer to the initialized structure or NULL if error.
 */
groupsig_config_t* groupsig_config_init(uint8_t code);

/** 
 * @fn int groupsig_config_free(groupsig_config_t *cfg)
 * @brief Frees memory for a config structure of the given scheme.
 *
 * @param[in] cfg The groupsig config structure to free.
 * 
 * @return IOK or IERROR.
 */
int groupsig_config_free(groupsig_config_t *cfg);

/** 
 * @fn int groupsig_sysenv_update(uint8_t code, void *data)
 * @brief Updates the scheme's specific environment data. 
 *
 * @param[in] code The scheme's code.
 * @param[in] data The scheme's specific environment data.
 * 
 * @return IOK or IERROR.
 */
int groupsig_sysenv_update(uint8_t code, void *data);

/** 
 * @fn void* groupsig_sysenv_get()
 * @brief Returns the current sysenv data.
 * 
 * @return A pointer to the current sysenv data or NULL if error.
 */
void* groupsig_sysenv_get(uint8_t code);

/** 
 * @fn int groupsig_sysenv_free(uint8_t code)
 * @brief  Frees the specific scheme environment data.
 *
 * @param[in] code The scheme's code.
 * 
 * @return IOK or IERROR.
 */
int groupsig_sysenv_free(uint8_t code);

/** 
 * @fn int groupsig_setup(uint8_t code, groupsig_key_t *grpkey, groupsig_key_t *mgrkey, 
 *		          gml_t *gml, groupsig_config_t *config)
 * @brief Executes the setup function of the scheme with the specified code. 
 *
 *  Executes the setup function of the scheme with the specified code, filling
 *  the group key, manager key and GML. Uses the input parameters contained in the
 *  specified config structure.
 *
 * @param[in] code The group signature scheme code.
 * @param[in,out] grpkey An initialized group key. Will be set to the final group
 *  key.
 * @param[in,out] mgrkey An initialized group manager key. Will be set to the final
 *  group manager key.
 * @param[in,out] gml An initialized GML. Will be set to the created GML.
 * @param[in] config The configuration strcture with the input parameters of the
 *  scheme. For details, see the main header file of the given scheme.
 * 
 * @return IOK or IERROR.
 * 
 */
int groupsig_setup(uint8_t code, groupsig_key_t *grpkey, groupsig_key_t *mgrkey, 
		   gml_t *gml, groupsig_config_t *config);

/** 
 * @fn int groupsig_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey)
 * @brief Executes the join member action of the scheme associated to the
 *  received tokens.
 *
 * The member key will be updated to the member-side generated key information.
 *
 * @param[in,out] memkey An initialized group member key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_join_mem(groupsig_key_t *memkey, groupsig_key_t *grpkey);

/** 
 * @fn int groupsig_join_mgr(gml_t *gml, groupsig_key_t *memkey, 
 *		      groupsig_key_t *mgrkey, groupsig_key_t *grpkey)
 * @brief Runs the manager side join of the specified group signature scheme.
 *
 * Runs the manager side join of the specified group signature scheme. As a result,
 * the received member key is completed, and a new entry related to the new member
 * is added to the GML.
 *
 * @param[in,out] gml The GML.
 * @param[in,out] memkey The partial member key.
 * @param[in] mgrkey The manager key.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_join_mgr(gml_t *gml, groupsig_key_t *memkey, 
		      groupsig_key_t *mgrkey, groupsig_key_t *grpkey);

/** 
 * @fn int groupsig_sign(groupsig_signature_t *sig, message_t *msg, 
 *		  groupsig_key_t *memkey, 
 *		  groupsig_key_t *grpkey, unsigned int seed)
 * @brief Runs the signing algorithm of the scheme associated with the received
 *        tokens.
 *
 * @param[in,out] sig The group signature structure to be filled. Must be initialized
 *  by the caller.
 * @param[in] msg The message to sign.
 * @param[in] memkey The member key to use for signing.
 * @param[in] grpkey The group key.
 * @param[in] seed When different to UINT_MAX, the specified seed will be sued
 *  for reseeding the PRNG. If UINT_MAX, the current state of the random number
 *  generator will be used.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_sign(groupsig_signature_t *sig, message_t *msg, 
		  groupsig_key_t *memkey, 
		  groupsig_key_t *grpkey, unsigned int seed);

/** 
 * @fn int groupsig_verify(uint8_t *ok, groupsig_signature_t *sig, 
 *                  message_t *msg, groupsig_key_t *grpkey)
 * @brief Verifies group signatures of the given scheme.
 *
 * @param[in,out] ok Will be set to 1 if the signature is correct. To 0 otherwise.
 * @param[in] sig The signature to verify.
 * @param[in] msg The message related to the signature.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_verify(uint8_t *ok, groupsig_signature_t *sig, message_t *msg, 
		    groupsig_key_t *grpkey);

/** 
 * @fn int groupsig_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index)
 * @brief Reveals the tracing trapdoor of the member in position <i>index</i> within 
 *  the given GML.
 * 
 *  Reveals the tracing trapdoor of the member in position <i>index</i> within 
 *  the given GML. If a CRL is provided, a new entry with the retrieved trapdoor
 *  is added to it.
 *
 * @param[in,out] trap An initialized trapdoor. Will be updated with the trapdoor
 *  of the member with <i>index</i> index.
 * @param[in,out] crl When not NULL, a new entry will be added corresponding to the
 *  member with the specified index.
 * @param[in] gml The GML.
 * @param[in] index The position within the GML of the member to be revealed.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_reveal(trapdoor_t *trap, crl_t *crl, gml_t *gml, uint64_t index);

/** 
 * @fn int groupsig_open(identity_t *id, crl_t *crl, groupsig_signature_t *sig,  
 *		         groupsig_key_t *grpkey, groupsig_key_t *mgrkey, gml_t *gml)
 * @brief Returns the real identity of the issuer of the given signature. 
 *
 * Returns the real identity of the issuer of the given signature. Currently, the
 * identity is the index within the given GML.
 *
 * @param[in,out] id An initialized identity. Will be set to the identity of the
 *  issuer of the given signature.
 * @param[in,out] proof An initialized proof of opening, or NULL if the called 
 *  scheme does not produce proofs of opening.
 * @param[in] crl If not NULL, a new entry will be added to the CRL, associated
 *  to the member with the given index.
 * @param[in] sig The signature to open.
 * @param[in] grpkey The group key.
 * @param[in] mgrkey The manager key.
 * @param[in] gml The GML.
 * 
 * @return IOK if it was possible to open the signature. IFAIL if the open
 *  trapdoor was not found, IERROR otherwise.
 */
int groupsig_open(identity_t *id, groupsig_proof_t *proof, crl_t *crl, 
		  groupsig_signature_t *sig, groupsig_key_t *grpkey, 
		  groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @fn int open_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *                     groupsig_signature_t *sig, groupsig_key_t *grpkey)
 * 
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 *  signature.
 * @param[in] id The identity produced by the open algorithm.
 * @param[in] proof The proof of opening.
 * @param[in] sig The group signature associated to the proof.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR
 */
int groupsig_open_verify(uint8_t *ok, identity_t *id,
			 groupsig_proof_t *proof, 
			 groupsig_signature_t *sig, 
			 groupsig_key_t *grpkey);

/** 
 * @fn int groupsig_trace(uint8_t *ok, groupsig_signature_t *sig, 
 *		   groupsig_key_t *grpkey, crl_t *crl)
 * @brief Determines whether or not the issuer of the specified signature has
 *  been revoked according to the given CRL.
 *
 * @param[in,out] ok Will be set to 1 if the issuer of <i>sig</i> is revoked, to
 *  0 otherwise.
 * @param[in] sig The group signature to trace.
 * @param[in] grpkey The group key.
 * @param[in] crl The CRL.
 * @param[in] mgrkey Schemes that do not include native support for tracing may
 *  "emulate" it by opening group signatures. Hence, they will need the manager
 *  key. In traceable schemes, this parameter may be ignored.
 * @param[in] gml Schemes that do not include native support for tracing may
 *  "emulate" it by opening group signatures. Hence, they will need the GML.
 *  In traceable schemes, this parameter may be ignored.
 * 
 * @return IOK or IERROR.
 */
int groupsig_trace(uint8_t *ok, groupsig_signature_t *sig, groupsig_key_t *grpkey, crl_t *crl, groupsig_key_t *mgrkey, gml_t *gml);

/** 
 * @fn int groupsig_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *                 groupsig_key_t *grpkey, groupsig_signature_t *sig)
 * @brief Issues a proof claiming having issued the specified signature.
 *
 * The proofs generated with this function are ZKP.
 *
 * @param[in,out] proof An initialized proof. Will be updated to contain the 
 *  generated proof.
 * @param[in] memkey The group member key.
 * @param[in] grpkey The group key.
 * @param[in] sig The signature to prove.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_claim(groupsig_proof_t *proof, groupsig_key_t *memkey, groupsig_key_t *grpkey, 
		   groupsig_signature_t *sig);

/** 
 * @fn int groupsig_claim_verify(uint8_t *ok, groupsig_proof_t *proof, groupsig_signature_t *sig, 
 *			  groupsig_key_t *grpkey)
 * @brief Verifies a "claim" proof.
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] sig The signature related to the proof.
 * @param[in] grpkey The group key.
 * 
 * @return IOK or IERROR.
 *
 */
int groupsig_claim_verify(uint8_t *ok, groupsig_proof_t *proof, groupsig_signature_t *sig, 
			  groupsig_key_t *grpkey);

/** 
 * @fn int groupsig_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
 *			    groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs)
 * @brief Issues a proof demonstrating that all the signatures in <i>sigs</i> have
 *  been issued using <i>memkey</i>.
 *
 *  The proofs generated using this function are ZKP.
 *
 * @param[in,out] proof An initialized proof. Will be updated with the generated
 * proof.
 * @param[in] memkey The member key used for generating the proof. Must be the same
 *  that was used for issuing <i>sigs</i>.
 * @param[in] grpkey The group key.
 * @param[in] sigs The signatures to prove.
 * @param[in] n_sigs The number of signatures in <i>sigs</i>.
 * 
 * @return IOK or IERROR.
 */
int groupsig_prove_equality(groupsig_proof_t *proof, groupsig_key_t *memkey, 
			    groupsig_key_t *grpkey, groupsig_signature_t **sigs, uint16_t n_sigs);

/** 
 * @fn int groupsig_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, 
 *				   groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
 *				   uint16_t n_sigs)
 * @brief Verifies "proofs of equality".
 *
 * @param[in,out] ok Will be set to 1 if the proof is correct, to 0 otherwise.
 * @param[in] proof The proof to verify.
 * @param[in] grpkey The group key.
 * @param[in] sigs The signatures related to the proof.
 * @param[in] n_sigs The number of signatures in <i>sigs</i>.
 * 
 * @return IOK or IERROR.
 */
int groupsig_prove_equality_verify(uint8_t *ok, groupsig_proof_t *proof, 
				   groupsig_key_t *grpkey, groupsig_signature_t **sigs, 
				   uint16_t n_sigs);

/** 
 * @fn int groupsig_get_code_from_str(uint8_t *code, char *name)
 * @brief Sets <i>groupsig</i> to the code associated to the given groupsig name.
 *
 * @param[in,out] code The groupsig code.
 * @param[in] name The groupsig name.
 * 
 * @return IOK with code set to the groupsig code. IFAIL if the name does not
 *  correspond to any groupsig name, or IERROR if error.
 */
int groupsig_get_code_from_str(uint8_t *code, char *name);

/* Include here all known group signature schemes */
#include "registered_groupsigs.h"

#endif /* _GROUPSIG_H */

/* groupsig.h ends here */
