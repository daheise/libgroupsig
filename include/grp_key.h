/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: grp_key.h
 * @brief: Basic types and declarations for group keys. 
 * @author: jesus
 * Maintainer: jesus
 * @date: mié jul  4 14:39:12 2012 (+0200)
 * @version: 0.1
 * Last-Updated: jue may 16 22:55:27 2013 (+0200)
 *           By: jesus
 *     Update #: 46
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _GRP_KEY_H
#define _GRP_KEY_H

#include "key.h"

/* Pointers to functions. Every type of grp_key must implement all the following 
   pointers to functions. */

/* "constructors" && "destructors" */
/**
 * @typedef grp_key_init_f
 * @brief Type of functions for initializing group keys.
 */
typedef groupsig_key_init_f grp_key_init_f;

/**
 * @typedef grp_key_free_f
 * @brief Type of functions for freeing group keys.
 */
typedef groupsig_key_free_f grp_key_free_f;

/* Copy */
/**
 * @typedef grp_key_copy_f
 * @brief Type of functions for copying group keys.
 */
typedef groupsig_key_copy_f grp_key_copy_f;

/**
 * @typedef groupsig_key_get_size_in_format_f grp_key_get_size_in_format_f;
 * @brief Type of functions for getting the memory needed for representing
 * group keys in a given format.
 */
typedef groupsig_key_get_size_in_format_f grp_key_get_size_in_format_f;

/* "getters"/"setters" */
typedef groupsig_key_prv_get_f grp_key_prv_get_f;
typedef groupsig_key_pub_get_f grp_key_pub_get_f;
typedef groupsig_key_prv_set_f grp_key_prv_set_f;
typedef groupsig_key_pub_set_f grp_key_pub_set_f;

/* Export/Import */

/**
 * @typedef grp_key_export_f
 * @brief Type of functions for exporting group keys.
 */
typedef groupsig_key_export_f grp_key_export_f;
typedef groupsig_key_pub_export_f grp_key_pub_export_f;
typedef groupsig_key_prv_export_f grp_key_prv_export_f;

/**
 * @typedef grp_key_import_f
 * @brief Type of functions for importing group keys.
 */
typedef groupsig_key_import_f grp_key_import_f;
typedef groupsig_key_prv_import_f grp_key_prv_import_f;
typedef groupsig_key_pub_import_f grp_key_pub_import_f;

/* Conversion to human readable strings */

/**
 * @typedef grp_key_to_string_f
 * @brief Type of functions for converting group keys to strings.
 */
typedef groupsig_key_to_string_f grp_key_to_string_f;
typedef groupsig_key_prv_to_string_f grp_key_prv_to_string_f;
typedef groupsig_key_pub_to_string_f grp_key_pub_to_string_f;

/**
 * @struct grp_key_handle_t
 * @brief Set of functions for managing group keys.
 */
typedef struct {
  uint8_t code; /**< The group scheme. */
  grp_key_init_f init; /**< Initializes group keys. */
  grp_key_free_f free; /**< Frees group keys. */
  grp_key_copy_f copy; /**< Copies group keys. */
  grp_key_export_f export; /**< Exports group keys. */
  grp_key_import_f import; /**< Imports group keys. */
  grp_key_to_string_f to_string; /**< Gets string representations of group keys. */
  grp_key_get_size_in_format_f get_size_in_format;
} grp_key_handle_t;

/** 
 * @fn const grp_key_handle_t* groupsig_grp_key_handle_from_code(uint8_t code)
 * @brief Returns the set of handles for group keys of the specified scheme.
 *
 * @param[in] code The scheme code. 
 * 
 * @return The set of managing functions for the specified scheme or NULL
 *  if error.
 */
const grp_key_handle_t* groupsig_grp_key_handle_from_code(uint8_t code);

/** 
 * @fn groupsig_key_t* groupsig_grp_key_init(uint8_t code)
 * @brief Initializes a group key of the specified scheme.
 *
 * @param[in] code The scheme code. 
 * 
 * @return A pointer to the initialized key or NULL if error.
 */
groupsig_key_t* groupsig_grp_key_init(uint8_t code);

/** 
 * @fn int groupsig_grp_key_free(groupsig_key_t *key)
 * @brief Frees the given group key.
 *
 * @param[in,out] key The key to free. 
 * 
 * @return IOK or IERROR.
 */
int groupsig_grp_key_free(groupsig_key_t *key);

/** 
 * @fn int groupsig_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies the source key into the destination group key.
 *
 * @param[in,out] dst The destination group key. Must have been initialized by
 *  the caller. 
 * @param[in] src The source key. 
 * 
 * @return IOK or IERROR.
 */
int groupsig_grp_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int groupsig_grp_key_get_size_in_format(groupsig_key_t *key,
 *                                             groupsig_key_format_t format)
 * @brief Returns the number of bytes needed to represent <i>key</i> using the
 *  format <i>format</i>.
 *
 * @param[in] key The key.
 * @param[in] format The format.
 *
 * @return The number of bytes needed. On error, errno must be set appropriately.
 */
int groupsig_grp_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format);

/**
 * @fn int groupsig_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, 
 *			    void *dst)
 * @brief Exports the given group key to the specified destination, in the 
 *  given format.
 *
 * @param[in] key The key to export. 
 * @param[in] format The format to use for exporting the key. 
 * @param[in] dst The destination information. 
 * 
 * @return IOK or IERROR.
 */
int groupsig_grp_key_export(groupsig_key_t *key, groupsig_key_format_t format, 
			    void *dst);

/** 
 * @fn groupsig_key_t* groupsig_grp_key_import(uint8_t code, groupsig_key_format_t format, 
 *					       void *source)
 * @brief Imports the group key in the specified source.
 *
 * @param[in] code The scheme code.
 * @param[in] format The format of the source.
 * @param[in] source The source information.
 * 
 * @return A pointer to the processed group key, or NULL if error.
 */
groupsig_key_t* groupsig_grp_key_import(uint8_t code, groupsig_key_format_t format, 
					void *source);

/** 
 * @fn char* groupsig_grp_key_to_string(groupsig_key_t *key)
 * @brief Gets the string representation of the given key.
 *
 * @param[in] key The key.
 * 
 * @return A pointer to the string representation of the group key or NULL if
 *  error.
 */
char* groupsig_grp_key_to_string(groupsig_key_t *key);

#endif /* _GRP_KEY_H */

/* grp_key.h ends here */
