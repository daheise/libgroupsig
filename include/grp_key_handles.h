/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: grp_key_handles.h
 * @brief: Set of supported group key implementations. 
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 19:17:04 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 12:51:43 2013 (+0200)
 *           By: jesus
 *     Update #: 12
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _GRP_KEY_HANDLES_H
#define _GRP_KEY_HANDLES_H

#include "grp_key.h"
#include "groupsig/kty04/grp_key.h"
#include "groupsig/bbs04/grp_key.h"
#include "groupsig/cpy06/grp_key.h"

/**
 * @def GROUPSIG_GRP_KEY_HANDLES_N
 * @brief Number of supported group key implementations.
 */
#define GROUPSIG_GRP_KEY_HANDLES_N 3

/**
 * @var GROUPSIG_GRP_KEY_HANDLES
 * @brief Set of handles for the known group key implementations.
 */
static const grp_key_handle_t *GROUPSIG_GRP_KEY_HANDLES[GROUPSIG_GRP_KEY_HANDLES_N] = { 
  &kty04_grp_key_handle,
  &bbs04_grp_key_handle,
  &cpy06_grp_key_handle,
};

#endif /* _GRP_KEY_HANDLES_H */

/* grp_key_handles.h ends here */
