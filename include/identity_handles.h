/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: identity_handles.h
 * @brief: Groups all the supported identity handles.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie ene 18 18:21:58 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 14:19:05 2013 (+0200)
 *           By: jesus
 *     Update #: 14
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _IDENTITY_HANDLES_H
#define _IDENTITY_HANDLES_H

#include "identity.h"
#include "groupsig/kty04/identity.h"
#include "groupsig/bbs04/identity.h"
#include "groupsig/cpy06/identity.h"

/**
 * @def IDENTITY_HANDLES_N
 * @brief Number of supported identity handles.
 */
#define IDENTITY_HANDLES_N 3

/**
 * @var IDENTITY_HANDLES
 * @brief List of supported identity handles.
 */
static const identity_handle_t *IDENTITY_HANDLES[IDENTITY_HANDLES_N] = {
  &kty04_identity_handle,
  &bbs04_identity_handle,
  &cpy06_identity_handle,
};

#endif /* _IDENTITY_HANDLES_H */

/* identity_handles.h ends here */
