/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: key.h
 * @brief: Type definitions for generic key management functions. 
 * @author: jesus
 * Maintainer: jesus
 * @date: mié jul  4 14:39:12 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun oct  7 20:49:39 2013 (+0200)
 *           By: jesus
 *     Update #: 52
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _GROUPSIG_KEY_H
#define _GROUPSIG_KEY_H

#include <stdint.h>
#include "exim.h"

/**
 * @typedef groupsig_key_format_t
 * @brief Defines all the known key formats.
 */
typedef enum {
  GROUPSIG_KEY_FORMAT_FILE_NULL =
      EXIM_FORMAT_FILE_NULL, /* For "custom" formats in a file */
  GROUPSIG_KEY_FORMAT_FILE_NULL_B64 =
      EXIM_FORMAT_FILE_NULL_B64, /* For "custom" formats stored as base64 strings in a file */
  GROUPSIG_KEY_FORMAT_BYTEARRAY =
      EXIM_FORMAT_BYTEARRAY_NULL,
  GROUPSIG_KEY_FORMAT_STRING_NULL_B64 =
      EXIM_FORMAT_STRING_NULL_B64, /* For "custom" formats stored as base64 strings */
  GROUPSIG_KEY_FORMAT_MESSAGE_NULL =
      EXIM_FORMAT_MESSAGE_NULL, /* For "custom" formats stored as messages. */
  GROUPSIG_KEY_FORMAT_MESSAGE_NULL_B64 =
      EXIM_FORMAT_MESSAGE_NULL_B64, /* For "custom" formats stored as base64 encoded messages. */
} groupsig_key_format_t;

/**
 * @typedef groupsig_key_types
 * @brief Defines all the known key types.
 */
typedef enum {
  GROUPSIG_KEY_GRPKEY, 
  GROUPSIG_KEY_MGRKEY,
  GROUPSIG_KEY_MEMKEY,
} groupsig_key_types;

/**
 * @struct groupsig_key_t
 * @brief Basic structure for group signature schemes keys.
 */
typedef struct {
  uint8_t scheme; /**< The scheme of which this key is an instance of. */
  /* uint8_t type; /\**< Specifies the type of key (e.g. group, member, manager) *\/ */
  void *key; /**< The key itself. */
} groupsig_key_t;

/* Pointers to functions. Every type of key must implement all the following 
   pointers to functions. */

/**
 * @typedef groupsig_key_t* (*groupsig_key_init_f)(void)
 * @brief Type of functions for initializing keys.
 *
 * @return A pointer to the intialized key or NULL if error.
 */
typedef groupsig_key_t* (*groupsig_key_init_f)(void);

/** 
 * @typedef int (*groupsig_key_free_f)(groupsig_key_t *key)
 * @brief Type of functions for freeing keys.
 *
 * @param[in,out] key A pointer to the key to free.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_key_free_f)(groupsig_key_t *key);

/**
 * @typedef int (*groupsig_key_copy_f)(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Type of functions for copying keys.
 *
 * @param[in,out] dst The destiniation key. Must have been initialized by the caller.
 * @param[in] src The source key.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_key_copy_f)(groupsig_key_t *dst, groupsig_key_t *src);

/**
 * @typedef int (*groupsig_key_get_size_in_format_f)(groupsig_key_t *key, 
 *                                                   groupsig_key_format_t format)
 * @brief Type of functions for determining the size of a key in a specific format.
 *
 * Functions of this type return the number of bytes that a given key will need
 * to be represented in the specified format.
 *
 * @param[in] key The key.
 * @param[in] format the specific format.
 *
 * @return The number of bytes needed to represent <i>key</i> in <i>format</i> format.
 *  On error, errno must be set appropriately.
 */
typedef int (*groupsig_key_get_size_in_format_f)(groupsig_key_t *key, groupsig_key_format_t format);

/* "getters"/"setters" */
typedef groupsig_key_t* (*groupsig_key_prv_get_f)(groupsig_key_t *key);
typedef groupsig_key_t* (*groupsig_key_pub_get_f)(groupsig_key_t *key);
typedef int (*groupsig_key_prv_set_f)(void *dst, void *src);
typedef int (*groupsig_key_pub_set_f)(void *dst, void *src);

/**
 * @typedef int (*groupsig_key_export_f)(groupsig_key_t *key, 
 *                                       groupsig_key_format_t format, 
 *                                       void *dst)
 * @brief Type of functions for exporting keys.
 *
 * Functions of this type export <i>key</i> to the destination specified by
 * <i>dst</i>, using the format given by <i>format</i>.
 * 
 * @param[in] key The key to export.
 * @param[in] format The format to use for exporting the key.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_key_export_f)(groupsig_key_t *key, 
				     groupsig_key_format_t format, 
				     void *dst);

typedef int (*groupsig_key_pub_export_f)(groupsig_key_t *key, 
					 groupsig_key_format_t format, 
					 void *dst);
typedef int (*groupsig_key_prv_export_f)(groupsig_key_t *key, 
					 groupsig_key_format_t format, 
					 void *dst);

/**
 * @typedef groupsig_key_t* (*groupsig_key_import_f)(groupsig_key_format_t format, 
 *						 void *source)
 * @brief Type of functions for importing keys.
 *
 * Functions of this type process the given <i>source</i> to retrieve a key
 * in the format <i>format</i>.
 *
 * @param[in] format The format in which the key is stored in <i>source</i>.
 * @param[in] source The source information.
 *
 * @return A pointer to the recovered key or NULL if error.
 */
typedef groupsig_key_t* (*groupsig_key_import_f)(groupsig_key_format_t format, 
						 void *source);

typedef groupsig_key_t* (*groupsig_key_prv_import_f)(groupsig_key_format_t format, void *source);
typedef groupsig_key_t* (*groupsig_key_pub_import_f)(groupsig_key_format_t format, void *source);

/**
 * @typedef char* (*groupsig_key_to_string_f)(groupsig_key_t *key)
 * @brief Type of functions for converting keys to printable strings.
 *
 * @param[in] key The key to convert.
 *
 * @return A pointer to the produced string or NULL if error.
 */
typedef char* (*groupsig_key_to_string_f)(groupsig_key_t *key);
typedef char* (*groupsig_key_prv_to_string_f)(groupsig_key_t *key);
typedef char* (*groupsig_key_pub_to_string_f)(groupsig_key_t *key);

#endif /* _GROUPSIG_KEY_H */

/* key.h ends here */
