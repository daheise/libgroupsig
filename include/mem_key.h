/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mem_key.h
 * @brief: Function types and declaration of functions for managing member keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié jul  4 14:39:12 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun may 20 22:03:29 2013 (+0200)
 *           By: jesus
 *     Update #: 29
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _MEM_KEY_H
#define _MEM_KEY_H

#include "key.h"

/* Pointers to functions. Every type of mem_key must implement all the following 
   pointers to functions. */

/* "constructors" && "destructors" */

/**
 * @typedef groupsig_key_init_f mem_key_init_f
 * @brief Type of functions for initializing member keys.
 */
typedef groupsig_key_init_f mem_key_init_f;

/**
 * @typedef groupsig_key_free_f mem_key_free_f;
 * @brief Type of functions for freeing member keys.
 */
typedef groupsig_key_free_f mem_key_free_f;

/* Copy */

/**
 * @typedef groupsig_key_copy_f mem_key_copy_f;
 * @brief Type of functions for copying member keys.
 */
typedef groupsig_key_copy_f mem_key_copy_f;

/**
 * @typedef groupsig_key_get_size_in_format_f mem_key_get_size_in_format_f;
 * @brief Type of functions for getting the memory needed for represeting member
 *  keys in a given format.
 */
typedef groupsig_key_get_size_in_format_f mem_key_get_size_in_format_f;

/* "getters"/"setters" */
typedef groupsig_key_prv_get_f mem_key_prv_get_f;
typedef groupsig_key_pub_get_f mem_key_pub_get_f;
typedef groupsig_key_prv_set_f mem_key_prv_set_f;
typedef groupsig_key_pub_set_f mem_key_pub_set_f;

/* Export/Import */

/**
 * @typedef groupsig_key_export_f mem_key_export_f;
 * @brief Type of functions for exporting member keys.
 */
typedef groupsig_key_export_f mem_key_export_f;
typedef groupsig_key_pub_export_f mem_key_pub_export_f;
typedef groupsig_key_prv_export_f mem_key_prv_export_f;

/**
 * @typedef groupsig_key_import_f mem_key_import_f;
 * @brief Type of functions for importing member keys.
 */
typedef groupsig_key_import_f mem_key_import_f;
typedef groupsig_key_prv_import_f mem_key_prv_import_f;
typedef groupsig_key_pub_import_f mem_key_pub_import_f;

/* Conversion to human readable strings */

/**
 * @typedef groupsig_key_to_string_f mem_key_to_string_f;
 * @brief Type of functions for getting printable string representations of 
 *  member keys.
 */
typedef groupsig_key_to_string_f mem_key_to_string_f;
typedef groupsig_key_prv_to_string_f mem_key_prv_to_string_f;
typedef groupsig_key_pub_to_string_f mem_key_pub_to_string_f;

/**
 * @struct mem_key_handle_t
 * @brief Bundles together all the function handles for managing member keys.
 */
typedef struct {
  uint8_t code; /**< The scheme code. */
  mem_key_init_f init; /**< Initializes member keys. */
  mem_key_free_f free; /**< Frees member keys. */
  mem_key_copy_f copy; /**< Copies member keys. */
  mem_key_get_size_in_format_f get_size_in_format; /**< Returns the size in bytes of
						      a specific member key in the
						      given format. */
  mem_key_export_f export; /**< Exports member keys.*/
  mem_key_import_f import; /**< Imports member keys. */
  mem_key_to_string_f to_string; /**< Returns a printable string version of 
				    member keys. */
} mem_key_handle_t;

/** 
 * @fn const mem_key_handle_t* groupsig_mem_key_handle_from_code(uint8_t code)
 * @brief Returns the bundle of function handles for the given code.
 *
 * @param[in] code The code.
 * 
 * @return A pointer to the appropriate bundle or NULL if error.
 */
const mem_key_handle_t* groupsig_mem_key_handle_from_code(uint8_t code);

/** 
 * @fn groupsig_key_t* groupsig_mem_key_init(uint8_t code)
 * @brief Initializes a member key of the given scheme.
 *
 * @param[in] code The scheme's code.
 * 
 * @return A pointer to the initialized member key or NULL if error.
 */
groupsig_key_t* groupsig_mem_key_init(uint8_t code);

/** 
 * @fn int groupsig_mem_key_free(groupsig_key_t *key)
 * @brief Frees the memory allocated for <i>key</i>.
 *
 * @param[in,out] key The key to free.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mem_key_free(groupsig_key_t *key);

/** 
 * @fn int groupsig_mem_key_copy(groupsig_key_t *dst, groupsig_key_t *src)
 * @brief Copies the member key in <i>src</i> into <i>dst</i>.
 *
 * @param[in,out] dst The destination member key. Must have been initialized by
 *  the caller.
 * @param[in] src The source member key.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mem_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int groupsig_mem_key_get_size_in_format(groupsig_key_t *key,
 *                                             groupsig_key_format_t format)
 * @brief Returns the number of bytes needed to represent <i>key</i> using the
 *  format <i>format</i>.
 *
 * @param[in] key The key.
 * @param[in] format The format.
 * 
 * @return The number of bytes needed. On error, errno must be set appropriately.
 */
int groupsig_mem_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format);

/** 
 * @fn int groupsig_mem_key_export(groupsig_key_t *key, groupsig_key_format_t format, 
 *                                 void *dst);
 * @brief Exports the member key in <i>key</i> to <i>dst</i> using the format
 *  <i>format</i>.
 *
 * @param[in] key The member key to export.
 * @param[in] format The format to use for exporting the key.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mem_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);

/** 
 * @fn groupsig_key_t* groupsig_mem_key_import(uint8_t code, 
 *                                             groupsig_key_format_t format, 
 *                                             void *source);
 * @brief Imports a member key of the specified scheme.
 *
 * Imports a member key of the scheme <i>code</i>, which is stored in the format
 * <i>format</i> in <i>source</i>.
 *
 * @param[in] code The scheme of the member key.
 * @param[in] format The format in which the key is stored.
 * @param[in] source The source information.
 * 
 * @return A pointer to the retrieved key or NULL if error.
 */
groupsig_key_t* groupsig_mem_key_import(uint8_t code, groupsig_key_format_t format, void *source);

/** 
 * @fn char* groupsig_mem_key_to_string(groupsig_key_t *key);
 * @brief Returns a printable string of the given key.
 *
 * @param[in] key The key to convert.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
char* groupsig_mem_key_to_string(groupsig_key_t *key);


#endif /* _MEM_KEY_H */

/* mem_key.h ends here */
