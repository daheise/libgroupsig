/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mem_key_handles.h
 * @brief: Defines the list of supported member key handles.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 19:17:04 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 12:52:24 2013 (+0200)
 *           By: jesus
 *     Update #: 13
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _MEM_KEY_HANDLES_H
#define _MEM_KEY_HANDLES_H

#include "mem_key.h"
#include "groupsig/kty04/mem_key.h"
#include "groupsig/bbs04/mem_key.h"
#include "groupsig/cpy06/mem_key.h"

/**
 * @def GROUPSIG_MEM_KEY_HANDLES_N
 * @brief Number of known handles of member key schemes.
 */
#define GROUPSIG_MEM_KEY_HANDLES_N 3

/**
 * @var GROUPSIG_MEM_KEY_HANDLES
 * @brief List of handles of supported member key schemes.
 */
static const mem_key_handle_t *GROUPSIG_MEM_KEY_HANDLES[GROUPSIG_MEM_KEY_HANDLES_N] = { 
  &kty04_mem_key_handle,
  &bbs04_mem_key_handle,
  &cpy06_mem_key_handle,
};

#endif /* _MEM_KEY_HANDLES_H */

/* mem_key_handles.h ends here */
