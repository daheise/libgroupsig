/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key.h
 * @brief: Function types and function declarations for managing manager keys.
 * @author: jesus
 * Maintainer: jesus
 * @date: mié jul  4 14:39:12 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun may 20 22:14:52 2013 (+0200)
 *           By: jesus
 *     Update #: 26
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _MGR_KEY_H
#define _MGR_KEY_H

#include "key.h"

/* Pointers to functions. Every type of mgr_key must implement all the following 
   pointers to functions. */

/* "constructors" && "destructors" */

/**
 * @typedef groupsig_key_init_f mgr_key_init_f;
 * @brief Types of functions for initializing manager keys.
 */
typedef groupsig_key_init_f mgr_key_init_f;

/**
 * @typedef groupsig_key_free_f mgr_key_free_f;
 * @brief Type of functions for freeing manager keys.
 */
typedef groupsig_key_free_f mgr_key_free_f;

/* Copy */

/**
 * @typedef groupsig_key_copy_f mgr_key_copy_f;
 * @brief Type of functions for copying manager keys.
 */
typedef groupsig_key_copy_f mgr_key_copy_f;

/**
 * @typedef groupsig_key_get_size_in_format_f mgr_key_get_size_in_format_f;
 * @brief Type of functions for getting the memory needed for representing
 * group keys in a given format.
 */
typedef groupsig_key_get_size_in_format_f mgr_key_get_size_in_format_f;

/* "getters"/"setters" */
typedef groupsig_key_prv_get_f mgr_key_prv_get_f;
typedef groupsig_key_pub_get_f mgr_key_pub_get;
typedef groupsig_key_prv_set_f mgr_key_prv_set_f;
typedef groupsig_key_pub_set_f mgr_key_pub_set_f;

/* Export/Import */

/**
 * @typedef groupsig_key_export_f mgr_key_export_f;
 * @brief Type of functions for exporting manager keys.
 */
typedef groupsig_key_export_f mgr_key_export_f;
typedef groupsig_key_pub_export_f mgr_key_pub_export_f;
typedef groupsig_key_prv_export_f mgr_key_prv_export_f;

/**
 * @typedef groupsig_key_import_f mgr_key_import_f;
 * @brief Type of functions for importing manager keys.
 */
typedef groupsig_key_import_f mgr_key_import_f;
typedef groupsig_key_prv_import_f mgr_key_prv_import_f;
typedef groupsig_key_pub_import_f mgr_key_pub_import_f;

/* Conversion to human readable strings */

/**
 * @typedef groupsig_key_to_string_f mgr_key_to_string_f;
 * @brief Type of functions for producing printable strings of manager keys.
 */
typedef groupsig_key_to_string_f mgr_key_to_string_f;
typedef groupsig_key_prv_to_string_f mgr_key_prv_to_string_f;
typedef groupsig_key_pub_to_string_f mgr_key_pub_to_string_f;

/**
 * @struct mgr_key_handle_t
 * @brief Bundles together the set of function handles for managing manager keys.
 */
typedef struct {
  uint8_t code; /**< The scheme code. */
  mgr_key_init_f init; /**< Iniatilizes manager keys. */
  mgr_key_free_f free; /**< Frees manager keys. */
  mgr_key_copy_f copy; /**< Copies manager keys. */
  mgr_key_export_f export; /**< Exports manager keys. */
  mgr_key_import_f import; /**< Imports manager keys. */
  mgr_key_to_string_f to_string; /**< Produces printable strings of manager keys. */
  mgr_key_get_size_in_format_f get_size_in_format;
} mgr_key_handle_t;

/** 
 * @fn const mgr_key_handle_t* groupsig_mgr_key_handle_from_code(uint8_t code);
 * @brief Returns the bundle of function handles for managing manager keys of the
 *  specified scheme code.
 *
 * @param[in] code The scheme code.
 * 
 * @return A pointer to the appropriate bundle or NULL if error.
 */
const mgr_key_handle_t* groupsig_mgr_key_handle_from_code(uint8_t code);

/** 
 * @fn groupsig_key_t* groupsig_mgr_key_init(uint8_t code);
 * @brief Initializes a group manager key of the given scheme.
 *
 * @param[in] code The scheme code.
 * 
 * @return A pointer to the initialized manager key or NULL if error.
 */
groupsig_key_t* groupsig_mgr_key_init(uint8_t code);

/** 
 * @fn int groupsig_mgr_key_free(groupsig_key_t *key);
 * @brief Frees the memory allocated for the given manager key.
 *
 * @param[in,out] key The key to free.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mgr_key_free(groupsig_key_t *key);

/** 
 * @fn int groupsig_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src);
 * @brief Copies the source manager key into the destination manager key.
 *
 * @param[in,out] dst The destination manager key. Must have been initialized
 *  by the caller.
 * @param[in] src The source manager key.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mgr_key_copy(groupsig_key_t *dst, groupsig_key_t *src);

/** 
 * @fn int groupsig_mgr_key_get_size_in_format(groupsig_key_t *key,
 *                                             groupsig_key_format_t format)
 * @brief Returns the number of bytes needed to represent <i>key</i> using the
 *  format <i>format</i>.
 *
 * @param[in] key The key.
 * @param[in] format The format.
 *
 * @return The number of bytes needed. On error, errno must be set appropriately.
 */
int groupsig_mgr_key_get_size_in_format(groupsig_key_t *key, groupsig_key_format_t format);

/**
 * @fn int groupsig_mgr_key_export(groupsig_key_t *key, 
 *                                 groupsig_key_format_t format, 
 *                                 void *dst);
 * @brief Exports <i>key</i> to <i>dst</i> in the format <i>format</i>.
 *
 * @param[in] key The manager key to export.
 * @param[in] format The format to use for exporting <i>key</i>.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int groupsig_mgr_key_export(groupsig_key_t *key, groupsig_key_format_t format, void *dst);


/** 
 * @fn groupsig_key_t* groupsig_mgr_key_import(uint8_t code, 
 *                                             groupsig_key_format_t format, 
 *                                             void *source);
 * @brief Imports a key of the given scheme and format from the specified source.
 *
 * @param[in] code The scheme code.
 * @param[in] format The format used for storing the key in <i>source</i>.
 * @param[in] source The source information.
 * 
 * @return A pointer to the retrieved group manager key or NULL if error.
 */
groupsig_key_t* groupsig_mgr_key_import(uint8_t code, 
					groupsig_key_format_t format, 
					void *source);

/** 
 * @fn char* groupsig_mgr_key_to_string(groupsig_key_t *key);
 * @brief Gets a printable string associated to <i>key</i>.
 *
 * @param[in] key The key to convert.
 * 
 * @return A pointer to the obtained string or NULL if error.
 */
char* groupsig_mgr_key_to_string(groupsig_key_t *key);

#endif /* _MGR_KEY_H */

/* mgr_key.h ends here */
