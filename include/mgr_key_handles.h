/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mgr_key_handles.h
 * @brief: Defines the supported bundles of manager keys handles.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue ene 17 19:17:04 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 12:52:43 2013 (+0200)
 *           By: jesus
 *     Update #: 11
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _MGR_KEY_HANDLES_H
#define _MGR_KEY_HANDLES_H

#include "mgr_key.h"
#include "groupsig/kty04/mgr_key.h"
#include "groupsig/bbs04/mgr_key.h"
#include "groupsig/cpy06/mgr_key.h"

/**
 * @def GROUPSIG_MGR_KEY_HANDLES_N
 * @brief Number of supported bundles of manager key handles.
 */
#define GROUPSIG_MGR_KEY_HANDLES_N 3

/**
 * @var GROUPSIG_MGR_KEY_HANDLES
 * @brief List of supported bundles of manager key handles.
 */
static const mgr_key_handle_t *GROUPSIG_MGR_KEY_HANDLES[GROUPSIG_MGR_KEY_HANDLES_N] = { 
  &kty04_mgr_key_handle,
  &bbs04_mgr_key_handle,
  &cpy06_mgr_key_handle,
};

#endif /* _MGR_KEY_HANDLES_H */

/* mgr_key_handles.h ends here */
