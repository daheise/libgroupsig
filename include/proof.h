/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: proof.h
 * @brief: Type definitions and function types for managing group signature ZK proofs.
 * @author: jesus
 * Maintainer: jesus
 * @date: lun dic 10 21:33:01 2012 (-0500)
 * @version: 0.1
 * Last-Updated: mar oct  8 23:14:44 2013 (+0200)
 *           By: jesus
 *     Update #: 17
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _PROOF_H
#define _PROOF_H

#include "types.h"
#include "exim.h"

/* Type definitions */

/**
 * @typedef groupsig_proof_format_t
 * @brief Supported proof formats.
 */
typedef enum {
  GROUPSIG_PROOF_FORMAT_FILE_NULL =
      EXIM_FORMAT_FILE_NULL, /* For "custom" formats in a file */
  GROUPSIG_PROOF_FORMAT_FILE_NULL_B64 =
      EXIM_FORMAT_FILE_NULL_B64, /* For "custom" formats stored as base64 strings in a file */
  GROUPSIG_PROOF_FORMAT_BYTEARRAY =
      EXIM_FORMAT_BYTEARRAY_NULL,
  GROUPSIG_PROOF_FORMAT_STRING_NULL_B64 =
      EXIM_FORMAT_STRING_NULL_B64, /* For "custom" formats stored as base64 strings */
  GROUPSIG_PROOF_FORMAT_MESSAGE_NULL =
      EXIM_FORMAT_MESSAGE_NULL, /* For "custom" formats stored as messages. */
  GROUPSIG_PROOF_FORMAT_MESSAGE_NULL_B64 =
      EXIM_FORMAT_MESSAGE_NULL_B64, /* For "custom" formats stored as base64 encoded messages. */
} groupsig_proof_format_t;

/**
 * @struct groupsig_proof_t
 * @brief Structure for group signature schemes general ZK proofs.
 */
typedef struct {
  uint8_t scheme; /**< The scheme of which this proof is an instance of. */
  void *proof; /**< The proof itself. */
} groupsig_proof_t;

/* Pointers to functions: 
   The functions of specific schemes must follow these definitions
 */

/**
 * @typedef groupsig_proof_t* (*groupsig_proof_init_f)(void);
 * @brief Type of functions for initializing proofs.
 *
 * @return A pointer to the initialized proof or NULL if error.
 */
typedef groupsig_proof_t* (*groupsig_proof_init_f)(void);

/**
 * @typedef int (*groupsig_proof_free_f)(groupsig_proof_t *proof);
 * @brief Type of functions for freeing proofs.
 *
 * @param[in,out] proof The proof to be freed.
 *
 * @return IOK or IERROR.
 */
typedef int (*groupsig_proof_free_f)(groupsig_proof_t *proof);

/** 
 * @typedef int (*groupsig_proof_get_size_in_format_f)(groupsig_proof_t *proof, 
 *                                                     groupsig_proof_format_t format);
 * @brief Type of functions for getting the size in bytes of the <i>proof</i> in
 *  format <i>format</i>.
 * 
 * @param[in] proof The proof.
 * @param[in] format The format.
 * 
 * @return The number of bytes needed to represent <i>proof</i> in <i>format</i> format.
 *  On error, errno must be set appropriately.
 */
typedef int (*groupsig_proof_get_size_in_format_f)(groupsig_proof_t *proof,
						   groupsig_proof_format_t format);

/** 
 * @typedef int (*groupsig_proof_export_f)(groupsig_proof_t *proof, 
 *				       groupsig_proof_format_t format, 
 *				       void *dst);
 * @brief Type of functions for exporting proofs.
 * 
 * @param[in] proof The proof to export.
 * @param[in] format The format to use for exporting the proof.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_proof_export_f)(groupsig_proof_t *proof, 
				       groupsig_proof_format_t format, 
				       void *dst);

/** 
 * @typedef groupsig_proof_t* (*groupsig_proof_import_f)(groupsig_proof_format_t format, 
 *                                                       void *source);
 * @brief Type of functions for importing proofs.
 *
 * @param[in] format The format used in which the proof is stored in <i>source</i>.
 * @param[in] src The source information.
 * 
 * @return A pointer to the retrieved proof or NULL if error.
 */
typedef groupsig_proof_t* (*groupsig_proof_import_f)(groupsig_proof_format_t format, 
						     void *source);

/** 
 * @fn typedef char* (*groupsig_proof_to_string_f)(groupsig_proof_t *proof);
 * @brief Type of functions for producing printable strings representations of
 *  proofs.
 *
 * @param[in] proof The proof.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
typedef char* (*groupsig_proof_to_string_f)(groupsig_proof_t *proof);

/**
 * @struct groupsig_proof_handle_t
 * @brief Bundles together all the function handles for managing proofs.
 */
typedef struct {
  uint8_t scheme; /**< The scheme code. */
  groupsig_proof_init_f init; /**< Initializes proofs. */
  groupsig_proof_free_f free; /**< Frees proofs. */
  groupsig_proof_get_size_in_format_f get_size_in_format; /**< Returns the size in
							     bytes needed to represent
							     a proof in a given format. */
  groupsig_proof_export_f export; /**< Exports proofs. */
  groupsig_proof_import_f import; /**< Imports proofs. */
  groupsig_proof_to_string_f to_string; /**< Produces printable string versions 
					   of proofs. */
} groupsig_proof_handle_t;

/** 
 * @fn const groupsig_proof_handle_t* groupsig_proof_handle_from_code(uint8_t code);
 * @brief Returns the bundle of function handles for managing proofs of the
 *  given scheme.
 *
 * @param[in] code The scheme code.
 * 
 * @return A pointer to the functions bundle or NULL if error.
 */
const groupsig_proof_handle_t* groupsig_proof_handle_from_code(uint8_t code);

/** 
 * @fn groupsig_proof_t* groupsig_proof_init(uint8_t code);
 * @brief Initializes a proof of the given scheme.
 *
 * @param[in] code The scheme code.
 * 
 * @return A pointer to the initialized proof or NULL if error.
 */
groupsig_proof_t* groupsig_proof_init(uint8_t code);

/** 
 * @fn int groupsig_proof_free(groupsig_proof_t *proof);
 * @brief Frees the memory allocated for the given proof.
 *
 * @param[in,out] proof The proof to free.
 * 
 * @return IOK or IERROR.
 */
int groupsig_proof_free(groupsig_proof_t *proof);

/** 
 * @fn int groupsig_proof_get_size_in_format(groupsig_proof_t *proof, 
 *                                           groupsig_proof_format_t format);
 * @brief Returns the number of bytes necessary to represent <i>proof</i> using
 *  the format <i>format</i>.
 *
 * @param[in] proof The proof.
 * @param[in] format The format.
 * 
 * @return The number of bytes necessary to represent the proof. On error, errno
 *  must be set appropriately.
 */
int groupsig_proof_get_size_in_format(groupsig_proof_t *proof, 
				      groupsig_proof_format_t format);

/** 
 * @fn int groupsig_proof_export(groupsig_proof_t *proof, 
 *                               groupsig_proof_format_t format, 
 *                               void *dst);
 * @brief Exports <i>proof</i> to <i>dst</i> using the format <i>format</i>.
 *
 * @param[in] proof The proof to export.
 * @param[in] format The format to use for exporting the proof.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int groupsig_proof_export(groupsig_proof_t *proof, 
			  groupsig_proof_format_t format, 
			  void *dst);

/** 
 * @fn groupsig_proof_t* groupsig_proof_import(uint8_t code, 
 *					groupsig_proof_format_t format, 
 *					void *source);
 * @brief Imports a proof of the scheme <i>code</i>, stored using the format
 *  <i>format</i> in <i>src</i>.
 * 
 * @param[in] code The scheme code.
 * @param[in] format The format used when storing the key in <i>src</i>.
 * @param[in] source The source information.
 * 
 * @return A pointer to the recovered proof or NULL if error.
 */
groupsig_proof_t* groupsig_proof_import(uint8_t code, 
					groupsig_proof_format_t format, 
					void *source);

/** 
 * @fn char* groupsig_proof_to_string(groupsig_proof_t *proof);
 * @brief Returns a printable string version of the given proof.
 *
 * @param[in] proof The proof.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
char* groupsig_proof_to_string(groupsig_proof_t *proof);

#endif /* _PROOF_H */

/* proof.h ends here */
