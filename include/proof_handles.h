/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: proof_handles.h
 * @brief: Declares the supported handles for managing group signature proofs.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie ene 18 18:21:58 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 19:51:04 2013 (+0200)
 *           By: jesus
 *     Update #: 9
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _PROOF_HANDLES_H
#define _PROOF_HANDLES_H

#include "proof.h"
#include "groupsig/kty04/proof.h"
#include "groupsig/cpy06/proof.h"

/**
 * @def GROUPSIG_PROOF_HANDLES_N
 * @brief Number of supported proof bundles.
 */
#define GROUPSIG_PROOF_HANDLES_N 2

/**
 * @var GROUPSIG_PROOF_HANDLES
 * @brief List of supported bundles for managing proofs.
 */
static const groupsig_proof_handle_t *GROUPSIG_PROOF_HANDLES[GROUPSIG_PROOF_HANDLES_N] = {
  &kty04_proof_handle,
  &cpy06_proof_handle,
};

#endif /* _PROOF_HANDLES_H */

/* proof_handles.h ends here */
