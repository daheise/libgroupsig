/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: registered_groupsigs.h
 * @brief: "Stub" for avoiding circular #includes. Bundles all the known group
 *  signature scheme's main files.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie ago  3 09:37:49 2012 (+0200)
 * @version: 0.1
 * Last-Updated: jue jul 10 21:16:21 2014 (+0200)
 *           By: jesus
 *     Update #: 25
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _REGISTERED_GROUPSIGS_H
#define _REGISTERED_GROUPSIGS_H

/* Registered groupsigs */
#include "groupsig.h"
#include "kty04.h"
#include "bbs04.h"
#include "cpy06.h"

#endif /* _REGISTERED_GROUPSIGS_H */

/* registered_groupsigs.h ends here */
