/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature.h
 * @brief: Basic types and functions declarations for managing group signatures.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie jul  6 15:16:01 2012 (+0200)
 * @version: 0.1
 * Last-Updated: lun oct  7 20:15:23 2013 (+0200)
 *           By: jesus
 *     Update #: 57
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _GROUPSIG_SIGNATURE_H
#define _GROUPSIG_SIGNATURE_H

#include "types.h"
#include "exim.h"

/* Type definitions */

/**
 * @typedef groupsig_signature_format_t
 * @brief Supported group signature formats.
 */
typedef enum {
  GROUPSIG_SIGNATURE_FORMAT_FILE_NULL =
      EXIM_FORMAT_FILE_NULL, /* For "custom" formats in a file */
  GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64 =
      EXIM_FORMAT_FILE_NULL_B64, /* For "custom" formats stored as base64 strings in a file */
  GROUPSIG_SIGNATURE_FORMAT_BYTEARRAY =
      EXIM_FORMAT_BYTEARRAY_NULL,
  GROUPSIG_SIGNATURE_FORMAT_STRING_NULL_B64 =
      EXIM_FORMAT_STRING_NULL_B64, /* For "custom" formats stored as base64 strings */
  GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL =
      EXIM_FORMAT_MESSAGE_NULL, /* For "custom" formats stored as messages. */
  GROUPSIG_SIGNATURE_FORMAT_MESSAGE_NULL_B64 =
      EXIM_FORMAT_MESSAGE_NULL_B64, /* For "custom" formats stored as base64 encoded messages. */
} groupsig_signature_format_t;

/**
 * @struct groupsig_signature_t
 * @brief Main structure for group signatures.
 */
typedef struct {
  uint8_t scheme; /**< The scheme of which this signature is an instance of. */
  void *sig; /**< The signature itself. */
} groupsig_signature_t;

/* Pointers to functions: 
   The functions of specific schemes must follow these definitions
 */

/**
 * @typedef groupsig_signature_t* (*groupsig_signature_init_f)(void)
 * @brief Type of functions for initializing group signatures.
 *
 * @return A pointer to an initialized group signature, or NULL if error.
 */
typedef groupsig_signature_t* (*groupsig_signature_init_f)(void);

/** 
 * @typedef int (*groupsig_signature_free_f)(groupsig_signature_t *signature);
 * @brief Type of functions for freeing group signatures.
 *
 * @param[in,out] signature The group signature to free.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_signature_free_f)(groupsig_signature_t *signature);

/** 
 * @typedef int (*groupsig_signature_copy_f)(groupsig_signature_t *dst, groupsig_signature_t *src);
 * @brief Type of functions for copying group signatures.
 *
 * @param[in,out] dst The destination group signature. Must have been initialized
 *  by the caller.
 * @param[in] src The source group signature.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_signature_copy_f)(groupsig_signature_t *dst, groupsig_signature_t *src);

/** 
 * @typedef int (*groupsig_signature_get_size_in_format_f)(groupsig_signature_t *sig, 
 *                                                         groupsig_signature_format_t format);
 * @brief Type of functions for getting the size in bytes of a group signature
 *  in a specific format.
 *
 * @param[in] sig The signature.
 * @param[in] format The format.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_signature_get_size_in_format_f)(groupsig_signature_t *sig, 
						       groupsig_signature_format_t format);

/** 
 * @typedef int (*groupsig_signature_export_f)(groupsig_signature_t *signature, 
 *					   groupsig_signature_format_t format, 
 *					   void *dst)
 * @brief Type of functions for exporting group signatures.
 * 
 * @param[in] signature The group signature to export.
 * @param[in] format The format to use for exporting the group signature.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
typedef int (*groupsig_signature_export_f)(groupsig_signature_t *signature, 
					   groupsig_signature_format_t format, 
					   void *dst);

/** 
 * @typedef groupsig_signature_t* (*groupsig_signature_import_f)(groupsig_signature_format_t format, 
 *							     void *source)
 * @brief Type of functions for importing group signatures.
 * 
 * @param[in] format The format in which the signature is stored in <i>source</i>.
 * @param[in] source The source information.
 * 
 * @return IOK or IERROR.
 */
typedef groupsig_signature_t* (*groupsig_signature_import_f)(groupsig_signature_format_t format, 
							     void *source);

/** 
 * @typedef char* (*groupsig_signature_to_string_f)(groupsig_signature_t *signature)
 * @brief Type of functions for obtaining printable strings of group signatures.
 *
 * @param[in] signature The group signature to convert.
 * 
 * @return A pointer to the produced string or NULL if error.
 */
typedef char* (*groupsig_signature_to_string_f)(groupsig_signature_t *signature);

/**
 * @struct groupsig_signature_handle_t
 * @brief Struct containing the set of handles for managing group signatures.
 */
typedef struct {
  uint8_t scheme; /**< The group signature scheme. */
  groupsig_signature_init_f init; /**< Initializes group signatures. */
  groupsig_signature_free_f free; /**< Frees group signatures. */
  groupsig_signature_copy_f copy; /**< Copies group signatures. */
  groupsig_signature_get_size_in_format_f get_size_in_format; /**< Returns the size in bytes
								 of specific signatures in 
								 the given format. */
  groupsig_signature_export_f export; /**< Exports group signatures. */
  groupsig_signature_import_f import; /**< Imports group signatures. */
  groupsig_signature_to_string_f to_string; /**< Gets printable strings of group signatures. */
} groupsig_signature_handle_t; 

/** 
 * @fn const groupsig_signature_handle_t* groupsig_signature_handle_from_code(uint8_t code);
 * @brief Returns the set of handles for managing group signatures of scheme <i>code</i>.
 *
 * @param[in] code The group signature scheme code.
 * 
 * @return A pointer to the struct of handles to manage group signatures of the
 *  scheme <i>code</i> or NULL if error.
 */
const groupsig_signature_handle_t* groupsig_signature_handle_from_code(uint8_t code);

/** 
 * @fn groupsig_signature_t* groupsig_signature_init(uint8_t code)
 * @brief Initializes a group signature of scheme <i>code</i>.
 *
 * @param[in] code The scheme code.
 * 
 * @return A pointer to the initialized group signature or NULL if error.
 */
groupsig_signature_t* groupsig_signature_init(uint8_t code);

/** 
 * @fn int groupsig_signature_free(groupsig_signature_t *sig)
 * @brief Frees the memory allocated for the given group signature.
 *
 * @param[in,out] sig The group signature to free.
 * 
 * @return IOK or IERROR.
 */
int groupsig_signature_free(groupsig_signature_t *sig);

/** 
 * @fn int groupsig_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src)
 * @brief Copies the group signature in <i>src</i> into <i>dst</i>.
 *
 * @param[in,out] dst The destination group signature. Must have been initialized
 *  by the caller.
 * @param[in] src The source group signature.
 * 
 * @return IOK or IERROR.
 */
int groupsig_signature_copy(groupsig_signature_t *dst, groupsig_signature_t *src);

/** 
 * @fn int groupsig_signature_get_size_in_format(groupsig_signature_t *sig, 
 *                                               groupsig_signature_format_t format)
 * @brief Returns the exact number of bytes needed to represent the group signature
 *  <i>sig</i> using the format <i>format</i>.
 *
 * @param[in] sig The group signature.
 * @param[in] format The format.
 * 
 * @return The number of bytes needed to represent <i>sig</i> in <i>format</i> or
 *  -1 if error, with errno appropriately set.
 */
int groupsig_signature_get_size_in_format(groupsig_signature_t *sig, groupsig_signature_format_t format);

/** 
 * @fn int groupsig_signature_export(groupsig_signature_t *sig, 
 *                                   groupsig_signature_format_t format, 
 *                                   void *dst)
 * @brief Exports <i>sig</i> into <i>dst</i> using the format <i>format</i>.
 *
 * @param[in] sig The group signature to export.
 * @param[in] format The format to use.
 * @param[in] dst The destination information.
 * 
 * @return IOK or IERROR.
 */
int groupsig_signature_export(groupsig_signature_t *sig, groupsig_signature_format_t format, void *dst);

/** 
 * @fn groupsig_signature_t* groupsig_signature_import(uint8_t code, 
 *                                                     groupsig_signature_format_t format, 
 *                                                     void *source);
 * @brief Imports the group signature stored in <i>source</i> using the format
 *  <i>format</i>.
 *
 * @param[in] code The group signature scheme code.
 * @param[in] format The format.
 * @param[in] source The source information.
 * 
 * @return A pointer to the imported group signature or NULL if error.
 */
groupsig_signature_t* groupsig_signature_import(uint8_t code, groupsig_signature_format_t format, void *source);

/** 
 * @fn char* groupsig_signature_to_string(groupsig_signature_t *sig)
 * @brief Returns a printable string of <i>sig</i>.
 *
 * @param[in] sig The group signature.
 * 
 * @return A pointer to the obtained string or NULL if error.
 */
char* groupsig_signature_to_string(groupsig_signature_t *sig);

#endif /* _GROUPSIG_SIGNATURE_H */

/* signature.h ends here */
