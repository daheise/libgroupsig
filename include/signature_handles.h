/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: signature_handles.h
 * @brief: Puts together all the supported group signatures' handles.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie ene 18 18:21:58 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 12:53:20 2013 (+0200)
 *           By: jesus
 *     Update #: 9
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _SIGNATURE_HANDLES_H
#define _SIGNATURE_HANDLES_H

#include "signature.h"
#include "groupsig/kty04/signature.h"
#include "groupsig/bbs04/signature.h"
#include "groupsig/cpy06/signature.h"

/**
 * @def GROUPSIG_SIGNATURE_HANDLES_N
 * @brief Number of supported set of handles for managing group signatures.
 */
#define GROUPSIG_SIGNATURE_HANDLES_N 3

/**
 * @var GROUPSIG_SIGNATURE_HANDLES
 * @brief List of supported set of handles for managing group signatures.
 */
static const groupsig_signature_handle_t *GROUPSIG_SIGNATURE_HANDLES[GROUPSIG_SIGNATURE_HANDLES_N] = {
  &kty04_signature_handle,
  &bbs04_signature_handle,
  &cpy06_signature_handle,
};

#endif /* _SIGNATURE_HANDLES_H */

/* signature_handles.h ends here */
