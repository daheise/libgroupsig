/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: sysenv.h
 * @brief: System wide declarations and types.
 * @author: jesus
 * Maintainer: jesus
 * @date: jue abr 19 15:34:26 2012 (+0200)
 * @version: 0.1
 * Last-Updated: mié sep 25 20:49:46 2013 (+0200)
 *           By: jesus
 *     Update #: 30
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _SYSENV_H
#define _SYSENV_H

/* #include <gnutls/gnutls.h> */
#include "types.h"
#include "logger.h"
#include "bigz.h"

/**
 * @struct sysenv_t
 * @brief System wide shared data structure.
 */
typedef struct {
  gmp_randstate_t big_rand; /**< Big numbers random numbers generator. */
  void *data; /**< Any additional data required or that would be useful to be
		 widely accessible, but specific to individual schemes should
		 be included here. */

} sysenv_t;

/**
 * @var sysenv
 * @brief System wide shared data.
 */
sysenv_t *sysenv;

/** 
 * @fn sysenv_t* sysenv_init()
 *  @brief Initializes all system wide complex parameters (randomness, etc.)
 *
 * @param[in] seed The seed to use for PRNG. When set to UINT_MAX, a random
 *  seed will be used.
 *
 * @return A pointer to the initialized structure or NULL if error. 
 */
sysenv_t* sysenv_init(unsigned int seed);

/** 
 * @fn int sysenv_free(sysenv_t *sysenv)
 * @brief Frees all the memory allocated for sysenv.
 *
 * @param[in] sysenv Structure to free.
 * 
 * @return IOK or IERROR.
 */
int sysenv_free(sysenv_t *sysenv);

/** 
 * @fn int sysenv_reseed(unsigned int seed)
 * @brief Resets the seed of the PRNGs. If a seed distinct than UINT_MAX is passed, 
 * that seed is used. Otherwise, a random seed is obtained.
 *
 * @param[in] seed The seed to use
 * 
 * @return IOK or IERROR.
 */
int sysenv_reseed(unsigned int seed);

#endif

/* sysenv.h ends here */
