/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: trapdoor_handles.h
 * @brief: Groups together the sets of handles for managing the supported 
 *  trapdoor types.
 * @author: jesus
 * Maintainer: jesus
 * @date: vie ene 18 18:21:58 2013 (+0100)
 * @version: 0.1
 * Last-Updated: vie oct  4 14:18:54 2013 (+0200)
 *           By: jesus
 *     Update #: 14
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _TRAPDOOR_HANDLES_H
#define _TRAPDOOR_HANDLES_H

#include "trapdoor.h"
#include "groupsig/kty04/trapdoor.h"
#include "groupsig/bbs04/trapdoor.h"
#include "groupsig/cpy06/trapdoor.h"

/**
 * @def TRAPDOOR_HANDLES_N
 * @brief Number of supported trapdoors.
 */
#define TRAPDOOR_HANDLES_N 3

/**
 * @var TRAPDOOR_HANDLES
 * @brief List of supported trapdoors.
 */
static const trapdoor_handle_t *TRAPDOOR_HANDLES[TRAPDOOR_HANDLES_N] = { 
  &kty04_trapdoor_handle,
  &bbs04_trapdoor_handle,
  &cpy06_trapdoor_handle,
};

#endif /* _TRAPDOOR_HANDLES_H */

/* trapdoor_handles.h ends here */
