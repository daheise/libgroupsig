/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: types.h
 * @brief: Basic types shared by all the different submodules. 
 * @author: jesus
 * Maintainer: jesus
 * @date: mié abr 18 21:51:33 2012 (+0200)
 * @version: 0.1
 * Last-Updated: Tue Jun 11 06:09:17 2013 (-0400)
 *           By: jesus
 *     Update #: 44
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _TYPES_H
#define _TYPES_H

#include "logger.h"

/**
 * @var logger
 * @brief Must be defined in the main program (if logging support is desired).
 */
extern log_t logger;

/* Constants */

/* Application metadata */
/**
 * @def GROUPSIGNAME
 * @brief Specifies the name of the software
 */
#define GROUPSIGNAME "groupsigs"

/**
 * @def GROUPSIGVERSION
 * @brief Specifies the current version of the software.
 */
#define GROUPSIGVERSION "0.0"

/* /\** */
/*  * @def GROUPSIGCOPYRIGHT */
/*  * @brief Short copyright statement. */
/*  *\/ */
/* #define GROUPSIGCOPYRIGHT " " */

/**
 * @def IOK
 * @brief Integer return code for OK (execution successful).
 */
#define IOK 0

/**
 * @def IERROR
 * @brief Integer return code for ERROR (meaning that somehow, there was a malfunction). 
 *  When this code is returned, errno should be consequently set.
 */
#define IERROR 1

/**
 * @def IFAIL
 *  Integer return code for ERROR (execution failed). This code is used when a 
 *  function does not successfully accomplish its task, but not due to an 
 *  "unexpected error". E.g. if a parsing function does not return what it was
 *  supposed to find due to an EOF, instead of due to lack of memory. Usually,
 *  this code will be accompanied by a "reason" code.
 */
#define IFAIL 2

/**
 * @def IEXISTS
 * @brief Integer return code for functions that try to add something to a list (or 
 * similar) but find out that the received element already exists in the list.
 */
#define IEXISTS 3

/**
 * @def SHA1_DIGEST_LENGTH_DIGITS
 * @brief Number of decimal digits needed to represent a SHA1 digest as a decimal number.
 */
#define SHA1_DIGEST_LENGTH_DIGITS 49

/**
 * @def MPZ_PRIMALITY_ITERS
 * @brief Sets the number of iterations to run in the primality tests of the libgmp library.
 *  5 to 10 are the recommended number of iterations.
 */
#define MPZ_PRIMALITY_ITERS 10

/* Type definitions */

typedef unsigned char byte_t;

/* Macros */

/**
 * @def GOTOENDRC
 * @brief Useful for modifying return codes and redirecting to goto labels.
 */
#define GOTOENDRC(c, f)				\
  {						\
  rc = c;					\
  goto f ## _end;				\
  }

#endif

/* types.h ends here */
