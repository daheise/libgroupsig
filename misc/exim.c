#include <stdio.h>
#include <math.h>

#include "wrappers/base64.h"
#include "sys/mem.h"
#include "types.h"
#include "misc.h"
#include "exim.h"


/**
 * @fn static int exim_get_size_bytearray_null(exim_t* obj)
 * @brief Calculates the size of a byte representation of the data contained in
 * obj.
 *
 * @param[in] obj An exim wrapper containing the data to be sized.
 *
 * @return The size in bytes of the data when exported as a byte array.
 *         -1 on error.
 */
static int exim_get_size_bytearray_null(exim_t* obj){
  int size;
  //TODO: get_size_bytearray_null() should eventually take a
  // (void*)obj->eximable, not the exim_t object
  size = obj->funcs->get_size_bytearray_null(obj);
  return size;
}

/**
 * @fn  int exim_get_size_string_null_b64(exim_t* obj)
 * @brief Returns the size in bytes of a base64 representation.
 *
 * @param[in] obj The exim object to be sized.
 *
 * @return The size in bytes of a base64 representation. -1 on error.
 */
static int exim_get_size_string_null_b64(exim_t* obj){

  int size;
  size = exim_get_size_bytearray_null(obj);
  if(size == -1) return -1;

  /* Base64 has a length of ceil(4/3) bytes of the original byte representation */
  size = ceil((float) size / (float) 3) * 4;

  return size;
}

/**
 * @fn int exim_get_size_in_format(exim_t *obj, exim_format_t format);
 * @brief Calculates the size of the representation of the data contained in
 * obj in the given format.
 *
 * @param[in] obj An exim wrapper containing the data to be sized.
 * @param[in] format The format the size is requested for.
 *
 * @return The size in bytes of the data when exported in format
 *         -1 on error.
 */
int exim_get_size_in_format(exim_t *obj, exim_format_t format){
  if(!obj || !obj->eximable) {
    LOG_EINVAL(&logger, __FILE__, "kty04_mem_key_export", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Apply the specified conversion */
  switch(format) {
  case EXIM_FORMAT_FILE_NULL:
    return exim_get_size_bytearray_null(obj);
  case EXIM_FORMAT_FILE_NULL_B64:
    return exim_get_size_string_null_b64(obj);
  case EXIM_FORMAT_STRING_NULL_B64:
    return exim_get_size_string_null_b64(obj);
  case EXIM_FORMAT_MESSAGE_NULL:
    return exim_get_size_bytearray_null(obj);
  case EXIM_FORMAT_MESSAGE_NULL_B64:
    return exim_get_size_string_null_b64(obj);
  case EXIM_FORMAT_BYTEARRAY_NULL:
    return exim_get_size_bytearray_null(obj);
  default:
    LOG_EINVAL_MSG(&logger, __FILE__, "exim_export", __LINE__,
           "Unexpected format.", LOGERROR);
    return -1;
  }

  return -1;
}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_bytearray_fd(exim_t* obj, FILE *fd)
 * @brief Creates a representation of the given key as a bytes written to
 * a file descriptor.

 * @param[in] obj The exim object to export.
 * @param[out] fd An open filestream to write to.
 *
 * @return IOK or IERROR
 */
static int exim_export_bytearray_fd(exim_t* obj, FILE *fd) {
  //TODO: export_bytearray_fd() should eventually take a
  // (void*)obj->eximable, not the exim_t object
  int retval;
  retval = obj->funcs->export_bytearray_fd(obj, fd);
  fflush(fd);
  return retval;
}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_bytearray_null(exim_t* obj, byte_t *dst)
 * @brief Creates a representation of the given key as a bytearray written to
 * dst. dst needs to be exim_get_size_string_null_b64() in length.

 * @param[in] obj The exim object to export.
 * @param[out] dst A bytearray to write to.
 *
 * @return IOK or IERROR
 */
static int exim_export_bytearray_null(exim_t* obj, byte_t *dst) {
  FILE* fd;
  uint64_t size;
  byte_t* buffer;

  if(!obj || !dst) {
    LOG_EINVAL(&logger, __FILE__, "_export_bytearray_null", __LINE__, LOGERROR);
    return IERROR;
  }

  size =  exim_get_size_bytearray_null(obj);
  /*
   * We use a buffer because fmemopen puts a null byte at the end of the buffer
   * it writes to, making its space requirements size+1. It would be nice not
   * to need this since it doubles the memory requirement of this function.
   */
  buffer = (byte_t*)mem_malloc(size+1);
  if(!(fd = fmemopen(buffer, size+1, "r+"))){
    mem_free(buffer); buffer = NULL;
    return IERROR;
  }

  if((exim_export_bytearray_fd(obj, fd) != IOK)){
    mem_free(buffer); buffer = NULL;
    fclose(fd); fd= NULL;
    return IERROR;
  }
  fclose(fd); fd = NULL;
  memcpy(dst, buffer, size);

  mem_free(buffer); buffer = NULL;

  return IOK;

}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_string_null_b64(exim_t* obj, char *dst)
 * @brief Creates a representation of the given key as a base64 string written
 * to dst. dst needs to be exim_get_size_bytearray_null() in length.

 * @param[in] obj The exim object to export.
 * @param[out] dst A bytearray to write to.
 *
 * @return IOK or IERROR
 */
static int exim_export_string_null_b64(exim_t* obj, char *dst) {

  char *b64;
  uint64_t size;
  byte_t* bytearray;

  if(!dst){
    return IERROR;
  }

  size = exim_get_size_bytearray_null(obj);
  if(!(bytearray = (byte_t *)mem_malloc(size))){
    return IERROR;
  }
  if(exim_export_bytearray_null(obj, bytearray) == IERROR){
    mem_free(bytearray); bytearray = NULL;
    return IERROR;
  }

  b64 = base64_encode(bytearray, size);
  if(!strcpy(dst, b64)){
    mem_free(b64); b64= NULL;
    mem_free(bytearray); bytearray = NULL;
    return IERROR;
  }

  mem_free(b64); b64= NULL;
  mem_free(bytearray); bytearray = NULL;

  return IOK;

}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_message_null(exim_t *obj, message_t* msg)
 * @brief Creates a bytearray representation of the given exim object
 * as a libmsg message_t written to msg->bytes. Client does not need to
 * allocate space for the bytes. Client is responsible for freeing the object
 * stored there.
 *
 * @param[in] obj The exim object to export.
 * @param[out] msg A message to write to.
 *
 * @return IOK or IERROR
 */
static int exim_export_message_null(exim_t *obj, message_t* msg){
  if(!obj || !msg) {
    LOG_EINVAL(&logger, __FILE__, "_kty04_proof_export_file_null_b64", __LINE__,
           LOGERROR);
    return IERROR;
  }

  if((msg->length = exim_get_size_bytearray_null(obj)) == -1) {
    return IERROR;
  }

  msg->bytes = (byte_t *)mem_malloc(msg->length);
  if(exim_export_bytearray_null(obj, msg->bytes) == IERROR) {
    mem_free(msg->bytes); msg->bytes = NULL;
    return IERROR;
  }

  return IOK;
}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_message_null_b64(exim_t *obj, message_t* msg)
 * @brief Creates a base64 string representation of the given exim object
 * as a libmsg message_t written to msg->bytes. Client does not need to
 * allocate space for the bytes. Client is responsible for freeing the object
 * stored there.
 *
 * @param[in] obj The exim object to export.
 * @param[out] msg A message to write to.
 *
 * @return IOK or IERROR
 */
static int exim_export_message_null_b64(exim_t *obj, message_t* msg){
  if(!obj || !msg) {
    LOG_EINVAL(&logger, __FILE__, "_kty04_proof_export_file_null_b64", __LINE__,
           LOGERROR);
    return IERROR;
  }

  msg->length = exim_get_size_string_null_b64(obj);
  msg->bytes = (byte_t *)mem_malloc(msg->length+1);
  if(exim_export_string_null_b64(obj, (char*)msg->bytes) == IERROR) {
    mem_free(msg->bytes); msg->bytes = NULL;
    return IERROR;
  }

  return IOK;
}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_file_null_b64(exim_t* obj, char *filename)
 * @brief Creates a base64 string representation of the given exim object
 * and writes it to a file.
 *
 * @param[in] obj The exim object to export.
 * @param[out] filepath A filepath to be written to.
 *
 * @return IOK or IERROR
 */
static int exim_export_file_null_b64(exim_t* obj, char *filepath) {
  FILE *fd;
  char* buffer;
  uint64_t size;

  if(!obj || !filepath) {
    LOG_EINVAL(&logger, __FILE__, "_export_file_null", __LINE__, LOGERROR);
    return IERROR;
  }

  size = exim_get_size_string_null_b64(obj);
  if(!(buffer = (char*)mem_malloc(size+1))){
    LOG_ERRORCODE(&logger, __FILE__, "_export_file_null", __LINE__,
          errno, LOGERROR);
    return IERROR;
  }

  if(exim_export_string_null_b64(obj, buffer) != IOK){
    LOG_ERRORCODE(&logger, __FILE__, "_export_file_null", __LINE__,
          errno, LOGERROR);
    mem_free(buffer); buffer= NULL;
    return IERROR;
  }

  /* Open the file */
  if(!(fd = fopen(filepath, "w"))) {
    LOG_ERRORCODE(&logger, __FILE__, "_export_file_null", __LINE__,
          errno, LOGERROR);
    mem_free(buffer); buffer= NULL;
    return IERROR;
  }

  if(fwrite(buffer, size, 1, fd) != 1){
    LOG_ERRORCODE(&logger, __FILE__, "_export_file_null", __LINE__,
          errno, LOGERROR);
    fclose(fd); fd = NULL;
    mem_free(buffer); buffer= NULL;
    return IERROR;
  }
  fclose(fd); fd = NULL;
  mem_free(buffer); buffer= NULL;

  return IOK;

}

// TODO: This should probably return the number of bytes written
/**
 * @fn int exim_export_file_null(exim_t* obj, char *filepath)
 * @brief Creates a bytearray representation of the given exim object
 * and writes it to a file.
 *
 * @param[in] obj The exim object to export.
 * @param[out] filepath A filepath to be written to.
 *
 * @return IOK or IERROR
 */
static int exim_export_file_null(exim_t* obj, char *filepath) {
  FILE *fd;

  if(!obj || !filepath) {
    LOG_EINVAL(&logger, __FILE__, "_export_file_null", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Open the file */
  if(!(fd = fopen(filepath, "w"))) {
    LOG_ERRORCODE(&logger, __FILE__, "_export_file_null", __LINE__,
		  errno, LOGERROR);
    return IERROR;
  }

  if(exim_export_bytearray_fd(obj, fd) != IOK){
    fclose(fd); fd = NULL;
    return IERROR;
  }
  fclose(fd); fd = NULL;

  return IOK;

}


// TODO: This should probably return the number of bytes written, which
// should be compared to exim_get_size_in_format()
/**
 * @fn int exim_export(exim_t* obj, exim_format_t format, void *dst)
 * @brief Writes a byte representation of the data in obj to dst.
 * The destination should be allocated by the client to accept
 * at least exim_get_size_in_format() bytes.
 *
 * The type of dst depends on format:
 * FORMAT_FILE_NULL: (char*) The filename to be written to.
 * FORMAT_FILE_NULL_B64: (char*) The filename to be written to.
 * FORMAT_STRING_NULL_B64: (char*) The C string to be written to.
 * FORMAT_MESSAGE_NULL: (message_t) A libmsg object to be written to.
 * FORMAT_MESSAGE_NULL_B64: (message_t) A libmsg object to be written to.
 * FORMAT_BYTEARRAY_NULL: (byte_t) A bytearray to be written to.
 *
 * @param[in] obj An exim wrapper containing the data to be exported.
 * @param[in] format The format to export the data in obj as.
 * @param[in] dst The destination to write to.
 *
 * @return IOK or IERROR
 */
int exim_export(exim_t* obj, exim_format_t format, void *dst){
  if(!obj || !obj->eximable) {
      LOG_EINVAL(&logger, __FILE__, "kty04_mem_key_export", __LINE__, LOGERROR);
      return IERROR;
    }

    /* Apply the specified conversion */
    switch(format) {
    case EXIM_FORMAT_FILE_NULL:
      return exim_export_file_null(obj, dst);
    case EXIM_FORMAT_FILE_NULL_B64:
      return exim_export_file_null_b64(obj, dst);
    case EXIM_FORMAT_STRING_NULL_B64:
      return exim_export_string_null_b64(obj, dst);
    case EXIM_FORMAT_MESSAGE_NULL:
      return exim_export_message_null(obj, dst);
    case EXIM_FORMAT_MESSAGE_NULL_B64:
      return exim_export_message_null_b64(obj, dst);
    case EXIM_FORMAT_BYTEARRAY_NULL:
      return exim_export_bytearray_null(obj, dst);
    default:
      LOG_EINVAL_MSG(&logger, __FILE__, "exim_export", __LINE__,
             "Unexpected format.", LOGERROR);
      return IERROR;
    }

    return IERROR;
}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_bytearray_fd(FILE *fd, exim_t* obj)
 * @brief Reads a representation of the given exim object as bytes from
 * a file descriptor.

 * @param[in] fd The file descriptor to read fom
 * @param[out] obj The exim object to write to
 *
 * @return IOK or IERROR
 */
static int exim_import_bytearray_fd(FILE *fd, exim_t* obj) {
  //TODO: import_bytearray_fd() should eventually take a
  // (void*)obj->eximable, not the exim_t object
  int retval;
  retval = obj->funcs->import_bytearray_fd(fd, obj);
  return retval;
}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_bytearray_null(byte_t *source, uint64_t size, exim_t* obj)
 * @brief Reads a representation of the given exim object as bytes from
 * a file descriptor. source should be at least size bytes long. size should
 * come from exim_get_size_bytearray_null().

 * @param[in] source The bytes to read an exim object from.
 * @param[out] size The size of the object to be read.
 * @param[out] obj The exim object to store the read bytes in
 *
 * @return IOK or IERROR
 */
static int exim_import_bytearray_null(byte_t *source, uint64_t size, exim_t* obj) {
  FILE* fd;

  if(!source) {
    LOG_EINVAL(&logger, __FILE__, "_import_bytearray_null", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(fd = fmemopen(source, size, "r"))){
    LOG_EINVAL(&logger, __FILE__, "_import_bytearray_null", __LINE__, LOGERROR);
    return IERROR;
  }

  if(exim_import_bytearray_fd(fd, obj) != IOK){
    LOG_EINVAL(&logger, __FILE__, "_import_bytearray_null", __LINE__, LOGERROR);
    fclose(fd);
    return IERROR;
  }
  fclose(fd);

  return IOK;

}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_string_null_b64(char *b64, exim_t* obj)
 * @brief Reads a representation of the given exim object from
 * a base64 string.

 * @param[in] source A base64 string.
 * @param[out] obj The exim object to store the object in
 *
 * @return IOK or IERROR
 */
static int exim_import_string_null_b64(char *b64, exim_t* obj) {
  byte_t *bytes;
  uint64_t length;

  if(!(bytes = base64_decode(b64, &length))) {
    return IERROR;
  }

  if(exim_import_bytearray_null(bytes, length, obj) != IOK){
    return IERROR;
  }

  return IOK;
}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_string_null_b64(char *b64, exim_t* obj)
 * @brief Reads a representation of the given exim object from
 * a libmsg message

 * @param[in] msg A libmsg message.
 * @param[out] obj The exim object to store the object in
 *
 * @return IOK or IERROR
 */
static int exim_import_message_null(message_t* msg, exim_t *obj){

  if(!msg || !obj) {
    LOG_EINVAL(&logger, __FILE__, "exim_import_message_null_b64",
           __LINE__, LOGERROR);
    return IERROR;
  }

  return exim_import_bytearray_null(msg->bytes, msg->length, obj);

}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_string_null_b64(char *b64, exim_t* obj)
 * @brief Reads a representation of the given exim object from
 * a libmsg message holding a base64 string

 * @param[in] msg A libmsg message.
 * @param[out] obj The exim object to store the object in
 *
 * @return IOK or IERROR
 */
static int exim_import_message_null_b64(message_t* msg, exim_t *obj){

  if(!msg || !obj) {
    LOG_EINVAL(&logger, __FILE__, "exim_import_message_null_b64",
           __LINE__, LOGERROR);
    return IERROR;
  }

  return exim_import_string_null_b64((char *) msg->bytes, obj);

}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_file_null_b64(char *filepath, exim_t* obj)
 * @brief Reads a representation of the given exim object from
 * a file holding a byte representation

 * @param[in] filepath A filepath string
 * @param[out] obj The exim object to store the object in
 *
 * @return IOK or IERROR
 */
static int exim_import_file_null_b64(char *filepath, exim_t* obj) {
  byte_t *bytes;
  FILE *fd;
  int size;

  if(!filepath) {
    LOG_EINVAL(&logger, __FILE__, "exim_import_file_null_b64", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(fd = fopen(filepath, "r"))) {
    LOG_ERRORCODE(&logger, __FILE__, "exim_import_file_null_b64", __LINE__,
           errno, LOGERROR);
    return IERROR;
  }

  size = misc_get_fd_size(fd);
  if(!(bytes = (byte_t*)mem_malloc(size+1))){
    LOG_ERRORCODE(&logger, __FILE__, "exim_import_file_null_b64", __LINE__,
               errno, LOGERROR);
    fclose(fd);
    return IERROR;
  }

  if(fread(bytes, size, 1, fd) != 1){
    LOG_ERRORCODE(&logger, __FILE__, "exim_import_file_null_b64", __LINE__,
                   errno, LOGERROR);
    mem_free(bytes); bytes = NULL;
    fclose(fd);
    return IERROR;
  }
  bytes[size] = '\0';

  if(exim_import_string_null_b64((char*)bytes, obj) != IOK){
    mem_free(bytes); bytes = NULL;
    fclose(fd);
    return IERROR;
  }
  fclose(fd);
  mem_free(bytes); bytes = NULL;

  return IOK;

}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import_file_null_b64(char *filepath, exim_t* obj)
 * @brief Reads a representation of the given exim object from
 * a file holding a base64 representation

 * @param[in] filepath A filepath string
 * @param[out] obj The exim object to store the object in
 *
 * @return IOK or IERROR
 */
static int exim_import_file_null(char *filepath, exim_t* obj) {
  FILE *fd;

  if(!filepath) {
    LOG_EINVAL(&logger, __FILE__, "exim_import_file_null", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(fd = fopen(filepath, "r"))) {
    LOG_ERRORCODE(&logger, __FILE__, "exim_import_file_null", __LINE__,
		   errno, LOGERROR);
    return IERROR;
  }

  if(exim_import_bytearray_fd(fd, obj) != IOK){
    fclose(fd); fd = NULL;
    return IERROR;
  }
  fclose(fd);

  return IOK;

}

// TODO: This should probably return the number of bytes read or allocated
/**
 * @fn int exim_import(exim_format_t format, void *source, exim_t* obj);
 * @brief Reads the data from source into the eximable field of obj based on
 * the given format. The data returned in obj is newly allocated and it is
 * the responsibility of the caller to free it
 *
 * The type of sourve depends on format:
 * FORMAT_FILE_NULL: (char*) The filename to be read from.
 * FORMAT_FILE_NULL_B64: (char*) The filename to be read from.
 * FORMAT_STRING_NULL_B64: (char*) The C string to be read from.
 * FORMAT_MESSAGE_NULL: (message_t) A libmsg object to be read from.
 * FORMAT_MESSAGE_NULL_B64: (message_t) A libmsg object to be read from.
 * FORMAT_BYTEARRAY_NULL: (byte_t) A bytearray to be read from.
 *
 * @param[in] format The format the data in fd is stored as
 * @param[in] source The source to be read from.
 * @param[in] obj The exim wrapper to have the newly allocated data written to
 *
 * @return IOK or IERROR
 */
int exim_import(exim_format_t format, void *source, exim_t* obj){
  if(!obj) {
    LOG_EINVAL(&logger, __FILE__, "exim_import", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Apply the specified conversion */
  switch(format) {
  case EXIM_FORMAT_FILE_NULL:
    if(exim_import_file_null(source, obj) != IOK){
      return IERROR;
    }
    break;
  case EXIM_FORMAT_FILE_NULL_B64:
    if(exim_import_file_null_b64(source, obj)  != IOK){
      return IERROR;
    }
    break;
  case EXIM_FORMAT_STRING_NULL_B64:
    if(exim_import_string_null_b64(source, obj)  != IOK){
      return IERROR;
    }
    break;
  case EXIM_FORMAT_MESSAGE_NULL:
    if(exim_import_message_null(source, obj)  != IOK){
      return IERROR;
    }
    break;
  case EXIM_FORMAT_MESSAGE_NULL_B64:
    if(exim_import_message_null_b64(source, obj)  != IOK){
      return IERROR;
    }
    break;
  case EXIM_FORMAT_BYTEARRAY_NULL:
    if(exim_import_bytearray_null(source,
        exim_get_size_bytearray_null(obj),
        obj) != IOK){
      return IERROR;
    }
    break;
  default:
    LOG_EINVAL_MSG(&logger, __FILE__, "exim_import", __LINE__,
           "Unexpected format.", LOGERROR);
    return IERROR;
  }

  return IOK;
}
