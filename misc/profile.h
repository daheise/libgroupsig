/*                               -*- Mode: C -*- 
 * @file: profile.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: Tue Jul  9 05:33:37 2013 (-0400)
 * @version: 
 * Last-Updated: mar ago 26 22:02:17 2014 (+0200)
 *           By: jesus
 *     Update #: 29
 * URL: 
 */

#ifndef _PROFILE_H
#define _PROFILE_H

#include <stdint.h>
#include <time.h>
#include <sys/time.h>

typedef struct {
  struct timeval tvbegin;
  struct timeval tvend;
  clock_t clckbegin;
  clock_t clckend;
  uint64_t cyclebegin;
  uint64_t cycleend;
} profile_entry_t;

typedef struct {
  char *filename;
  profile_entry_t *entries;
  uint64_t n;
  uint64_t printed;
} profile_t;

uint64_t rdtsc();
profile_t* profile_begin(char *filename);
int profile_free(profile_t *profile);
int profile_get_time(struct timeval *tv, clock_t *clck, uint64_t *cycle);
int profile_add_entry(profile_t *profile, struct timeval *tvbegin, struct timeval *tvend, 
		      clock_t clckbegin, clock_t clckend, uint64_t cyclebegin, uint64_t cycleend);
/* Same as profile_add_entry, but directly prints the new entry to the file
   (useful for long experiments that may fail before running all iterations) */
int profile_dump_entry(profile_t *profile, struct timeval *tvbegin, struct timeval *tvend, 
		       clock_t clckbegin, clock_t clckend, uint64_t cyclebegin, uint64_t cycleend);
int profile_dump_data(profile_t *prof);

#endif

/* profile.h ends here */
