/*                               -*- Mode: C -*- 
 * @file: base64.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue jul  5 14:42:56 2012 (+0200)
 * @version: 
 * Last-Updated: Fri Jun  7 09:04:20 2013 (-0400)
 *           By: jesus
 *     Update #: 38
 * URL: 
 */

#include "base64.h"

#include "config.h"
#include <glib.h>

#include "logger.h"

char* message_base64_encode(const byte_t *in, uint64_t length) {

  if(!in || !length) {
    LOG_EINVAL(&logger, __FILE__, "message_base64_encode", __LINE__, LOGERROR);
    return NULL;
  }

  return g_base64_encode(in, length);
   
}

byte_t* message_base64_decode(const char *in, uint64_t length, uint64_t *length_dec) {

  if(!in || !length) {
    LOG_EINVAL(&logger, __FILE__, "message_base64_decode", __LINE__, LOGERROR);
    return NULL;
  }

  return g_base64_decode(in, length_dec);

}


/* base64.c ends here */
