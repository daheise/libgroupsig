/*                               -*- Mode: C -*- 
 * @file: base64.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue jul  5 14:43:16 2012 (+0200)
 * @version: 
 * Last-Updated: Fri Jun  7 06:53:34 2013 (-0400)
 *           By: jesus
 *     Update #: 21
 * URL: 
 */

#ifndef _BASE64_H
#define _BASE64_H

#include <stdint.h>

#include "types.h"

char* message_base64_encode(const byte_t *in, uint64_t length);
byte_t* message_base64_decode(const char *in, uint64_t length, uint64_t *length_dec);

#endif /* _BASE64_H */

/* base64.h ends here */
