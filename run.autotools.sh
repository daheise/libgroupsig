#!/bin/bash

echo "Running libtoolize"
libtoolize

echo "Running aclocal"
aclocal

echo "Running automake -ac --add-missing"
automake -ac --add-missing

echo "Running autoheader"
autoheader

echo "Running autoconf"
autoconf
