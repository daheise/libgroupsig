/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mem.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mar dic 18 22:47:20 2012 (-0500)
 * @version: 
 * Last-Updated: jue jul 10 21:36:30 2014 (+0200)
 *           By: jesus
 *     Update #: 10
 * URL: 
 */

#include "config.h"
#include "mem.h"

#include <stdlib.h>
#include <string.h>
#include "logger.h"
#include "types.h"

void* mem_malloc(size_t size) {

  void *p;

  if(!size) {
    LOG_EINVAL(&logger, __FILE__, "mem_malloc", __LINE__, LOGERROR);
    return NULL;
  }

  if(!(p = malloc(size))) {
    LOG_ERRORCODE(&logger, __FILE__, "mem_malloc", __LINE__, errno, LOGERROR);
    return NULL;
  }

  memset(p, 0, size);

  return p;  

}

void* mem_realloc(void *ptr, size_t size) {

  if(!ptr) return mem_malloc(size);

  if(!(ptr = realloc(ptr, size))) {
    LOG_ERRORCODE(&logger, __FILE__, "mem_realloc", __LINE__, errno, LOGERROR);
    return NULL;
  }

  return ptr;

}

int mem_free(void *p) {

  if(!p) {
    LOG_EINVAL_MSG(&logger, __FILE__, "mem_free", __LINE__, 
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  free(p); 
  p = NULL;
  return IOK;

}

/* mem.c ends here */
