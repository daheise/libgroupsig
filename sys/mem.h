/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: mem.h
 * @brief: For now, just for custom mallocs and frees.
 * @author: jesus
 * Maintainer: jesus
 * @date: mar dic 18 22:42:23 2012 (-0500)
 * @version: 0.1
 * Last-Updated: jue jul 10 21:36:57 2014 (+0200)
 *           By: jesus
 *     Update #: 9
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _MEM_H
#define _MEM_H

#include <stdio.h>

/** 
 * @fn void* mem_malloc(size_t *size)
 * @brief Like malloc, but sets all the allocated bytes to 0.
 * 
 * @param[in] size The number of bytes to allocate.
 * 
 * @return The allocated pointer, or NULL.
 */
void* mem_malloc(size_t size);

/** 
 * @fn void* mem_realloc(void* ptr, size_t size)
 * Like normal realloc, but sets to 0 all the newly allocated memory.
 * Actually... @todo
 *
 * @param[in,out] ptr A pointer to the memory to reallocate.
 * @param size The new size.
 * 
 * @return A pointer to the reallocated memory.
 */
void* mem_realloc(void* ptr, size_t size);

/** 
 * @fn int mem_free(void* p)
 * @brief Frees the given pointer and sets it to NULL;
 * 
 * @param[in,out] p The pointer to free.
 * 
 * @return IOK.
 */
int mem_free(void* p);

#endif /* _MEM_H */

/* mem.h ends here */
