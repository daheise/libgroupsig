/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: sysenv.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue abr 19 15:33:39 2012 (+0200)
 * @version: 
 * Last-Updated: Tue Jun 11 06:10:38 2013 (-0400)
 *           By: jesus
 *     Update #: 59
 * URL: 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <time.h>

#include "sysenv.h"

sysenv_t* sysenv_init(unsigned int seed) {

  sysenv_t *sysenv;
  unsigned int s;

  if(!(sysenv = (sysenv_t *) malloc(sizeof(sysenv_t)))) {
    LOG_ERRORCODE(&logger, __FILE__, "sysenv_init", __LINE__, errno, LOGERROR);
    return NULL;
  }
  memset(sysenv, 0, sizeof(sysenv_t));
  sysenv->data = NULL;

  /* /\* If we do not receive a seed, generate one *\/ */
  /* /\* Passing a seed serves to reproduce some executions *\/ */
  /* if(seed == UINT_MAX) { */
    s = time(NULL);
  /* } else { */
    /* s = seed; */
  /* } */

  /* Initialize big numbers random */
  bigz_randinit_default(sysenv->big_rand);
  bigz_randseed_ui(sysenv->big_rand, s);

  /* Initialize default random */
  srand(time(NULL));

  return sysenv;

}

int sysenv_free(sysenv_t *sysenv) {

  if(!sysenv) {
    LOG_EINVAL_MSG(&logger, __FILE__, "sysenv_free", __LINE__, 
		   "Nothing to free.", LOGERROR); 
    return IOK;
  }

  bigz_randclear(sysenv->big_rand);

  free(sysenv); sysenv = NULL;

  /** @todo Protect gnutls deinitialization with a mutex! */
  /* gnutls_global_deinit(); */

  return IOK;

}

int sysenv_reseed(unsigned int seed) {

  unsigned int s;

  /* If we do not receive a seed, generate one */
  /* Passing a seed serves to reproduce some executions */
  if(seed == UINT_MAX) {
    s = time(NULL);
  } else {
    s = seed;
  }

  /* Initialize big numbers random */
  bigz_randclear(sysenv->big_rand);
  bigz_randinit_default(sysenv->big_rand);
  bigz_randseed_ui(sysenv->big_rand, s);

  /* Initialize default random */
  srand(time(NULL));

  return IOK;

}

/* sysenv.c ends here */
