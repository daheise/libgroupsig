#include <stdio.h>
#include <stdlib.h>

#include <groupsig/groupsig.h>

int main(int argc, char **argv) {

  uint8_t scheme;
  groupsig_config_t *cfg = NULL;

  if(argc <= 1 || !argv) {
	fprintf(stderr, "Error: invalid arguments.\n");
	return 1;
  }

  if((groupsig_get_code_from_str(&scheme, argv[1])) == IERROR) {
    fprintf(stderr, "Error: Wrong scheme %s\n", argv[1]);
    return IERROR;
  }

  if(!(cfg = groupsig_config_init(scheme))) {
    return IERROR;
  }

  fprintf(stdout, "Successfully initialized the groupsig config structure.\n");

  return 0;

}

/* sf_sysboot.c ends here */
