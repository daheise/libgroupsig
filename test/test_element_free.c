#include <stdio.h>

#include <pbc/pbc.h>

size_t bitlimit = 160;

static int _param_generated(pbc_cm_t cm, void *data) {                                                                              
  
  pbc_param_t *param;
  
  /* Initialize params */
   param = (pbc_param_t *) data;
   pbc_param_init_d_gen(*param, cm);
   if(mpz_sizeinbase(cm->q, 2) < bitlimit - bitlimit/40 ||
      mpz_sizeinbase(cm->q, 2) > bitlimit + bitlimit/20) {
     pbc_param_clear(*param);
    return 0;
   }
  
  return 1;
                                                                                                         
}

int main(int argc, char **argv) {

  pbc_param_t param;
  pairing_t pairing;
  element_t e;
  unsigned int d;
  int found;

  d = 9563; found = 0;
  while(/* d < BBS04_D_MAX &&  */!found) {

    /* d must be congruent with 0 or 3 mod 4 */
    if(!(found = pbc_cm_search_d(_param_generated, &param, d, bitlimit))) {
      d++;
      if(!(found = pbc_cm_search_d(_param_generated, &param, d, bitlimit))) {
	d+=3;
      }
    }
    
  }

  fprintf(stdout, "Found!\n");
  
  /* init pairing */
  pairing_init_pbc_param(pairing, param);

  /* init element */
  element_init_G1(e, pairing);

  /* free element */
  element_clear(e);

  /* free pairing */
  pairing_clear(pairing);

  /* free param */
  pbc_param_clear(param);

  return 0;

}
