#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <groupsig/groupsig.h>

int main(int argc, char **argv) {

  uint8_t scheme;
  groupsig_config_t *cfg = NULL;
  groupsig_key_t *mem = NULL, *mgr = NULL, *grp = NULL;
  gml_t *gml = NULL;

  if(argc <= 1 || !argv) {
	fprintf(stderr, "Error: invalid arguments.\n");
	return 1;
  }

  if((groupsig_get_code_from_str(&scheme, argv[1])) == IERROR) {
    fprintf(stderr, "Error: Wrong scheme %s\n", argv[1]);
    return IERROR;
  }

  if(groupsig_init(UINT_MAX) == IERROR) {
    fprintf(stderr, "Error initializing groupsig.\n");
    return IERROR;
  }

  if(!(cfg = groupsig_config_init(scheme))) {
    fprintf(stderr, "Error initializing group scheme config.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully initialized the groupsig config structure.\n");

  /* Initialize the keys structures */
  if(!(grp = groupsig_grp_key_init(scheme))) {
    fprintf(stderr, "Error initializing group key.\n");
    return IERROR;
  }

  if(!(mgr = groupsig_mgr_key_init(scheme))) {
    fprintf(stderr, "Error initializing manager key.\n");
    return IERROR;
  }

  if(!(mem = groupsig_mem_key_init(scheme))) {
    fprintf(stderr, "Error initializing member key.\n");
    return IERROR;
  }

  /* Initialize the gml */
  if(!(gml = gml_init(scheme))) {
    fprintf(stderr, "Error initializing gml.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully initialized the keys and gml structures.\n");

  /* Initialize the scheme */
  if(groupsig_setup(scheme, grp, mgr, gml, cfg) == IERROR) {
    fprintf(stderr, "Error initializing the scheme.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully initialized scheme.\n");

  /* Run the member join procedure */
  if(groupsig_join_mem(mem, grp) == IERROR) {
    fprintf(stderr, "Error in the member join side.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully executed the join_mem process.\n");

  /* Free the keys structures */
  if(groupsig_mem_key_free(mem) == IERROR) {
    fprintf(stderr, "Error freeing member key.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully freed the member key.\n");

  if(groupsig_grp_key_free(grp) == IERROR) {
    fprintf(stderr, "Error freeing group key.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully freed the group key.\n");

  if(groupsig_mgr_key_free(mgr) == IERROR) {
    fprintf(stderr, "Error freeing manager key.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully freed the manager key.\n");

  /* Free gml */
  if(gml_free(gml) == IERROR) {
    fprintf(stderr, "Error freeing gml.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully freed the gml.\n");

  /* Free scheme config */
  if(groupsig_config_free(cfg) == IERROR) {
    fprintf(stderr, "Error freeing groupsig config.\n");
    return IERROR;
  }

  /* Free groupsig environment */
  if(groupsig_clear(scheme) == IERROR) {
    fprintf(stderr, "Error freeing groupsig environment.\n");
    return IERROR;
  }

  fprintf(stdout, "Successfully freed groupsig environment.\n");

  return IOK;

}
