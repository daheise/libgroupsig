/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: gs.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: vie ene  4 15:38:42 2013 (+0100)
 * @version: 
 * Last-Updated: Tue Jun 11 06:12:28 2013 (-0400)
 *           By: jesus
 *     Update #: 69
 * URL: 
 */

#include "config.h"
#include <stdio.h>
#include <limits.h>

#include "logger.h"

#include "include/action.h"
#include "include/groupsig.h"
#include "gs_options.h"

log_t logger;

int main(int argc, char *argv[]) {

  gs_options_t opt = {
    0, /* uint8_t help */
    NULL,  /* const groupsig_t* gs */
    UINT8_MAX,  /* uint8_t action */
    NULL, /* grp_key_t *grpkey */
    NULL, /* char *grpkey_name */
    NULL, /* mgr_key_t *mgrkey */
    NULL, /* char mgrkey_name */
    NULL, /* mem_key_t *memkey */
    NULL, /* char *memkey_name */
    NULL, /* groupsig_signature_t *signature */
    NULL, /* char *signature_name */
    NULL, /* groupsig_signature_t **signatures */
    0, /* uint16_t n_sigs */
    NULL, /* groupsig_proof_t *proof */
    NULL, /* char *proof_name */
    NULL, /* gml_t *gml */
    NULL, /* char *gml_name */
    NULL, /* crl_t *crl */
    NULL, /* char *crl_name */
    NULL, /* groupsig_config_t *config */
    0, /* uint64_t security */
    0, /* uint64_t primesize */
    0.f, /* double epsilon */
    0, /* uint64_t index */
    NULL, /* message_t *msg */
    NULL, /* char *msg_name */
    NULL, /* trapdoor_t *trap */
    NULL, /* identity_t *id */
    0,  /* uint8_t verbosity */
    LOGERROR,  /* uint8_t log_level */
    "gs.log",  /* char *log */
    NULL, /* char *dir */
  };
  uint8_t bool, code;
  int rc;

  /* Parse options */
  if(gs_options_parse(argc, argv, &opt) == IERROR) return IERROR;

  /* Initialize log */
  if(log_init(opt.log, LOGERROR, 0, &logger) == IERROR) {
    return IERROR;
  }

  /* Initialize system environment */
  if(!(sysenv = sysenv_init(UINT_MAX))) {
    return IERROR;
  }
  
  rc = IOK;
  bool = 0; code = opt.gs->desc->code;
  switch(opt.action) {

  case ACTION_SETUP_CODE:

    /* Run the setup */
    rc = groupsig_setup(code, opt.grpkey, opt.mgrkey, opt.gml, opt.config);
    
    /* Export the results */
    if(rc == IOK) {
      rc += groupsig_grp_key_export(opt.grpkey, GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
				    opt.grpkey_name);
      rc += groupsig_mgr_key_export(opt.mgrkey, GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
				    opt.mgrkey_name);
      rc += gml_export(opt.gml, opt.gml_name, GML_FILE);
      if(rc) rc = IERROR;
    }

    break;

  case ACTION_JOIN_MEM_CODE:
    
    /* Run the join */
    rc = groupsig_join_mem(code, opt.memkey, opt.grpkey);
    
    /* Export the results */
    if(rc == IOK) {
      rc = groupsig_mem_key_export(opt.memkey, GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
				   opt.memkey_name);
    }

    break;

  case ACTION_JOIN_MGR_CODE:

    /* Run the join */
    rc = groupsig_join_mgr(code, opt.gml, opt.memkey, opt.mgrkey, opt.grpkey);

    /* Export the results */
    if(rc == IOK) {
      rc = groupsig_mem_key_export(opt.memkey, GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
				   opt.memkey_name);
      rc = gml_export(opt.gml, opt.gml_name, GML_FILE);
    }

    break;

  case ACTION_SIGN_CODE:

    /* Run the sign */
    rc = groupsig_sign(code, opt.signature, opt.msg, opt.memkey, opt.grpkey, UINT_MAX);

    /* Export the results */
    if(rc == IOK) {
      rc = groupsig_signature_export(opt.signature, GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
				     opt.signature_name);
    }

    break;

  case ACTION_VERIFY_CODE:

    /* Run the verify */
    rc = groupsig_verify(code, &bool, opt.signature, opt.msg, opt.grpkey);

    /* Print the results */
    if(rc == IOK) {
      if(bool) fprintf(stdout, "Signature OK.\n");
      else fprintf(stdout, "Signature INVALID.\n");
    }

    break;

  case ACTION_OPEN_CODE:

    /* Run the open */
    rc = groupsig_open(code, opt.id, opt.signature, opt.grpkey, opt.mgrkey, opt.gml);

    /* Print the results */
    if(rc == IOK) {

      char *s;

      if(!(s = identity_to_string(opt.id))) {
	fprintf(stdout, "Error converting ID.\n");
      }

      fprintf(stdout, "The identity of the signer is: %s\n", s);

    }

    break;

  case ACTION_REVEAL_CODE:

    /* Run the reveal */
    rc = groupsig_reveal(code, opt.trap, opt.crl, opt.gml, opt.index);

    /* Print and export the results */
    if(rc == IOK) {

      char *s;

      if(!(s = trapdoor_to_string(opt.trap))) {
	fprintf(stdout, "Error converting trapdoor.\n");
      }

      fprintf(stdout, "The trapdoor of the requested user is %s\n", s);

      rc = crl_export(opt.crl, opt.crl_name, CRL_FILE);

    }

    break;

  case ACTION_TRACE_CODE:
    
    /* Run the trace */
    rc = groupsig_trace(code, &bool, opt.signature, opt.grpkey, opt.crl);

    /* Print the results */
    if(rc == IOK) {
      if(bool) fprintf(stdout, "Issuer REVOKED.\n");
      else fprintf(stdout, "Issuer NOT revoked.\n");
    }

    break;

  case ACTION_CLAIM_CODE:
    
    /* Run the claim */
    rc = groupsig_claim(code, opt.proof, opt.memkey, opt.grpkey, opt.signature);

    /* Export the results */
    if(rc == IOK) {
      rc = groupsig_proof_export(opt.proof, GROUPSIG_PROOF_FORMAT_FILE_NULL_B64, 
				 opt.proof_name);      
    }

    break;

  case ACTION_CLAIM_VERIFY_CODE:

    /* Run the claim verify */
    rc = groupsig_claim_verify(code, &bool, opt.proof, opt.signature, opt.grpkey);

    /* Print the results */
    if(rc == IOK) {
      if(bool) fprintf(stdout, "Proof OK.\n");
      else fprintf(stdout, "Proof INVALID.\n");
    }

    break;

  case ACTION_PROVE_EQUALITY_CODE:

    /* Run the prove equality */
    rc = groupsig_prove_equality(code, opt.proof, opt.memkey, opt.grpkey,
				 opt.signatures, opt.n_sigs);

    /* Export the results */
    if(rc == IOK) {
      rc = groupsig_proof_export(opt.proof, GROUPSIG_PROOF_FORMAT_FILE_NULL_B64, 
				 opt.proof_name);      
    }
    break;

  case ACTION_PROVE_EQUALITY_VERIFY_CODE:

    /* Run the prove equality verify */
    rc = groupsig_prove_equality_verify(code, &bool, opt.proof, opt.grpkey,
					opt.signatures, opt.n_sigs);

    /* Print the results */
    if(rc == IOK) {
      if(bool) fprintf(stdout, "Proof OK.\n");
      else fprintf(stdout, "Proof INVALID.\n");
    }
    break;

  default:
    fprintf(stdout, "Unknown error.");
    rc = IERROR;
    break;
  }

  if(rc == IERROR) {
    fprintf(stdout, "Error while processing the operation.\n");
  }

  return rc;

}

/* gs.c ends here */
