/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: gs_options.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: mié abr 18 21:40:51 2012 (+0200)
 * @version: 
 * Last-Updated: mié sep 25 20:37:01 2013 (+0200)
 *           By: jesus
 *     Update #: 549
 * URL: 
 */

#include "config.h"
#include <stdio.h> 
#include <errno.h>
#include <getopt.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>
#include <limits.h>

/* #define _GNU_SOURCE */
#include <getopt.h>

#include "message.h"
#include "sysenv.h"
#include "include/groupsig.h"
#include "include/action.h"
#include "sys/mem.h"

#include "gs_options.h"

struct option long_options[] = {
  {"help", optional_argument, 0, 'h'},
  {"action", required_argument, 0, 0},
  {"verbosity", required_argument, 0, 0},
  {"log-level", required_argument, 0, 0},
  {"log", required_argument, 0, 0},
  {"directory", required_argument, 0, 0}, // delete when changed to PF_INET sockets model.
  {NULL,0,0,0}
};

/**
 * @fn static int _gs_options_help(char *argv[])
 * @brief Prints the help menu to stderr.
 *
 * @param[in] argv Program parameters
 * @return IOK
 */
static int _gs_options_help(char *argv[]) {

  fprintf(stderr, "\n%s version %s\n\n"
	  "Usage: %s <scheme> <action> [options]\n\n"
	  "OPTIONS:\n"
	  " General:\n"
	  "  -h[<scheme>] | --help[=<scheme>]\n"
	  "       Prints this help menu. If an optional parameter specifying a valid"
	  "       scheme is passed, help for that scheme will be printed.\n\n"
	  "  -a | --action\n"
	  " \n\n"
	  " g:M:m:s:S:p:G:c:k:P:e:"
	  "  -v <int> | --verbosity <int>\n"
	  "       Specifies a verbosity level, from 1 to 10, with increasing "
	  "level of detail.\n\n"
	  "  -l <string> | --log-level <string>\n"
	  "       Specifies the logging profile. The mandatory parameter must "
	  "be either 'ERROR', 'WARNING' or 'DEBUG',\n"
	  "       and the log will contain just the error messages, the error+warning"
	  " messages, or the error+warning+debug messages. The default is ERROR.\n\n"
	  "  -L <string> | --log <string>\n"
	  "       Specifies the log file to use. Defaults to anoncerts.log.\n\n",
	  GROUPSIGNAME, GROUPSIGVERSION, argv[0]);
  exit(IOK);
}

static int _gs_options_parse_setup(int argc, char *argv[], gs_options_t *opt) {

  kty04_config_t *kty04_cfg;
  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_setup", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3;
  
  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'g': /* grpkey_name */
      opt->grpkey_name = strdup(optarg);
      break;

    case 'M': /* mgrkey_name */
      opt->mgrkey_name = strdup(optarg);
      break;

    case 'G': /* gml_name */

      opt->gml_name = strdup(optarg);
      break;

    case 'k': /* security */
      
      errno = 0;
      opt->security = strtoul(optarg, NULL, 10);
      if(errno) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong security parameter.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'P': /* primesize */

      errno = 0;
      opt->primesize = strtoul(optarg, NULL, 10);
      if(errno) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong primesize parameter.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'e': /* epsilon */

      errno = 0;
      opt->epsilon = strtod(optarg, NULL);
      if(errno) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong epsilon parameter.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
      
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!opt->security || !opt->primesize || !opt->epsilon ||
     !opt->grpkey_name || !opt->mgrkey_name || !opt->gml_name) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Security, Primesize, Epsilon, Group Key, "
			  "Manager Key and GML are mandatory.", &logger, __FILE__, 
			  "gs_options_parse", __LINE__, EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->grpkey = groupsig_grp_key_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing group key.", &logger, __FILE__,
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  if(!(opt->mgrkey = groupsig_mgr_key_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing manager key.", &logger, __FILE__,
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  if(!(opt->gml = gml_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing GML.", &logger, __FILE__,
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  /* Prepare config structure */
  if(!(kty04_cfg = (kty04_config_t *) mem_malloc(sizeof(kty04_config_t)))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing configuration structure.", 
			  &logger, __FILE__, "gs_options_parse", __LINE__,
			  errno, LOGERROR);
    return IERROR;
  }

  kty04_cfg->epsilon = opt->epsilon;
  kty04_cfg->security = opt->security;
  kty04_cfg->primesize = opt->primesize;

  if(!(opt->config = (groupsig_config_t *) mem_malloc(sizeof(groupsig_config_t)))) {
    mem_free(kty04_cfg); kty04_cfg = NULL;
  }

  opt->config->scheme = GROUPSIG_KTY04_CODE;
  opt->config->config = kty04_cfg;


  return IOK;

}

static int _gs_options_parse_join_mem(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_mem", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0;  optind = 3;

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'm': /* memkey_name */
      opt->memkey_name = strdup(optarg);
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);

      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->memkey = groupsig_mem_key_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing member key.", &logger, __FILE__,
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_join_mgr(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_join_mgr", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'M': /* mgrkey */

      opt->mgrkey_name = strdup(optarg);
      if(!(opt->mgrkey = groupsig_mgr_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->mgrkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong manager key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'm': /* memkey */

      opt->memkey_name = strdup(optarg);
      if(!(opt->memkey = groupsig_mem_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->memkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong member key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }

      opt->memkey_name = strdup(optarg);
      break;

    case 'G': /* GML */

      opt->gml_name = strdup(optarg);
      if(!(opt->gml = gml_import(opt->gs->desc->code, 
				 GML_FILE, opt->gml_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong GML.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }



  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }


  return IOK;

}

static int _gs_options_parse_sign(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_sign", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'i':

      opt->msg_name = strdup(optarg);

      if(!(opt->msg = message_init())) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Error importing message.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;	
      }

      if(message_import(opt->msg, MESSAGE_FORMAT_NULL_FILE, opt->msg_name) == IERROR) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Error importing message.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;	
      }
      break;

    case 's': /* signature_name */
      opt->signature_name = strdup(optarg);
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'm': /* memkey */

      opt->memkey_name = strdup(optarg);
      if(!(opt->memkey = groupsig_mem_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->memkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong member key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->signature = groupsig_signature_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing signature.", &logger, __FILE__,
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_verify(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_sign", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'm': /* memkey */

      opt->memkey_name = strdup(optarg);
      if(!(opt->memkey = groupsig_mem_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->memkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong member key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 's': /* signature */

      opt->signature_name = strdup(optarg);
      if(!(opt->signature = groupsig_signature_import(opt->gs->desc->code, 
						      GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
						      opt->signature_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'i':

      opt->msg_name = strdup(optarg);

      if(!(opt->msg = message_init())) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Error importing message.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;	
      }

      if(message_import(opt->msg, MESSAGE_FORMAT_NULL_FILE, opt->msg_name) == IERROR) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Error importing message.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;	
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }


  return IOK;

}

static int _gs_options_parse_open(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_open", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
      
    case 'M': /* mgrkey */

      opt->mgrkey_name = strdup(optarg);
      if(!(opt->mgrkey = groupsig_mgr_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64,
						 opt->mgrkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong manager key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 's': /* signature */

      opt->signature_name = strdup(optarg);
      if(!(opt->signature = groupsig_signature_import(opt->gs->desc->code, 
						      GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
						      opt->signature_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'G': /* GML */

      opt->gml_name = strdup(optarg);
      if(!(opt->gml = gml_import(opt->gs->desc->code, 
				 GML_FILE, opt->gml_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong GML.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->id = identity_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing identity.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_reveal(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_reveal", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'i': /* index */

      errno = 0;
      opt->index = strtoul(optarg, NULL, 10);
      if(errno) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong index parameter.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'G': /* GML */

      opt->gml_name = strdup(optarg);
      if(!(opt->gml = gml_import(opt->gs->desc->code, 
				 GML_FILE, opt->gml_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong GML.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'c': /* CRL */

      opt->crl_name = strdup(optarg);
      if(!(opt->crl = crl_import(opt->gs->desc->code, 
				 CRL_FILE, opt->crl_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong CRL.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->trap = trapdoor_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing trapdoor.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_trace(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_trace", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 's': /* signature */

      opt->signature_name = strdup(optarg);
      if(!(opt->signature = groupsig_signature_import(opt->gs->desc->code, 
						      GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
						      opt->signature_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;      

    case 'c': /* CRL */

      opt->crl_name = strdup(optarg);
      if(!(opt->crl = crl_import(opt->gs->desc->code, 
				 CRL_FILE, opt->crl_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong CRL.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->trap = trapdoor_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing trapdoor.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_claim(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_claim", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'p': /* proof_name */
      opt->proof_name = strdup(optarg);
      break;

    case 'm': /* memkey */

      opt->memkey_name = strdup(optarg);
      if(!(opt->memkey = groupsig_mem_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->memkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong member key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;     

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 's': /* signature */

      opt->signature_name = strdup(optarg);
      if(!(opt->signature = groupsig_signature_import(opt->gs->desc->code, 
						      GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
						      opt->signature_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->proof = groupsig_proof_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing proof.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_claim_verify(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_claim_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'p': /* proof */
      
      opt->proof_name = strdup(optarg);
      if(!(opt->proof = groupsig_proof_import(opt->gs->desc->code, 
					      GROUPSIG_PROOF_FORMAT_FILE_NULL_B64, 
					      opt->proof_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong proof.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 's': /* signature */

      opt->signature_name = strdup(optarg);
      if(!(opt->signature = groupsig_signature_import(opt->gs->desc->code, 
						      GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
						      opt->signature_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}


static int _gs_options_parse_prove_equality(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_prove_equality", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'p': /* proof_name */
      opt->proof_name = strdup(optarg);
      break;

    case 'm': /* memkey */

      opt->memkey_name = strdup(optarg);
      if(!(opt->memkey = groupsig_mem_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->memkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong member key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;     

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'S': /* signatures */

      {

	char *name;

	/* We have to manage optind manually: it automatically increments in one 
	   after processing the option (in this case, 'S') and moves to the next one. 
	   We decrement it until getting the first "filename". We keep on incrementing
	   it until we reach the next option (when we reach a '-' at the beginning of
	   an argv) or until we reach the end of argv. */
	optind--; 
	opt->n_sigs = 0;
	while(optind < argc && argv[optind][0] != '-') {

	  if(!(opt->signatures = (groupsig_signature_t **) 
	       realloc(opt->signatures, sizeof(groupsig_signature_t *)*(opt->n_sigs+1)))) {
	    OPTIONS_ERROR_LOG_MSG(stdout, strerror(errno), &logger, __FILE__, 
				  "gs_options_parse", __LINE__, errno, LOGERROR);
	    return IERROR;
	  }

	  name = strdup(optarg);
	  if(!(opt->signatures[opt->n_sigs] = 
	       groupsig_signature_import(opt->gs->desc->code,
					 GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, name))) {
	    OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
				  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	    return IERROR;
	  }
	  free(name);

	  opt->n_sigs++;
	  optind++;

	}

      }

      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  if(!(opt->proof = groupsig_proof_init(opt->gs->desc->code))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Error initializing proof.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

static int _gs_options_parse_prove_equality_verify(int argc, char *argv[], gs_options_t *opt) {

  uint64_t uint64;
  int rc, ret, option_index;

  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "_gs_options_parse_prove_equality_verify", __LINE__, LOGERROR);
    return IERROR;
  }

  opterr = 0; optind = 3; 

  ret = 0;
  rc = IERROR;
  while((ret = getopt_long(argc, argv, OPT_STRING, long_options, &option_index)) != -1) {

    rc = IOK;
    switch(ret) {
    case 0:
      
      /* 0 means long option */
      if(!strcmp(long_options[option_index].name, "help")) { /* help */
	_gs_options_help(argv);
	return IOK;
      } else if(!strcmp(long_options[option_index].name, "verbosity")) { /* verbosity */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->verbosity = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log-level")) { /* log-level */
	errno = 0;
	uint64 = strtol(optarg, NULL, 10);
	if(errno || uint64 > UINT8_MAX) {
	  if(uint64 > UINT8_MAX) errno = ERANGE;
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
	opt->log_level = (uint8_t) uint64;
      } else if(!strcmp(long_options[option_index].name, "log")) { /* log */
	if(!(opt->log = strdup(optarg))) {
	  OPTIONS_ERROR_LOG_OPT(stdout, long_options[option_index].name, optarg, 
				&logger, __FILE__, "gs_options_parse", __LINE__, 
				errno, LOGERROR);
	  return IERROR;
	}
      } else { /* Wat? */
	OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",
			      &logger, __FILE__, "gs_options_parse", __LINE__,
			      errno, LOGERROR);
	return IERROR;
      } 
      break;

    case 'h': /* help */
      opt->help = 1;
      if(optarg) {
	_gs_options_help(argv);
      }
      break;

    case 'p': /* proof */
      
      opt->proof_name = strdup(optarg);
      if(!(opt->proof = groupsig_proof_import(opt->gs->desc->code, 
					      GROUPSIG_PROOF_FORMAT_FILE_NULL_B64, 
					      opt->proof_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong proof.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'g': /* grpkey */

      opt->grpkey_name = strdup(optarg);
      if(!(opt->grpkey = groupsig_grp_key_import(opt->gs->desc->code, 
						 GROUPSIG_KEY_FORMAT_FILE_NULL_B64, 
						 opt->grpkey_name))) {
	OPTIONS_ERROR_LOG_MSG(stdout, "Wrong group key.", &logger, __FILE__,
			      "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	return IERROR;
      }
      break;

    case 'S': /* signatures */

      {

	char *name;

	/* We have to manage optind manually: it automatically increments in one 
	   after processing the option (in this case, 'S') and moves to the next one. 
	   We decrement it until getting the first "filename". We keep on incrementing
	   it until we reach the next option (when we reach a '-' at the beginning of
	   an argv) or until we reach the end of argv. */
	optind--; 
	opt->n_sigs = 0;
	while(optind < argc && argv[optind][0] != '-') {

	  if(!(opt->signatures = (groupsig_signature_t **) 
	       realloc(opt->signatures, sizeof(groupsig_signature_t *)*(opt->n_sigs+1)))) {
	    OPTIONS_ERROR_LOG_MSG(stdout, strerror(errno), &logger, __FILE__, 
				  "gs_options_parse", __LINE__, errno, LOGERROR);
	    return IERROR;
	  }

	  name = strdup(optarg);
	  if(!(opt->signatures[opt->n_sigs] = 
	       groupsig_signature_import(opt->gs->desc->code,
					 GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64, 
					 name))) {
	    OPTIONS_ERROR_LOG_MSG(stdout, "Wrong signature.", &logger, __FILE__,
				  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
	    return IERROR;
	  }
	  free(name);

	  opt->n_sigs++;
	  optind++;

	}

      }

      break;
     
    case 'v': /* verbosity */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "v", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->verbosity = (uint8_t) uint64;
      break;

    case 'l': /* log-level */
      errno = 0;
      uint64 = strtol(optarg, NULL, 10);
      if(errno || uint64 > UINT8_MAX) {
	if(uint64 > UINT8_MAX) errno = ERANGE;
	OPTIONS_ERROR_LOG_OPT(stdout, "l", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      opt->log_level = (uint8_t) uint64;
      break;

    case 'L': /* log */
      if(!(opt->log = strdup(optarg))) {
	OPTIONS_ERROR_LOG_OPT(stdout, "L", optarg, 
			      &logger, __FILE__, "gs_options_parse", __LINE__, 
			      errno, LOGERROR);
	return IERROR;
      }
      break;

    case '?': /* Character not included in optstring */

      /* Ignore it, since it may well be a specific scheme option. */
      break;

    default:

      /* We may receive options that are defined in specific schemes:
	 for now, ignore this */
      /* OPTIONS_ERROR_LOG_MSG(stdout, "Unkown option.",  */
      /* 			  &logger, __FILE__, "gs_options_parse", __LINE__,  */
      /* 			  EDQUOT, LOGERROR); */
      /* return IERROR; */
      break;

    }

  }

  if(rc == IERROR) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown error.",
			  &logger, __FILE__, "gs_options_parse", __LINE__, 
			  EDQUOT, LOGERROR);
    return IERROR;
  }

  return IOK;

}

int gs_options_parse(int argc, char *argv[], gs_options_t *opt) {
  
  if(argc <= 1) {
    _gs_options_help(argv);
    errno = 0;
    return IERROR;
  }

  if(!argv || !opt) {
    LOG_EINVAL(&logger, __FILE__, "gs_options_parse", __LINE__, LOGERROR);
    return IERROR;
  }

  /* (1) See if the first argument is a valid GROUPSIG scheme name */
  if(!(opt->gs = groupsig_get_groupsig_from_str(argv[1]))) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unknown scheme.", &logger, __FILE__, 
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;
  }

  /* (2) See if the second argument is a valid action name */
  if(action_code_from_str(argv[2], &opt->action) != IOK) {
    OPTIONS_ERROR_LOG_MSG(stdout, "Unkown action.", &logger, __FILE__, 
			  "gs_options_parse", __LINE__, EINVAL, LOGERROR);
    return IERROR;	  
  }

  /* Parse the options according to the specific action */
  switch(opt->action) {
  case ACTION_SETUP_CODE:
    return _gs_options_parse_setup(argc, argv, opt);
  case ACTION_JOIN_MEM_CODE:
    return _gs_options_parse_join_mem(argc, argv, opt);
  case ACTION_JOIN_MGR_CODE:
    return _gs_options_parse_join_mgr(argc, argv, opt);
  case ACTION_SIGN_CODE:
    return _gs_options_parse_sign(argc, argv, opt);
  case ACTION_VERIFY_CODE:
    return _gs_options_parse_verify(argc, argv, opt);
  case ACTION_OPEN_CODE:
    return _gs_options_parse_open(argc, argv, opt);
  case ACTION_REVEAL_CODE:
    return _gs_options_parse_reveal(argc, argv, opt);
  case ACTION_TRACE_CODE:
    return _gs_options_parse_trace(argc, argv, opt);
  case ACTION_CLAIM_CODE:
    return _gs_options_parse_claim(argc, argv, opt);
  case ACTION_CLAIM_VERIFY_CODE:
    return _gs_options_parse_claim_verify(argc, argv, opt);
  case ACTION_PROVE_EQUALITY_CODE:
    return _gs_options_parse_prove_equality(argc, argv, opt);
  case ACTION_PROVE_EQUALITY_VERIFY_CODE:
    return _gs_options_parse_prove_equality_verify(argc, argv, opt);
  default:
    fprintf(stdout, "Unknown error.");
    return IERROR;
  }

  return IERROR;
  
}

int gs_options_free(gs_options_t *opt) {
  
  if(!opt) {
    LOG_EINVAL_MSG(&logger, __FILE__, "gs_options_free", __LINE__, 
		   "Nothing to free.", LOGWARN);
    return IOK;
  }

  /* Free the logname iff it is not the default name (this means that it the
     string has not been dynamically allocated) */
  if(opt->log && strcmp(opt->log, DEFAULT_ERROR_LOG)) { 
    free(opt->log); opt->log = NULL; 
  }

  return IOK;

}


/* gs_options.c ends here */
