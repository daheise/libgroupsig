/*                               -*- Mode: C -*- 
 * @file: join.c
 * @brief: Adds new group members to the specified group.
 * @author: jesus
 * Maintainer: 
 * @date: Fri Mar 22 10:30:37 2013 (+0000)
 * @version: 
 * Last-Updated: lun oct 13 20:08:48 2014 (+0200)
 *           By: jesus
 *     Update #: 26
 * URL: 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>        
#include <sys/stat.h>
#include <stdarg.h>
#include <glib.h>
#include <libgen.h>

#include "groupsig.h"
#include "gml.h"
#include "kty04.h"

#include "types.h"
#include "sys/mem.h"
#include "misc/misc.h"

#ifdef PROFILE
#include "misc/profile.h"
#endif

log_t logger;


char* uint64_to_string(uint64_t u) {

  return g_strdup_printf("%lu", u);  

}

static char* _keyfile_name(char *dirfile, uint64_t index) {

  char *s, *sindex;

  if(!dirfile) {
    fprintf(stderr, "Error: %s\n", strerror(EINVAL));
    return NULL;
  }

  if(!(sindex = uint64_to_string(index))) {
    return NULL;
  }

  if(!(s = (char *) mem_malloc(sizeof(char)*
			       (strlen(dirfile)+strlen(sindex)+strlen(".key")+2)))) {
    mem_free(sindex); sindex = NULL;
    return NULL;
  }

  sprintf(s, "%s/%s.key", dirfile, sindex);

  mem_free(sindex); sindex = NULL;

  return s;

}

/* @todo: Error management and memory cleanup. */
int main(int argc, char **argv) {

  int argnum = 1; // Next argument to process
  char *dir_mem, *keyfile, *s_grpkey, *s_mgrkey, *s_gml;
  groupsig_key_t *grpkey, *mgrkey, *memkey;
  gml_t *gml;
  uint64_t i, n;
  int key_format;
  uint8_t scheme;
#ifdef PROFILE
  profile_t *prof;
  struct timeval tv_begin, tv_end;
  clock_t clck_begin, clck_end;
  uint64_t cycle_begin, cycle_end;
  uint8_t profile_skip;
#endif

  if(argc == 1) {
    fprintf(stdout, "Usage: %s <scheme> <key format> <group key> <mgr key> <gml> <members dir> [<n members> = 1]\n",
	    basename(argv[0]));
    return IOK;
  }

  if((groupsig_get_code_from_str(&scheme, argv[argnum])) == IERROR) {
    fprintf(stderr, "Error: Wrong scheme %s\n", argv[1]);
    return IERROR;
  }
  argnum++;

  if(strcmp(argv[argnum], "bin") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
  }
  else if(strcmp(argv[argnum], "b64") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
  }
  else {
    fprintf(stderr, "Error: Invalid format %s\n", argv[1]);
    return IERROR;
  }
  argnum++;

  s_grpkey = argv[argnum];
  argnum++;

  s_mgrkey = argv[argnum];
  argnum++;

  s_gml = argv[argnum];
  argnum++;

  dir_mem = argv[argnum];
  argnum++;

  if(argc > argnum) n = atoi(argv[argnum]); else n = 1;

  /* Initialize the group signature environment */
  groupsig_init(time(NULL));
  
  /*switch(scheme) {
  case GROUPSIG_KTY04_CODE:
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
    break;
  case GROUPSIG_BBS04_CODE:
  case GROUPSIG_CPY06_CODE:
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
    break;
  default:
    fprintf(stderr, "Error: unknown scheme.\n");
    return IERROR;
  }*/

  /* Initialize the group key, manager key and GML variables */

  /* Group key */
  if(!(grpkey = groupsig_grp_key_import(scheme, key_format, s_grpkey))) {
    fprintf(stderr, "Error: invalid group key %s.\n", s_grpkey);
    return IERROR;
  }

  /* Manager key */
  if(!(mgrkey = groupsig_mgr_key_import(scheme, key_format, s_mgrkey))) {
    fprintf(stderr, "Error: invalid manager key %s.\n", s_mgrkey);
    return IERROR;
  }

  /* GML */
  if(!(gml = gml_import(scheme, GML_FILE, s_gml))) {
    fprintf(stderr, "Error: invalid GML %s.\n", s_gml);
    return IERROR;
  }

#ifdef PROFILE
  if(!(prof = profile_begin("join.prf"))) {
    return IERROR;
  }
#endif

  /* Create n member keys. */
  for(i=0; i<n; i++) {

#ifdef PROFILE
    if(profile_get_time(&tv_begin, &clck_begin, &cycle_begin) == IERROR) {
      profile_skip = 1;
    } else {
      profile_skip = 0;
    }
#endif

    if(!(memkey = groupsig_mem_key_init(scheme))) {
      return IERROR;
    }

    if(groupsig_join_mem(memkey, grpkey) == IERROR) {
      return IERROR;
    }

    if(groupsig_join_mgr(gml, memkey, mgrkey, grpkey) == IERROR) {
      return IERROR;
    }

#ifdef PROFILE
    if(!profile_skip && profile_get_time(&tv_end, &clck_end, &cycle_end) == IOK) {
      profile_dump_entry(prof, &tv_begin, &tv_end, clck_begin, clck_end, cycle_begin, cycle_end);
    }

#endif

    /* Write the key into a file */
    if(!(keyfile = _keyfile_name(dir_mem, i))) {
      return IERROR;
    }

    if(groupsig_mem_key_export(memkey, key_format, keyfile) == IERROR) {
      return IERROR;
    }

    groupsig_mem_key_free(memkey); memkey = NULL;
    mem_free(keyfile); keyfile = NULL;

  }

  if(gml_export(gml, s_gml, GML_FILE) == IERROR) {
    return IERROR;
  }

  /* 3. Done. */
  groupsig_mgr_key_free(mgrkey); mgrkey = NULL;
  gml_free(gml); gml = NULL;
  groupsig_grp_key_free(grpkey); grpkey = NULL;

#ifdef PROFILE
  profile_free(prof); prof = NULL;
#endif
  
  return IOK;

}

/* join.c ends here */
