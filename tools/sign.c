/*                               -*- Mode: C -*- 
 * @file: revoke.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: Thu Apr  4 15:50:07 2013 (+0000)
 * @version: 
 * Last-Updated: lun oct 13 20:10:10 2014 (+0200)
 *           By: jesus
 *     Update #: 59
 * URL: 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <glib.h>

#include "groupsig.h"
#include "gml.h"
#include "crl.h"

#include "message.h"
#include "logger.h"

#include <libgen.h>

#ifdef PROFILE
#include "misc/profile.h"
#endif

log_t logger;

int main(int argc, char *argv[]) {
  int argnum = 1; // Next argument to process
  char *s_sig, *s_msg, *s_grpkey, *s_memkey;
  groupsig_key_t *grpkey, *memkey;
  groupsig_signature_t *sig;
  message_t *msg;
#ifdef PROFILE
  profile_t *prof;
  char *s_sig_i;
  struct timeval tv_begin, tv_end;
  uint64_t n, i, uint64;
  clock_t clck_begin, clck_end;
  uint64_t cycle_begin, cycle_end;
  uint8_t profile_skip;
#endif
  int key_format, sig_format;
  uint8_t scheme;

  if(argc == 1) {
    fprintf(stdout, "Usage: %s <scheme> <format> <signature file> <msg file> <member key> <group key>\n",
	    basename(argv[0]));
    return IOK;
  }

  if((groupsig_get_code_from_str(&scheme, argv[argnum])) == IERROR) {
    fprintf(stderr, "Error: Wrong scheme %s\n", argv[argnum]);
    return IERROR;
  }
  argnum++;

  if(strcmp(argv[argnum], "bin") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL;
  }
  else if(strcmp(argv[argnum], "b64") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64;
  }
  else {
    fprintf(stderr, "Error: Invalid format %s\n", argv[1]);
    return IERROR;
  }
  argnum++;
  /*switch(scheme) {
  case GROUPSIG_KTY04_CODE:
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64;
    break;
  case GROUPSIG_BBS04_CODE:
  case GROUPSIG_CPY06_CODE:
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL;
    break;
  default:
    fprintf(stderr, "Error: unknown scheme.\n");
    return IERROR;
  }*/

  s_sig = argv[argnum];
  argnum++;

  s_msg = argv[argnum];
  argnum++;

  s_memkey = argv[argnum];
  argnum++;

  s_grpkey = argv[argnum];
  argnum++;


#ifdef PROFILE      
  errno = 0;
  uint64 = strtoul(argv[argnum], NULL, 10);
  argnum++;
  if(errno) {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return IERROR;
  }
  n = uint64;
#endif

  /* Initialize the group signature environment */
  groupsig_init(time(NULL));

  /* Initialize the message object */
  if(!(msg = message_init())) {
    fprintf(stderr, "Error: failed to initialize message.\n");
    return IERROR;
  }

  /* Import the message from the external file into the initialized message object */
  if(message_import(msg, MESSAGE_FORMAT_NULL_FILE, s_msg) == IERROR) {
    fprintf(stderr, "Error: failed to import message.\n");
    message_free(msg); msg = NULL;
    return IERROR;
  }

  /* Import the group key */
  if(!(grpkey = groupsig_grp_key_import(scheme, key_format, s_grpkey))) {
    fprintf(stderr, "Error: invalid group key %s.\n", s_grpkey);
    return IERROR;
  }

#ifdef PROFILE
  if(!(prof = profile_begin("sign.prf"))) {
    return IERROR;
  }
  
  for(i=0; i<n; i++) {
#endif

  /* Initialize the group signature object */
  if(!(sig = groupsig_signature_init(scheme))) {
    fprintf(stderr, "Error: failed to initialize the group signature object.\n");
    return IERROR;
  }

  /* Import the member key */
  if(!(memkey = groupsig_mem_key_import(scheme, key_format, s_memkey))) {
    fprintf(stderr, "Error: invalid member key %s.\n", s_memkey);
    return IERROR;
  }

#ifdef PROFILE
    if(profile_get_time(&tv_begin, &clck_begin, &cycle_begin) == IERROR) {
      profile_skip = 1;
    } else {
      profile_skip = 0;
    }
#endif

  /* Sign the message: setting the seed to UINT_MAX forces to get a new pseudo
     random number for this signature instead of using a pre-fixed random number. */
  if(groupsig_sign(sig, msg, memkey, grpkey, UINT_MAX) == IERROR) {
    fprintf(stderr, "Error: signing failure.\n");
    return IERROR;
  }

#ifdef PROFILE
    if(!profile_skip && profile_get_time(&tv_end, &clck_end, &cycle_end) == IOK) {
      profile_dump_entry(prof, &tv_begin, &tv_end, clck_begin, clck_end, cycle_begin, cycle_end);
    }

    // @todo fixed to 10 char filename (without extension)
    if(!(s_sig_i = (char *) malloc(sizeof(char)*(strlen(s_sig)+10)))) {
      return IERROR;
    }

    sprintf(s_sig_i, "%s_%d", s_sig, i);

    /* Export the signature to the specified file */
    if(groupsig_signature_export(sig, sig_format, s_sig_i) == IERROR) {
      fprintf(stderr, "Error: failed to export the group signature.\n");
      return IERROR;
    }

    free(s_sig_i); s_sig_i = NULL;
    groupsig_signature_free(sig); sig = NULL;
    groupsig_mem_key_free(memkey); memkey = NULL;

  }

#else

  /* Export the signature to the specified file */
  if(groupsig_signature_export(sig, sig_format, s_sig) == IERROR) {
    fprintf(stderr, "Error: failed to export the group signature.\n");
    return IERROR;
  }

#endif
  
  /* Free resources */
  if(sig) { groupsig_signature_free(sig); sig = NULL; }
  if(grpkey) { groupsig_grp_key_free(grpkey); grpkey = NULL; }
  if(memkey) { groupsig_mem_key_free(memkey); memkey = NULL; }
  message_free(msg); msg = NULL;  

#ifdef PROFILE
  profile_free(prof); prof = NULL;
#endif
  
  return IOK;
  
}

/* revoke.c ends here */
