/*                               -*- Mode: C -*- 
 * @file: revoke.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: Thu Apr  4 15:50:07 2013 (+0000)
 * @version: 
 * Last-Updated: lun oct 13 20:10:31 2014 (+0200)
 *           By: jesus
 *     Update #: 40
 * URL: 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <glib.h>

#include "groupsig.h"
#include "gml.h"
#include "crl.h"

#include "message.h"
#include "logger.h"

#include <libgen.h>

#ifdef PROFILE
#include "misc/profile.h"
#endif

log_t logger;

int main(int argc, char *argv[]) {
  int argnum = 1;
  char *s_sig, *s_msg, *s_grpkey;
  groupsig_key_t *grpkey;
  groupsig_signature_t *sig;
  message_t *msg;
#ifdef PROFILE
  profile_t *prof;
  uint64_t n, i, uint64;
  struct timeval tv_begin, tv_end;
  clock_t clck_begin, clck_end;
  uint64_t cycle_begin, cycle_end;
  uint8_t profile_skip;
  char *s_sig_i;
#endif
  int key_format, sig_format;
  uint8_t bool, scheme;

  if(argc == 1) {
    fprintf(stdout, "Usage: %s <scheme> <file format> <signature file> <msg file> <group key>\n",
	    basename(argv[0]));
    return IOK;
  }

  if((groupsig_get_code_from_str(&scheme, argv[argnum])) == IERROR) {
    fprintf(stderr, "Error: Wrong scheme %s\n", argv[argnum]);
    return IERROR;
  }
  argnum++;

  if(strcmp(argv[argnum], "bin") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL;
  }
  else if(strcmp(argv[argnum], "b64") == 0){
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64;
  }
  else {
    fprintf(stderr, "Error: Invalid format %s\n", argv[1]);
    return IERROR;
  }
  argnum++;

  /* @todo fixed key formats! */
  /*if(scheme == GROUPSIG_KTY04_CODE) {
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL_B64;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL_B64;
  } else if (scheme == GROUPSIG_BBS04_CODE ||
	     scheme == GROUPSIG_CPY06_CODE) {
    key_format = GROUPSIG_KEY_FORMAT_FILE_NULL;
    sig_format = GROUPSIG_SIGNATURE_FORMAT_FILE_NULL;
  }*/

  s_sig = argv[argnum];
  argnum++;

  s_msg = argv[argnum];
  argnum++;

  s_grpkey = argv[argnum];
  argnum++;


#ifdef PROFILE      
  errno = 0;
  uint64 = strtoul(argv[argnum], NULL, 10);
  argnum++;
  if(errno) {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return IERROR;
  }
  n = uint64;
#endif

  /* Initialize the group signature environment */
  groupsig_init(time(NULL));

  /* Initialize the message object */
  if(!(msg = message_init())) {
    fprintf(stderr, "Error: failed to initialize message.\n");
    return IERROR;
  }

  /* Import the group key */
  if(!(grpkey = groupsig_grp_key_import(scheme, key_format, s_grpkey))) {
    fprintf(stderr, "Error: invalid group key %s.\n", s_grpkey);
    return IERROR;
  }

#ifdef PROFILE
  if(!(prof = profile_begin("verify.prf"))) {
    return IERROR;
  }
  
  for(i=0; i<n; i++) {

    // @todo fixed to 10 char filename (without extension)
    if(!(s_sig_i = (char *) malloc(sizeof(char)*(strlen(s_sig)+10)))) {
      return IERROR;
    }

    sprintf(s_sig_i, "%s_%d", s_sig, i);
    /* Import the signature */
    if(!(sig = groupsig_signature_import(scheme, sig_format, s_sig_i))) {
      fprintf(stderr, "Error: failed to import signature.\n");
      return IERROR;
    }

#else
    /* Import the signature */
    if(!(sig = groupsig_signature_import(scheme, sig_format, s_sig))) {
      fprintf(stderr, "Error: failed to import signature.\n");
      return IERROR;
    }
#endif
    /* Import the message from the external file into the initialized message object */
    if(message_import(msg, MESSAGE_FORMAT_NULL_FILE, s_msg) == IERROR) {
      fprintf(stderr, "Error: failed to import message.\n");
      message_free(msg); msg = NULL;
      return IERROR;
    }

#ifdef PROFILE
    if(profile_get_time(&tv_begin, &clck_begin, &cycle_begin) == IERROR) {
      profile_skip = 1;
    } else {
      profile_skip = 0;
    }
#endif

    if(groupsig_verify(&bool, sig, msg, grpkey) == IERROR) {
      fprintf(stderr, "Error: verification failure.\n");
      return IERROR;
    }

#ifdef PROFILE
    if(!profile_skip && profile_get_time(&tv_end, &clck_end, &cycle_end) == IOK) {
      profile_dump_entry(prof, &tv_begin, &tv_end, clck_begin, clck_end, cycle_begin, cycle_end);
    }
#endif

    if(!bool) {
      fprintf(stdout, "WRONG signature.\n");
      return IERROR;
    } else {
      fprintf(stdout, "VALID signature.\n");
    }
  
    /* Free resources */
    groupsig_signature_free(sig); sig = NULL;

#ifdef PROFILE
  }
#endif

  groupsig_grp_key_free(grpkey); grpkey = NULL;
  message_free(msg); msg = NULL;  
  
  return IOK;
  
}

/* revoke.c ends here */
