/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: base64.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: jue jul  5 14:42:56 2012 (+0200)
 * @version: 
 * Last-Updated: mar ago 20 22:32:45 2013 (+0200)
 *           By: jesus
 *     Update #: 32
 * URL: https://bitbucket.org/jdiazvico/libgroupsig
 */

#include "config.h"
#include <glib.h>

#include "base64.h"
#include "types.h"

char* base64_encode(const byte_t *in, uint64_t length) {

  if(!in || !length) {
    LOG_EINVAL(&logger, __FILE__, "base64_encode", __LINE__, LOGERROR);
    return NULL;
  }

  return g_base64_encode(in, length);
   
}

byte_t* base64_decode(const char *in, uint64_t *length_dec) {

  if(!in || !length_dec) {
    LOG_EINVAL(&logger, __FILE__, "base64_decode", __LINE__, LOGERROR);
    return NULL;
  }

  return g_base64_decode(in, length_dec);

}


/* base64.c ends here */
