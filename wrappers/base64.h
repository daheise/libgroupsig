/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: base64.h
 * @brief: Wrappers for Base64 functionality.
 *
 * Currently, the internal library used for Base64 encoding/decoding is Glib.
 *
 * @author: jesus
 * Maintainer: jesus
 * @date: jue jul  5 14:43:16 2012 (+0200)
 * @version: 0.1
 * Last-Updated: dom may 12 20:09:14 2013 (+0200)
 *           By: jesus
 *     Update #: 18
 * URL: bitbucket.org/jdiazvico/libgroupsig
 */

#ifndef _BASE64_H
#define _BASE64_H

#include <stdint.h>
#include "types.h"

/** 
 * @fn char* base64_encode(const byte_t *in, uint64_t length)
 * @brief Base64-encodes the specified byte array.
 *
 * @param[in] in The byte array to encode.
 * @param[in] length The number of bytes in <i>in</i>.
 * 
 * @return A pointer to the resulting Base64 string, or NULL if error.
 */
char* base64_encode(const byte_t *in, uint64_t length);

/** 
 * @fn byte_t* base64_decode(const char *in, uint64_t *length_dec)
 * @brief Decodes the given Base64 encoded string.
 *
 * @param[in] in The Base64 string to decode.
 * @param[in,out] length_dec Will be set to the size of the decoded byte array.
 * 
 * @return A pointer to the decoded byte array.
 */
byte_t* base64_decode(const char *in, uint64_t *length_dec);

#endif /* _BASE64_H */

/* base64.h ends here */
