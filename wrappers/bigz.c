/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: bigz.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: lun jul  2 16:22:30 2012 (+0200)
 * @version: 
 * Last-Updated: Tue Jun 11 06:13:49 2013 (-0400)
 *           By: jesus
 *     Update #: 105
 * URL: 
 */
#include "config.h"
#include <stdlib.h>

#include <sys/mem.h>
#include "bigz.h"
#include "bigf.h"

#include "logger.h"

bigz_t bigz_init() {

  bigz_t bz;

  /* if(!(bz = (mpz_t *) malloc(sizeof(mpz_t)))) { */
  if(!(bz = (bigz_t) malloc(sizeof(mpz_t)))) {
    LOG_ERRORCODE(&logger, __FILE__, "bigz_init", __LINE__, errno, LOGERROR);
    return NULL;
  }

  mpz_init(*bz);

  return bz;

}

bigz_t bigz_init_set(bigz_t op) {

  bigz_t bz;

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_init_set", __LINE__, LOGERROR);
    return NULL;    
  }

  if(!(bz = bigz_init())) {
    return NULL;
  }

  mpz_set(*bz, *op);
  
  return bz;

}

bigz_t bigz_init_set_ui(unsigned long int op) {

  bigz_t bz;

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_init_set_ui", __LINE__, LOGERROR);
    return NULL;    
  }

  if(!(bz = bigz_init())) {
    return NULL;
  }

  mpz_init_set_ui(*bz, op);
  
  return bz;

}

int bigz_free(bigz_t op) {
  
  /* If there is nothing to free, ok, but throw warning... */
  if(!op) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "bigz_free", __LINE__, EINVAL, 
		      "Nothing to free.", LOGWARN);
    return IERROR;
  }
  
  mpz_clear(*op);
  free(op); 
  op = NULL;
  
  return IOK;

}

int bigz_set(bigz_t rop, bigz_t op) {

  if(!rop || !op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_set", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_set(*rop, *op);

  return IOK;

}

int bigz_set_ui(bigz_t rop, unsigned long int op) {

  if(!rop) {
    LOG_EINVAL(&logger, __FILE__, "bigz_set_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_set_ui(*rop, op);

  return IOK;

}

int bigz_set_f(bigz_t z, bigf_t f) {

  if(!z || !f) {
    LOG_EINVAL(&logger, __FILE__, "bigz_set_f", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_set_f(*z, *f);
  
  return IOK;

}

int bigz_sgn(bigz_t op) {

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_sgn", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_sgn(*op);

}

int bigz_cmp(bigz_t op1, bigz_t op2) {

  if(!op1 || !op2) {
    LOG_EINVAL(&logger, __FILE__, "bigz_cmp", __LINE__, LOGERROR);
    return IERROR; /* This does not really matter here. What signals the error is errno */
  }

  return mpz_cmp(*op1, *op2);

}

int bigz_cmp_ui(bigz_t op1, unsigned long int op2) {

  if(!op1) {
    LOG_EINVAL(&logger, __FILE__, "bigz_cmp_ui", __LINE__, LOGERROR);
    return IERROR; /* This does not really matter here. What signals the error is errno */
  }

  return mpz_cmp_ui(*op1, op2);

}

int bigz_neg(bigz_t rop, bigz_t op) {

  if(!rop || !op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_neg", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_neg(*rop, *op);

  return IOK;

}

int bigz_add(bigz_t rop, bigz_t op1, bigz_t op2) {

  if(!rop || !op1 || !op2) {
    LOG_EINVAL(&logger, __FILE__, "bigz_add", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_add(*rop, *op1, *op2);

  return IOK;

}

int bigz_add_ui(bigz_t rop, bigz_t op1, unsigned long int op2) {

  if(!rop || !op1) {
    LOG_EINVAL(&logger, __FILE__, "bigz_add_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_add_ui(*rop, *op1, op2);

  return IOK;

}

int bigz_sub(bigz_t rop, bigz_t op1, bigz_t op2) {

  if(!rop || !op1 || !op2) {
    LOG_EINVAL(&logger, __FILE__, "bigz_sub", __LINE__,  LOGERROR);
    return IERROR;    
  }

  mpz_sub(*rop, *op1, *op2);

  return IOK;
  
}

int bigz_sub_ui(bigz_t rop, bigz_t op1, unsigned long int op2) {

  if(!rop || !op1) {
    LOG_EINVAL(&logger, __FILE__, "bigz_sub_ui", __LINE__,  LOGERROR);
    return IERROR;    
  }

  mpz_sub_ui(*rop, *op1, op2);

  return IOK;
  
}

int bigz_mul(bigz_t rop, bigz_t op1, bigz_t op2) {

  if(!rop || !op1 || !op2) {
    LOG_EINVAL(&logger, __FILE__, "bigz_mul", __LINE__, LOGERROR);
    return IERROR;    
  }
  
  mpz_mul(*rop, *op1, *op2);

  return IOK;

}

int bigz_mul_ui(bigz_t rop, bigz_t op1, unsigned long int op2) {

  if(!rop || !op1) {
    LOG_EINVAL(&logger, __FILE__, "bigz_mul_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_mul_ui(*rop, *op1, op2);

  return IOK;

}

int bigz_tdiv(bigz_t q, bigz_t r, bigz_t D, bigz_t d) {

  if(!D || !d || (!q && !r)) {
    LOG_EINVAL(&logger, __FILE__, "bigz_tdiv", __LINE__, LOGERROR);
    return IERROR;
  }

  errno = 0;
  if(!bigz_cmp_ui(d, 0) || errno) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bigz_tdiv", __LINE__,
		   "Division by zero.", LOGERROR);
    return IERROR;
  }

  /* If we receive both q and r, get the quotient and the remainder */
  if(q && r) {
    mpz_tdiv_qr(*q, *r, *D, *d);
  } else if(q && !r) {  /* Just quotient */
    mpz_tdiv_q(*q, *D, *d);
  } else {  /* Just remainder */
    mpz_tdiv_r(*r, *D, *d);
  }

  return IOK;

}

int bigz_tdiv_ui(bigz_t q, bigz_t r, bigz_t D, unsigned long int d) {

  if(!D || (!q && !r)) {
    LOG_EINVAL(&logger, __FILE__, "bigz_tdiv_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!d) {
    LOG_EINVAL_MSG(&logger, __FILE__, "bigz_tdiv", __LINE__,
		   "Division by zero.", LOGERROR);
    return IERROR;
  }

  /* If we receive both q and r, get the quotient and the remainder */
  if(q && r) {
    mpz_tdiv_qr_ui(*q, *r, *D, d);
  } else if(q && !r) {  /* Just quotient */
    mpz_tdiv_q_ui(*q, *D, d);
  } else {  /* Just remainder */
    mpz_tdiv_r_ui(*r, *D, d);
  }

  return IOK;

  
}

int bigz_divisible_p(bigz_t n, bigz_t d) {

  if(!n || !d) {
    LOG_EINVAL(&logger, __FILE__, "bigz_divisible_p", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_divisible_p(*n, *d);

}

int bigz_divexact(bigz_t rop, bigz_t n, bigz_t d) {

  if(!rop || !n || !d) {
    LOG_EINVAL(&logger, __FILE__, "bigz_divexact", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_divexact(*rop, *n, *d);

  return IOK;

}

int bigz_divexact_ui(bigz_t rop, bigz_t n, unsigned long int d) {

  if(!rop || !n) {
    LOG_EINVAL(&logger, __FILE__, "bigz_divexact_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_divexact_ui(*rop, *n, d);

  return IOK;

}

int bigz_mod(bigz_t rop, bigz_t op, bigz_t mod) {

  if(!rop || !op || !mod) {
    LOG_EINVAL(&logger, __FILE__, "bigz_mod", __LINE__, LOGERROR);
    return IERROR;    
  }

  mpz_mod(*rop, *op, *mod);
  
  return IOK;

}

int bigz_powm(bigz_t rop, bigz_t base, bigz_t exp, bigz_t mod) {

  if(!rop || !base || !exp || !mod) {
    LOG_EINVAL(&logger, __FILE__, "bigz_powm", __LINE__, LOGERROR);
    return IERROR;    
  }

  mpz_powm(*rop, *base, *exp, *mod);

  return IOK;

}

int bigz_pow_ui(bigz_t rop, bigz_t base, unsigned long int exp) {

  if(!rop) {
    LOG_EINVAL(&logger, __FILE__, "bigz_pow_ui", __LINE__, LOGERROR);
    return IERROR;
  }
  
  mpz_pow_ui(*rop, *base, exp);
  
  return IOK;

}

int bigz_ui_pow_ui(bigz_t rop, unsigned long int base, unsigned long int exp) {

  if(!rop) {
    LOG_EINVAL(&logger, __FILE__, "bigz_ui_pow_ui", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_ui_pow_ui(*rop, base, exp);
  
  return IOK;

}

int bigz_invert(bigz_t rop, bigz_t op, bigz_t mod) {

  if(!rop || !op || !mod) {
    LOG_EINVAL(&logger, __FILE__, "bigz_inverse", __LINE__, LOGERROR);
    return IERROR;    
  }

  if(!mpz_invert(*rop, *op, *mod)) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "bigz_inverse", __LINE__, EDQUOT,
		      "The given number is not invertible.\n", LOGWARN);
  }

  return IOK;

}

int bigz_probab_prime_p(bigz_t n, int reps) {

  if(!n || !reps) {
    LOG_EINVAL(&logger, __FILE__, "bigz_probab_prime_p", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_probab_prime_p(*n, reps);

}

int bigz_nextprime(bigz_t rop, bigz_t lower) {

  if(!rop || !lower) {
    LOG_EINVAL(&logger, __FILE__, "bigz_nextprime", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_nextprime(*rop, *lower);
  
  return IOK;

}

int bigz_gcd(bigz_t rop, bigz_t op1, bigz_t op2) {

  if(!rop || !op1 || !op2) {
    LOG_EINVAL(&logger, __FILE__, "bigz_gcd", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_gcd(*rop, *op1, *op2);

  return IOK;

}

size_t bigz_sizeinbase(bigz_t op, int base) {

  if(!op || base < 2 || base > 62) {
    LOG_EINVAL(&logger, __FILE__, "bigz_sizeinbase", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_sizeinbase(*op, base);

}

char* bigz_get_str(int base, bigz_t op) {

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_get_str", __LINE__, LOGERROR);
    return NULL;
  }

  return mpz_get_str(NULL, base, *op);

}

int bigz_set_str(bigz_t rop, char *str, int base) {
  
  if(!rop || !str || base < 2 || base > 62) {
    LOG_EINVAL(&logger, __FILE__, "bigz_set_str", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_set_str(*rop, str, base);

}

byte_t* bigz_export(bigz_t op, size_t *length) {

  if(!op || !length) {
    LOG_EINVAL(&logger, __FILE__, "bigz_export", __LINE__, LOGERROR);
    return NULL;
  }
  
  return mpz_export(NULL, length, 1, 1, 1, 0, *op);
    
}

int bigz_dump_bigz_fd(bigz_t z, FILE* fd, uint8_t include_sign){
  uint8_t neg = 0;
  char sign = '\0';
  byte_t *bytes;
  size_t size;

  if(!(bytes = bigz_export(z, &size))){
    return IERROR;
  }
  if(include_sign){
    errno = 0;
    if(bigz_sgn(z) < 0) {
      if(errno) {
        mem_free(bytes); bytes = NULL;
        return IERROR;
      }
      neg = 1;
    }

    if(neg) {
      sign = '-';
    }
    else{
      sign = '+';
    }
    fwrite(&sign, sizeof(char), 1, fd);
  }

  ///write the size
  fwrite(&size, sizeof(size_t), 1, fd);
  //write the bytes
  fwrite(bytes, size, 1, fd);
  mem_free(bytes);
  return IOK;
}


bigz_t bigz_import(byte_t *bytearray, size_t length) {

  bigz_t bz;

  if(!bytearray || !length) {
    LOG_EINVAL(&logger, __FILE__, "bigz_import_bytearray", __LINE__, LOGERROR);
    return NULL;
  }
  
  if(!(bz = bigz_init())) {
    return NULL;
  }

  mpz_import(*bz, length, 1, 1, 1, 0, bytearray);

  return bz;

}

int bigz_get_bigz_fd(bigz_t* z, FILE *fd, uint8_t include_sign) {
  size_t size = 0;
  byte_t* bytes;
  char sign = '\0';
  bigz_t buffer;
  if (include_sign) {
    fread(&sign, sizeof(char), 1, fd);
  }
  *z = bigz_init();
  fread(&size, sizeof(size_t), 1, fd);
  bytes = (byte_t*)mem_malloc(size);
  fread(bytes, size, 1, fd);
  buffer = bigz_import(bytes, size);
  if(sign == '-'){
    bigz_neg(buffer, buffer);
  }
  bigz_set(*z, buffer);
  return IOK;
}

void bigz_randinit_default(bigz_randstate_t rand) {
  return gmp_randinit_default(rand);
}

void bigz_randclear(bigz_randstate_t rand) {
  return gmp_randclear(rand);
}

void bigz_randseed_ui(bigz_randstate_t rand, unsigned long int seed) {
  return gmp_randseed_ui(rand, seed);
}

int bigz_urandomm(bigz_t rop, bigz_randstate_t state, bigz_t n) {

  if(!rop || !n) {
    LOG_EINVAL(&logger, __FILE__, "bigz_urandomm", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_urandomm(*rop, state, *n);
 
  return IOK;
  
}

int bigz_urandomb(bigz_t rop, bigz_randstate_t state, unsigned long int n) {
  
  if(!rop) {
    LOG_EINVAL(&logger, __FILE__, "bigz_urandomb", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_urandomb(*rop, state, n);

  return IOK;

}

int bigz_clrbit(bigz_t op, unsigned long int index) {

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_clrbit", __LINE__, LOGERROR);
    return IERROR;
  }

  mpz_clrbit(*op, index);

  return IOK;

}

int bigz_tstbit(bigz_t op, unsigned long int index) {

  if(!op) {
    LOG_EINVAL(&logger, __FILE__, "bigz_tstbit", __LINE__, LOGERROR);
    return IERROR;
  }

  return mpz_tstbit(*op, index);

}

/* Not just wrappers... */
int bigz_log2(bigf_t log2n, bigz_t n, uint64_t precission) {

  bigf_t f_n;

  if(!n || !log2n) {
    LOG_EINVAL(&logger, __FILE__, "bigz_log2", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(f_n = bigf_init())) {
    return IERROR;
  }
  
  if(bigf_set_z(f_n, n) == IERROR) {
    bigf_free(f_n);
    return IERROR;
  }

  if(bigf_log2(log2n, f_n, precission) == IERROR) {
    bigf_free(f_n);
    return IERROR;
  }

  bigf_free(f_n);

  return IOK;

}


/* bigz.c ends here */
