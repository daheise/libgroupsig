/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: pbc_ext.c
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: Fri Aug  9 04:56:30 2013 (-0400)
 * @version: 
 * Last-Updated: mar oct  8 00:02:47 2013 (+0200)
 *           By: jesus
 *     Update #: 53
 * URL: 
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "pbc_ext.h"
#include "base64.h"
#include "sys/mem.h"
#include "misc/misc.h"

int pbcext_element_export_bytes(byte_t **be, int *n, element_t e) {

  byte_t *_be;
  int _n;

  if(!be || !n) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_element_export_bytes", __LINE__, LOGERROR);
    return IERROR;
  }

  _n = element_length_in_bytes(e);

  /* Allocate internal memory when necessary */
  if(!*be) {
    if(!(_be = mem_malloc(sizeof(byte_t)*_n))) {
      return IERROR;
    }
  } else {
    _be = *be;
  }

  if(element_to_bytes(_be, e) != _n) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_element_export_bytes", __LINE__,
		      EDQUOT, "element_to_bytes: unexpected number of bytes written.", 
		      LOGERROR);
    if(!*be) { mem_free(_be); _be = NULL; }
    return IERROR;
  }

  if(!*be) {
    *be = _be;
  }  

  *n = _n;

  return IOK;
  
}

char* pbcext_element_export_b64(element_t e) {

  byte_t *bytes;
  int n;

  /* Get the byte representation */
  bytes = NULL;
  if(pbcext_element_export_bytes(&bytes, &n, e) == IERROR) {
    return NULL;
  }

  /* Convert it to Base64 */
  return base64_encode(bytes, n);

}

int pbcext_element_import_b64(element_t e, char *b64) {

  byte_t *bytes;
  uint64_t n;

  if(!b64) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_element_import_b64", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Get the byte representation of the Base64 string */
  if(!(bytes = base64_decode(b64, &n))) {
    return IERROR;
  }

  /* Get the element from the byte representation */
  element_from_bytes(e, bytes);
  mem_free(bytes); bytes = NULL;

  return IOK;

}

int pbcext_dump_element_fd(element_t e, FILE *fd) {

  byte_t *bytes;
  int size;

  /* Dump size of the element, in bytes */
  size = element_length_in_bytes(e);
  if(fwrite(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_element_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  if(!(bytes = mem_malloc(sizeof(byte_t)*(size)))) {
    return IERROR;
  }
  memset(bytes, 0, size);
  
  /* Dump the element */
  if(element_to_bytes(bytes, e) != size) {
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }

  if(fwrite(bytes, size, 1, fd)  != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_element_fd", __LINE__, errno, LOGERROR);
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }

  mem_free(bytes); bytes = NULL;

  return IOK;

}

int pbcext_dump_element_bytes(byte_t *bytes, uint64_t *written, element_t e) {

  int size;

  if(!bytes || !written) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_dump_element_bytes", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Dump size of the element, in bytes */
  size = element_length_in_bytes(e);
  if(!(memcpy(bytes, &size, sizeof(int)))) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_element_bytes", __LINE__, errno, LOGERROR);
    return IERROR;
  }
  
  *written = sizeof(int);
  
  /* Dump the element */
  if(element_to_bytes(&bytes[sizeof(int)], e) != size) {
    return IERROR;
  }

  *written += size;

  return IOK;

}

int pbcext_get_element_fd(element_t e, FILE *fd) {

  byte_t *bytes;
  int size;

  if(!fd) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_get_element_fd", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Read the number of bytes of the element */
  if(fread(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_get_element_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Read the element bytes */
  if(!(bytes = (byte_t *) mem_malloc(sizeof(byte_t)*size))) {
    return IERROR;
  }

  if(fread(bytes, size, 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_get_element_fd", __LINE__, errno, LOGERROR);
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }
  
  /* Import the element */
  if(element_from_bytes(e, bytes) != size) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_get_element_fd", __LINE__, 
		      EDQUOT, "element_from_bytes", LOGERROR);
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }
  
  mem_free(bytes); bytes = NULL;

  return IOK;

}

int pbcext_get_element_bytes(element_t e, uint64_t *read, byte_t *bytes) {

  int size;

  if(!read || !bytes) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_get_element_bytes", __LINE__, LOGERROR);
    return IERROR;
  }

  if(!(memcpy(&size, bytes, sizeof(int)))) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_get_element_bytes", __LINE__, 
        errno, LOGERROR);
    return IERROR;
  }

  *read = sizeof(int);

  if(element_from_bytes(e, &bytes[*read]) != size) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_get_element_bytes", __LINE__, 
        EDQUOT, "element_from_bytes", LOGERROR);
    return IERROR;
  }

  *read += size;

  return IOK;

}

int pbcext_get_dump_param_size(pbc_param_t param){
  FILE* fd;
  char *ptr;
  size_t size;
  fd = open_memstream(&ptr, &size);
  pbcext_dump_param_fd(param, fd);
  fclose(fd);
  mem_free(ptr); ptr = NULL;
  return size;
}

int pbcext_dump_param_fd(pbc_param_t param, FILE *fd) {

  int size;
  long offset, offset_end;

  /* Get current position within the file */
  if((offset = ftell(fd)) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Leave the field size to 0 */
  size = 0;
  if(fwrite(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Dump the params */
  pbc_param_out_str(fd, param);

  /* Get current position within the file */
  if((offset_end = ftell(fd)) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Write the size of the params */
  size = (int) (offset_end - offset - sizeof(int));

  /* Move to the beginning of the size field */
  if(fseek(fd, offset, SEEK_SET) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Write the size */
  if(fwrite(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Move back to the end of the params */
  if(fseek(fd, offset_end, SEEK_SET) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  return IOK;

}

int pbcext_dump_param_bytes(byte_t **bytes, uint64_t *written, pbc_param_t param) {

  FILE *fd;
  uint64_t rd;

  if(!bytes || !written) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_dump_param_bytes", __LINE__, LOGERROR);
    return IERROR;
  }
  
  /* XXX @todo SUPER-DIRTY WORKAROUND:
     The PBC API does not provide a (directly, at least) way to dump params into 
     bytearrays, being only possible to dump them into an fd. 
     Hence, in order to dump them into a bytearray (without having to dive into
     the PBC code...) we dump them into an fd and then read that fd into a 
     bytearray...
   */
  if(!(fd = tmpfile())) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_bytes", __LINE__, 
		  errno, LOGERROR);
    return IERROR;
  }

  if(pbcext_dump_param_fd(param, fd) == IERROR) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  /* Move the pointer back to the beginning of fd */
  if(fseek(fd, 0, SEEK_SET)) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_dump_param_bytes", __LINE__, 
		  errno, LOGERROR);
    return IERROR;
  }
  
  /* Read the file into a bytearray */
  if(misc_read_fd_to_bytestring(fd, bytes, &rd) == IERROR) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  *written = rd;
  fclose(fd); fd = NULL;

  return IOK;
}

int pbcext_get_param_fd(pbc_param_t param, FILE *fd) {

  byte_t *bytes;
  int size;

  /* Read the number of bytes of the params */
  if(fread(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_get_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Read the params bytes */
  if(!(bytes = (byte_t *) mem_malloc(sizeof(byte_t)*size))) {
    return IERROR;
  }

  if(fread(bytes, size, 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_get_param_fd", __LINE__, errno, LOGERROR);
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }

  /* Init the params */
  if(pbc_param_init_set_buf(param, (const char *) bytes, size)) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_get_param_fd", __LINE__, EDQUOT, 
		      "pbc_param_init_set_buff", LOGERROR);
    mem_free(bytes); bytes = NULL;
    return IERROR;
  }
  
  mem_free(bytes); bytes = NULL;

  return IOK;

}

int pbcext_skip_param_fd(FILE *fd) {

  int size;

  /* Read the number of bytes of the params */
  if(fread(&size, sizeof(int), 1, fd) != 1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_skip_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  /* Skip size bytes from fd */
  if (fseek(fd, size, SEEK_CUR)) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_skip_param_fd", __LINE__, errno, LOGERROR);
    return IERROR;
  }

  return IOK;

}

int pbcext_get_param_bytes(pbc_param_t param, uint64_t *read, byte_t *bytes) {

  int size;

  if(!read || !bytes) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_get_param_bytes", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Read the number of bytes of the params */
  memcpy(&size, bytes, sizeof(int));
  *read = sizeof(int);

  /* Init the params */
  if(pbc_param_init_set_buf(param, (const char *) &bytes[sizeof(int)], size)) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_get_param_fd", __LINE__, EDQUOT, 
		      "pbc_param_init_set_buff", LOGERROR);
    return IERROR;
  }
  
  *read += size;

  return IOK;

}

int pbcext_skip_param_bytes(uint64_t *read, byte_t *bytes) {

  int size;

  if(!read || !bytes) {
    LOG_EINVAL(&logger, __FILE__, "pbcext_skip_param_bytes", __LINE__, LOGERROR);
    return IERROR;
  }

  /* Read the number of bytes of the params */
  memcpy(&size, bytes, sizeof(int));
  *read = sizeof(int);

  /* Just add size to the number of "read" bytes */
  *read += size;

  return IOK;

}

int pbcext_pairing_param_copy(pairing_t pairing, pbc_param_t par_dst, pbc_param_t par_src) {

  FILE *fd;
  char *s;
  uint64_t count;

  /* @todo The PBC library does not offer a direct way to copy pairings and params,
     thus, we use a DIRTY workaround: export the params to a buffer in a temporary
     file, read the file into a string, and init the  pairing from the string. */
  if(!(fd = tmpfile())) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_pairing_param_copy", __LINE__, 
		  errno, LOGERROR);
    return IERROR;
  }

  pbc_param_out_str(fd, par_src);

  /* Reposition at the beginning of the file */
  if(fseek(fd, 0, SEEK_SET) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_pairing_param_copy", __LINE__, 
		  errno, LOGERROR);
    fclose(fd); fd = NULL;
    return IERROR;   
  }

  /* Read the file into a bytestring (actually, a string) */
  s = NULL; count = 0;
  if(misc_read_fd_to_bytestring(fd, (byte_t **) &s, &count) == IERROR) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  /* We don't need the file anymore */
  fclose(fd); fd = NULL;

  if(pbc_param_init_set_str(par_dst, s)) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_pairing_param_copy", __LINE__, 
		      EDQUOT, "pbc_param_init_set", LOGERROR);
    mem_free(s); s = NULL;
    return IERROR;       
  }

  /* See function pbc_demo_parigin_init from the PBC library file: include/pbc_test.h */
  if(pairing_init_set_buf(pairing, s, strlen(s))) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_pairing_param_copy", __LINE__, 
		      errno, "Pairing init failed.", LOGERROR);
    mem_free(s); s = NULL;
    return IERROR;
  }

  mem_free(s); s = NULL;

  return IOK;

}

int pbcext_param_copy(pbc_param_t dst, pbc_param_t src) {

  FILE *fd;
  char *s;
  uint64_t count;

  /* @todo The PBC library does not offer a direct way to copy pairings and params,
     thus, we use a DIRTY workaround: export the params to a buffer in a temporary
     file, read the file into a string, and init the  pairing from the string. */
  if(!(fd = tmpfile())) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_param_copy", __LINE__, 
		  errno, LOGERROR);
    return IERROR;
  }

  pbc_param_out_str(fd, src);

  /* Reposition at the beginning of the file */
  if(fseek(fd, 0, SEEK_SET) == -1) {
    LOG_ERRORCODE(&logger, __FILE__, "pbcext_param_copy", __LINE__, 
		  errno, LOGERROR);
    fclose(fd); fd = NULL;
    return IERROR;   
  }

  /* Read the file into a bytestring (actually, a string) */
  s = NULL; count = 0;
  if(misc_read_fd_to_bytestring(fd, (byte_t **) &s, &count) == IERROR) {
    fclose(fd); fd = NULL;
    return IERROR;
  }

  /* We don't need the file anymore */
  fclose(fd); fd = NULL;

  if(pbc_param_init_set_str(dst, s)) {
    LOG_ERRORCODE_MSG(&logger, __FILE__, "pbcext_param_copy", __LINE__, 
		      EDQUOT, "pbc_param_init_set", LOGERROR);
    mem_free(s); s = NULL;
    return IERROR;       
  }

  mem_free(s); s = NULL;

  return IOK;

}

/* int pbcext_field_copy(field_t dst, field_t src) { */

/*   /\* See field_s struct in include/pbc_field.h *\/ */

/*   if(!(dst.nqr = mem_malloc(sizeof(element_t)))) { */
/*     return IERROR; */
/*   } */

/*   dst.field_clear = src.field_clear; */
/*   dst.init = src.init; */
/*   dst.clear = src.clear; */
/*   dst.set_mpz = src.set_mpz; */
/*   dst.set_multiz = src.set_multiz; */
/*   dst.set = src.set; */
/*   dst.set0 = src.set0; */
/*   dst.set1 = src.set1; */
/*   dst.set_str = src.set_str; */
/*   dst.out_str = src.out_str; */
/*   dst.add = src.add; */
/*   dst.sub = src.sub; */
/*   dst.mul = src.mul; */
/*   dst.is_sqr = src.is_sqr; */
/*   dst.sqrt = src.sqrt; */
/*   dst.item_count = src.item_count; */
/*   dst.get_x = src.get_x; */
/*   dst.get_y = src.get_y; */
/*   dst.set_si = src.set_si; */
/*   dst.add_ui = src.add_ui; */
/*   dst.mul_mpz = src.mul_mpz; */
/*   dst.mul_si = src.mul_si; */
/*   dst.div = sr.div; */
/*   dst.doub = src.doub; */
/*   dst.multi_doub = src.multi_doub; */
/*   dst.multi_add = src.multi_add; */
/*   dst.halve = src.halve; */
/*   dst.square = src.square; */
/*   dst.cubic = src.cubic; */
/*   dst.pow_mpz = src.pow_mpz; */
/*   dst.invert = src.invert; */
/*   dst.neg = src.neg; */
/*   dst.random = src.random; */
/*   dst.from_hash = src.from_hash; */
/*   dst.is1 = src.is1; */
/*   dst.is0 = src.is0; */
/*   dst.sign = src.sign; */
/*   dst.cmp = src.cmp; */
/*   dst.to_bytes = src.to_bytes; */
/*   dst.from_bytes = src.from_bytes; */
/*   dst.length_in_bytes = src.length_in_bytes; */
/*   dst.fixed_length_in_bytes = src.fixed_length_in_bytes; */
/*   dst.snprint = src.snprint; */
/*   dst.to_mpz = src.to_mpz; */
/*   dst.out_info = src.out_info; */
/*   dst.pp_init = src.pp_init; */
/*   dst.pp_clear = src.pp_clear; */
/*   dst.pp_pow = src.pp_pow; */
/*   dst.pairing = src.pairing; */
/*   mpz_init_set(dst.order, src.order); */

/*   element_init(*dst.nqr); */
/*   element_set(*dst.nqr, *src.nqr); */
  
/*   dst.name = strdup(src.name); */
  
/*   /\* I don't know how to handle the "void *data" field... The following */
/*      will probably blow something up... *\/ */
/*   dst->data = src->data; */

/*   return IOK; */

/* } */

/* pbcext_param_copy(pbc_param_t dst, pbc_param_t src) { */

  /* int i, d; */

  /* /\* Because PBC uses mpz_t, we do not make use of the bigz wrapper here. *\/ */

  /* /\* Currently, only PBC params of type D are supported *\/ */
  /* if(type != PBCEXT_PARAM_D) { */
  /*   LOG_EINVAL(&logger, __FILE__, "pbcext_param_copy", __LINE__, LOGERROR); */
  /*   return IERROR; */
  /* } */

  /* /\* See param fields at ecc/d_param.c *\/ */

  /* d = src->k / 2; */
  /* if(!(dst->coeff = mem_malloc(sizeof(mpz_t)*d))) { */
  /*   return IERROR; */
  /* } */

  /* mpz_init_set(dst.q, src.q); */
  /* mpz_init_set(dst.n, src.n); */
  /* mpz_init_set(dst.h, src.h); */
  /* mpz_init_set(dst.r, src.r); */
  /* mpz_init_set(dst.a, src.a); */
  /* mpz_init_set(dst.b, src.b); */
  /* dst.k = src.k; */
  /* mpz_init_set(dst.nk, src.nk); */
  /* mpz_init_set(dst.hk, src.hk); */
  /* mpz_init_set(dst.nqr, src.nqr); */

  /* for(i=0; i<d; i++) { */
  /*   mpz_init_set(dst->coeff[i], src->coeff[i]); */
  /* } */

  /* return IOK; */

/* } */

/* int pbcext_pairing_copy(pairing_t dst, pairing_t src) { */

/*   /\* See pairing fields at include/pbc_pairing.h *\/ */

/*   if(pbcext_field_copy(dst.Zr, src.Zr) == IERROR) return IERROR; */
/*   if(pbcext_field_copy(dst.G1, src.G2) == IERROR) return IERROR; */
/*   if(pbcext_field_copy(dst.GT, src.GT) == IERROR) return IERROR; */

/*   mpz_init_set(dst.r, src.r); */
/*   mpz_init_set(dst.phikonr, src.phikonr); */
/*   dst.phi = src.phi; */
/*   dst.map = src.map; */
/*   dst.prod_pairings = src.prod_pairings; */
/*   dst.is_almost_coddh = src.is_almost_coddh; */
/*   dst.clear_func = src.clear_func; */
/*   dst.pp_init = src.pp_init; */
/*   dst.pp_clear = src.pp_clear; */
/*   dst.pp_apply = src.pp_apply; */
/*   dst.pp_finalpow = src.pp_finalpow; */
/*   dst.option_set = src.option_set; */
  
/*   /\* @todo I don't know how to handle the "void *data" field, this will */
/*      probably blow things up... *\/ */
/*   dst.data = src.data;   */

/*   return IOK; */

/* } */

/* pbc_ext.c ends here */
