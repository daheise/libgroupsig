/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: pbc_ext.h
 * @brief: 
 * @author: jesus
 * Maintainer: 
 * @date: Fri Aug  9 04:57:18 2013 (-0400)
 * @version: 
 * Last-Updated: lun oct  7 23:39:32 2013 (+0200)
 *           By: jesus
 *     Update #: 23
 * URL: 
 */

#ifndef _PBC_EXT_H
#define _PBC_EXT_H

#include "types.h"
#include <pbc/pbc.h>

/**
 * @def PBCEXT_PARAM_A
 * @brief Constant identifier for PBC params of type A.
 */
#define PBCEXT_PARAM_A 0

/**
 * @def PBCEXT_PARAM_B
 * @brief Constant identifier for PBC params of type B.
 */
#define PBCEXT_PARAM_B 1

/**
 * @def PBCEXT_PARAM_C
 * @brief Constant identifier for PBC params of type C.
 */
#define PBCEXT_PARAM_C 2

/**
 * @def PBCEXT_PARAM_D
 * @brief Constant identifier for PBC params of type D.
 */
#define PBCEXT_PARAM_D 3

/**
 * @def PBCEXT_PARAM_E
 * @brief Constant identifier for PBC params of type E.
 */
#define PBCEXT_PARAM_E 4

/**
 * @def PBCEXT_PARAM_F
 * @brief Constant identifier for PBC params of type f.
 */
#define PBCEXT_PARAM_F 5

/** 
 * @fn byte_t *pbcext_element_export_bytes(byte_t **be, int *n, element_t e)
 * @brief Converts the given element into a byte string.
 *
 * @param[in,out] be Will be set to the byte representation of <i>e</i>. If *be 
 *  is NULL, memory will be internally allocated. Otherwise, it must be big 
 *  enough to store the result.
 * @param[in,out] n Will be set to the number of bytes written into *be.
 * @param[in] e The element to convert.
 * 
 * @return IOK or IERROR.
 */
int pbcext_element_export_bytes(byte_t **be, int *n, element_t e);

/** 
 * @fn char* pbcext_element_export_b64(element_t e)
 * @brief Converts the given element into a Base64 string.
 *
 * @param[in] e The element to convert.
 * 
 * @return A pointer to the produced Base64 string or NULL if error.
 */
char* pbcext_element_export_b64(element_t e);

/** 
 * @fn int pbcext_element_import_b64(element_t e, char *b64)
 * @brief Gets the element contained in the Base64 string <i>b64</i>.
 *
 * @param[in,out] e Will be set to the retrieved element.
 * @param b64 The string to parse.
 * 
 * @return IOK or IERROR.
 */
int pbcext_element_import_b64(element_t e, char *b64);

/** 
 * @fn int pbcext_dump_element_fd(element_t e, FILE *fd)
 * @brief Dumps element <i>e</i> into the specified file descriptor (at its
 *  current position) as binary data.
 *
 * Dumps the element into the current position of the received file descdriptor
 * prepending them by an int indicating the number of bytes of the element.
 *
 * @param[in] e The element to dump.
 * @param[in] fd The file descriptor.
 * 
 * @return IOK or IERROR.
 */
int pbcext_dump_element_fd(element_t e, FILE *fd);

/** 
 * @fn int pbcext_dump_element_bytes(byte_t *bytes, uint64_t *written, element_t e)
 * @brief Dumps the number of bytes of the given element (as an int), followed by 
 *  the element into the given byte array.
 *
 * @param[in,out] bytes The byte array to write into. Must be big enough to store
 *  the result.
 * @param[in,out] written Will be set to the number of bytes written into <i>bytes</i>.
 * @param[in] e The element to dump.
 * 
 * @return IOK or IERROR.
 */
int pbcext_dump_element_bytes(byte_t *bytes, uint64_t *written, element_t e);

/** 
 * @fn int pbcext_get_element_fd(element_t e, FILE *fd)
 * @brief Gets the element stored at the current position of the specified
 *  file descriptor.
 *
 * Reads the number of bytes the element occupies (contained in an int field)
 * and then reads exactly the same amount of bytes and loads them into an
 * element.
 *
 * @param[in,out] e Will be set to the received element.
 * @param[in] fd The file descriptor.
 * 
 * @return IOK or IERROR.
 */
int pbcext_get_element_fd(element_t e, FILE *fd);

/** 
 * @fn int pbcext_get_element_bytes(element_t e, uint64_t *read, byte_t *bytes)
 * @brief Gets an element encoded as specified in <i>pbcext_dump_element_bytes</i>
 * from the bytearray <i>bytes</i>.
 *
 * @param[in,out] e Will be set to the retrieved element.
 * @param[in,out] read Will be set to the number of bytes read.
 * @param[in] bytes Bytearray containing the element.
 * 
 * @return IOK or IERROR.
 */
int pbcext_get_element_bytes(element_t e, uint64_t *read, byte_t *bytes);

/** 
 * @fn int pbcext_get_dump_param_size(pbc_param_t param)
 * @brief Get the size of a param when serialized by pbcext_dump_*
 *
 * @param[in] param The params to size;
 *
 * @return
 */
int pbcext_get_dump_param_size(pbc_param_t param);

/**
 * @fn int pbcext_dump_param_fd(pbc_param_t param, FILE *fd)
 * @brief Dumps the given PBC params at the current position of <i>fd</i>.
 *
 * Dumps the params into the current position of the received file descdriptor
 * prepending them by an int indicating the number of bytes of the params.
 *
 * @param[in] param The params to dump.
 * @param[in] fd The file descriptor.
 * 
 * @return 
 */
int pbcext_dump_param_fd(pbc_param_t param, FILE *fd);

/** 
 * @fn int pbcext_dump_param_bytes(byte_t **bytes, uint64_t *written, pbc_param_t param)
 * @brief Dump the given params into the specified bytearray.
 *
 * @param[in,out] bytes The bytearray to dump the params into. When *bytes is NULL,
 *  memory is internally allocated. Otherwise, it must be big enough in order to
 *  hold all the data.
 * @param[in,out] written Will be set to the number of bytes written in <i>bytes</i>.
 * @param[in] param The params to dump.
 * 
 * @return IOK or IERROR.
 */
int pbcext_dump_param_bytes(byte_t **bytes, uint64_t *written, pbc_param_t param);

/** 
 * @fn int pbcext_get_param_fd(pbc_param_t param, FILE *fd)
 * @brief Gets the params stored at the current position of <i>fd</i>.
 *
 * Reads the number of bytes the params occupy (contained in an int field)
 * and then reads exactly the same amount of bytes and loads them into a
 * PBC param struct.
 *
 * @param[in,out] param Will be set to the parsed params.
 * @param[in] fd The file descriptor.
 * 
 * @return IOK or IERROR.
 */
int pbcext_get_param_fd(pbc_param_t param, FILE *fd);

/** 
 * @fn int pbcext_skip_param_fd(pbc_param_t param, FILE *fd)
 * @brief Gets the params stored at the current position of <i>fd</i>.
 *
 * Reads the number of bytes the params occupy (contained in an int field)
 * and then skips exactly the same amount of bytes.
 *
 * @param[in] fd The file descriptor.
 * 
 * @return IOK or IERROR.
 */
int pbcext_skip_param_fd(FILE *fd);

/** 
 * @fn int pbcext_get_param_bytes(pbc_param_t param, uint64_t *read, byte_t *bytes)
 * @brief Gets the params stored at the given bytearray.
 *
 * Gets the params stored in the specified bytearray. See <i>pbcext_dump_param_bytes</i>.
 *
 * @param[in,out] param Will be set to the parsed params.
 * @param[in,out] read Will be set to the number of bytes read.
 * @param[in] bytes The bytes to parse.
 * 
 * @return IOK or IERROR.
 */
int pbcext_get_param_bytes(pbc_param_t param, uint64_t *read, byte_t *bytes);

/** 
 * @fn int pbcext_skip_param_bytes(uint64_t *read, byte_t *bytes)
 * @brief Skips the params stored at the given bytearray.
 *
 * Skips the params stored in the specified bytearray. See <i>pbcext_dump_param_bytes</i>.
 *
 * @param[in,out] read Will be set to the number of bytes read.
 * @param[in] bytes The bytes to parse.
 * 
 * @return IOK or IERROR.
 */
int pbcext_skip_param_bytes(uint64_t *rd, byte_t *bytes);

/** 
 * @fn int pbcext_pairing_param_copy(pairing_t pairing, pbc_param_t par_dst, 
 *                                   pbc_param_t par_src)
 * @brief Copies the source params into the destination params, and sets the
 *  associated pairing structure.
 * 
 * @param[in,out] pairing The pairing to set.
 * @param[in,out] par_dst The destination pbc_param_t structure.
 * @param[in] par_src The source pbc_param_t structure.
 * 
 * @return IOK or IERROR
 */
int pbcext_pairing_param_copy(pairing_t pairing, pbc_param_t par_dst, pbc_param_t par_src);

/**
 * @fn int pbcext_param_copy(pbc_param_t dst, pbc_param_t src)
 * @brief Copies the pbc_param_t source structure into the destination.
 *
 * AFAIK, the PBC library does not offer (as of its version 0.5.14 at least) a
 * function for copying pbc_param_t structures. This function is a (not very
 * elegant) workaround.
 *
 * @param[in,out] dst An initialized destination pbc_param_t structure.
 * @param[in] src The pbc_param_t structure to copy.
 *
 * @return IOK or IERROR.
 */
int pbcext_param_copy(pbc_param_t dst, pbc_param_t src);

/* /\**  */
/*  * @fn int pbcext_field_copy(field_t dst, field_t src) */
/*  * @brief Copies the field source structure into the destination. */
/*  *  */
/*  * AFAIK, the PBC library does not offer (as of its version 0.5.14 at least) a */
/*  * function for copying field_t structures. This function is a (not very */
/*  * elegant) workaround.  */
/*  * */
/*  * @param[in,out] dst An initialized destination field_t structure. */
/*  * @param[in] src The field_t structure to copy. */
/*  *  */
/*  * @return IOK or IERROR. */
/*  *\/ */
/* int pbcext_field_copy(field_t dst, field_t src); */

/* /\**  */
/*  * @fn int pbcext_param_copy(pbc_param_t dst, pbc_param_t src, int type) */
/*  * @brief Copies the pbc_param_t source structure into the destination. */
/*  *  */
/*  * AFAIK, the PBC library does not offer (as of its version 0.5.14 at least) a */
/*  * function for copying pbc_param_t structures. This function is a (not very */
/*  * elegant) workaround.  */
/*  * */
/*  * @param[in,out] dst An initialized destination pbc_param_t structure. */
/*  * @param[in] src The pbc_param_t structure to copy. */
/*  * @param[in] type The type of param structure to copy. Currently, only */
/*  *  PBCEXT_PARAM_D is supported. */
/*  *  */
/*  * @return IOK or IERROR. */
/*  *\/ */
/* int pbcext_param_copy(pbc_param_t dst, pbc_param_t src, int type); */

/* /\**  */
/*  * @fn int pbcext_pairing_copy(pairing_t dst, pairing_t src) */
/*  * @brief Copies the pairing source structure into the destination. */
/*  *  */
/*  * AFAIK, the PBC library does not offer (as of its version 0.5.14 at least) a */
/*  * function for copying pairing_t structures. This function is a (not very */
/*  * elegant) workaround.  */
/*  * */
/*  * @param[in,out] dst An initialized destination pairing_t structure. */
/*  * @param[in] src The pairing_t structure to copy. */
/*  *  */
/*  * @return IOK or IERROR. */
/*  *\/ */
/* int pbcext_pairing_copy(pairing_t dst, pairing_t src); */

#endif /* _PBC_EXT_H */

/* pbc_ext.h ends here */
