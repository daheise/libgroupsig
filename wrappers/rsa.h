/*                               -*- Mode: C -*- 
 *
 *	libgroupsig Group Signatures library
 *	Copyright (C) 2012-2013 Jesus Diaz Vico
 *
 *		
 *
 *	This file is part of the libgroupsig Group Signatures library.
 *
 *
 *  The libgroupsig library is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License as 
 *  defined by the Free Software Foundation, either version 3 of the License, 
 *  or any later version.
 *
 *  The libroupsig library is distributed WITHOUT ANY WARRANTY; without even 
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU Lesser General Public License for more details.
 *
 *
 *  You should have received a copy of the GNU Lesser General Public License 
 *  along with Group Signature Crypto Library.  If not, see <http://www.gnu.org/
 *  licenses/>
 *
 * @file: rsa.h
 * @brief: Customized RSA functions.
 * @author: jesus
 * Maintainer: jesus
 * @date: mar abr 24 12:59:35 2012 (+0200)
 * @version: 0.1
 * Last-Updated: mié sep 25 20:39:49 2013 (+0200)
 *           By: jesus
 *     Update #: 8
 * URL: bitbucket.org/jdiazvico/libgroupsig
 *
 * @todo This should be implemented more efficiently (look for suitable external libraries)
 */

#ifndef _RSA_H
#define _RSA_H

#include <stdint.h>
#include "types.h"
#include "sysenv.h"

/**
 * @struct rsa_keypair_t
 * @brief An RSA cryptosystem structure.
 */
typedef struct {
  mpz_t p; /**< The p prime */
  mpz_t q; /**< The q prime */
  mpz_t n; /**< The modulus */
  mpz_t phin; /**< phi(n) */
  mpz_t e; /**< Public exponent */
  mpz_t d; /**< Private exponent */
} rsa_keypair_t;

/** 
 * @fn int rsa_keypair_init(rsa_keypair_t *rsa)
 * @brief Initializes a rsa keypair structure.
 * 
 * @param[in,out] rsa A pointer to the structure
 * 
 * @return IOK or IERROR
 */
int rsa_keypair_init(rsa_keypair_t *rsa);

/** 
 * @fn int rsa_keypair_free(rsa_keypair_t *rsa)
 * @brief Frees the memory allocated forthe received rsa keypair structure.
 * 
 * @param[in,out] rsa A pointer to the structure to free.
 * 
 * @return IOK or IERROR
 */
int rsa_keypair_free(rsa_keypair_t *rsa);

/** 
 * @fn int rsa_keypair_fprintf(FILE *fd, rsa_keypair_t *rsa)
 * @brief Prints the keypair into the specified ouptut.
 *
 * @param[in] fd The file descriptor to print to.
 * @param[in] rsa The key to print.
 * 
 * @return IOK or IERROR
 */
int rsa_keypair_fprintf(FILE *fd, rsa_keypair_t *rsa);

/** 
 * @fn int rsa_keygen(uint64_t primesize, rsa_keys_t *rsa)
 * @brief Generates a private-public RSA keypair.
 *
 * The generated keypair is special in the sense that the generated primes p and q
 * are of the shape t=2Rt'+1, where t' is also prime and R is some composite of
 * known factorization.
 *
 * @param[in] primesize The desired (approximate) size, in bits, of the RSA primes.
 * @param rsa The structure to store the keypair in.
 * 
 * @return IOK or IERROR
 */
int rsa_keygen(uint64_t primesize, rsa_keypair_t *rsa);

#endif /* _RSA_H */

/* rsa.h ends here */
